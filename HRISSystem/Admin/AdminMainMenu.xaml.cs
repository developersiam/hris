﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.DirectoryServices.AccountManagement;
namespace HRISSystem.Admin
{
    /// <summary>
    /// Interaction logic for AdminMainMenu.xaml
    /// </summary>
    public partial class AdminMainMenu : Page
    {
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        public AdminMainMenu()
        {
            InitializeComponent();
        }

        private void NavigateToSetupAdminButton_Click(object sender, RoutedEventArgs e)
        {

            if (_singleton.deptCode == "HRM" || _singleton.deptCode == "IT" || _singleton.username == "Saruta" || _singleton.username == "saruta")
            {
                this.NavigationService.Navigate(new OTByAdmin.FrmSetupAdmin());
            }
            
            
        }

        
    }
}
