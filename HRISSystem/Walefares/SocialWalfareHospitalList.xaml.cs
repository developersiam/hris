﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for SocialWalfareHospitalList.xaml
    /// </summary>
    public partial class SocialWalfareHospitalList : Page
    {
        public List<SSOHospitalList> ObjList;
        private SSOHospitalList ssoHospitalList;
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XFrom;
        public SocialWalfareHospitalList()
        {
            InitializeComponent();
        }
        public SocialWalfareHospitalList(string _xfrom)
        {
            InitializeComponent();
            XFrom = _xfrom;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllSSOHospitalLists().ToList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ssoHospitalList = (SSOHospitalList)DataGrid.SelectedValue;
                AddButton.IsEnabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private static string SubRight(string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (txtHospitalNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อโรงพยาบาล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtHospitalNameTH.Focus();
                    return;
                }
                if (txtTelno.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์เบอร์ติดต่อโรงพยาบาล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTelno.Focus();
                    return;
                }

                string X_ID;
                List<SSOHospitalList> Xvalue;
                Xvalue = _hrisBusinessLayer.GetAllSSOHospitalLists().ToList();
                if (Xvalue.Count > 0)
                {
                    X_ID = Convert.ToString(Convert.ToInt32(SubRight(Xvalue.Max(c => c.SSOHospital_ID), 2)) + 1);
                    X_ID = "T" + string.Format("{0:00}", Convert.ToInt32(X_ID));

                }
                else
                {
                    X_ID = "T01";
                }
                txtID.Text = X_ID;

                SSOHospitalList[] ssoHospitalListObj = new SSOHospitalList[1];
                SSOHospitalList query1 = new SSOHospitalList();  // created 1 instance or object
                query1.SSOHospital_ID = txtID.Text;
                query1.HospitalNameTH = txtHospitalNameTH.Text;
                query1.HospitalNameEN = txtHospitalNameEN.Text;
                query1.Telephone = txtTelno.Text;
                query1.Fax = txtFAX.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;
                ssoHospitalListObj[0] = query1;
                _hrisBusinessLayer.AddSSOHospitalList(ssoHospitalListObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T024";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "HospitalList_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtHospitalNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                this.NavigationService.Navigate(new Walefares.SocialWalfare(query1));
                
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtHospitalNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อโรงพยาบาล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtHospitalNameTH.Focus();
                    return;
                }
                if (txtTelno.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์เบอร์ติดต่อโรงพยาบาล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTelno.Focus();
                    return;
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                SSOHospitalList query1 = new SSOHospitalList();  // created 1 instance or object
                query1.SSOHospital_ID = txtID.Text;
                query1.HospitalNameTH = txtHospitalNameTH.Text;
                query1.HospitalNameEN = txtHospitalNameEN.Text;
                query1.Telephone = txtTelno.Text;
                query1.Fax = txtFAX.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateSSOHospitalList(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T024";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "HospitalList_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = ssoHospitalList.HospitalNameTH;
                transactionLog1.NewData = txtHospitalNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtHospitalNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อโรงพยาบาล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtHospitalNameTH.Focus();
                    return;
                }
                if (txtTelno.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์เบอร์ติดต่อโรงพยาบาล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTelno.Focus();
                    return;
                }

                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                _hrisBusinessLayer.RemoveSSOHospitalList(ssoHospitalList);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T024";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "HospitalList_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = ssoHospitalList.HospitalNameTH;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearData()
        {
            try
            {
                txtID.Text = "";
                txtHospitalNameTH.Text = "";
                txtHospitalNameEN.Text = "";
                txtTelno.Text = "";
                txtFAX.Text = "";
                AddButton.IsEnabled = true;
                txtHospitalNameTH.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ssoHospitalList != null)
                {
                    this.NavigationService.Navigate(new Walefares.SocialWalfare(ssoHospitalList));
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
