﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem
{
    class SingletonConfiguration
    {
        private static SingletonConfiguration instance = new SingletonConfiguration();

        public string username { get; set; }
        public DateTime LoggedIn { get; set; }
        public string deptCode { get; set; }
        public string emailFrom { get; set; }
        public string permissionLevel { get; set; }
        public string singleton_employee_ID { get; set; }


        private SingletonConfiguration()
        {

        }
        
        public static SingletonConfiguration getInstance()
        {
            return instance;
        }
        
    }

    
}
