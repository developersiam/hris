﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ToPayroll
{
    /// <summary>
    /// Interaction logic for HRToPayrollSetup.xaml
    /// </summary>
    public partial class HRToPayrollSetup : Page
    {
        private List<BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass> otSummaryListClass = new List<BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass>();
        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        private class LockStatus
        {
            public string LockStatusFlag { get; set; }
            public string LockStatusFlagName { get; set; }
        }

        public HRToPayrollSetup()
        {
            InitializeComponent();
            //Create view model class for holiday flag combo box.
            List<LockStatus> lockStatusList = new List<LockStatus>();

            LockStatus h1 = new LockStatus { LockStatusFlag = "1", LockStatusFlagName = "Unlock" };
            LockStatus h2 = new LockStatus { LockStatusFlag = "2", LockStatusFlagName = "Lock" };

            lockStatusList.Add(h1);
            lockStatusList.Add(h2);

            cmbLabourLockedStatus.ItemsSource = lockStatusList;
            cmbLabourLockedStatus.SelectedIndex = 0;

            cmbStaffLockedStatus.ItemsSource = lockStatusList;
            cmbStaffLockedStatus.SelectedIndex = 0;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbLabourLockedStatus.IsEnabled = true;
                cmbStaffLockedStatus.IsEnabled = true;
                cmbLabourLockedStatus.SelectedIndex = 0;
                cmbStaffLockedStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                
             
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cmbStaffLockedStatus.IsEnabled = false;
                    cmbLabourLockedStatus.IsEnabled = false;
                    cmbStaffLockedStatus.SelectedIndex = 1;
                    cmbLabourLockedStatus.SelectedIndex = 1;
                    return;
                }

                //ถ้ายังไม่ได้ upload ข้อมูลวันทำงานของพนักงานรายวัน  labour_work ไม่อนุญาตให้lock ข้อมูล
                List<Labour_work> labout_worklst = new List<Labour_work>();
                labout_worklst = _hrisSTECPayrollBusinessLayer.GetAllLabour_Works().Where(b => b.Lot_Month == lot_numberList.Lot_month && b.Lot_Year == lot_numberList.Lot_year).ToList();
                if (labout_worklst.Count() <= 0)
                {
                    MessageBox.Show("คุณยังไม่ได้ upload ข้อมูลวันทำงานของพนักงานรายวัน รายวันประจำเข้าระบบ Payroll กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);                    
                    return;
                }

                //ถ้าไม่มีข้อมูลการทำโอที ใน ot_detail ระบบจะแจ้งและให้ confirm ข้อมูล
                List<OT_Detail> ot_detaillst = new List<OT_Detail>();
                ot_detaillst = _hrisSTECPayrollBusinessLayer.GetAllOT_Details().Where(b => b.Lot_Month == lot_numberList.Lot_month && b.Lot_Year == lot_numberList.Lot_year).ToList();
                if (ot_detaillst.Count() <= 0)
                {
                    if (MessageBox.Show("ยังไม่มีข้อมูล OT ในะระบบ Payroll คุณยังต้องการปิดงวดใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }

                }

                Lot_Number lot_numberOld = new Lot_Number();
                Lot_Number query1 = new Lot_Number();
                lot_numberOld = _hrisSTECPayrollBusinessLayer.GetLot_Number(lot_numberList.Lot_month,lot_numberList.Lot_year);

                query1.Lot_Month = lot_numberOld.Lot_Month;
                query1.Lot_Year = lot_numberOld.Lot_Year;
                query1.Lock_Hr = (string)cmbStaffLockedStatus.SelectedValue;
                query1.Lock_Hr_Labor = (string)cmbLabourLockedStatus.SelectedValue;
                query1.Lock_Acc = lot_numberOld.Lock_Acc;
                query1.Lock_Acc_Labor = lot_numberOld.Lock_Acc_Labor;
                query1.Start_date = lot_numberOld.Start_date;
                query1.Finish_date = lot_numberOld.Finish_date;
                query1.Salary_Paid_date = lot_numberOld.Salary_Paid_date;
                query1.Ot_Paid_date = lot_numberOld.Ot_Paid_date;
                query1.LockedAll = lot_numberOld.LockedAll;
                _hrisSTECPayrollBusinessLayer.UpdateLot_Number(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T016";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Lot_number";
                transactionLog1.PKFields = cmbLot.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = cmbLabourLockedStatus.Text;
                transactionLog1.NewData = cmbStaffLockedStatus.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLog1.Noted = "Log lot number by HRM";
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                MessageBox.Show("Lock/Unlock ข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                
                chkLot();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                chkLot();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void chkLot()
        {
            try
            {
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Hr == "2")
                {
                    cmbStaffLockedStatus.IsEnabled = true;
                    cmbStaffLockedStatus.SelectedIndex = 1;
                    cmbStaffLockedStatus.Background = Brushes.Red;

                }
                else
                {
                    cmbStaffLockedStatus.IsEnabled = true;
                    cmbStaffLockedStatus.SelectedIndex = 0;
                    cmbStaffLockedStatus.Background = Brushes.White;

                }

                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    cmbLabourLockedStatus.IsEnabled = true;
                    cmbLabourLockedStatus.SelectedIndex = 1;
                    cmbLabourLockedStatus.Background = Brushes.Red;

                }
                else
                {
                    cmbLabourLockedStatus.IsEnabled = true;
                    cmbLabourLockedStatus.SelectedIndex = 0;
                    cmbLabourLockedStatus.Background = Brushes.White;

                }


                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cmbStaffLockedStatus.IsEnabled = false;
                    cmbLabourLockedStatus.IsEnabled = false;
                    cmbStaffLockedStatus.SelectedIndex = 1;
                    cmbLabourLockedStatus.SelectedIndex = 1;
                    cmbStaffLockedStatus.Background = Brushes.Red;
                    cmbLabourLockedStatus.Background = Brushes.Red;
                    return;
                }
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        
        }
    }
}
