namespace DataAccessLayer_HRISSystem.Payroll
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PayrollModel : DbContext
    {
        public PayrollModel()
            : base("name=PayrollModel")
        {
        }

        public virtual DbSet<Dept_code> Dept_code { get; set; }
        public virtual DbSet<Payroll> Payrolls { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payroll>()
                .Property(e => e.Salary)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Wage)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Ot_hour_rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Ot_holiday_rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Ot_hour_holiday_rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Ot_special_rate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Special)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Total_Income)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Total_Income_Labour)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Tax)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Tax_Labour)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.SSO)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Cut_wage)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.Net_Income)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Payroll>()
                .Property(e => e.NetSalary)
                .HasPrecision(19, 4);
        }
    }
}
