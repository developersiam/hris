﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace DataAccessLayer_HRISSystem
{
    public class HRISTimeSTECDataRepository
    {
        public interface IUserInfoRepository : IGenericTimeSTECDataRepository<userinfo>
        {

        }

        public class UserInfoRepository : GenericTimeSTECDataRepository<userinfo>, IUserInfoRepository
        {

        }

        public interface IDepartmentTimeSTECRepository : IGenericTimeSTECDataRepository<department>
        {

        }

        public class DepartmentTimeSTECRepository : GenericTimeSTECDataRepository<department>, IDepartmentTimeSTECRepository
        {

        }

        public interface IUserInfo_attareaRepository : IGenericTimeSTECDataRepository<userinfo_attarea>
        {

        }

        public class UserInfo_attreaRepository : GenericTimeSTECDataRepository<userinfo_attarea>, IUserInfo_attareaRepository
        {

        }
        public interface ICheckInOutRepository : IGenericTimeSTECDataRepository<checkinout>
        {

        }

        public class CheckInOutRepository : GenericTimeSTECDataRepository<checkinout>, ICheckInOutRepository
        {

        }


    }
}
