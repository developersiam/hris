﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class Lot_NumberClass
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();

        public string Lot_month { get; set; }
        public string Lot_year { get; set; }
        public string LotName { get; set; }
        public DateTime Start_date { get; set; }
        public DateTime Finish_date { get; set; }
        public string Lock_Hr { get; set; }
        public string Lock_Acc { get; set; }
        public string Lock_Hr_Labor { get; set; }
        public string Lock_Acc_Labor { get; set; }
        public Boolean LockedAll { get; set; }

        public List<Lot_NumberClass> getLot_Number(int _year)
        {
            List<Lot_Number> oE = new List<Lot_Number>();
            oE = _hrisSTECPayrollBusinessLayer.GetAllLot_Numbers().Where(b => Convert.ToInt16(b.Lot_Year) >= Convert.ToInt16(_year) - 1).OrderByDescending(b => b.Finish_date).ToList();

            var query = (
                            from a in oE

                            select new Lot_NumberClass
                            {
                                Lot_month = a.Lot_Month,
                                Lot_year = a.Lot_Year,
                                LotName = a.Lot_Month + "/" + a.Lot_Year,
                                Start_date = Convert.ToDateTime(a.Start_date),
                                Finish_date = Convert.ToDateTime(a.Finish_date),
                                Lock_Hr = a.Lock_Hr,
                                Lock_Acc = a.Lock_Acc,
                                Lock_Hr_Labor = a.Lock_Hr_Labor,
                                Lock_Acc_Labor = a.Lock_Acc_Labor,
                                LockedAll = Convert.ToBoolean(a.LockedAll)
                            });
            return query.ToList();
        }

        public string CurrentLot()
        {
            //var todayLot = _hrisSTECPayrollBusinessLayer.GetAllLot_Numbers().Single(w => w.Start_date <= DateTime.Now && w.Finish_date >= DateTime.Now);
            var currentYearLot = _hrisSTECPayrollBusinessLayer.GetAllLot_Numbers().Where(w => w.Lot_Year == DateTime.Now.Year.ToString()).ToList();
            var currentMonthLot = currentYearLot.SingleOrDefault(w => w.Lot_Month == DateTime.Now.Month.ToString("D2"));
            string strTodayLot = string.Format("{0}/{1}", currentMonthLot.Lot_Month, currentMonthLot.Lot_Year);
            return strTodayLot;
        }
    }

}
