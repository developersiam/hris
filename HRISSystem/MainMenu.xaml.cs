﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;

namespace HRISSystem
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page

    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        public MainMenu()
        {
            InitializeComponent();
            
        }
             
        private void NavigateToReferenceMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ReferencesTable.ReferenceTableMainMenu());
        }

        private void NavigateToAdminMenuButton_Click(object sender, RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Admin.AdminMainMenu());
            //MessageBox.Show("ระบบไม่อนุญาตให้ใช้งานหน้า Admin");
            //return;
            //this.NavigationService.Navigate(new HumanResource.HolidaySetup());
            //this.NavigationService.Navigate(new OrganizationViewModel.DemoOrg());
            //this.NavigationService.Navigate(new Page1());
            this.NavigationService.Navigate(new Admin.AdminMainMenu());
        }
    
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (_singleton.username.ToLower() == "netchanok" || _singleton.username.ToLower() == "m.tanainan")
            {
                this.NavigationService.Navigate(new Report.RepRPTHR026());
                return;
            }
            else if (_singleton.deptCode != "IT" && _singleton.deptCode != "HRM" && _singleton.username.ToLower() != "saruta")
            {
                if (_singleton.deptCode == "ACC")
                {
                    NavigateToReferenceMenuButton.IsEnabled = false;
                    NavigateToEmployeeButton.IsEnabled = false;
                    NavigateToWalefareButton.IsEnabled = false;
                    NavigateToPayrollMuButton.IsEnabled = false;
                    NavigateToTrainingMuButton.IsEnabled = false;
                    NavigateToReportMenuButton.IsEnabled = true;
                    NavigateToOTByAdminMenuButton.IsEnabled = false;
                    NavigateToOTApproveByManagerMenuButton.IsEnabled = false;
                    NavigateToOTApproveByHRMenuButton.IsEnabled = false;
                    NavigateToOTReportByAdminMenuButton.IsEnabled = false;

                }
                else
                {
                    NavigateToReferenceMenuButton.IsEnabled = false;
                    NavigateToEmployeeButton.IsEnabled = false;
                    NavigateToWalefareButton.IsEnabled = false;
                    NavigateToPayrollMuButton.IsEnabled = false;
                    NavigateToTrainingMuButton.IsEnabled = false;
                    NavigateToReportMenuButton.IsEnabled = true;
                    NavigateToOTByAdminMenuButton.IsEnabled = true;
                    NavigateToOTApproveByManagerMenuButton.IsEnabled = true;
                    NavigateToOTApproveByHRMenuButton.IsEnabled = true;
                    NavigateToOTReportByAdminMenuButton.IsEnabled = true;

                    //NavigateToReferenceMenuButton.IsEnabled = true;
                    //NavigateToEmployeeButton.IsEnabled = true;
                    //NavigateToWalefareButton.IsEnabled = true;
                    //NavigateToPayrollMuButton.IsEnabled = true;
                    //NavigateToTrainingMuButton.IsEnabled = true;
                    //NavigateToReportMenuButton.IsEnabled = true;
                    //NavigateToOTByAdminMenuButton.IsEnabled = true;
                    //NavigateToOTApproveByManagerMenuButton.IsEnabled = true;
                    //NavigateToOTApproveByHRMenuButton.IsEnabled = true;
                    //NavigateToOTReportByAdminMenuButton.IsEnabled = true;
                }

                //ถ้าเป็นแผนกอื่นให้ตรวจสอบว่าเป็นผู้จัดการหรือ Admin
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeByEmail(_singleton.emailFrom);
                if (e2 == null)
                {

                    MessageBox.Show("กรุณาตรวจสอบอีเมลล์พนักงานท่านนี้ เนื่องจากฟังก์ชัน OT Onlines จะใช้อีเมลล์ในการรับ-ส่งข้อมูลแจ้งเตือน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Row1.Visibility = Visibility.Hidden;
                    return;
                }
                else
                {
                    if (e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        Row1.Visibility = Visibility.Hidden;
                        return;
                    }
                }
                _singleton.singleton_employee_ID = e2.Employee_ID;
                //เช็คต่อว่ารหัสนี้อยู่ในกลุ่มไหนระหว่าง Admin กับ Manager

                //ถ้าเป็น Admin
                OTOnlineSetupAdmin a2 = new OTOnlineSetupAdmin();
                a2 = _hrisBusinessLayer.GetOTonlineSetuAdminByEmployee(_singleton.singleton_employee_ID);
                if (a2 == null)
                {
                    //ถ้าเป็น Manager
                    List<OTOnlineSetupAdminDetail> b2 = new List<OTOnlineSetupAdminDetail>();
                    b2 = _hrisBusinessLayer.GetOTOnlineSetupAdminDetailbyReportTo(_singleton.singleton_employee_ID).ToList();
                    if (b2 == null)
                    {
                        MessageBox.Show("กรุณาตรวจสอบสิทธิ์การใช้งานของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        Row1.Visibility = Visibility.Hidden;
                        return;
                    }
                    else
                    {
                        Row1.Visibility = Visibility.Collapsed;
                        NavigateToOTByAdminMenuButton.IsEnabled = false;
                        NavigateToOTApproveByManagerMenuButton.Visibility = Visibility.Visible;
                        NavigateToOTApproveByHRMenuButton.IsEnabled = false;
                        NavigateToOTReportByAdminMenuButton.Visibility = Visibility.Visible;
                    }

                    
                }
                else if(a2.Employee_ID != "")
                {
                    Row1.Visibility = Visibility.Collapsed;
                    NavigateToOTByAdminMenuButton.Visibility = Visibility.Visible;
                    NavigateToOTApproveByManagerMenuButton.IsEnabled = false;
                    NavigateToOTApproveByHRMenuButton.IsEnabled = false;
                    NavigateToOTReportByAdminMenuButton.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("กรุณาตรวจสอบสิทธิ์การใช้งานของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Row1.Visibility = Visibility.Hidden;
                    return;
                }                                        
            }
            else
            {
                //ถ้าเป็นแผนกอื่นให้ตรวจสอบว่าเป็นผู้จัดการหรือ Admin
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeByEmail(_singleton.emailFrom);
                if (e2 == null)
                {
                    MessageBox.Show("กรุณาระบุ Email ของคุณเพิ่มเติมในหน้าข้อมูลพนักงานเนื่องจากระบบจะต้องใช้สำหรับฟังก์ชันการทำโอทีออนไลน์", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    
                    return;
                }
                else
                {
                    if (e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาระบุ Email ของคุณเพิ่มเติมในหน้าข้อมูลพนักงานเนื่องจากระบบจะต้องใช้สำหรับฟังก์ชันการทำโอทีออนไลน์", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                       
                        return;
                    }
                }
                _singleton.singleton_employee_ID = e2.Employee_ID;


                Row1.Visibility = Visibility.Visible;
            }

            
            
        }

        private void NavigateToWalefareButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Walefares.WalefareMenu());
        }

        private void NavigateToEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Employees.HumanResource_Employee());
            this.NavigationService.Navigate(new Employees.EmployeesMenu());
        }

        private void NavigateToPayrollMuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ToPayroll.ToPayrollMenu());
        }

        private void NavigateToReportMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Report.ReportsMenu());
        }

        private void NavigateToTrainingMuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ReferencesTable.RefTraining());
        }

        private void NavigateToOTByAdminMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new OTByAdmin.FrmRequestOT());
        }

        private void NavigateToOTReportByAdminMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Report.RepRPTHR021());
        }

        private void NavigateToOTApproveByManagerMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new OTByAdmin.FrmApproveOTByManager());
        }

        private void NavigateToOTApproveByHRMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new OTByAdmin.FrmApproveOTByHRM());
        }

        private void ShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ReferencesTable.ShiftControl());
        }
    }
}
