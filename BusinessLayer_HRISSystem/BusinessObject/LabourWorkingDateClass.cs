﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem.BusinessObject
{
   public class LabourWorkingDateClass
    {
        public string Employee_ID { get; set; }
        public string AccCode { get; set; }
        public string FingerScanID { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string Payroll_Date { get; set; }
        public decimal NWorking { get; set; }
    }
}
