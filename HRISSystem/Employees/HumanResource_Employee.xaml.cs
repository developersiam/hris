﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Employees
{
    /// <summary>
    /// Interaction logic for HumanResource_Employee.xaml
    /// </summary>
    public partial class HumanResource_Employee : Page
    {
        //Person personFromPerson;
        public List<Employee> employeeList;
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        private class FingerScanFlage
        {
            public bool FingerScanFlag { get; set; }
            public string FingerScanFlagName { get; set; }
        }
        public HumanResource_Employee()
        {
            InitializeComponent();
            GetAllInformation();
            employeeList = new List<Employee>();

            List<FingerScanFlage> fingerScanFlagList = new List<FingerScanFlage>();

            FingerScanFlage h1 = new FingerScanFlage { FingerScanFlag = true, FingerScanFlagName = "สแกน" };
            FingerScanFlage h2 = new FingerScanFlage { FingerScanFlag = false, FingerScanFlagName = "ไม่สแกน" };

            fingerScanFlagList.Add(h1);
            fingerScanFlagList.Add(h2);

            cmbFingerScanFlag.ItemsSource = fingerScanFlagList;
        }

        public HumanResource_Employee(Person person)
        {
            InitializeComponent();
            
            GetpersonInformation(person);
            GetAllInformation();
            DataGrid.ItemsSource = null; //clear 
            DataGrid.ItemsSource = _hrisBusinessLayer.GetAllEmployees().Where(p => p.Person_ID == txtPerson_ID.Text);
        }

        public void GetAllInformation()
        {
            IList<Shift> shift = _hrisBusinessLayer.GetAllShifts();
            cmbShiftControlShift.ItemsSource = shift.OrderBy(b=> b.StartTime);

            IList<Organization> organizationUnit = _hrisBusinessLayer.GetAllOrganizations();
            cmbOrganizationUnit_ID.ItemsSource = organizationUnit.OrderBy(b=>b.OrganizationUnit.OrganizationUnitName);           
        }

        public void GetpersonInformation(Person personInformation)
        {
            try
            {
                
                Person person2 = new Person();
                person2 = personInformation;
                txtPerson_ID.Text = person2.Person_ID;
                txtTitleNameTH.Text = person2.TitleName.TitleNameTH;
                txtFirstNameTH.Text = person2.FirstNameTH;
                txtLastNameTH.Text = person2.LastNameTH;

                txtShiftControlEmployee_ID.Text = txtEmployee_ID.Text;
                txtShiftControlTitleNameTH.Text = person2.TitleName.TitleNameTH;
                txtShiftControlFirstNameTH.Text = person2.FirstNameTH;
                txtShiftControlLastNameTH.Text = person2.LastNameTH;

                txtPositionEmployee_ID.Text = txtEmployee_ID.Text;
                txtPositionTitleNameTH.Text = person2.TitleName.TitleNameTH;
                txtPositionFirstNameTH.Text = person2.FirstNameTH;
                txtPositionLastNameTH.Text = person2.LastNameTH;
                           
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearData(int TabIndex)
        {
            try
            {             
               if(TabIndex == 0)
                {
                    AddButton.IsEnabled = true;
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);

                    txtEmployee_ID.Text = "";
                    txtFingerScan_ID.Text = "";
                    txtPerson_ID.Text = "";
                    txtTitleNameTH.Text = "";
                    txtFirstNameTH.Text = "";
                    txtLastNameTH.Text = "";
                    txtBaseSalary.Text = "";
                    txtNoted.Text = "";

                    DataGrid.ItemsSource = null; //clear 
                    DataGrid.ItemsSource = _hrisBusinessLayer.GetAllEmployees();
                }
               else if (TabIndex == 1)
                {
                    AddShiftControlButton.IsEnabled = true;
                    txtShiftControlNoted.Text = "";

                    DataGridShiftControl.ItemsSource = null;
                    DataGridShiftControl.ItemsSource = _hrisBusinessLayer.GetShiftControlByEmployee(txtShiftControlEmployee_ID.Text);
                    GetAllInformation();
                }
               else if(TabIndex ==2)
                {
                    AddPositionButton.IsEnabled = true;
                    //Tab Position
                    DataGridPosition.ItemsSource = null;
                    DataGridPosition.ItemsSource = _hrisBusinessLayer.GetPositionByEmployee(txtPositionEmployee_ID.Text);
                    GetAllInformation();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                employeeList = _hrisBusinessLayer.GetAllEmployees().ToList();
                //DataGrid.ItemsSource = employeeList;
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StartDate.Text == "")
                    StartDate.Text = Convert.ToString(DateTime.MinValue);
                if (EndDate.Text == "")
                    EndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (txtBaseSalary.Text == "")
                    txtBaseSalary.Text = Convert.ToString(0);
                
                if(txtNoted.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ใช้กับระบบบัญชี");
                    return;
                }

                int XCount = 0;
                XCount = employeeList.Where(b => b.Noted == txtNoted.Text).Count();
                if(XCount != 0)
                {
                    MessageBox.Show("รหัสพนักงานที่ใช้กับระบบบัญชีนี้มีแล้ว กรุณาตรวจสอบความถูกต้องของข้อมูล");
                    return;
                }

                XCount = 0;
                XCount = employeeList.Where(b => b.Person_ID == txtPerson_ID.Text && Convert.ToDateTime( b.EndDate) >=  Convert.ToDateTime( DateTime.Now)).Count();
                if (XCount != 0)
                {
                    MessageBox.Show("รหัสPerson นี้ถูกใช้และยังไม่ได้อัพเดทวันสิ้นสุดทำงาน กรุณาตรวจสอบความถูกต้องของข้อมูล");
                    return;
                }

                Employee[] employeeObj = new Employee[1];
                Employee query1 = new Employee();  // created 1 instance or object

                query1.Employee_ID = txtEmployee_ID.Text;
                query1.Person_ID = txtPerson_ID.Text;
                query1.FingerScanID = txtFingerScan_ID.Text;               
                query1.CreatedDate = DateTime.Now;
                query1.StartDate = Convert.ToDateTime(StartDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.BaseSalary = Convert.ToDecimal(txtBaseSalary.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.Noted = txtNoted.Text;

                employeeObj[0] = query1;
                _hrisBusinessLayer.AddEmployee(employeeObj);
                ClearData(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StartDate.Text == "")
                    StartDate.Text = Convert.ToString(DateTime.MinValue);
                if (EndDate.Text == "")
                    EndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (txtBaseSalary.Text == "")
                    txtBaseSalary.Text = Convert.ToString(0);

                if (txtNoted.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ใช้กับระบบบัญชี");
                    return;
                }
                int XCount = 0;
                XCount = employeeList.Where(b => b.Noted == txtNoted.Text  && b.Employee_ID != txtEmployee_ID.Text).Count();
                if (XCount != 0)
                {
                    MessageBox.Show("รหัสพนักงานที่ใช้กับระบบบัญชีนี้มีแล้ว กรุณาตรวจสอบความถูกต้องของข้อมูล");
                    return;
                }
                XCount = 0;
                XCount = employeeList.Where(b => b.Person_ID == txtPerson_ID.Text && Convert.ToDateTime(b.EndDate) >= Convert.ToDateTime(DateTime.Now) && b.Employee_ID != txtEmployee_ID.Text).Count();
                if (XCount != 0)
                {
                    MessageBox.Show("รหัสPerson นี้ถูกใช้และยังไม่ได้อัพเดทวันสิ้นสุดทำงาน กรุณาตรวจสอบความถูกต้องของข้อมูล");
                    return;
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                Employee query1 = new Employee();  // created 1 instance or object
                Employee query2 = new Employee();  // created 1 instance or object

                query2 = (Employee)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.Employee_ID = txtEmployee_ID.Text;
                query1.Person_ID = txtPerson_ID.Text;
                query1.FingerScanID = txtFingerScan_ID.Text;
                query1.CreatedDate = query2.CreatedDate;
                query1.StartDate = Convert.ToDateTime(StartDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.BaseSalary = Convert.ToDecimal(txtBaseSalary.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.Noted = txtNoted.Text;

                _hrisBusinessLayer.UpdateEmployee(query1);
                ClearData(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Employee query1 = new Employee();  // created 1 instance or object
                query1 = (Employee)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveEmployee(query1);
                ClearData(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Employee query1 = new Employee();  // created 1 instance or object
                query1 = (Employee)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtEmployee_ID.IsReadOnly = true;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.LightGray);
                }

                GetpersonInformation(query1.Person);
                
                //Tab ShiftControl
                DataGridShiftControl.ItemsSource = null;
                DataGridShiftControl.ItemsSource = _hrisBusinessLayer.GetShiftControlByEmployee(txtShiftControlEmployee_ID.Text);

                //Tab Position
                DataGridPosition.ItemsSource = null;
                DataGridPosition.ItemsSource = _hrisBusinessLayer.GetPositionByEmployee(txtPositionEmployee_ID.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Employees.Person_Person());
        }

        private void AddShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShiftControlValidFrom.Text == "")
                    ShiftControlValidFrom.Text = Convert.ToString(DateTime.MinValue);
                if (ShiftControlEndDate.Text == "")
                    ShiftControlEndDate.Text = Convert.ToString(DateTime.MaxValue);

                ShiftControl[] shiftControlObj = new ShiftControl[1];
                ShiftControl query1 = new ShiftControl();  // created 1 instance or object

                query1.Employee_ID = txtShiftControlEmployee_ID.Text;
                query1.Shift_ID = (string)cmbShiftControlShift.SelectedValue;
                query1.Noted = txtShiftControlNoted.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ShiftControlValidFrom.Text);
                query1.EndDate = Convert.ToDateTime(ShiftControlEndDate.Text);
                query1.ModifiedDate = DateTime.Now;

                shiftControlObj[0] = query1;
                _hrisBusinessLayer.AddShiftControl(shiftControlObj);
                ClearData(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShiftControlValidFrom.Text == "")
                    ShiftControlValidFrom.Text = Convert.ToString(DateTime.MinValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                ShiftControl query2 = new ShiftControl();  // created 1 instance or object

                query2 = (ShiftControl)DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
             

                query1.Employee_ID = txtShiftControlEmployee_ID.Text;
                query1.Shift_ID = (string)cmbShiftControlShift.SelectedValue;
                query1.Noted = txtShiftControlNoted.Text;
                query1.CreatedDate = query2.CreatedDate;
                query1.ValidFrom = Convert.ToDateTime(ShiftControlValidFrom.Text);
                query1.EndDate = Convert.ToDateTime(ShiftControlEndDate.Text);
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateShiftControl(query1);
                ClearData(1);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                query1 = (ShiftControl) DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveShiftControl(query1);
                ClearData(1);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridShiftControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {

                try
                {
                    ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                    query1 = (ShiftControl)DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                    if (query1 == null)
                    {
                        AddShiftControlButton.IsEnabled = true;                      
                    }
                    else
                    {
                        AddShiftControlButton.IsEnabled = false;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtBaseSalary_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //key numberic only
            try
            {
                if (!char.IsDigit(e.Text, e.Text.Length - 1))
                    e.Handled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

 
        private void AddPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PositionHireDate.Text == "")
                    PositionHireDate.Text = Convert.ToString(DateTime.MinValue);
                if (PositionEndDate.Text == "")
                    PositionEndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (cmbFingerScanFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก use finger scan condition");
                    return;
                }

                EmployeePosition[] employeePositonObj = new EmployeePosition[1];
                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object

                query1.Employee_ID = txtPositionEmployee_ID.Text;
                query1.PositionOrganization_ID = (string)cmbPostion_ID.SelectedValue;
                query1.CreatedDate = DateTime.Now;
                query1.HireDate = Convert.ToDateTime(PositionHireDate.Text);
                query1.EndDate = Convert.ToDateTime(PositionEndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.FingerScanFlag = (Boolean)cmbFingerScanFlag.SelectedValue;
                
                //query1.PositionType_ID = (string)cmbPositionType_ID.SelectedValue;

                employeePositonObj[0] = query1;
                _hrisBusinessLayer.AddEmployeePosition(employeePositonObj);
                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PositionHireDate.Text == "")
                    PositionHireDate.Text = Convert.ToString(DateTime.MinValue);
                if(cmbFingerScanFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก use finger scan condition");
                    return;
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object
                EmployeePosition query2 = new EmployeePosition();  // created 1 instance or object

                query2 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.Employee_ID = txtPositionEmployee_ID.Text;
                query1.PositionOrganization_ID = (string)cmbPostion_ID.SelectedValue;
                query1.CreatedDate = query2.CreatedDate;
                query1.HireDate = Convert.ToDateTime(PositionHireDate.Text);
                query1.EndDate = Convert.ToDateTime(PositionEndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.FingerScanFlag = (Boolean)cmbFingerScanFlag.SelectedValue;
                //query1.PositionType_ID = (string)cmbPositionType_ID.SelectedValue;


                _hrisBusinessLayer.UpdateEmployeePosition(query1);
                ClearData(2);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeletePositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object

                query1 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveEmployeePosition(query1);
                ClearData(2);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       
        private void TcEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // ... Get TabControl reference.
                var item = sender as TabControl;
                // ... Set Title to selected tab header.
                var selected = item.SelectedItem as TabItem;
                this.Title = selected.Header.ToString();
                int TcEmployeeTabIndex = TcEmployee.SelectedIndex;
                if ((sender as TabControl).SelectedIndex != 0)
                    {
                    Employee query1 = _hrisBusinessLayer.GetEmployeeInformation(txtEmployee_ID.Text);
                    if (query1 == null)
                    {
                        MessageBox.Show("ให้เลือกรหัสพนักงานที่ได้บันทึกไปแล้วจาก Tab Employee");
                        int newIndex = TcEmployee.SelectedIndex + 1;
                        newIndex = 0;
                        TcEmployee.SelectedIndex = newIndex;
                        return;
                    }
                    else if ((sender as TabControl).SelectedIndex == 1)
                    {
                        txtShiftControlEmployee_ID.Text = query1.Employee_ID;
                        //DataGridShiftControl.ItemsSource = null;
                        //DataGridShiftControl.ItemsSource = _hrisBusinessLayer.GetShiftControlByEmployee(txtShiftControlEmployee_ID.Text);
                    }
                    else if (TcEmployeeTabIndex == 2)
                    {
                        txtPositionEmployee_ID.Text = query1.Employee_ID;

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

      
        private void NavigateToSearchPositionButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ReferencesTable.Organization_Position());
        }

        private void DataGridPosition_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {

                try
                {
                    EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object
                    query1 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                    if (query1 == null)
                    {
                        AddPositionButton.IsEnabled = true;
                    }
                    else
                    {
                        AddPositionButton.IsEnabled = false;
                        //cmbPostion_ID.SelectedValuePath = query1.PositionOrganization_ID;
                        String XOrganization_ID = (String)cmbOrganizationUnit_ID.SelectedValue;
                        IList<PositionOrganization> positionOrganization = _hrisBusinessLayer.GetAllPositionOrganizations().Where(p => p.Organization_ID == query1.PositionOrganization.Organization_ID).ToList();
                        cmbPostion_ID.ItemsSource = positionOrganization.OrderBy(b=>b.Position.PositionNameTH);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbOrganizationUnit_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                String XOrganization_ID = (String)cmbOrganizationUnit_ID.SelectedValue;
                IList<PositionOrganization> positionOrganization = _hrisBusinessLayer.GetAllPositionOrganizations().Where(p => p.Organization_ID == XOrganization_ID).ToList();
                cmbPostion_ID.ItemsSource = positionOrganization.OrderBy(b => b.Position.PositionNameTH);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllEmployees();
                DataGrid.ItemsSource = employeeList.Where(p => p.Person.FirstNameTH.Contains(txtSearch.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {           
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
