﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.DirectoryServices.AccountManagement;

namespace HRISSystem.Employees
{
    /// <summary>
    /// Interaction logic for Person_Person.xaml
    /// </summary>
    
    public partial class Person_Person : Page
    {
        public List<Person> personList;
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new HRISSTECPayrollBusinessLayer();
        public IDCardInfo IDcardInfoSelectRow; //get  current row select from datagridIDCard
        public string  person_IDfromGetViewPerson ;
        private string employee_IDfromSearchPage;
        
        private List<EducationQualification> educationQualificationList = new List<EducationQualification>();
        private List<Institute> institueList = new List<Institute>();
        private List<Major> majorList = new List<Major>();
        private List<FamilyMemberType> familyMemberType = new List<FamilyMemberType>();
        private FamilyMember famSelected = new FamilyMember();
        private ContactPersonInfo conSelected = new ContactPersonInfo();
        private List<VitalStau> vitalStatus = new List<VitalStau>();
        private List<OriginalWorkHistory> originalWorkHistory = new List<OriginalWorkHistory>();
        private List<ContactPersonInfo> contactPersonInfo = new List<ContactPersonInfo>();
        private OriginalWorkHistory oriSelected = new OriginalWorkHistory();

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();


        public Person_Person()
        {
            InitializeComponent();

            IList<TitleName> titleName = _hrisBusinessLayer.GetAllTitleName();
            cmbTitleName_ID.ItemsSource = titleName;
            cmbTitleName_ID.SelectedIndex = 0;
            cmbFamTitleName.ItemsSource = titleName;
            cmbFamTitleName.SelectedIndex = 0;
            cmbConTitleName.ItemsSource = titleName;
            cmbConTitleName.SelectedIndex = 0;

            IList<Gender> gender = _hrisBusinessLayer.GetAllGender();
            cmbGender_ID.ItemsSource = gender;
            cmbGender_ID.SelectedIndex = 0;

            IList<MaritalStatu> maritalStatus = _hrisBusinessLayer.GetAllMaritalStatus();
            cmbMaritalStatus_ID.ItemsSource = maritalStatus;
            cmbMaritalStatus_ID.SelectedIndex = 0;

            IList<Nationality> nationality = _hrisBusinessLayer.GetAllNationality();
            cmbNationality_ID.ItemsSource = nationality;
            cmbNationality_ID.SelectedIndex = 0;

            IList<Race> race = _hrisBusinessLayer.GetAllRace();
            cmbRace_ID.ItemsSource = race;
            cmbRace_ID.SelectedIndex = 0;

            IList<Religion> religion = _hrisBusinessLayer.GetAllReligion();
            cmbReligion_ID.ItemsSource = religion;
            cmbReligion_ID.SelectedIndex = 0;

            IList<Blood> blood = _hrisBusinessLayer.GetAllBloods();
            cmbBlood_ID.ItemsSource = blood;
            cmbBlood_ID.SelectedIndex = 0;

            IList<Province> province = _hrisBusinessLayer.GetAllProvinces();
            cmbIDCardIssuedAtProvince.ItemsSource = province.OrderBy(b=>b.ProvinceNameTH);
            cmbAddress1NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);
            cmbAddress2NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);
            cmbAddress3NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);

            IList<District> district = _hrisBusinessLayer.GetAllDistrict();
            cmbIDCardIssuedAtDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress1NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress2NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress3NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);

            IList<SubDistrict> subDistrict = _hrisBusinessLayer.getAllSubDistrict();
            cmbAddress1NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);
            cmbAddress2NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);
            cmbAddress3NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);

            IList<Country> country = _hrisBusinessLayer.GetAllCountrys();
            cmbAddress1NewCountry.ItemsSource = country;
            cmbAddress2NewCountry.ItemsSource = country;
            cmbAddress3NewCountry.ItemsSource = country;
            cmbEduCountry.ItemsSource = country;

            IList<EducationLevel> educationLeveList = _hrisBusinessLayer.GetAllEducationLevels();
            cmbEduLevel.ItemsSource = educationLeveList;

            institueList = _hrisBusinessLayer.GetAllInstitutes().ToList();
            cmbEduInstituteName.ItemsSource = institueList;

            educationQualificationList = _hrisBusinessLayer.GetAllEducationQualifications().ToList();
            cmbEduFull.ItemsSource = educationQualificationList;

            majorList = _hrisBusinessLayer.GetAllMajors().ToList();
            cmbEduMajor.ItemsSource = majorList;

            familyMemberType = _hrisBusinessLayer.GetAllFamilyMemberTypes().ToList();
            cmbFamMemberTyepe.ItemsSource = familyMemberType;

            vitalStatus = _hrisBusinessLayer.GetAllVitalStatuss().ToList();
            cmbFamVitalStatus.ItemsSource = vitalStatus;

            personList = new List<Person>();
        }       
        public Person_Person(string xperson_ID,string xFrom ,string xemployee_IDFromSearchPage,string eduFull)
        {
            InitializeComponent();
            IList<TitleName> titleName = _hrisBusinessLayer.GetAllTitleName();
            cmbTitleName_ID.ItemsSource = titleName;
            cmbTitleName_ID.SelectedIndex = 0;
            cmbFamTitleName.ItemsSource = titleName;
            cmbFamTitleName.SelectedIndex = 0;
            cmbConTitleName.ItemsSource = titleName;
            cmbConTitleName.SelectedIndex = 0;

            IList<Gender> gender = _hrisBusinessLayer.GetAllGender();
            cmbGender_ID.ItemsSource = gender;
            cmbGender_ID.SelectedIndex = 0;

            IList<MaritalStatu> maritalStatus = _hrisBusinessLayer.GetAllMaritalStatus();
            cmbMaritalStatus_ID.ItemsSource = maritalStatus;
            cmbMaritalStatus_ID.SelectedIndex = 0;

            IList<Nationality> nationality = _hrisBusinessLayer.GetAllNationality();
            cmbNationality_ID.ItemsSource = nationality;
            cmbNationality_ID.SelectedIndex = 0;

            IList<Race> race = _hrisBusinessLayer.GetAllRace();
            cmbRace_ID.ItemsSource = race;
            cmbRace_ID.SelectedIndex = 0;

            IList<Religion> religion = _hrisBusinessLayer.GetAllReligion();
            cmbReligion_ID.ItemsSource = religion;
            cmbReligion_ID.SelectedIndex = 0;

            IList<Blood> blood = _hrisBusinessLayer.GetAllBloods();
            cmbBlood_ID.ItemsSource = blood;
            cmbBlood_ID.SelectedIndex = 0;

            IList<Province> province = _hrisBusinessLayer.GetAllProvinces();
            cmbIDCardIssuedAtProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);
            cmbAddress1NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);
            cmbAddress2NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);
            cmbAddress3NewProvince.ItemsSource = province.OrderBy(b => b.ProvinceNameTH);

            IList<District> district = _hrisBusinessLayer.GetAllDistrict();
            cmbIDCardIssuedAtDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress1NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress2NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);
            cmbAddress3NewDistrict.ItemsSource = district.OrderBy(b => b.Province_ID);

            IList<SubDistrict> subDistrict = _hrisBusinessLayer.getAllSubDistrict();
            cmbAddress1NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);
            cmbAddress2NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);
            cmbAddress3NewSubDistrict.ItemsSource = subDistrict.OrderBy(b => b.District_ID);

            IList<Country> country = _hrisBusinessLayer.GetAllCountrys();
            cmbAddress1NewCountry.ItemsSource = country;
            cmbAddress2NewCountry.ItemsSource = country;
            cmbAddress3NewCountry.ItemsSource = country;
            cmbEduCountry.ItemsSource = country;

            IList<EducationLevel> educationLeveList = _hrisBusinessLayer.GetAllEducationLevels();
            cmbEduLevel.ItemsSource = educationLeveList;

            institueList = _hrisBusinessLayer.GetAllInstitutes().ToList();
            cmbEduInstituteName.ItemsSource = institueList;

            educationQualificationList = _hrisBusinessLayer.GetAllEducationQualifications().ToList();
            cmbEduFull.ItemsSource = educationQualificationList;

            majorList = _hrisBusinessLayer.GetAllMajors().ToList();
            cmbEduMajor.ItemsSource = majorList;

            familyMemberType = _hrisBusinessLayer.GetAllFamilyMemberTypes().ToList();
            cmbFamMemberTyepe.ItemsSource = familyMemberType;

            vitalStatus = _hrisBusinessLayer.GetAllVitalStatuss().ToList();
            cmbFamVitalStatus.ItemsSource = vitalStatus;

            personList = new List<Person>();

            if (xFrom == "EmpSearch")
            {
                person_IDfromGetViewPerson = xperson_ID;
                employee_IDfromSearchPage = xemployee_IDFromSearchPage;

                DataGridEdu.ItemsSource = _hrisBusinessLayer.GetEducationByPersonID(xperson_ID);
                DataGridFam.ItemsSource = _hrisBusinessLayer.GetFamilyMemberByPerson_ID(xperson_ID);
            }
            else if (xFrom == "SetupEduQualification" || xFrom == "SetupEduMajor") //ถ้ามาจากหน้า setup education qualification
            {
                person_IDfromGetViewPerson = xperson_ID;
                //employee_IDfromSearchPage = xemployee_IDFromSearchPage;
                cmbEduFull.SelectedValue = eduFull;
                TcPerson.SelectedIndex = 4; //selectedindex  = 4 คือ page education
                if (eduFull != "")
                {
                    GetEducationShortQualification(eduFull);
                }

                DataGridEdu.ItemsSource = _hrisBusinessLayer.GetEducationByPersonID(xperson_ID);
            }
            else if (xFrom == "SetupfamilyMemberType")
            {
                person_IDfromGetViewPerson = xperson_ID;
                cmbFamMemberTyepe.SelectedValue = eduFull;
                TcPerson.SelectedIndex = 5;
                DataGridFam.ItemsSource = _hrisBusinessLayer.GetFamilyMemberByPerson_ID(xperson_ID);
            }
        }

        public void GetEducationShortQualification(string edu_ID) //หา ชื่อย่อวุฒิการศึกษา
        {
            if (edu_ID != "")
            {
                EducationQualification edu = new EducationQualification();
                edu = _hrisBusinessLayer.GetEducationQualificationByID(edu_ID);
                txtEduShort.Text = edu.EducationQualificationInitialTH;
            }            
        }
     
        public void GetPersonInformation(string person_ID)
        {
            
            PersonInformation personInformation = new PersonInformation(person_ID);

            //ID card
            txtIDCardInfoPerson_ID.Text = personInformation.xPersonID;
            txtIDCardInfoTitleName.Text = personInformation.TitleNameTH;
            txtIDCardFirstName.Text = personInformation.FirstNameTH;
            txtIDCardLastName.Text = personInformation.LastNameTH;
            txtNewIDCard.Text = personInformation.IDCard;

            //Education
            txtEduPerson_ID.Text = personInformation.xPersonID;
            txtEduTitleName.Text = personInformation.TitleNameTH;
            txtEduFirstName.Text = personInformation.FirstNameTH;
            txtEduLastName.Text = personInformation.LastNameTH;

            //Family
            txtFamPerson_ID.Text = personInformation.xPersonID;
            txtFamTitleName.Text = personInformation.TitleNameTH;
            txtFamFirstName.Text = personInformation.FirstNameTH;
            txtFamLastName.Text = personInformation.LastNameTH;

            //OriginalWorkHistory 
            txtOriPerson_ID.Text = personInformation.xPersonID;
            txtOriTitleName.Text = personInformation.TitleNameTH;
            txtOriFirstName.Text = personInformation.FirstNameTH;
            txtOriLastName.Text = personInformation.LastNameTH;

            //ContactPersonInfo
            txtConLPerson_ID.Text = personInformation.xPersonID;
            txtConLTitleName.Text = personInformation.TitleNameTH;
            txtConLFirstName.Text = personInformation.FirstNameTH;
            txtConLLastName.Text = personInformation.LastNameTH;
           
            DataGridIDCard.ItemsSource = null;
            DataGridIDCard.ItemsSource = _hrisBusinessLayer.GetIDCardInfo(person_ID).OrderByDescending(c=>c.ExpiredDate) ;  //get idcard information list from HRISBusinesssLayer

            DataGridFam.ItemsSource = null; 
            DataGridFam.ItemsSource = _hrisBusinessLayer.GetFamilyMemberByPerson_ID(person_ID);

            DataGridOri.ItemsSource = null;
            DataGridOri.ItemsSource = _hrisBusinessLayer.GetOriginalWorkHistoryByPersonID(person_ID);

            DataGridCon.ItemsSource = null;
            DataGridCon.ItemsSource = _hrisBusinessLayer.GetContactPersonInfoByPersonID(person_ID);
           
            txtAddressPersonID.Text = personInformation.xPersonID;
            txtAddressTitleName.Text = personInformation.TitleNameTH;
            txtAddressFirstName.Text = personInformation.FirstNameTH;
            txtAddressLastName.Text = personInformation.LastNameTH;

            txtAddress1HomeNo.Text = personInformation.AddressByIDCARDHomeNumber;
            txtAddress1Moo.Text = Convert.ToString( personInformation.AddressByIDCARDMoo );
            txtAddress1HousingProject.Text = personInformation.AddressByIDCARDHousingProject ;
            txtAddress1Street.Text = personInformation.AddressByIDCARDStreet;
            txtAddress1Soi.Text = Convert.ToString( personInformation.AddressByIDCARDSoi);
            txtAddress1Province.Text = personInformation.AddressByIDCARDProvince;
            txtAddress1District.Text = personInformation.AddressByIDCARDDistrict;
            txtAddress1SubDistrict.Text = personInformation.AddressByIDCARDSubDistrict;
            txtAddress1Postcode.Text = Convert.ToString(personInformation.AddressByIDCARDPostcode);
            txtAddress1Country.Text = personInformation.AddressByIDCARDCountry;
            txtAddress1HomePhoneNumber.Text = personInformation.AddressByIDCARDHomePhoneNumber;
            if (personInformation.AddressByIDCARDHomeNumber != null)
                AddAddress1Button.IsEnabled = false;
 
            txtAddress1NewHomeNo.Text = personInformation.AddressByIDCARDHomeNumber;
            txtAddress1NewMoo.Text = Convert.ToString(personInformation.AddressByIDCARDMoo );
            txtAddress1NewHousingProject.Text = personInformation.AddressByIDCARDHousingProject;
            txtAddress1NewStreet.Text = personInformation.AddressByIDCARDStreet;
            txtAddress1NewSoi.Text = Convert.ToString(personInformation.AddressByIDCARDSoi);
            cmbAddress1NewProvince.Text = personInformation.AddressByIDCARDProvince;
            cmbAddress1NewDistrict.Text = personInformation.AddressByIDCARDDistrict;
            cmbAddress1NewSubDistrict.Text = personInformation.AddressByIDCARDSubDistrict;
            txtAddress1NewPostcode.Text = Convert.ToString(personInformation.AddressByIDCARDPostcode);
            cmbAddress1NewCountry.Text = personInformation.AddressByIDCARDCountry;
            txtAddress1NewHomePhoneNumber.Text = personInformation.AddressByIDCARDHomePhoneNumber;

            txtAddress2HomeNo.Text = personInformation.AddressByPermanentHomeNumber;
            txtAddress2Moo.Text = Convert.ToString(personInformation.AddressByPermanentMoo);
            txtAddress2HousingProject.Text = personInformation.AddressByPermanentHousingProject;
            txtAddress2Street.Text = personInformation.AddressByPermanentStreet;
            txtAddress2Soi.Text = Convert.ToString(personInformation.AddressByPermanentSoi);
            txtAddress2Province.Text = personInformation.AddressByPermanentProvince;
            txtAddress2District.Text = personInformation.AddressByPermanentDistrict;
            txtAddress2SubDistrict.Text = personInformation.AddressByPermanentSubDistrict;
            txtAddress2Postcode.Text = Convert.ToString(personInformation.AddressByPermanentPostcode);
            txtAddress2Country.Text = personInformation.AddressByPermanentCountry;
            txtAddress2HomePhoneNumber.Text = personInformation.AddressByPermanentHomePhoneNumber;
            if (personInformation.AddressByPermanentHomeNumber != null)
                AddAddress2Button.IsEnabled = false;

            txtAddress2NewHomeNo.Text = personInformation.AddressByPermanentHomeNumber;
            txtAddress2NewMoo.Text = Convert.ToString(personInformation.AddressByPermanentMoo);
            txtAddress2NewHousingProject.Text = personInformation.AddressByPermanentHousingProject;
            txtAddress2NewStreet.Text = personInformation.AddressByPermanentStreet;
            txtAddress2NewSoi.Text = Convert.ToString(personInformation.AddressByPermanentSoi);
            cmbAddress2NewProvince.Text = personInformation.AddressByPermanentProvince;
            cmbAddress2NewDistrict.Text = personInformation.AddressByPermanentDistrict;
            cmbAddress2NewSubDistrict.Text = personInformation.AddressByPermanentSubDistrict;
            txtAddress2NewPostcode.Text = Convert.ToString(personInformation.AddressByPermanentPostcode);
            cmbAddress2NewCountry.Text = personInformation.AddressByPermanentCountry;
            txtAddress2NewHomePhoneNumber.Text = personInformation.AddressByPermanentHomePhoneNumber;

            txtAddress3HomeNo.Text = personInformation.AddressByCurrentHomeNumber;
            txtAddress3Moo.Text = Convert.ToString(personInformation.AddressByCurrentMoo);
            txtAddress3HousingProject.Text = personInformation.AddressByCurrentHousingProject;
            txtAddress3Street.Text = personInformation.AddressByCurrentStreet;
            txtAddress3Soi.Text = Convert.ToString(personInformation.AddressByCurrentSoi);
            txtAddress3Province.Text = personInformation.AddressByCurrentProvince;
            txtAddress3District.Text = personInformation.AddressByCurrentDistrict;
            txtAddress3SubDistrict.Text = personInformation.AddressByCurrentSubDistrict;
            txtAddress3Postcode.Text = Convert.ToString(personInformation.AddressByCurrentPostcode);
            txtAddress3Country.Text = personInformation.AddressByCurrentCountry;
            txtAddress3HomePhoneNumber.Text = personInformation.AddressByCurrentHomePhoneNumber;
            if (personInformation.AddressByCurrentHomeNumber != null)
                AddAddress3Button.IsEnabled = false;

            txtAddress3NewHomeNo.Text = personInformation.AddressByPermanentHomeNumber;
            txtAddress3NewMoo.Text = Convert.ToString(personInformation.AddressByPermanentMoo);
            txtAddress3NewHousingProject.Text = personInformation.AddressByPermanentHousingProject;
            txtAddress3NewStreet.Text = personInformation.AddressByPermanentStreet;
            txtAddress3NewSoi.Text = Convert.ToString(personInformation.AddressByPermanentSoi);
            cmbAddress3NewProvince.Text = personInformation.AddressByCurrentProvince;
            cmbAddress3NewDistrict.Text = personInformation.AddressByCurrentDistrict;
            cmbAddress3NewSubDistrict.Text = personInformation.AddressByCurrentSubDistrict;
            txtAddress3NewPostcode.Text = Convert.ToString(personInformation.AddressByPermanentPostcode);
            cmbAddress3NewCountry.Text = personInformation.AddressByCurrentCountry;
            txtAddress3NewHomePhoneNumber.Text = personInformation.AddressByPermanentHomePhoneNumber;
        }

        public void ClearData(int TabIndex)
        {
            try
            {
                if (TabIndex == 0) //Person
                {
                    AddButton.IsEnabled = true;
                    txtPerson_ID.IsReadOnly = false;
                    txtPerson_ID.Background = new SolidColorBrush(Colors.White);

                    txtPerson_ID.Text = "";
                    txtFirstNameTH.Text = "";
                    txtMidNameTH.Text = "";
                    txtLastNameTH.Text = "";
                    txtFirstNameEN.Text = "";
                    txtMidNameEN.Text = "";
                    txtLastNameEN.Text = "";
                    txtEmail.Text = "";
                    txtHeight.Text = "";
                    txtWeight.Text = "";


                    personList = _hrisBusinessLayer.GetAllPersons().ToList();
                  
                }
                else if (TabIndex == 1) //History
                {
                    
                }
                else if (TabIndex == 2) //iDcard information
                {
                    AddIDCardInfoButton.IsEnabled = true;
                    GetPersonInformation(txtIDCardInfoPerson_ID.Text);
                                   
                }else if (TabIndex == 3) //Address
                {
                    AddAddress1Button.IsEnabled = true;
                    GetPersonInformation(txtAddressPersonID.Text);
                }
                else if (TabIndex == 4)//Education
                {
                    AddEduButton.IsEnabled = true;
                    GetPersonInformation(txtEduPerson_ID.Text);

                    //DataGridEdu.ItemsSource = _hrisBusinessLayer.GetEducationByPersonID(txtEduPerson_ID.Text);
                }
                else if(TabIndex == 5 )//Family
                {
                    AddFamButton.IsEnabled = true;
                    //DataGridFam.ItemsSource = _hrisBusinessLayer.GetFamilyMemberByPerson_ID(txtFamPerson_ID.Text);
                    GetPersonInformation(txtFamPerson_ID.Text);
                }
                else if (TabIndex == 6)//OriginalWorkHistory
                {
                    AddOriButton.IsEnabled = true;
                    GetPersonInformation(txtOriPerson_ID.Text);
                    //DataGridOri.ItemsSource = _hrisBusinessLayer.GetOriginalWorkHistoryByPersonID(txtOriPerson_ID.Text);
                }
                else if (TabIndex == 7)//contactperson
                {
                    AddConButton.IsEnabled = true;
                    GetPersonInformation(txtConLPerson_ID.Text);
                    //DataGridCon.ItemsSource = _hrisBusinessLayer.GetContactPersonInfoByPersonID(txtConLPerson_ID.Text);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

       

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                                
                personList = _hrisBusinessLayer.GetAllPersons().ToList();
            
                DataGrid.ItemsSource = null;
         

                //ถ้าข้อมูลถูกส่งมาจากหน้า employeeSearch
                if(person_IDfromGetViewPerson != ""  &&  person_IDfromGetViewPerson != null  )
                {
                    PersonInformation x = new PersonInformation(person_IDfromGetViewPerson);
                    txtPerson_ID.Text = x.xPersonID;
                    cmbTitleName_ID.Text = x.TitleNameTH;
                    txtFirstNameTH.Text = x.FirstNameTH;                   
                    txtMidNameTH.Text = x.MidNameTH;
                    txtLastNameTH.Text = x.LastNameTH;
                    txtFirstNameEN.Text = x.FirstNameEN;
                    txtMidNameEN.Text = x.MidNameEN;
                    txtLastNameEN.Text = x.LastNameEN;
                    txtEmail.Text = x.Email;
                    DateOfBirth.Text = Convert.ToString( x.DateOfBirth);
                    txtHeight.Text = Convert.ToString( x.Height);
                    txtWeight.Text = Convert.ToString( x.Weight);
                    cmbBlood_ID.Text = x.Blood_ID != null ? x.Blood_Name : "";                   
                    cmbGender_ID.Text = x.GenDer_Name != null  ? x.GenDer_Name : "";
                    cmbMaritalStatus_ID.Text = x.MaritalStatus_Name != null ? x.MaritalStatus_Name : "";
                    cmbNationality_ID.Text = x.Nationality_Name != null ? x.Nationality_Name : "";
                    cmbRace_ID.SelectedValue = x.Race_ID != null ? x.Race_ID : "";
                    cmbReligion_ID.Text =x.Religion_Name != null ? x.Religion_Name : "";
                    GetPersonInformation(x.xPersonID);

                
                    AddButton.IsEnabled = false;                    
                    ToEmployeePageButton.IsEnabled = false;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                if (DateOfBirth.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if(  Convert.ToDateTime(DateOfBirth.Text) > DateTime.Now ) {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิดให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                    
                if (txtHeight.Text == "") txtHeight.Text = Convert.ToString( 0);
                if (txtWeight.Text =="") txtWeight.Text = Convert.ToString(0);


                cmbBlood_ID.SelectedIndex = 0;
                if (cmbBlood_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุหมู่โลหิต", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                

                if (cmbGender_ID.Text == "หญิง" && cmbTitleName_ID.Text == "นาย")
                {
                    MessageBox.Show("กรุณาระบุเพศให้ตรง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                cmbMaritalStatus_ID.SelectedIndex = 0;
                if (cmbMaritalStatus_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานภาพ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                cmbNationality_ID.SelectedIndex = 0;
                if (cmbNationality_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสัญชาติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                cmbRace_ID.SelectedIndex = 0;
                if (cmbRace_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเชื้อชาติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                cmbReligion_ID.SelectedIndex = 0;
                if (cmbReligion_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุศาสนา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                

                Double XmaxPerson_ID = Convert.ToDouble( personList.Max(c => c.Person_ID)) +1 ;
                txtPerson_ID.Text = Convert.ToString(XmaxPerson_ID);

                Person[] personObj = new Person[1];
                Person person1 = new Person(); //created  1 instanct or object
                person1.Person_ID = txtPerson_ID.Text;
                person1.TitleName_ID = (string)cmbTitleName_ID.SelectedValue;
                person1.FirstNameTH = txtFirstNameTH.Text;
                person1.MidNameTH = txtMidNameTH.Text;
                person1.LastNameTH = txtLastNameTH.Text;
                person1.FirstNameEN = txtFirstNameEN.Text;
                person1.MidNameEN = txtMidNameEN.Text;
                person1.LastNameEN = txtLastNameEN.Text;
                person1.Email = txtEmail.Text;
                person1.DateOfBirth = Convert.ToDateTime(DateOfBirth.Text) ;
                person1.Height = Convert.ToDecimal(txtHeight.Text);
                person1.Weight = Convert.ToDecimal(txtWeight.Text);               
                person1.Gender_ID = (string)cmbGender_ID.SelectedValue;
                person1.MaritalStatus_ID = (string)cmbMaritalStatus_ID.SelectedValue;
                person1.Nationality_ID = (string)cmbNationality_ID.SelectedValue;
                person1.Race_ID = (string)cmbRace_ID.SelectedValue;
                person1.Religion_ID = (string)cmbReligion_ID.SelectedValue;
                person1.Blood_ID = (string)cmbBlood_ID.SelectedValue;

                personObj[0] = person1;
                _hrisBusinessLayer.AddPerson(personObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T003";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtPerson_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.IsEnabled = true;
                txtSearch.Text = txtFirstNameTH.Text;

                personList = _hrisBusinessLayer.GetAllPersons().ToList();
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = personList.Where(p => p.FirstNameTH.Contains(txtSearch.Text));


                GetPersonInformation(txtPerson_ID.Text);

        }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DateOfBirth.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtHeight.Text == "")
                    txtHeight.Text = Convert.ToString(0);
                if (txtWeight.Text == "")
                    txtWeight.Text = Convert.ToString(0);

                if (cmbBlood_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุหมู่โลหิต", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbGender_ID.Text == "หญิง" && cmbTitleName_ID.Text == "นาย")
                {
                    MessageBox.Show("กรุณาระบุเพศให้ตรง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbMaritalStatus_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานภาพ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbNationality_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสัญชาติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbRace_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเชื้อชาติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbReligion_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุศาสนา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Person person1 = new Person(); //created  1 instanct or object
                person1.Person_ID = txtPerson_ID.Text;
                person1.TitleName_ID = (string)cmbTitleName_ID.SelectedValue;
                person1.FirstNameTH = txtFirstNameTH.Text;
                person1.MidNameTH = txtMidNameTH.Text;
                person1.LastNameTH = txtLastNameTH.Text;
                person1.FirstNameEN = txtFirstNameEN.Text;
                person1.MidNameEN = txtMidNameEN.Text;
                person1.LastNameEN = txtLastNameEN.Text;
                person1.Email = txtEmail.Text;
                person1.DateOfBirth = Convert.ToDateTime(DateOfBirth.Text);
                person1.Height =  Convert.ToDecimal(txtHeight.Text);
                person1.Weight = Convert.ToDecimal(txtWeight.Text);               
                person1.Gender_ID = (string)cmbGender_ID.SelectedValue;
                person1.MaritalStatus_ID = (string)cmbMaritalStatus_ID.SelectedValue;
                person1.Nationality_ID = (string)cmbNationality_ID.SelectedValue;
                person1.Race_ID = (string)cmbRace_ID.SelectedValue;
                person1.Religion_ID = (string)cmbReligion_ID.SelectedValue;
                person1.Blood_ID = (string)cmbBlood_ID.SelectedValue;

                _hrisBusinessLayer.UpdatePerson(person1);

                //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee_IDfromSearchPage);
                if (employeeSTECPayroll != null)
                {
                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                    queryX.Employee_id = employeeSTECPayroll.Employee_id;
                    queryX.Dept_code = employeeSTECPayroll.Dept_code;
                    queryX.Tname = (string)cmbTitleName_ID.Text;
                    queryX.Fname = txtFirstNameTH.Text;
                    queryX.Lname = txtLastNameTH.Text;
                    queryX.Tname_Eng = ((TitleName)cmbTitleName_ID.SelectedItem).TitleNameEN;
                    queryX.Fname_Eng = txtFirstNameEN.Text;
                    queryX.Lname_Eng = txtLastNameEN.Text;
                    queryX.Birth = Convert.ToDateTime(DateOfBirth.Text);
                    queryX.Id_card = employeeSTECPayroll.Id_card;
                    queryX.Start_date = employeeSTECPayroll.Start_date;
                    queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                    queryX.Staff_status = employeeSTECPayroll.Staff_status;
                    queryX.Staff_type = employeeSTECPayroll.Staff_type;
                    queryX.Address = employeeSTECPayroll.Address;
                    queryX.Moo = employeeSTECPayroll.Moo;
                    queryX.Tambon = employeeSTECPayroll.Tambon;
                    queryX.Amphur = employeeSTECPayroll.Amphur;
                    queryX.Province = employeeSTECPayroll.Province;
                    queryX.Zipcode = employeeSTECPayroll.Zipcode;
                    queryX.Shift = employeeSTECPayroll.Shift;
                    queryX.Wage = employeeSTECPayroll.Wage;
                    queryX.Salary = employeeSTECPayroll.Salary;
                    queryX.NetSalary = employeeSTECPayroll.NetSalary;
                    queryX.Ot_hour_rate = employeeSTECPayroll.Ot_hour_rate;
                    queryX.Ot_holiday_rate = employeeSTECPayroll.Ot_holiday_rate;
                    queryX.Ot_hour_holiday_rate = employeeSTECPayroll.Ot_hour_holiday_rate;
                    employeeSTECObj[0] = queryX;
                    _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                }
                else
                {
                    //เช็คว่าถ้า person_ID นี้มีใน employee แล้วให้ update ค่าใน payroll_stec ด้วย
                    Employee employee = new Employee();
                    employee = _hrisBusinessLayer.GetAllEmployees().Where(b => b.Person_ID == txtPerson_ID.Text).OrderByDescending(b => b.EndDate).FirstOrDefault();
                    if (employee != null)
                    {
                        //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                        EmployeeSTECPayroll employeeSTECPayroll2 = new EmployeeSTECPayroll();
                        employeeSTECPayroll2 = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee.Noted);
                        if (employeeSTECPayroll2 != null)
                        {
                            EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                            EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                            queryX.Employee_id = employeeSTECPayroll2.Employee_id;
                            queryX.Dept_code = employeeSTECPayroll2.Dept_code;
                            queryX.Tname = (string)cmbTitleName_ID.Text;
                            queryX.Fname = txtFirstNameTH.Text;
                            queryX.Lname = txtLastNameTH.Text;
                            queryX.Tname_Eng = ((TitleName)cmbTitleName_ID.SelectedItem).TitleNameEN;
                            queryX.Fname_Eng = txtFirstNameEN.Text;
                            queryX.Lname_Eng = txtLastNameEN.Text;
                            queryX.Birth = Convert.ToDateTime(DateOfBirth.Text);
                            queryX.Id_card = employeeSTECPayroll2.Id_card;
                            queryX.Start_date = employeeSTECPayroll2.Start_date;
                            queryX.Bank_acc = employeeSTECPayroll2.Bank_acc;
                            queryX.Staff_status = employeeSTECPayroll2.Staff_status;
                            queryX.Staff_type = employeeSTECPayroll2.Staff_type;
                            queryX.Address = employeeSTECPayroll2.Address;
                            queryX.Moo = employeeSTECPayroll2.Moo;
                            queryX.Tambon = employeeSTECPayroll2.Tambon;
                            queryX.Amphur = employeeSTECPayroll2.Amphur;
                            queryX.Province = employeeSTECPayroll2.Province;
                            queryX.Zipcode = employeeSTECPayroll2.Zipcode;
                            queryX.Shift = employeeSTECPayroll2.Shift;
                            queryX.Wage = employeeSTECPayroll2.Wage;
                            queryX.Salary = employeeSTECPayroll2.Salary;
                            queryX.NetSalary = employeeSTECPayroll2.NetSalary;
                            queryX.Ot_hour_rate = employeeSTECPayroll2.Ot_hour_rate;
                            queryX.Ot_holiday_rate = employeeSTECPayroll2.Ot_holiday_rate;
                            queryX.Ot_hour_holiday_rate = employeeSTECPayroll2.Ot_hour_holiday_rate;
                            employeeSTECObj[0] = queryX;
                            _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                        }
                    }
                }


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T003";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtPerson_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.IsEnabled = true;
                txtSearch.Text = txtFirstNameTH.Text;

                personList = _hrisBusinessLayer.GetAllPersons().ToList();
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = personList.Where(p => p.FirstNameTH.Contains(txtSearch.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Person person1 = new Person();
                person1 = (Person)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                _hrisBusinessLayer.RemovePerson(person1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T003";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "";
                transactionLog1.PKFields = txtIDCardInfoPerson_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtIDCardInfoPerson_ID.Text + "," + txtNewIDCard.Text + "," + IDCardInfoIssuedDate.Text + "," + IDCardInfoExpireDate.Text + "," + cmbIDCardIssuedAtProvince.Text + "," + cmbIDCardIssuedAtDistrict.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //ClearData(0);
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.IsEnabled = true;
                txtSearch.Text = txtFirstNameTH.Text;

                personList = _hrisBusinessLayer.GetAllPersons().ToList();
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = personList.Where(p => p.FirstNameTH.Contains(txtSearch.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Person person1 = new Person();
                person1 = (Person)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (person1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtPerson_ID.IsReadOnly = false;
                    txtPerson_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtPerson_ID.IsReadOnly = true;
                    txtPerson_ID.Background = new SolidColorBrush(Colors.LightGray);

                    GetPersonInformation(person1.Person_ID);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       

        private void ToEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    Person person1 = new Person();
            //    person1 = (Person)DataGrid.SelectedItem;
            //    this.NavigationService.Navigate(new Employees.HumanResource_Employee(person1));
                
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void TcPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // ... Get TabControl reference.
                var item = sender as TabControl;
                // ... Set Title to selected tab header.
                var selected = item.SelectedItem as TabItem;
                //this.Title = selected.Header.ToString();
                int TcPersonTabIndex = TcPerson.SelectedIndex;
                if ((sender as TabControl).SelectedIndex != 0)
                {

                    Person query1 = _hrisBusinessLayer.GetPerson(txtPerson_ID.Text);
                    if ((sender as TabControl).SelectedIndex == 1) //history
                    {
                        DataGridHistory.ItemsSource = null;
                        DataGridHistory.ItemsSource = _hrisBusinessLayer.GetPersonHistory(txtPerson_ID.Text).OrderByDescending(b => b.EndDate);
                    }
                    else if ((sender as TabControl).SelectedIndex == 2) //idCard information
                    {


                    }
                    else if ((sender as TabControl).SelectedIndex == 3) //Address
                    {

                    }
                    else if ((sender as TabControl).SelectedIndex == 4) //Education
                    {
                        //DataGridEdu.ItemsSource = null;
                        //DataGridEdu.ItemsSource = _hrisBusinessLayer.GetEducationByPersonID(txtEduPerson_ID.Text);
                    }
                    else if ((sender as TabControl).SelectedIndex == 5) //Family
                    {
                        //DataGridFam.ItemsSource = null;
                        //DataGridFam.ItemsSource = _hrisBusinessLayer.GetFamilyMemberByPerson_ID(txtFamPerson_ID.Text);
                    }
                    else if ((sender as TabControl).SelectedIndex == 6)//OriginalWorkHistory
                    {
                        //DataGridOri.ItemsSource = null;
                        //DataGridOri.ItemsSource = _hrisBusinessLayer.GetOriginalWorkHistoryByPersonID(txtOriPerson_ID.Text);
                    }
                    else if((sender as TabControl).SelectedIndex == 7) //ContactPersonInfo
                    {
                        //DataGridCon.ItemsSource = null;
                        //DataGridCon.ItemsSource = _hrisBusinessLayer.GetContactPersonInfoByPersonID(txtConLPerson_ID.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddIDCardInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (IDCardInfoIssuedDate.Text == "")
                    IDCardInfoIssuedDate.Text = Convert.ToString(DateTime.Now);
                if (IDCardInfoExpireDate.Text == "")
                    IDCardInfoExpireDate.Text = Convert.ToString(DateTime.Now);
                if(cmbIDCardIssuedAtProvince.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbIDCardIssuedAtDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์อำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

              
                IDCardInfo[] idCardObj = new IDCardInfo[1];
                IDCardInfo idCard1 = new IDCardInfo(); //created  1 instanct or object1
                idCard1.Person_ID = txtIDCardInfoPerson_ID.Text;
                idCard1.Card_ID = txtNewIDCard.Text;               
                idCard1.IssuedDate = Convert.ToDateTime(IDCardInfoIssuedDate.Text);
                idCard1.ExpiredDate = Convert.ToDateTime(IDCardInfoExpireDate.Text);
                idCard1.IssuedAtProvinceID = (string)cmbIDCardIssuedAtProvince.SelectedValue;
                idCard1.IssuedAtDistrictID = (string)cmbIDCardIssuedAtDistrict.SelectedValue;
                idCard1.CreatedDate = DateTime.Now;
                idCard1.ModifiedDate = DateTime.Now;
                
                idCardObj[0] = idCard1;
                _hrisBusinessLayer.AddIDCardInfo(idCardObj);

                //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll =  _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee_IDfromSearchPage);
                if (employeeSTECPayroll != null)
                {
                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                    queryX.Employee_id = employeeSTECPayroll.Employee_id;
                    queryX.Dept_code = employeeSTECPayroll.Dept_code;
                    queryX.Tname = employeeSTECPayroll.Tname;
                    queryX.Fname = employeeSTECPayroll.Fname;
                    queryX.Lname = employeeSTECPayroll.Lname;
                    queryX.Birth = employeeSTECPayroll.Birth;
                    queryX.Id_card = txtNewIDCard.Text;
                    queryX.Start_date = employeeSTECPayroll.Start_date;
                    queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                    queryX.Staff_status = employeeSTECPayroll.Staff_status;
                    queryX.Staff_type = employeeSTECPayroll.Staff_type;
                    queryX.Address = employeeSTECPayroll.Address;
                    queryX.Moo = employeeSTECPayroll.Moo;
                    queryX.Tambon = employeeSTECPayroll.Tambon;
                    queryX.Amphur = employeeSTECPayroll.Amphur;
                    queryX.Province = employeeSTECPayroll.Province;
                    queryX.Zipcode = employeeSTECPayroll.Zipcode;
                    queryX.Shift = employeeSTECPayroll.Shift;
                    employeeSTECObj[0] = queryX;
                    _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                }
                else
                {
                    //เช็คว่าถ้า person_ID นี้มีใน employee แล้วให้ update ค่าใน payroll_stec ด้วย
                    Employee employee = new Employee();
                    employee = _hrisBusinessLayer.GetAllEmployees().Where(b => b.Person_ID == txtIDCardInfoPerson_ID.Text).OrderByDescending(b => b.EndDate).FirstOrDefault();                   
                    if (employee != null)
                    {
                        //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                        EmployeeSTECPayroll employeeSTECPayroll2 = new EmployeeSTECPayroll();
                        employeeSTECPayroll2 = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee.Noted);
                        if (employeeSTECPayroll2 != null)
                        {
                            EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                            EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                            queryX.Employee_id = employeeSTECPayroll2.Employee_id;
                            queryX.Dept_code = employeeSTECPayroll2.Dept_code;
                            queryX.Tname = employeeSTECPayroll2.Tname;
                            queryX.Fname = employeeSTECPayroll2.Fname;
                            queryX.Lname = employeeSTECPayroll2.Lname;
                            queryX.Birth = employeeSTECPayroll2.Birth;
                            queryX.Id_card = txtNewIDCard.Text;
                            queryX.Start_date = employeeSTECPayroll2.Start_date;
                            queryX.Bank_acc = employeeSTECPayroll2.Bank_acc;
                            queryX.Staff_status = employeeSTECPayroll2.Staff_status;
                            queryX.Staff_type = employeeSTECPayroll2.Staff_type;
                            queryX.Address = employeeSTECPayroll2.Address;
                            queryX.Moo = employeeSTECPayroll2.Moo;
                            queryX.Tambon = employeeSTECPayroll2.Tambon;
                            queryX.Amphur = employeeSTECPayroll2.Amphur;
                            queryX.Province = employeeSTECPayroll2.Province;
                            queryX.Zipcode = employeeSTECPayroll2.Zipcode;
                            queryX.Shift = employeeSTECPayroll2.Shift;
                            employeeSTECObj[0] = queryX;
                            _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                        }
                    }
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T001";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "";
                transactionLog1.PKFields = txtNewIDCard.Text;
                transactionLog1.FKFields = txtIDCardInfoPerson_ID.Text;
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtNewIDCard.Text + "," + IDCardInfoIssuedDate.Text + "," + IDCardInfoExpireDate.Text + "," + cmbIDCardIssuedAtProvince.Text + "," + cmbIDCardIssuedAtDistrict.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetPersonInformation(txtIDCardInfoPerson_ID.Text);

                txtNewIDCard.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditIDCardInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (IDCardInfoIssuedDate.Text == "")
                    IDCardInfoIssuedDate.Text = Convert.ToString(DateTime.Now);
                if (IDCardInfoExpireDate.Text == "")
                    IDCardInfoExpireDate.Text = Convert.ToString(DateTime.Now);
                if (cmbIDCardIssuedAtProvince.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbIDCardIssuedAtDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์อำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T001";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "";
                transactionLog1.PKFields = IDcardInfoSelectRow.Card_ID;
                transactionLog1.FKFields = Convert.ToString(IDcardInfoSelectRow.ExpiredDate);
                transactionLog1.OldData = IDcardInfoSelectRow.Person_ID + ","+ IDcardInfoSelectRow.Card_ID + "," + IDcardInfoSelectRow.IssuedDate + "," + IDcardInfoSelectRow.ExpiredDate + "," + IDcardInfoSelectRow.Province.ProvinceNameTH + "," + IDcardInfoSelectRow.District.DistrictNameTH;
                transactionLog1.NewData = IDcardInfoSelectRow.Person_ID + "," + txtNewIDCard.Text + "," + IDCardInfoIssuedDate.Text + "," + IDCardInfoExpireDate.Text + "," + cmbIDCardIssuedAtProvince.Text + "," + cmbIDCardIssuedAtDistrict.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                IDCardInfo[] idCardObj = new IDCardInfo[1];
                IDCardInfo idCard1 = new IDCardInfo(); //created  1 instanct or object1              
                idCard1.Person_ID = txtPerson_ID.Text;
                idCard1.Card_ID = txtNewIDCard.Text;
                idCard1.IssuedDate = Convert.ToDateTime(IDCardInfoIssuedDate.Text);
                idCard1.ExpiredDate = Convert.ToDateTime(IDCardInfoExpireDate.Text);
                idCard1.IssuedAtProvinceID = (string)cmbIDCardIssuedAtProvince.SelectedValue;
                idCard1.IssuedAtDistrictID = (string)cmbIDCardIssuedAtDistrict.SelectedValue;
                idCard1.CreatedDate = IDcardInfoSelectRow.CreatedDate;
                idCard1.ModifiedDate = DateTime.Now;
                idCardObj[0] = idCard1;
                _hrisBusinessLayer.RemoveIDCardInfo(IDcardInfoSelectRow);
                _hrisBusinessLayer.AddIDCardInfo(idCardObj);

                //--edit in stecpayroll
                //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee_IDfromSearchPage);
                if (employeeSTECPayroll != null)
                {
                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                    queryX.Employee_id = employeeSTECPayroll.Employee_id;
                    queryX.Dept_code = employeeSTECPayroll.Dept_code;
                    queryX.Tname = employeeSTECPayroll.Tname;
                    queryX.Fname = employeeSTECPayroll.Fname;
                    queryX.Lname = employeeSTECPayroll.Lname;
                    queryX.Birth = employeeSTECPayroll.Birth;
                    queryX.Id_card = txtNewIDCard.Text;
                    queryX.Start_date = employeeSTECPayroll.Start_date;
                    queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                    queryX.Staff_status = employeeSTECPayroll.Staff_status;
                    queryX.Staff_type = employeeSTECPayroll.Staff_type;
                    queryX.Address = employeeSTECPayroll.Address;
                    queryX.Moo = employeeSTECPayroll.Moo;
                    queryX.Tambon = employeeSTECPayroll.Tambon;
                    queryX.Amphur = employeeSTECPayroll.Amphur;
                    queryX.Province = employeeSTECPayroll.Province;
                    queryX.Zipcode = employeeSTECPayroll.Zipcode;
                    queryX.Shift = employeeSTECPayroll.Shift;
                    employeeSTECObj[0] = queryX;
                    _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                }
                else
                {
                    //เช็คว่าถ้า person_ID นี้มีใน employee แล้วให้ update ค่าใน payroll_stec ด้วย
                    Employee employee = new Employee();
                    employee = _hrisBusinessLayer.GetAllEmployees().Where(b => b.Person_ID == txtPerson_ID.Text).OrderByDescending(b => b.EndDate).FirstOrDefault();
                    if (employee != null)
                    {
                        //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                        EmployeeSTECPayroll employeeSTECPayroll2 = new EmployeeSTECPayroll();
                        employeeSTECPayroll2 = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee.Noted);
                        if (employeeSTECPayroll2 != null)
                        {
                            EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                            EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                            queryX.Employee_id = employeeSTECPayroll2.Employee_id;
                            queryX.Dept_code = employeeSTECPayroll2.Dept_code;
                            queryX.Tname = employeeSTECPayroll2.Tname;
                            queryX.Fname = employeeSTECPayroll2.Fname;
                            queryX.Lname = employeeSTECPayroll2.Lname;
                            queryX.Birth = employeeSTECPayroll2.Birth;
                            queryX.Id_card = txtNewIDCard.Text;
                            queryX.Start_date = employeeSTECPayroll2.Start_date;
                            queryX.Bank_acc = employeeSTECPayroll2.Bank_acc;
                            queryX.Staff_status = employeeSTECPayroll2.Staff_status;
                            queryX.Staff_type = employeeSTECPayroll2.Staff_type;
                            queryX.Address = employeeSTECPayroll2.Address;
                            queryX.Moo = employeeSTECPayroll2.Moo;
                            queryX.Tambon = employeeSTECPayroll2.Tambon;
                            queryX.Amphur = employeeSTECPayroll2.Amphur;
                            queryX.Province = employeeSTECPayroll2.Province;
                            queryX.Zipcode = employeeSTECPayroll2.Zipcode;
                            queryX.Shift = employeeSTECPayroll2.Shift;
                            employeeSTECObj[0] = queryX;
                            _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                        }
                    }
                }

                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                
            }
        }

        private void DeleteIDCardInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                
                _hrisBusinessLayer.RemoveIDCardInfo(IDcardInfoSelectRow);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T001";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "";
                transactionLog1.PKFields = IDcardInfoSelectRow.Card_ID;
                transactionLog1.FKFields = Convert.ToString(IDcardInfoSelectRow.ExpiredDate);
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtIDCardInfoPerson_ID + "," + txtNewIDCard.Text + "," + IDCardInfoIssuedDate.Text + "," + IDCardInfoExpireDate.Text + "," + cmbIDCardIssuedAtProvince.Text + "," + cmbIDCardIssuedAtDistrict.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearIDCardInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(2);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbIDCardIssuedAtProvince_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                String XIDCardIssuedAtProvince = (String)cmbIDCardIssuedAtProvince.SelectedValue;
                cmbIDCardIssuedAtDistrict.ItemsSource = null;
                if (XIDCardIssuedAtProvince != "")
                {
                    cmbIDCardIssuedAtDistrict.ItemsSource = _hrisBusinessLayer.GetDistrictByProvince(XIDCardIssuedAtProvince);  
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       

        private void AddAddress1Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "" ||cmbAddress1NewProvince.Text == null)
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "" || cmbAddress1NewDistrict.Text == null)
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "" || cmbAddress1NewSubDistrict.Text == null)
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "" || cmbAddress1NewCountry.Text == null)
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "1";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "1";
                address1.HomeNumber = txtAddress1NewHomeNo.Text;
                address1.HousingProject = txtAddress1NewHousingProject.Text;
                address1.Street = txtAddress1NewStreet.Text;
                address1.SubDistrict_ID = (string)cmbAddress1NewSubDistrict.SelectedValue ;
                address1.Moo = string.IsNullOrEmpty(txtAddress1NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress1NewMoo.Text);
                address1.Soi = txtAddress1NewSoi.Text;
                address1.Postcode = string.IsNullOrEmpty(txtAddress1NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress1NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress1NewCountry.SelectedValue ;
                address1.HomePhoneNumber = txtAddress1HomePhoneNumber.Text;
                addressObj[0] = address1;
                _hrisBusinessLayer.AddAddress(addressObj);

                //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                 EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                 employeeSTECPayroll = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee_IDfromSearchPage);
                 if (employeeSTECPayroll != null)
                 {
                     EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                     EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                     queryX.Employee_id = employeeSTECPayroll.Employee_id;
                     queryX.Dept_code = employeeSTECPayroll.Dept_code;
                     queryX.Tname = employeeSTECPayroll.Tname;
                     queryX.Fname = employeeSTECPayroll.Fname;
                     queryX.Lname = employeeSTECPayroll.Lname;
                     queryX.Birth = employeeSTECPayroll.Birth;
                     queryX.Id_card = employeeSTECPayroll.Id_card;
                     queryX.Start_date = employeeSTECPayroll.Start_date;
                     queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                     queryX.Staff_status = employeeSTECPayroll.Staff_status;
                     queryX.Staff_type = employeeSTECPayroll.Staff_type;
                     queryX.Address = txtAddress1NewHomeNo.Text;
                     queryX.Moo = txtAddress1NewMoo.Text;
                     queryX.Tambon = (string)cmbAddress1NewSubDistrict.Text;
                     queryX.Amphur = (string)cmbAddress1NewDistrict.Text;
                     queryX.Province = (string)cmbAddress1NewProvince.Text;
                     queryX.Zipcode = txtAddress1NewPostcode.Text;
                     queryX.Shift = employeeSTECPayroll.Shift;
                     employeeSTECObj[0] = queryX;
                     _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                 }
                 else
                 {
                     //เช็คว่าถ้า person_ID นี้มีใน employee แล้วให้ update ค่าใน payroll_stec ด้วย
                     Employee employee = new Employee();
                     employee = _hrisBusinessLayer.GetAllEmployees().Where(b => b.Person_ID == txtAddressPersonID.Text).OrderByDescending(b => b.EndDate).FirstOrDefault();
                     if (employee != null)
                     {
                         //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                         EmployeeSTECPayroll employeeSTECPayroll2 = new EmployeeSTECPayroll();
                         employeeSTECPayroll2 = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee.Noted);
                         if (employeeSTECPayroll2 != null)
                         {
                             EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                             EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                             queryX.Employee_id = employeeSTECPayroll2.Employee_id;
                             queryX.Dept_code = employeeSTECPayroll2.Dept_code;
                             queryX.Tname = employeeSTECPayroll2.Tname;
                             queryX.Fname = employeeSTECPayroll2.Fname;
                             queryX.Lname = employeeSTECPayroll2.Lname;
                             queryX.Birth = employeeSTECPayroll2.Birth;
                             queryX.Id_card = employeeSTECPayroll2.Id_card;
                             queryX.Start_date = employeeSTECPayroll2.Start_date;
                             queryX.Bank_acc = employeeSTECPayroll2.Bank_acc;
                             queryX.Staff_status = employeeSTECPayroll2.Staff_status;
                             queryX.Staff_type = employeeSTECPayroll2.Staff_type;
                             queryX.Address = txtAddress1NewHomeNo.Text;
                             queryX.Moo = txtAddress1NewMoo.Text;
                             queryX.Tambon = (string)cmbAddress1NewSubDistrict.Text;
                             queryX.Amphur = (string)cmbAddress1NewDistrict.Text;
                             queryX.Province = (string)cmbAddress1NewProvince.Text;
                             queryX.Zipcode = txtAddress1NewPostcode.Text;
                             queryX.Shift = employeeSTECPayroll2.Shift;
                         
                             employeeSTECObj[0] = queryX;
                             _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                         }
                     }
                 }

               
                ClearData(3);
                AddAddress1Button.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditAddress1Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "1";
                transactionLog1.OldData = txtAddress1HomeNo.Text + " " + txtAddress1Moo.Text + " " + txtAddress1SubDistrict.Text + " " + txtAddress1District.Text + " " + txtAddress1Province.Text;
                transactionLog1.NewData = txtAddress1NewHomeNo.Text + " "+ txtAddress1NewMoo.Text + " " + cmbAddress1NewSubDistrict.Text + " "+ cmbAddress1NewDistrict.Text+ " "+ cmbAddress1NewProvince.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "1";
                address1.HomeNumber = txtAddress1NewHomeNo.Text;
                address1.HousingProject = txtAddress1NewHousingProject.Text;
                address1.Street = txtAddress1NewStreet.Text;
                address1.SubDistrict_ID = (string)cmbAddress1NewSubDistrict.SelectedValue;
                address1.Moo = string.IsNullOrEmpty(txtAddress1NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress1NewMoo.Text);
                address1.Soi = txtAddress1NewSoi.Text;
                address1.Postcode = string.IsNullOrEmpty(txtAddress1NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress1NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress1NewCountry.SelectedValue;
                address1.HomePhoneNumber = txtAddress1HomePhoneNumber.Text;
                //addressObj[0] = address1;
                _hrisBusinessLayer.UpdateAddress(address1);

                //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
               
               EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
               employeeSTECPayroll = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee_IDfromSearchPage);
               if (employeeSTECPayroll != null)
               {
                   EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                   EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                   queryX.Employee_id = employeeSTECPayroll.Employee_id;
                   queryX.Dept_code = employeeSTECPayroll.Dept_code;
                   queryX.Tname = employeeSTECPayroll.Tname;
                   queryX.Fname = employeeSTECPayroll.Fname;
                   queryX.Lname = employeeSTECPayroll.Lname;
                   queryX.Birth = employeeSTECPayroll.Birth;
                   queryX.Id_card = employeeSTECPayroll.Id_card;
                   queryX.Start_date = employeeSTECPayroll.Start_date;
                   queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                   queryX.Staff_status = employeeSTECPayroll.Staff_status;
                   queryX.Staff_type = employeeSTECPayroll.Staff_type;
                   queryX.Address = txtAddress1NewHomeNo.Text;
                   queryX.Moo = txtAddress1NewMoo.Text;
                   queryX.Tambon = (string)cmbAddress1NewSubDistrict.Text;
                   queryX.Amphur = (string)cmbAddress1NewDistrict.Text;
                   queryX.Province = (string)cmbAddress1NewProvince.Text;
                   queryX.Zipcode = txtAddress1NewPostcode.Text;
                   queryX.Shift = employeeSTECPayroll.Shift;
                   employeeSTECObj[0] = queryX;
                   _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
               }
               else
               {
                   //เช็คว่าถ้า person_ID นี้มีใน employee แล้วให้ update ค่าใน payroll_stec ด้วย
                   Employee employee = new Employee();
                   employee = _hrisBusinessLayer.GetAllEmployees().Where(b => b.Person_ID == txtAddressPersonID.Text).OrderByDescending(b => b.EndDate).FirstOrDefault();
                   if (employee != null)
                   {
                       //ถ้า person_IDfromGetViewPerson != "" แสดงว่าต้องเข้าไป update id_card ใน employee tablet ใน STEC_Payroll ด้วย
                       EmployeeSTECPayroll employeeSTECPayroll2 = new EmployeeSTECPayroll();
                       employeeSTECPayroll2 = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(employee.Noted);
                       if (employeeSTECPayroll2 != null)
                       {
                           EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                           EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                           queryX.Employee_id = employeeSTECPayroll2.Employee_id;
                           queryX.Dept_code = employeeSTECPayroll2.Dept_code;
                           queryX.Tname = employeeSTECPayroll2.Tname;
                           queryX.Fname = employeeSTECPayroll2.Fname;
                           queryX.Lname = employeeSTECPayroll2.Lname;
                           queryX.Birth = employeeSTECPayroll2.Birth;
                           queryX.Id_card = employeeSTECPayroll2.Id_card;
                           queryX.Start_date = employeeSTECPayroll2.Start_date;
                           queryX.Bank_acc = employeeSTECPayroll2.Bank_acc;
                           queryX.Staff_status = employeeSTECPayroll2.Staff_status;
                           queryX.Staff_type = employeeSTECPayroll2.Staff_type;
                           queryX.Address = txtAddress1NewHomeNo.Text;
                           queryX.Moo = txtAddress1NewMoo.Text;
                           queryX.Tambon = (string)cmbAddress1NewSubDistrict.Text;
                           queryX.Amphur = (string)cmbAddress1NewDistrict.Text;
                           queryX.Province = (string)cmbAddress1NewProvince.Text;
                           queryX.Zipcode = txtAddress1NewPostcode.Text;
                           queryX.Shift = employeeSTECPayroll2.Shift;
                           employeeSTECObj[0] = queryX;
                           _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);
                       }
                   }
               }


                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Deleteddress1Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "1";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                _hrisBusinessLayer.RemoveAddress("1",txtAddressPersonID.Text);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearAddress1Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(3);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddAddress2Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "2";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "2";
                address1.HomeNumber = txtAddress2NewHomeNo.Text;
                address1.HousingProject = txtAddress2NewHousingProject.Text;
                address1.Street = txtAddress2NewStreet.Text;
                address1.SubDistrict_ID = (string)cmbAddress2NewSubDistrict.SelectedValue;
                address1.Moo = string.IsNullOrEmpty(txtAddress2NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress2NewMoo.Text);
                address1.Soi = txtAddress2NewSoi.Text;
                address1.Postcode = string.IsNullOrEmpty(txtAddress2NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress2NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress2NewCountry.SelectedValue;
                address1.HomePhoneNumber = txtAddress2HomePhoneNumber.Text;
                addressObj[0] = address1;
                _hrisBusinessLayer.AddAddress(addressObj);

                ClearData(3);
                AddAddress2Button.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditAddress2Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress2NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress2NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress2NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress2NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress2NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "2";
                transactionLog1.OldData = txtAddress2HomeNo.Text + " " + txtAddress2Moo.Text + " " + txtAddress2SubDistrict.Text + " " + txtAddress2District.Text + " " + txtAddress2Province.Text;
                transactionLog1.NewData = txtAddress2NewHomeNo.Text + " " + txtAddress2NewMoo.Text + " " + cmbAddress2NewSubDistrict.Text + " " + cmbAddress1NewDistrict.Text + " " + cmbAddress2NewProvince.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "2";
                address1.HomeNumber = txtAddress2NewHomeNo.Text;
                address1.HousingProject = txtAddress2NewHousingProject.Text;
                address1.Street = txtAddress2NewStreet.Text;
                address1.SubDistrict_ID = (string)cmbAddress2NewSubDistrict.SelectedValue;
                address1.Moo = string.IsNullOrEmpty(txtAddress2NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress2NewMoo.Text);
                address1.Soi = txtAddress2NewSoi.Text;
                address1.Postcode = string.IsNullOrEmpty(txtAddress2NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress2NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress2NewCountry.SelectedValue;
                address1.HomePhoneNumber = txtAddress2HomePhoneNumber.Text;
                //addressObj[0] = address1;
                _hrisBusinessLayer.UpdateAddress(address1);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Deleteddress2Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "2";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                _hrisBusinessLayer.RemoveAddress("2", txtAddressPersonID.Text);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearAddress2Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(3);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

     

        private void EditAddress3Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress3NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress3NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress3NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress3NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress3NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "3";
                transactionLog1.OldData = txtAddress3HomeNo.Text + " " + txtAddress3Moo.Text + " " + txtAddress3SubDistrict.Text + " " + txtAddress3District.Text + " " + txtAddress3Province.Text;
                transactionLog1.NewData = txtAddress3NewHomeNo.Text + " " + txtAddress3NewMoo.Text + " " + cmbAddress3NewSubDistrict.Text + " " + cmbAddress3NewDistrict.Text + " " + cmbAddress3NewProvince.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "3";
                address1.HomeNumber = txtAddress3NewHomeNo.Text;
                address1.Moo = string.IsNullOrEmpty(txtAddress3NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress3NewMoo.Text);
                address1.HousingProject = txtAddress3NewHousingProject.Text;
                address1.Street = txtAddress3NewStreet.Text;
                address1.Soi = txtAddress3NewSoi.Text;
                address1.SubDistrict_ID = (string)cmbAddress3NewSubDistrict.SelectedValue;
                address1.Postcode = string.IsNullOrEmpty(txtAddress3NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress3NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress3NewCountry.SelectedValue;
                address1.HomePhoneNumber = txtAddress3HomePhoneNumber.Text;
                //addressObj[0] = address1;
                _hrisBusinessLayer.UpdateAddress(address1);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Deleteddress3Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "3";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                _hrisBusinessLayer.RemoveAddress("3", txtAddressPersonID.Text);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearAddress3Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(3);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAddress3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChkAddress3.IsChecked == true)
                {
                    txtAddress3NewHomeNo.Text = txtAddress1NewHomeNo.Text;
                    txtAddress3NewMoo.Text = txtAddress1NewMoo.Text;
                    txtAddress3NewHousingProject.Text = txtAddress1NewHousingProject.Text;
                    txtAddress3NewStreet.Text = txtAddress1NewStreet.Text;
                    txtAddress3NewSoi.Text = txtAddress1NewSoi.Text;
                    cmbAddress3NewProvince.SelectedValue = cmbAddress1NewProvince.SelectedValue;
                    cmbAddress3NewDistrict.SelectedValue = cmbAddress1NewDistrict.SelectedValue;
                    cmbAddress3NewSubDistrict.SelectedValue = cmbAddress1NewSubDistrict.SelectedValue;
                    txtAddress3NewPostcode.Text = txtAddress1NewPostcode.Text;
                    cmbAddress3NewCountry.SelectedValue = cmbAddress1NewCountry.SelectedValue;
                    txtAddress3NewHomePhoneNumber.Text = txtAddress1NewHomePhoneNumber.Text;

                }
                else
                {
                    txtAddress3NewHomeNo.Text = "";
                    txtAddress3NewMoo.Text = "";
                    txtAddress3NewHousingProject.Text = "";
                    txtAddress3NewStreet.Text = "";
                    txtAddress3NewSoi.Text = "";
                    txtAddress3NewPostcode.Text = "";
                    txtAddress3NewHomePhoneNumber.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void AddAddress3Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtAddressPersonID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAddress1NewHomeNo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเลขที่อยู่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewProvince.Text == "")
                {
                    MessageBox.Show("กรุณาระบุจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอำเภอ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewSubDistrict.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำบล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbAddress1NewCountry.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเทศ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T002";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtAddressPersonID.Text;
                transactionLog1.FKFields = "3";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                Address[] addressObj = new Address[1];
                Address address1 = new Address();
                address1.Person_ID = txtAddressPersonID.Text;
                address1.AddressType_ID = "3";
                address1.HomeNumber = txtAddress3NewHomeNo.Text;
                address1.Moo = string.IsNullOrEmpty(txtAddress3NewMoo.Text) ? (int?)null : Convert.ToInt32(txtAddress3NewMoo.Text);
                address1.HousingProject = txtAddress3NewHousingProject.Text;
                address1.Street = txtAddress3NewStreet.Text;
                address1.Soi = txtAddress3NewSoi.Text;
                address1.SubDistrict_ID = (string)cmbAddress3NewSubDistrict.SelectedValue;
                address1.Postcode = string.IsNullOrEmpty(txtAddress3NewPostcode.Text) ? (int?)null : Convert.ToInt32(txtAddress3NewPostcode.Text);
                address1.Country_ID = (string)cmbAddress3NewCountry.SelectedValue;
                address1.HomePhoneNumber = txtAddress3HomePhoneNumber.Text;
                addressObj[0] = address1;
                _hrisBusinessLayer.AddAddress(addressObj);

                ClearData(3);
                AddAddress3Button.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

     

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = personList.Where(p => p.FirstNameTH.Contains(txtSearch.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    
        private void ToEmployeePageButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Person person1 = new Person();
                //person1 = (Person)DataGrid.SelectedItem;

                PersonInformation person1 = new PersonInformation(txtPerson_ID.Text);
                this.NavigationService.Navigate(new  Employees(person1));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridIDCard_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                IDcardInfoSelectRow  = new IDCardInfo();
                IDcardInfoSelectRow = (IDCardInfo)DataGridIDCard.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (IDcardInfoSelectRow == null)
                {
                    AddIDCardInfoButton.IsEnabled = true;                 
                }
                else
                {
                    AddIDCardInfoButton.IsEnabled = false;             
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void ChkAddress2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(ChkAddress2.IsChecked == true)
                {
                    txtAddress2NewHomeNo.Text = txtAddress1NewHomeNo.Text;
                    txtAddress2NewMoo.Text = txtAddress1NewMoo.Text;
                    txtAddress2NewHousingProject.Text = txtAddress1NewHousingProject.Text;
                    txtAddress2NewStreet.Text = txtAddress1NewStreet.Text;
                    txtAddress2NewSoi.Text = txtAddress1NewSoi.Text;
                    cmbAddress2NewProvince.SelectedValue = cmbAddress1NewProvince.SelectedValue;
                    cmbAddress2NewDistrict.SelectedValue = cmbAddress1NewDistrict.SelectedValue;
                    cmbAddress2NewSubDistrict.SelectedValue = cmbAddress1NewSubDistrict.SelectedValue;    
                    txtAddress2NewPostcode.Text = txtAddress1NewPostcode.Text;
                    cmbAddress2NewCountry.SelectedValue = cmbAddress1NewCountry.SelectedValue;
                    txtAddress2NewHomePhoneNumber.Text = txtAddress1NewHomePhoneNumber.Text;

                }
                else
                {
                    txtAddress2NewHomeNo.Text = "";
                    txtAddress2NewMoo.Text = "";
                    txtAddress2NewHousingProject.Text = "";
                    txtAddress2NewStreet.Text = "";
                    txtAddress2NewSoi.Text = "";
                    txtAddress2NewPostcode.Text = "";
                    txtAddress2NewHomePhoneNumber.Text = "";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void cmbAddress1NewProvince_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbAddress1NewProvince.Text != "")
                {
                    String XAddress1Province = (String)cmbAddress1NewProvince.SelectedValue;
                    cmbAddress1NewDistrict.ItemsSource = null;
                    cmbAddress1NewDistrict.ItemsSource = _hrisBusinessLayer.GetDistrictByProvince(XAddress1Province);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbAddress1NewDistrict_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbAddress1NewDistrict.Text != "")
                {
                    String XDistrict = (String)cmbAddress1NewDistrict.SelectedValue;
                    cmbAddress1NewSubDistrict.ItemsSource = null;
                    cmbAddress1NewSubDistrict.ItemsSource = _hrisBusinessLayer.GetSubDistrictByDistrict(XDistrict);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void cmbAddress2NewProvince_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    if (cmbAddress2NewProvince.Text != "")
            //    {
            //        String XAddress2Province = (String)cmbAddress2NewProvince.SelectedValue;
            //        cmbAddress2NewDistrict.ItemsSource = null;
            //        cmbAddress2NewDistrict.ItemsSource = _hrisBusinessLayer.GetDistrictByProvince(XAddress2Province);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void cmbAddress2NewDistrict_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    if (cmbAddress2NewDistrict.Text != "")
            //    {
            //        String XSubDistrict = (String)cmbAddress2NewDistrict.SelectedValue;
            //        cmbAddress2NewSubDistrict.ItemsSource = null;
            //        cmbAddress2NewSubDistrict.ItemsSource = _hrisBusinessLayer.GetSubDistrictByDistrict(XSubDistrict);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void cmbAddress3NewProvince_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    if (cmbAddress3NewProvince.Text != "")
            //    {
            //        String XAddress3Province = (String)cmbAddress3NewProvince.SelectedValue;
            //        cmbAddress3NewDistrict.ItemsSource = null;
            //        cmbAddress3NewDistrict.ItemsSource = _hrisBusinessLayer.GetDistrictByProvince(XAddress3Province);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void cmbAddress3NewDistrict_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    String XSubDistrict = (String)cmbAddress3NewDistrict.SelectedValue;
            //    cmbAddress3NewSubDistrict.ItemsSource = null;
            //    cmbAddress3NewSubDistrict.ItemsSource = _hrisBusinessLayer.GetSubDistrictByDistrict(XSubDistrict);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SearchNameButton.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddEduButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtEduPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduLevel.Text == "")
                {
                    MessageBox.Show("กรุณาระบุระดับการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduInstituteName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduCountry.Text == "")
                {
                    MessageBox.Show("กรุณาประเทศที่ศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduFull.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชื่อเต็มวุฒิการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduMajor.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชสาขาวิชาเอก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (EduAdmissionDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่เริ่มการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(EduAdmissionDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เริ่มการศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (EduGraduationDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สำเร็จการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(EduGraduationDate.Text) <= Convert.ToDateTime(EduAdmissionDate.Text))
                {
                    MessageBox.Show("กรุณาระบุวันที่สำเร็จการศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtEduGPA.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเกรดเฉลี่ยที่ได้รับ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T017";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtEduPerson_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = (string)cmbEduLevel.SelectedValue + "-" + (string)cmbEduFull.SelectedValue + "-" + (string)cmbEduInstituteName.SelectedValue + "-" + (string)cmbEduCountry.SelectedValue + "-" + EduAdmissionDate.Text + "-" + EduGraduationDate.Text + "-" + Convert.ToDecimal(txtEduGPA.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                Education[] eduOjb = new Education[1];
                Education education1 = new Education();
                education1.EducationLevel_ID = (string)cmbEduLevel.SelectedValue;
                education1.Person_ID = txtEduPerson_ID.Text;
                education1.EducationQualification_ID = (string)cmbEduFull.SelectedValue;
                education1.Institue_ID = (string)cmbEduInstituteName.SelectedValue;
                education1.Country_ID = (string)cmbEduCountry.SelectedValue;
                education1.AdmissionDate = Convert.ToDateTime(EduAdmissionDate.Text);
                education1.GraduatedDate = Convert.ToDateTime(EduGraduationDate.Text);
                education1.GPA = Convert.ToDecimal(txtEduGPA.Text);
                education1.ModifiedDate = DateTime.Now;
                eduOjb[0] = education1;
                _hrisBusinessLayer.AddEducation(eduOjb);

                ClearData(4);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditEduButton_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (txtEduPerson_ID.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (cmbEduLevel.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุระดับการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (cmbEduInstituteName.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุสถานศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (cmbEduCountry.Text == "")
            //    {
            //        MessageBox.Show("กรุณาประเทศที่ศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (cmbEduFull.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุชื่อเต็มวุฒิการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (cmbEduMajor.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุชื่อเต็มสาขาวิชา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    if (EduAdmissionDate.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุวันที่เริ่มเข้าศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    else if (Convert.ToDateTime(EduAdmissionDate.Text) >= DateTime.Now)
            //    {
            //        MessageBox.Show("กรุณาระบุวันที่เริ่มเข้าศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }

            //    if (EduGraduationDate.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุวันที่เริ่มเข้าศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
            //    else if (Convert.ToDateTime(EduGraduationDate.Text) <= Convert.ToDateTime(EduAdmissionDate.Text))
            //    {
            //        MessageBox.Show("กรุณาระบุวันที่สำเร็จการศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }

            //    if (txtEduGPA.Text == "")
            //    {
            //        MessageBox.Show("กรุณาระบุเกรดเฉลี่ยที่ได้รับ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            //        return;
            //    }
                

            //    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
            //    if (transactionLog.Count > 0)
            //    {
            //        XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
            //    }
            //    else
            //    {
            //        XMaxTransactionLog_ID = 1;
            //    }
            //    TransactionLog[] transactionLogObj = new TransactionLog[1];
            //    TransactionLog transactionLog1 = new TransactionLog();
            //    transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
            //    transactionLog1.TransactionType_ID = "U";
            //    transactionLog1.Table_ID = "T017";
            //    transactionLog1.TransactionDate = DateTime.Now;
            //    transactionLog1.FieldName = "Person_ID";
            //    transactionLog1.PKFields = txtEduPerson_ID.Text;
            //    transactionLog1.FKFields = "";             
            //    transactionLog1.OldData = EduData.EducationLevel_ID + "-" + EduData.EducationQualification_ID + "-" + EduData.Institue_ID + "-" + EduData.Country_ID + "-" + EduData.AdmissionDate + "-" + EduData.GraduatedDate + "-" + EduData.GPA;
            //    transactionLog1.NewData = (string)cmbEduLevel.SelectedValue + "-" + (string)cmbEduFull.SelectedValue + "-" + (string)cmbEduInstituteName.SelectedValue + "-" + (string)cmbEduCountry.SelectedValue + "-" + Convert.ToDateTime(EduAdmissionDate.Text) + "-" + Convert.ToDateTime(EduGraduationDate.Text) + Convert.ToDecimal(txtEduGPA.Text);
            //    transactionLog1.ModifiedDate = DateTime.Now;
            //    transactionLog1.ModifiedUser = _singleton.username;
            //    transactionLogObj[0] = transactionLog1;
            //    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                
            //    Education education1 = new Education();
            //    education1.EducationLevel_ID = (string)cmbEduLevel.SelectedValue;
            //    education1.Person_ID = txtEduPerson_ID.Text;
            //    education1.EducationQualification_ID = (string)cmbEduFull.SelectedValue;
            //    education1.Institue_ID = (string)cmbEduInstituteName.SelectedValue;
            //    education1.Country_ID = (string)cmbEduCountry.SelectedValue;
            //    education1.AdmissionDate = Convert.ToDateTime(EduAdmissionDate.Text);
            //    education1.GraduatedDate = Convert.ToDateTime(EduGraduationDate.Text);
            //    education1.GPA = Convert.ToDecimal(txtEduGPA.Text);
            //    education1.ModifiedDate = DateTime.Now;            
            //    _hrisBusinessLayer.UpdateEducation(education1);

            //    ClearData(4);

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void DeleteEduButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtEduPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduLevel.Text == "")
                {
                    MessageBox.Show("กรุณาระบุระดับการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduInstituteName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduCountry.Text == "")
                {
                    MessageBox.Show("กรุณาประเทศที่ศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduFull.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชื่อเต็มวุฒิการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbEduMajor.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชสาขาวิชาเอก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (EduAdmissionDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่เริ่มการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(EduAdmissionDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เริ่มการศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (EduGraduationDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สำเร็จการศึกษา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(EduGraduationDate.Text) <= Convert.ToDateTime(EduAdmissionDate.Text))
                {
                    MessageBox.Show("กรุณาระบุวันที่สำเร็จการศึกษาให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtEduGPA.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเกรดเฉลี่ยที่ได้รับ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                Education EduData2 = new Education();
                EduData2 = _hrisBusinessLayer.GetEducationByID(txtEduPerson_ID.Text, (string)cmbEduLevel.SelectedValue, (string)cmbEduFull.SelectedValue);

                if (EduData2 == null)
                {
                    MessageBox.Show("กรุณาเลือกรายการจากตารางด้านขวามือ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T017";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtEduPerson_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = EduData2.EducationLevel_ID + "-" + EduData2.EducationQualification_ID + "-" + EduData2.Institue_ID + "-" + EduData2.Country_ID + "-" + EduData2.AdmissionDate + "-" + EduData2.GraduatedDate + "-" + EduData2.GPA;
                transactionLog1.NewData = (string)cmbEduLevel.SelectedValue + "-" + (string)cmbEduFull.SelectedValue + "-" + (string)cmbEduInstituteName.SelectedValue + "-" + (string)cmbEduCountry.SelectedValue + "-" + EduAdmissionDate.Text + "-" + EduGraduationDate.Text + "-" + Convert.ToDecimal(txtEduGPA.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                _hrisBusinessLayer.RemoveEducation(EduData2);

                ClearData(4);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearEduButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEduButton.IsEnabled = true;
                DataGridEdu.ItemsSource = _hrisBusinessLayer.GetEducationByPersonID(txtEduPerson_ID.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridEdu_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {               
                AddEduButton.IsEnabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddEduFullButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.RefEducationQualification("Add", "", txtEduPerson_ID.Text,""));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
      
        private void AddEduMajorButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbEduFull.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวุฒิการศึกษาก่อนทำการเลือกสาขาวิชา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                this.NavigationService.Navigate(new ReferencesTable.RefEducationMajor("Add", (string)cmbEduFull.SelectedValue, "", txtEduPerson_ID.Text, ""));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEduSearchIns_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    institueList = _hrisBusinessLayer.GetAllInstitutes().ToList();
                    institueList = institueList.Where(b => b.InstituteNameTH.Contains(txtEduSearchIns.Text)).ToList();                
                    cmbEduInstituteName.ItemsSource = institueList;        
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEduSearchFull_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    educationQualificationList = _hrisBusinessLayer.GetAllEducationQualifications().ToList();
                    educationQualificationList = educationQualificationList.Where(b => b.EducationQualificaionTH.Contains(txtEduSearchFull.Text)).ToList();
                    cmbEduFull.ItemsSource = educationQualificationList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
       
        private void txtEduSearchMajor_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    majorList = _hrisBusinessLayer.GetAllMajors().ToList();
                    majorList = majorList.Where(b => b.MajorTH.Contains(txtEduSearchFull.Text)).ToList();
                    cmbEduMajor.ItemsSource = majorList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbEduFull_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                txtEduShort.Text = "";
                if (cmbEduFull.Text != "")
                {
                    EducationQualification edu = _hrisBusinessLayer.GetEducationQualificationByID((string)cmbEduFull.SelectedValue);
                    txtEduShort.Text = edu.EducationQualificationInitialTH;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddFamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                if (txtFamPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamMemberTyepe.Text=="")
                {
                    MessageBox.Show("กรุณาระบุความเกี่ยวข้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamTitleName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุคำนำหน้าชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamFirstname2.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamLastname2.Text == "")
                {
                    MessageBox.Show("กรุณาระบุนามสกุล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (FamDateOfBirth.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(FamDateOfBirth.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิดให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
              
                if (cmbFamVitalStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                IList<FamilyMember> familyMember_ID = new List<FamilyMember>();
                familyMember_ID = _hrisBusinessLayer.GetAllFamilyMembers().ToList();
                decimal XMaxFam_ID;
                if (familyMember_ID.Count > 0)
                {
                    XMaxFam_ID = Convert.ToDecimal(familyMember_ID.Max(c => Convert.ToDecimal(c.FamilyMember_ID)) + 1);
                }
                else
                {
                    XMaxFam_ID = 1;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T018";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtFamPerson_ID.Text;
                transactionLog1.FKFields = XMaxFam_ID.ToString();
                transactionLog1.OldData = "";
                transactionLog1.NewData = (string)cmbFamMemberTyepe.SelectedValue + "-" + (string)cmbFamTitleName.SelectedValue + "-" + (string)txtFamFirstname2.Text +"-" + (string)txtFamLastname2.Text + "-" + FamDateOfBirth.Text + "-" + (string)cmbFamVitalStatus.SelectedValue;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
               
 
                FamilyMember[] famOjb = new FamilyMember[1];
                FamilyMember fam1 = new FamilyMember();
                fam1.FamilyMember_ID = Convert.ToString( XMaxFam_ID) ;
                fam1.Person_ID = txtFamPerson_ID.Text;
                fam1.FamilyMemberType_ID = (string)cmbFamMemberTyepe.SelectedValue;
                fam1.TitleName_ID = (string)cmbFamTitleName.SelectedValue;
                fam1.FirstName = txtFamFirstname2.Text;
                fam1.MidName = txtFamMidname2.Text;
                fam1.LastName = txtFamLastname2.Text;
                fam1.DateOfBirth = Convert.ToDateTime(FamDateOfBirth.Text);
                fam1.VitalStatus_ID = (string)cmbFamVitalStatus.SelectedValue;                
                famOjb[0] = fam1;
                _hrisBusinessLayer.AddFamilyMember(famOjb);

                ClearData(5);
            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditFamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txtFamPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamMemberTyepe.Text == "")
                {
                    MessageBox.Show("กรุณาระบุความเกี่ยวข้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamTitleName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุคำนำหน้าชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamFirstname2.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamLastname2.Text == "")
                {
                    MessageBox.Show("กรุณาระบุนามสกุล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (FamDateOfBirth.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(FamDateOfBirth.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิดให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbFamVitalStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                
                

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T018";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtFamPerson_ID.Text;
                transactionLog1.FKFields = famSelected.FamilyMember_ID;
                transactionLog1.OldData = famSelected.FamilyMemberType_ID + "-" + famSelected.TitleName_ID + "-" + famSelected.FirstName + "-" + famSelected.MidName + "-" + famSelected.LastName + "-" + famSelected.DateOfBirth + "-" + famSelected.VitalStatus_ID;
                transactionLog1.NewData = (string)cmbFamMemberTyepe.SelectedValue + "-" + (string)cmbFamTitleName.SelectedValue + "-" + (string)txtFamFirstname2.Text + "-"+ txtFamMidname2.Text + "-" + (string)txtFamLastname2.Text + "-" + FamDateOfBirth.Text + "-" + (string)cmbFamVitalStatus.SelectedValue;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

               
                FamilyMember fam1 = new FamilyMember();
                fam1.FamilyMember_ID = famSelected.FamilyMember_ID;
                fam1.Person_ID = txtFamPerson_ID.Text;
                fam1.FamilyMemberType_ID = (string)cmbFamMemberTyepe.SelectedValue;
                fam1.TitleName_ID = (string)cmbFamTitleName.SelectedValue;
                fam1.FirstName = txtFamFirstname2.Text;
                fam1.MidName = txtFamMidname2.Text;
                fam1.LastName = txtFamLastname2.Text;
                fam1.DateOfBirth = Convert.ToDateTime(FamDateOfBirth.Text);
                fam1.VitalStatus_ID = (string)cmbFamVitalStatus.SelectedValue;             
                _hrisBusinessLayer.UpdateFamilyMember(fam1);

                ClearData(5);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteFamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txtFamPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamMemberTyepe.Text == "")
                {
                    MessageBox.Show("กรุณาระบุความเกี่ยวข้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbFamTitleName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุคำนำหน้าชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamFirstname2.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtFamLastname2.Text == "")
                {
                    MessageBox.Show("กรุณาระบุนามสกุล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (FamDateOfBirth.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(FamDateOfBirth.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันเดือนปีเกิดให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbFamVitalStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุสถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }




                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T018";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtFamPerson_ID.Text;
                transactionLog1.FKFields = famSelected.FamilyMember_ID;
                transactionLog1.OldData = famSelected.FamilyMemberType_ID + "-" + famSelected.TitleName_ID + "-" + famSelected.FirstName + "-" + famSelected.MidName + "-" + famSelected.LastName + "-" + famSelected.DateOfBirth + "-" + famSelected.VitalStatus_ID;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

             
                _hrisBusinessLayer.RemoveFamilyMember(famSelected);

                ClearData(5);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearFamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbFamTitleName.Text = "";
                txtFamFirstname2.Text = "";
                txtFamMidname2.Text = "";
                txtFamLastname2.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddFamMemberTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.RefFamilyMemberType("Add", txtFamPerson_ID.Text)); 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridFam_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                AddFamButton.IsEnabled = false;
                famSelected = (FamilyMember)DataGridFam.SelectedItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddOriButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txtOriPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtOriCompanyName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อบริษัทฯ/สถานประกอบการเดิมที่เคยทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtOriPositionName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำแหน่งงงานเดิมที่เคยทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
               
                if (OriJoinDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่ทำงานเดิม", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriJoinDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (OriEndDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดงานที่ทำงานเดิม", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriEndDate.Text) < Convert.ToDateTime(OriJoinDate.Text))
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriEndDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                IList<OriginalWorkHistory> ori_ID = new List<OriginalWorkHistory>();
                ori_ID = _hrisBusinessLayer.GetAllOriginalWorkHistorys();
                decimal XMaxOriID;
                if (ori_ID.Count > 0)
                {
                    XMaxOriID = Convert.ToDecimal(ori_ID.Max(c => Convert.ToDecimal(c.OrginalWork_ID)) + 1);
                }
                else
                {
                    XMaxOriID = 1;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T020";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtOriPerson_ID.Text;
                transactionLog1.FKFields = XMaxOriID.ToString();
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtOriCompanyName.Text + "-" + txtOriPositionName.Text + "-" + OriJoinDate.Text + "-" + OriEndDate.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                OriginalWorkHistory[] oriOjb = new OriginalWorkHistory[1];
                OriginalWorkHistory ori1 = new OriginalWorkHistory();
                ori1.OrginalWork_ID = Convert.ToString(XMaxOriID);
                ori1.Person_ID = txtOriPerson_ID.Text;
                ori1.CompanyName = txtOriCompanyName.Text;
                ori1.PositionName = txtOriPositionName.Text;
                ori1.Noted  = txtOriNoted.Text;
                ori1.JoinDate = Convert.ToDateTime(OriJoinDate.Text);
                ori1.EndDate = Convert.ToDateTime(OriEndDate.Text);
                ori1.ModifiedDate = DateTime.Now;
                oriOjb[0] = ori1;
                _hrisBusinessLayer.AddOriginalWorkHistory(oriOjb);

                ClearData(6);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditOriButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtOriPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtOriCompanyName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อบริษัทฯ/สถานประกอบการเดิมที่เคยทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtOriPositionName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุตำแหน่งงงานเดิมที่เคยทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (OriJoinDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่ทำงานเดิม", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriJoinDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (OriEndDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดงานที่ทำงานเดิม", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriEndDate.Text) < Convert.ToDateTime(OriJoinDate.Text))
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OriEndDate.Text) >= DateTime.Now)
                {
                    MessageBox.Show("กรุณาระบุวันที่เข้าทำงานที่เดิมให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
     
                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T020";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtOriPerson_ID.Text;
                transactionLog1.FKFields = oriSelected.OrginalWork_ID;
                transactionLog1.OldData = oriSelected.OrginalWork_ID + "-" + oriSelected.CompanyName + "-" + oriSelected.PositionName + "-" + oriSelected.JoinDate + "-" + oriSelected.EndDate ;
                transactionLog1.NewData = txtOriCompanyName.Text + "-" + txtOriPositionName.Text + "-" + OriJoinDate.Text + "-" + OriEndDate.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                OriginalWorkHistory ori1 = new OriginalWorkHistory();
                ori1.OrginalWork_ID = oriSelected.OrginalWork_ID;
                ori1.Person_ID = txtOriPerson_ID.Text;
                ori1.CompanyName = txtOriCompanyName.Text;
                ori1.PositionName = txtOriPositionName.Text;
                ori1.JoinDate = Convert.ToDateTime(OriJoinDate.Text);
                ori1.EndDate = Convert.ToDateTime(OriEndDate.Text);
                ori1.Noted = txtOriNoted.Text;
                ori1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateOriginalWorkHistory(ori1);

                ClearData(6);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeletOriButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtOriPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtOriCompanyName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อบริษัทฯ/สถานประกอบการเดิมที่เคยทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }              

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T020";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtOriPerson_ID.Text;
                transactionLog1.FKFields = oriSelected.OrginalWork_ID;
                transactionLog1.OldData = oriSelected.OrginalWork_ID + "-" + oriSelected.CompanyName + "-" + oriSelected.PositionName + "-" + oriSelected.JoinDate + "-" + oriSelected.EndDate;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                _hrisBusinessLayer.RemoveOriginalWorkHistory(oriSelected);

                ClearData(6);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearOriButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtOriCompanyName.Text = "";
                txtOriPositionName.Text = "";
                txtOriNoted.Text = "";
                OriJoinDate.Text = Convert.ToString(DateTime.Now);
                OriEndDate.Text = Convert.ToString(DateTime.Now);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridOri_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                AddOriButton.IsEnabled = false;
                oriSelected = (OriginalWorkHistory)DataGridOri.SelectedItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditConButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtConLPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConFirstName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConLastName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุนามสกุล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConRelation.Text == "")
                {
                    MessageBox.Show("กรุณาระบุความสัมพันธ์", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtConOccupation.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอาชีพ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConMobilePhone.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเบอร์โทรศัพท์มือถือ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T021";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtConLPerson_ID.Text;
                transactionLog1.FKFields = conSelected.ContactPersonInfo_ID;
                transactionLog1.OldData =conSelected.ContactPersonInfo_ID + "-" + conSelected.TitleName.TitleNameTH + "-" + conSelected.FirstName + "-" + conSelected.LastName + "-" + conSelected.Occupation + "-" + conSelected.CompanyName + "-" + conSelected.MobilePhone;
                transactionLog1.NewData = conSelected.ContactPersonInfo_ID + "-" + (string)cmbConTitleName.SelectedValue+ "-" + txtConFirstName.Text + "-" + txtConLastName.Text + "-" + txtConOccupation.Text + "-" + txtConCompanyName.Text + "-" + txtConMobilePhone.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                ContactPersonInfo ori1 = new ContactPersonInfo();
                ori1.ContactPersonInfo_ID = conSelected.ContactPersonInfo_ID;
                ori1.Person_ID = txtConLPerson_ID.Text;
                ori1.Relation = txtConRelation.Text;
                ori1.Title_ID = (string)cmbConTitleName.SelectedValue;
                ori1.FirstName = txtConFirstName.Text;
                ori1.MidName =  txtConMidName.Text;
                ori1.LastName = txtConLastName.Text;
                ori1.Occupation = txtConOccupation.Text;
                ori1.CompanyName = txtConCompanyName.Text;
                ori1.PositionName = txtConPositionName.Text;
                ori1.HomePhone = txtConHomePhone.Text;
                ori1.MobilePhone = txtConMobilePhone.Text;
                ori1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateContactPersonInfo(ori1);

                ClearData(7);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeletConButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtConLPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConFirstName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T021";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtConLPerson_ID.Text;
                transactionLog1.FKFields = conSelected.ContactPersonInfo_ID;
                transactionLog1.OldData = conSelected.ContactPersonInfo_ID + "-" + conSelected.TitleName.TitleNameTH + "-" + conSelected.FirstName + "-" + conSelected.LastName + "-" + conSelected.Occupation + "-" + conSelected.CompanyName;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                _hrisBusinessLayer.RemoveContactPersonInfo(conSelected);

                ClearData(7);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearConButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbConTitleName.Text = "";
                txtConFirstName.Text = "";
                txtConLastName.Text = "";
                txtConMidName.Text = "";
                txtConRelation.Text = "";
                txtConOccupation.Text = "";
                txtConCompanyName.Text = "";
                txtConPositionName.Text = "";
                txtConHomePhone.Text = "";
                txtConMobilePhone.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddConButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtConLPerson_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConFirstName.Text == "")
                {
                    MessageBox.Show("กรุณาชื่อ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConLastName.Text == "")
                {
                    MessageBox.Show("กรุณาระบุนามสกุล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConRelation.Text == "")
                {
                    MessageBox.Show("กรุณาระบุความสัมพันธ์", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtConOccupation.Text == "")
                {
                    MessageBox.Show("กรุณาระบุอาชีพ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtConMobilePhone.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเบอร์โทรศัพท์มือถือ เบอร์โทรศัพท์ต้องไม่มีขีดกลาง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                IList<ContactPersonInfo> con_ID = new List<ContactPersonInfo>();
                con_ID = _hrisBusinessLayer.GetAllContactpersonInfos();
                decimal XMaxID;
                if (con_ID.Count > 0)
                {
                    XMaxID = Convert.ToDecimal(con_ID.Max(c => Convert.ToDecimal(c.ContactPersonInfo_ID)) + 1);
                }
                else
                {
                    XMaxID = 1;
                }

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T021";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Person_ID";
                transactionLog1.PKFields = txtOriPerson_ID.Text;
                transactionLog1.FKFields = XMaxID.ToString();
                transactionLog1.OldData = "";
                transactionLog1.NewData = XMaxID + "-" + (string)cmbConTitleName.SelectedValue + "-" + txtConFirstName.Text + "-" + txtConLastName.Text + "-" + txtConOccupation.Text + "-" + txtConCompanyName.Text + "-" + txtConMobilePhone.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ContactPersonInfo[] conOjb = new ContactPersonInfo[1];
                ContactPersonInfo ori1 = new ContactPersonInfo();
                ori1.ContactPersonInfo_ID = Convert.ToString(XMaxID);
                ori1.Person_ID = txtConLPerson_ID.Text;
                ori1.Relation = txtConRelation.Text;
                ori1.Title_ID = (string)cmbConTitleName.SelectedValue;
                ori1.FirstName = txtConFirstName.Text;
                ori1.MidName = txtConMidName.Text;
                ori1.LastName = txtConLastName.Text;
                ori1.Occupation = txtConOccupation.Text;
                ori1.CompanyName = txtConCompanyName.Text;
                ori1.PositionName = txtConPositionName.Text;
                ori1.HomePhone = txtConHomePhone.Text;
                ori1.MobilePhone = txtConMobilePhone.Text;
                ori1.ModifiedDate = DateTime.Now;
                conOjb[0] = ori1;
                _hrisBusinessLayer.AddContactPersonInfo(conOjb);

                ClearData(7);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridCon_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                AddConButton.IsEnabled = false;
                conSelected = (ContactPersonInfo)DataGridCon.SelectedItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
      
    }
}
