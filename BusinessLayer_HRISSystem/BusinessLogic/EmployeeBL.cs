﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;
using DataAccessLayer_HRISSystem;
using DataAccessLayer_HRISSystem.Payroll;
using static BusinessLayer_HRISSystem.BusinessLogic.EmployeeBL;
using BusinessLayer_HRISSystem.BusinessObject;

namespace BusinessLayer_HRISSystem.BusinessLogic
{
    public interface IEmployeeBL
    {
        List<PrintSlipViewModel> GetEmployee(Func<PrintSlipViewModel, bool> predicate);
        List<sp_GetPayRollForPrintSlip_Result> GetPaySlip(string payrollLot, string departmentCode);
        List<PNDViewModel> getPND_by_PayrollLot(string PayrollLot);
        void AddNew(Employee emp);
        void Update(Employee emp);
        void Delete(string empId);
    }

    public class EmployeeBL : IEmployeeBL
    {
        public void AddNew(Employee emp)
        {
            throw new NotImplementedException();
        }

        public void Delete(string empId)
        {
            throw new NotImplementedException();
        }

        public void Update(Employee emp)
        {
            throw new NotImplementedException();
        }

        public List<PrintSlipViewModel> GetEmployee(Func<PrintSlipViewModel, bool> predicate)
        {
            //throw new NotImplementedException();
           
            var qryHRIS = new List<HRISProfile>();
            var qryPayroll = new List<PayrollProfile>();
            using (HRISSystemEntities hrcontext = new HRISSystemEntities())
            {
                var qrystrHRIS = "SELECT		T.TitleNameTH, per.FirstNameTH, per.LastNameTH, " +
                                 "              empHR.Noted [Noted],empHR.Employee_ID " +
                                 "FROM		    HRISSystem.Person.Person per " +
                                 "INNER JOIN	HRISSystem.HumanResource.Employee empHR " +
                                 "			    ON per.Person_ID = empHR.Person_ID " +
                                 "LEFT JOIN	    HRISSystem.Person.TitleName T " +
                                 "			    ON per.TitleName_ID = T.TitleName_ID ";
                qryHRIS = hrcontext.Database.SqlQuery<HRISProfile>(qrystrHRIS).ToList();
            }
            using (STEC_PayrollEdmx payRollContext = new STEC_PayrollEdmx())
            {
                var qrystrPayroll = "SELECT		    dc.Dept_name_thai, p.Payroll_date, emp.Employee_id,  " +
                                    "			    dc.Dept_name, dc.Dept_code, p.Staff_type " +
                                    "FROM		    Payroll p " +
                                    "INNER JOIN	    Dept_code dc " +
                                    "			    ON p.Dept_code = dc.Dept_code " +
                                    "INNER JOIN	    Employee emp " +
                                    "			    ON p.Employee_id = emp.Employee_id ";
                qryPayroll = payRollContext.Database.SqlQuery<PayrollProfile>(qrystrPayroll).ToList();
            }
            var selectEmp = qryHRIS.Join(qryPayroll, a => a.Noted, b => b.Employee_id, (a, b) => new { a, b })
              .Select(s => new PrintSlipViewModel
              {
                  EmployeeIdPayroll = s.a.Noted,
                  EmployeeIdHRIS = s.a.Employee_ID,
                  TitleNameTH = s.a.TitleNameTH,
                  FirstNameTH = s.a.FirstNameTH,
                  LastNameTH = s.a.LastNameTH,
                  Payroll_date =s.b.Payroll_date,
                  DepartmentCode = s.b.Dept_code,
                  DepartmentName = s.b.Dept_name_thai,
                  Staff_type = s.b.Staff_type,
                  Staff_type_Name = s.b.Staff_type == "1" ? "พนักงานประจำ" : (s.b.Staff_type == "2" ? "รายวันประจำ" : "รายวัน")
              }).ToList();
            var result = selectEmp.Where(predicate).ToList();
            return result;
        }

        public List<PNDViewModel> getPND_by_PayrollLot(string PayrollLot)
        {
            List<PNDViewModel> result = new List<PNDViewModel>();
            using (STEC_PayrollEdmx payRollContext = new STEC_PayrollEdmx())
            {
                int _pndIndex = 0;
                var payrollList = payRollContext.Payrolls.Where(w => w.Payroll_date == PayrollLot &&
                                                                    (w.Tax != null || w.Tax_Labour != null)).ToList()
                                                                                                            .OrderBy(o => o.Staff_type)
                                                                                                            .ThenBy(t => t.Dept_code).ToList();
                if (!payrollList.Any()) return null;
                else
                {
                    foreach (var item in payrollList)
                    {
                        _pndIndex++;
                        result.Add(new PNDViewModel
                        {
                            pndIndex = _pndIndex,
                            taxType = item.Staff_type == "1" ? "1" : "2",
                            citizenID = item.Id_card,
                            titleName = item.Tname,
                            employeeName = item.Fname + " " + item.Lname,
                            paidDate = item.Salary_paid_date,
                            actualSalary = item.Staff_type == "1" ? item.Total_Income : item.Total_Income_Labour,
                            incomeTax = item.Staff_type == "1" ? item.Tax : item.Tax_Labour,
                            revenueCondition = 1
                        });
                    }
                
                    return result;
                }
                
            }
        }

        public List<sp_GetPayRollForPrintSlip_Result> GetPaySlip(string payrollLot, string departmentCode)
        {
            using (HRISSystemEntities db = new HRISSystemEntities())
            {
                return db.sp_GetPayRollForPrintSlip(payrollLot, departmentCode).ToList();
            }            
        }
        public class HRISProfile
        {
            public string Noted { get; set; }
            public string Employee_ID { get; set; }
            public string TitleNameTH { get; set; }
            public string FirstNameTH { get; set; }
            public string LastNameTH { get; set; }
        }
        public class PayrollProfile
        {
            public string Employee_id { get; set; }
            public string Dept_name_thai { get; set; }
            public string Payroll_date { get; set; }
            public string Dept_name { get; set; }
            public string Dept_code { get; set; }
            public string Staff_type { get; set; }

        }
    }
}
