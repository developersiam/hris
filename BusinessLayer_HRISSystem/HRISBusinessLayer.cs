﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;
using DataAccessLayer_HRISSystem;

namespace BusinessLayer_HRISSystem
{
    public class HRISBusinessLayer:IHRISBusinessLayer
    {
        private readonly HRISDataRepository.ISSOHospitalListRepository _ssoHospitalListRepository;
        private readonly HRISDataRepository.ISSOHospitalRepository _ssoHospitalRepository;
        private readonly HRISDataRepository.ITrainingCourseRepository _trainingCourseRepository;
        private readonly HRISDataRepository.ITrainingRepository _trainingRepository;
        private readonly HRISDataRepository.IContactPersonInfoRepository _contactPersonInfoRepository;
        private readonly HRISDataRepository.IOriginalWorkHistoryRepository _originalWorkHistoryRepository;
        private readonly HRISDataRepository.IPayrollFlagRepository _payrollFlagRepository;
        private readonly HRISDataRepository.IAddressTypeRepository _addressTypeRepository;
        private readonly HRISDataRepository.IAddressRepository _addressRepository;
        private readonly HRISDataRepository.IBloodRepository _bloodRepository;
        private readonly HRISDataRepository.ICropSetupRepository _cropSetupRepository;
        private readonly HRISDataRepository.IHolidayRepository _holidayRepository;
        private readonly HRISDataRepository.ILeaveTypeRepository _leaveTypeRepository;
        private readonly HRISDataRepository.IShiftRepository _shiftRepository;
        private readonly HRISDataRepository.IBranchRepository _branchRepository;
        private readonly HRISDataRepository.IJobTitleRepository _jobTitleRepository;
        private readonly HRISDataRepository.IOrganizationRepository _organizationRepository;
        private readonly HRISDataRepository.IOrganizationTypeRepository _organizationTypeRepository;
        private readonly HRISDataRepository.IorganizationUnitRepository _organizationUnitRepository;
        private readonly HRISDataRepository.IPositionRepository _positionRepository;
        private readonly HRISDataRepository.IPositionOrgnizationRepository _positionOrganizationRepository;
        private readonly HRISDataRepository.IPositionTypeRepository _positionTypeRepository;
        private readonly HRISDataRepository.IPersonRepository _personRepository;
        private readonly HRISDataRepository.IEmployeeRepository _employeeRepository;
        private readonly HRISDataRepository.IGenderRepository _genderRepository;
        private readonly HRISDataRepository.ITitleNameRepository _titleNameRepository;
        private readonly HRISDataRepository.IEmployeePositionRepository _employeePositionRepository;
        private readonly HRISDataRepository.IShiftControlRepository _shiftControlRepository;
        private readonly HRISDataRepository.ILeaveRepository _leaveRepository;
        private readonly HRISDataRepository.IWorkUpCountryRepository _workUpCountryRepository;
        private readonly HRISDataRepository.IOTRepository _otRepository;
        //private readonly HRISDataRepository.ILeaveTypeBenefitsRepository _leaveTypeBenefitsRepository;
        private readonly HRISDataRepository.IIDCardInfoRepository _idCardRepository;
        private readonly HRISDataRepository.IProvinceRepository _provinceRepository;
        private readonly HRISDataRepository.IDistrictRepository _districtRepository;
        private readonly HRISDataRepository.ISubDistrictRepository _subDistrictRepository;
        private readonly HRISDataRepository.ICountryRepository _countryRepository;
        private readonly HRISDataRepository.IManualWorkDateTypeRepository _manualWorkDateTypeRepository;
        private readonly HRISDataRepository.IManualWorkDateRepository _manualWorkDateRepository;
        private readonly HRISDataRepository.ITransactionLogRepository _transactionLogRepository;
        private readonly HRISDataRepository.ITablesRepository _tableRepository;
        private readonly HRISDataRepository.IMaritalStatusRepository _maritalStatusRepository;
        private readonly HRISDataRepository.INationalityRepository _nationalityRepository;
        private readonly HRISDataRepository.IRaceRepository _raceRepository;
        private readonly HRISDataRepository.ReligionRepository _religionRepository;
        private readonly HRISDataRepository.EducationRepository _educationRepository;
        private readonly HRISDataRepository.EducationLevelRepository _educationLevelRepository;
        private readonly HRISDataRepository.EducationQualificationRepository _educationQualificationRepository;
        private readonly HRISDataRepository.InstituteRepository _instituteRepository;
        private readonly HRISDataRepository.IMajorRepository _majorRepository;
        private readonly HRISDataRepository.IVitalStatusRepository _vitalStatusRepository;
        private readonly HRISDataRepository.IFamilyMemberTypeReposotory _familyMemberTypeRepository;
        private readonly HRISDataRepository.IFamilyMemberRepository _familyMemberRepository;
        private readonly HRISDataRepository.IOTOnlineRepository _otOnlineRepository;
        private readonly HRISDataRepository.IOTOnlineDetailRepository _otOnlineDetailRepository;
      
        public HRISBusinessLayer() //for set instant 
        {
            _ssoHospitalListRepository = new HRISDataRepository.SSOHospitalListRepository();
            _ssoHospitalRepository = new HRISDataRepository.SSOHospitalRepository();
            _trainingCourseRepository = new HRISDataRepository.TrainingCourseRepository();
            _trainingRepository = new HRISDataRepository.TrainingRepository();
            _contactPersonInfoRepository = new HRISDataRepository.ContactPersonInfoRepository();
            _originalWorkHistoryRepository = new HRISDataRepository.OriginalWorkHistoryRepository();
            _payrollFlagRepository = new HRISDataRepository.PayrollFlagRepository();
            _addressTypeRepository = new HRISDataRepository.AddressTypeRepository();
            _addressRepository = new HRISDataRepository.AddressRepository();
            _bloodRepository = new HRISDataRepository.BloodRepository();
            _cropSetupRepository = new HRISDataRepository.CropSetupRepository();
            _holidayRepository = new HRISDataRepository.HolidayRepository();
            _leaveTypeRepository = new HRISDataRepository.LeaveTypeRepository();
            _shiftRepository = new HRISDataRepository.ShiftRepository();
            _branchRepository = new HRISDataRepository.BranchRepository();
            _jobTitleRepository = new HRISDataRepository.JobTitleRepository();
            _organizationTypeRepository = new HRISDataRepository.OrganizationTypeRepository();
            _organizationUnitRepository = new HRISDataRepository.OrganizationUnitRepository();
            _organizationRepository = new HRISDataRepository.OrganizationRepository();
            _positionRepository = new HRISDataRepository.PositionRepository();
            _positionOrganizationRepository = new HRISDataRepository.PositionOrganizationRepository();
            _positionTypeRepository = new HRISDataRepository.PositionTypeRepository();
            _personRepository = new HRISDataRepository.PersonRepository();
            _employeeRepository = new HRISDataRepository.EmployeeRepository();
            _genderRepository = new HRISDataRepository.GenderRepository();
            _titleNameRepository = new HRISDataRepository.TitleNameRepository();
            _employeePositionRepository = new HRISDataRepository.EmployeePositionRepository();
            _shiftControlRepository = new HRISDataRepository.ShiftControlRepository();
            _leaveRepository = new HRISDataRepository.LeaveRepository();
            _workUpCountryRepository = new HRISDataRepository.WorkUpCountryRepository();
            _otRepository = new HRISDataRepository.OTRepository();
            //_leaveTypeBenefitsRepository = new HRISDataRepository.LeaveTypeBenefitsRepository();
            _idCardRepository = new HRISDataRepository.IDCardRepository();
            _provinceRepository = new HRISDataRepository.ProvinceRepository();
            _districtRepository = new HRISDataRepository.DistrictRepository();
            _subDistrictRepository = new HRISDataRepository.SubDistrictRepository();
            _countryRepository = new HRISDataRepository.CountryRepository();
            _manualWorkDateTypeRepository = new HRISDataRepository.ManualWorkDateTypeRepository();
            _manualWorkDateRepository = new HRISDataRepository.ManualWorkDateRepository();
            _transactionLogRepository = new HRISDataRepository.TransactionLogRepository();
            _tableRepository = new HRISDataRepository.TableRepository();
            _maritalStatusRepository = new HRISDataRepository.MaritalStatusRepository();
            _nationalityRepository = new HRISDataRepository.NationalityRepository();
            _raceRepository = new HRISDataRepository.RaceRepository();
            _religionRepository = new HRISDataRepository.ReligionRepository();
            _educationRepository = new HRISDataRepository.EducationRepository();
            _educationLevelRepository = new HRISDataRepository.EducationLevelRepository();
            _educationQualificationRepository = new HRISDataRepository.EducationQualificationRepository();
            _instituteRepository = new HRISDataRepository.InstituteRepository();
            _majorRepository = new HRISDataRepository.MajorRepository();
            _vitalStatusRepository = new HRISDataRepository.VitalStatusRepository();
            _familyMemberTypeRepository = new HRISDataRepository.FamilyMemberTypeRepository();
            _familyMemberRepository = new HRISDataRepository.FamilyMemberRepository();
            _otOnlineRepository = new HRISDataRepository.OTOnlineRepository();
            _otOnlineDetailRepository = new HRISDataRepository.OTOnlineDetailRepository();
            
        }

      
        public void AddAddressType(params AddressType[] addressTypes)
        {
            try
            {
                if (addressTypes.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
       
                IList<AddressType> query1 = GetAllAddressTypes();
                foreach (AddressType c in addressTypes)
                {
                    //check if exits.
                    if (query1.Where(e => e.AddressType_ID == c.AddressType_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate AddressType ID");
                    }


                    if (c.AddressTypeName == null)
                        throw new ArgumentException("Please input Address type name");

                }
                
                _addressTypeRepository.Add(addressTypes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        public void AddBranch(params Branch[] branch)
        {
            try
            {
                if (branch.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<Branch> query1 = GetAllBranchs();
                foreach (Branch c in branch)
                {
                    //check if exits.
                    if (query1.Where(e => e.Branch_ID == c.Branch_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate Value");
                    }


                    if (c.Branch_ID == null || c.Branch_ID =="")
                        throw new ArgumentException("Please input branch ID");
                    if(c.BranchTH == null || c.BranchTH == "")
                        throw new ArgumentException("Please input branch name");
                }

                _branchRepository.Add(branch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddCropSetup(params CropSetup[] cropSetup)
        {
            try
            {
                if (cropSetup.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                IList<CropSetup> query1 = GetAllCropSetups();
                foreach (CropSetup c in cropSetup)
                {
                    //check if exits.
                    if (query1.Where(e => e.Crop == c.Crop).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate Crop");
                    }

                    //check if crop null.
                    //if (c.Crop == null)
                    //{
                    //    throw new ArgumentException("กรุณาระบุ Crop");
                    //}

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _cropSetupRepository.Add(cropSetup);
            }
            catch(Exception ex)
            {
                throw ex;
            }                
        }

      
        public void AddHoliday( params Holiday[] holiday)
        {
            try
            {
                if (holiday.Count() < 1)               
                    throw new ArgumentException("Please input datas");

               
                foreach (Holiday c in holiday)
                {
                    if (GetHoliday(c.Date) != null)
                    {
                        throw new ArgumentException("Duplicate value");
                    }
                    if (c.Date == null)
                        throw new ArgumentException("Please input Date");
                    if(c.HolidayNameTH == null || c.HolidayNameTH =="")
                        throw new ArgumentException("Please input name(TH)");

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");
                }
                _holidayRepository.Add(holiday);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddJobTitle(params JobTitle[] jobTitle)
        {
            try
            {
                if (jobTitle.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                foreach (JobTitle c in jobTitle)
                {
                    if (c.JobTitle_ID == null || c.JobTitleName == "")
                        throw new ArgumentException("Please input jobtitle ID");
                    if (GetJobTitle(c.JobTitle_ID) != null)
                        throw new ArgumentException("Duplicate value");
                    if (c.JobTitleName == null || c.JobTitleName =="")
                        throw new ArgumentException("Please input title name");

                }

                //JobTitle query1 = GetJobTitle(jobTitle.);
                //if (query1 == null)
                //    throw new ArgumentException("Not value");
                //JobTitle query1 = GetJobTitle(jobTitle.);
                //foreach (JobTitle c in jobTitle)
                //{
                //    //check if exits.
                //    if (query1.Where(e => e.Branch_ID == c.Branch_ID).Count() > 0)
                //    {
                //        throw new ArgumentException("Duplicate Value");
                //    }


                //    if (c.Branch_ID == null)
                //        throw new ArgumentException("Please input name");

                //}

                _jobTitleRepository.Add(jobTitle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddLeaveType(params LeaveType[] leaveType)
        {
            try
            {
                if (leaveType.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
              
                IList<LeaveType> query1 = GetAllLeaveTypes();
                foreach (LeaveType c in leaveType)
                {
                    //check if exits.
                    if (query1.Where(e => e.LeaveType_ID == c.LeaveType_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate leaveType ID");
                    }

                    //check if crop null.
                    //if (c.Crop == null)
                    //{
                    //    throw new ArgumentException("กรุณาระบุ Crop");
                    //}
                    if(c.LeaveType_ID == null || c.LeaveType_ID =="")
                        throw new ArgumentException("Please input LeaveType ID");
                    if (c.LeaveTypeNameTH == null || c.LeaveTypeNameTH =="")
                        throw new ArgumentException("Please input LeaveTypeName(thai)");
                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");
                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _leaveTypeRepository.Add(leaveType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddOrganization(params Organization[] organization)
        {
            try
            {
                if (organization.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                IList<Organization> query1 = GetAllOrganizations();
                foreach (Organization c in organization)
                {
                    if (c.Organization_ID==null || c.Organization_ID == "")
                    {
                        throw new ArgumentException("Please input organization ID");
                    }
                    //check if exits.
                    if (query1.Where(e => e.Organization_ID == c.Organization_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate organization ID");
                    }
                    if (c.Branch_ID == null || c.Branch_ID =="")
                        throw new ArgumentException("Please input branch");
                    if(c.ParentOrganizationNoted_ID == null || c.ParentOrganizationNoted_ID =="")
                        throw new ArgumentException("Please input parent Organization");
                    if (c.ParentOrganizationBusinessNoted_ID == null || c.ParentOrganizationBusinessNoted_ID == "")
                        throw new ArgumentException("Please input parent Organization business");
                    if (c.STECOrganizationNote_ID == null || c.STECOrganizationNote_ID == "")
                        throw new ArgumentException("Please input STEC Organization");
                    if (c.OrganizationType_ID == null || c.OrganizationType_ID =="")
                        throw new ArgumentException("Please input Organization type ID");
                    if (c.OrganizationUnit_ID == null || c.OrganizationUnit_ID == "")
                        throw new ArgumentException("Please input Organization unit ID");

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _organizationRepository.Add(organization);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddOrganizationType(params OrganizationType[] organizationType)
        {
            try
            {
                if (organizationType.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }

                IList<OrganizationType> query1 = GetAllOrganizationTypes();
                foreach (OrganizationType c in organizationType)
                {
                    //check if exits.
                    if (query1.Where(e => e.OrganizationType_ID == c.OrganizationType_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate organization type ID");
                    }
                  
                    if (c.OrganizationType_ID == null || c.OrganizationType_ID =="")
                        throw new ArgumentException("Please input organization type ID");
                    if(c.OrganizationTypeName == null || c.OrganizationTypeName =="")
                        throw new ArgumentException("Please input organization type name");

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _organizationTypeRepository.Add(organizationType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddOrganizationUnit(params OrganizationUnit[] organizationUnit)
        {
            try
            {
                if (organizationUnit.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }

                IList<OrganizationUnit> query1 = GetAllOrganizationUnits();
                foreach (OrganizationUnit c in organizationUnit)
                {
                    //check if exits.
                    if (query1.Where(e => e.OrganizationUnit_ID == c.OrganizationUnit_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate organization unit ID");
                    }

                    if (c.OrganizationUnit_ID == null || c.OrganizationUnit_ID =="")
                        throw new ArgumentException("Please input organization unit ID");
                    if(c.OrganizationUnitName =="" || c.OrganizationUnitName =="")
                        throw new ArgumentException("Please input organization unit name");

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _organizationUnitRepository.Add(organizationUnit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddPosition(params Position[] position)
        {
            try
            {
                if (position.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }

                IList<Position> query1 = GetAllPositions();
                foreach (Position c in position)
                {
                    if (c.Position_ID == null || c.Position_ID == "")
                        throw new ArgumentException("Please input position ID");
                    //check if exits.
                    if (query1.Where(e => e.Position_ID == c.Position_ID).Count() > 0)                    
                        throw new ArgumentException("Duplicate position");
                    if(c.PositionNameTH == null || c.PositionNameTH == "")
                        throw new ArgumentException("Please input position name (TH)");
                    //if (c.PositionNameEN == null || c.PositionNameEN == "")
                    //    throw new ArgumentException("Please input position name (EN)");
                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");
                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _positionRepository.Add(position);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddPositionType(params PositionType[] positionType)
        {
            try
            {
                if (positionType.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }

                IList<PositionType> query1 = GetAllPositionTypes();
                foreach (PositionType c in positionType)
                {
                    //check if exits.
                    if (query1.Where(e => e.PositionType_ID == c.PositionType_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate position type ID");
                    }

                    if (c.PositionType_ID == null || c.PositionType_ID == "")
                        throw new ArgumentException("Please input position type ID");
                    if (c.PositionTypeNameTH == null || c.PositionTypeNameTH == "")
                        throw new ArgumentException("Please input position type name");
                    if (c.SalaryFlag == null )
                        throw new ArgumentException("Please input payroll cal");
                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _positionTypeRepository.Add(positionType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddShift(params Shift[] shift)
        {
            try
            {
                if (shift.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<Shift> query1 = GetAllShifts();
                foreach (Shift c in shift)
                {
                    //check if exits.
                    if (query1.Where(e => e.Shift_ID == c.Shift_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate Shift ID");
                    }
                    if(c.Shift_ID==null || c.Shift_ID =="")
                        throw new ArgumentException("Please input shift ID");

                    if (c.StartTime == null)
                        throw new ArgumentException("Please input Start of time");

                    if (c.EndTime == null)
                        throw new ArgumentException("Please input End of time");

                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                
                _shiftRepository.Add(shift);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AddressType GetAddressTypeByName(string AddressTypeName)
        {
            throw new NotImplementedException();
        }

        public IList<AddressType> GetAllAddressTypes()
        {
            return _addressTypeRepository.GetAll();
        }

        public IList<Blood> GetAllBloods()
        {
            return _bloodRepository.GetAll();
        }

        public IList<Branch> GetAllBranchs()
        {
            return _branchRepository.GetAll();
        }

        public IList<CropSetup> GetAllCropSetups()
        {
            return _cropSetupRepository.GetAll(e=> e.Holidays);
        }

        public IList<LeaveType> GetAllLeaveTypes()
        {
            return _leaveTypeRepository.GetAll();
        }

        public IList<Organization> GetAllOrganizations()
        {
            return _organizationRepository.GetAll(e=> e.OrganizationUnit,e=> e.OrganizationType,e=>e.Branch, e => e.OrganizationUnit);
        }

        public IList< Organization> GetAllDepartments()
        {         
            return _organizationRepository.GetAll(e=>e.OrganizationUnit,e=>e.Organization1,e=>e.OrganizationType).OrderBy(e=>e.STECOrganizationNote_ID).ToList();
        }

        //public IList< Organization> GetSection()
        //{
        //    return _organizationRepository.GetList(e=>e.OrganizationType_ID.Equals("2"),e=>e.Organization1,e=>e.OrganizationUnit,e=>e.OrganizationType);
        //}

        public IList<OrganizationType> GetAllOrganizationTypes()
        {
            return _organizationTypeRepository.GetAll();
        }

        public IList<OrganizationUnit> GetAllOrganizationUnits()
        {
            return _organizationUnitRepository.GetAll(e=>e.Organizations);
        }

        public IList<Position> GetAllPositions()
        {
            return _positionRepository.GetAll(e=> e.PositionOrganizations);
        }

        public IList<PositionType> GetAllPositionTypes()
        {
            return _positionTypeRepository.GetAll();
        }

        public IList<Shift> GetAllShifts()
        {
            return _shiftRepository.GetAll();
        }

        public Branch GetBranch(string branch)
        {
            return _branchRepository.GetSingle(e => e.Branch_ID == branch, e => e.Organizations);
        }

        public CropSetup GetCropSetup(int crop)
        {
            return _cropSetupRepository.GetSingle(e => e.Crop == crop, e => e.Holidays);   
            
        }

        public Holiday GetHoliday(DateTime HolidayDate)
        {
            return _holidayRepository.GetSingle(e => e.Date.Day == HolidayDate.Day && e.Date.Month == HolidayDate.Month && e.Date.Year == HolidayDate.Year );
        }

        public IList<Holiday> GetHolidayByCrop(int crop)
        {
            return _holidayRepository.GetList(e => e.Crop.Equals(crop));
        }

        public JobTitle GetJobTitle(string JobTitle)
        {
            return _jobTitleRepository.GetSingle(e => e.JobTitle_ID == JobTitle, e => e.PositionOrganizations);
        }

        public LeaveType GetLeaveType(string leaveType)
        {
            return _leaveTypeRepository.GetSingle(e => e.LeaveType_ID == leaveType, e => e.Leaves);
        }

        

        public OrganizationType GetOrganizationType(string organizationType)
        {
            return _organizationTypeRepository.GetSingle(e => e.OrganizationType_ID == organizationType);
        }

        public OrganizationUnit GetOrganizationUnit(string organizationUnit)
        {
            return _organizationUnitRepository.GetSingle(e => e.OrganizationUnit_ID == organizationUnit);
        }

        public Position GetPosition(string position)
        {
            return _positionRepository.GetSingle(e => e.Position_ID == position, e => e.PositionOrganizations);
        }

        public PositionType GetPositionType(string positionType)
        {
            return _positionTypeRepository.GetSingle(e => e.PositionType_ID == positionType);
        }

        public Shift GetShift(string Shift_ID)
        {
            return _shiftRepository.GetSingle(e => e.Shift_ID == Shift_ID, e => e.ShiftControls);
        }

        public void RemoveAddressType(AddressType addressType)
        {
            try
            {             
                AddressType query1 = GetAddressTypeByName(addressType.AddressType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _addressTypeRepository.Remove(addressType);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemoveBlood(params Blood[] blood)
        {
            //try
            //{
            //    Blood query1 = (addressType.AddressType_ID);
            //    if (query1 == null)
            //        throw new ArgumentException("Not value");
            //    _addressTypeRepository.Remove(addressType);

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //};
        }

        public void RemoveBranch(Branch branch)
        {
            try
            {
                Branch query1 = GetBranch(branch.Branch_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _branchRepository.Remove(branch);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemoveCropSetup(CropSetup cropSetup)
        {
            try
            {

                //var query1 = from c in GetAllCropSetups() select c.Crop;
                CropSetup query1 = GetCropSetup(cropSetup.Crop);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                //if (query1.ValidFrom == null)
                //    throw new ArgumentException("Please input Validfrom");

                //if (query1.EndDate == null)
                //    throw new ArgumentException("Please input EndDate");


                _cropSetupRepository.Remove(cropSetup);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveHoliday(Holiday holiday)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                Holiday query1 = GetHoliday(holiday.Date);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                           
                _holidayRepository.Remove(holiday);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveJobTitle(JobTitle jobTitle)
        {
            try
            {
                JobTitle query1 = GetJobTitle(jobTitle.JobTitle_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _jobTitleRepository.Remove(jobTitle);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemoveLeaveType(LeaveType leaveType)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                LeaveType query1 = GetLeaveType(leaveType.LeaveType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");


                _leaveTypeRepository.Remove(leaveType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveOrganization(Organization organization)
        {
            try
            {
                Organization query1 = GetOrganization(organization);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _organizationRepository.Remove(organization);

            }
            catch (Exception ex)
            {
                throw ex;
            }; 
        }

        public void RemoveOrganizationType(OrganizationType organizationType)
        {
            try
            {
                OrganizationType query1 = GetOrganizationType(organizationType.OrganizationType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _organizationTypeRepository.Remove(organizationType);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemoveOrganizationUnit(OrganizationUnit organizationUnit)
        {
            try
            {
                OrganizationUnit query1 = GetOrganizationUnit(organizationUnit.OrganizationUnit_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _organizationUnitRepository.Remove(organizationUnit);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemovePosition(Position position)
        {
            try
            {
                Position query1 = GetPosition(position.Position_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _positionRepository.Remove(position);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemovePositionType(PositionType positionType)
        {
            try
            {
                PositionType query1 = GetPositionType(positionType.PositionType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                _positionTypeRepository.Remove(positionType);

            }
            catch (Exception ex)
            {
                throw ex;
            };
        }

        public void RemoveShift(Shift shift)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                Shift query1 = GetShift(shift.Shift_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");


                _shiftRepository.Remove(shift);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateAddressType(AddressType addressTypes)
        {
            try
            {
                AddressType query1 = GetAddressTypeByName(addressTypes.AddressType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if(addressTypes.AddressType_ID == null || addressTypes.AddressType_ID =="")
                    throw new ArgumentException("Please input address type ID");
                if(addressTypes.AddressTypeName == null || addressTypes.AddressTypeName =="")
                    throw new ArgumentException("Please input address type name");
                _addressTypeRepository.Update(addressTypes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void UpdateBranch(Branch branch)
        {
            try
            {
                Branch query1 = GetBranch(branch.Branch_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (branch.Branch_ID == null || branch.Branch_ID == "")
                    throw new ArgumentException("Please input branch ID");
                if (branch.BranchTH == null || branch.BranchTH =="")
                    throw new ArgumentException("Please input branch name");


                _branchRepository.Update(branch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateCropSetup(CropSetup cropSetup)
        {
            try
            {

                //var query1 = from c in GetAllCropSetups() select c.Crop;
                CropSetup query1 = GetCropSetup(cropSetup.Crop);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if(cropSetup.Crop < 0  )
                    throw new ArgumentException("Please input crop");
                if (cropSetup.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (cropSetup.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

            
                _cropSetupRepository.Update(cropSetup);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateHoliday(Holiday holiday)
        {
            try
            {
                Holiday query1 = GetHoliday(holiday.Date);
                if (query1 == null)
                    throw new ArgumentException("Date Not value");

                if (query1.Date == null)
                    throw new ArgumentException("Please input Date");

                if (query1.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (query1.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

                _holidayRepository.Update(holiday);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateJobTitle(JobTitle jobTitle)
        {
            try
            {
                JobTitle query1 = GetJobTitle(jobTitle.JobTitle_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");


                _jobTitleRepository.Update(jobTitle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLeaveType(LeaveType leaveType)
        {
            try
            {
                LeaveType query1 = GetLeaveType(leaveType.LeaveType_ID);
                if (query1 == null)
                    throw new ArgumentException("LeaveTypeID Not value");

                if (leaveType.LeaveTypeNameTH == null || leaveType.LeaveTypeNameTH == "")
                    throw new ArgumentException("Please input leaveTypeName(thai)");

                if (leaveType.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (leaveType.EndDate == null)
                    throw new ArgumentException("Please input EndDate");
                

                _leaveTypeRepository.Update(leaveType);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrganization(Organization organization)
        {
            try
            {
                Organization query1 = GetOrganization(organization);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (query1.Organization_ID == null || organization.Organization_ID == "")
                    throw new ArgumentException("Please input organization ID");
                if (query1.Branch_ID == null || organization.Branch_ID == "")
                    throw new ArgumentException("Please input branch");
                if (query1.ParentOrganizationNoted_ID == null || organization.ParentOrganizationNoted_ID == "")
                    throw new ArgumentException("Please input parent Organization");
                if (query1.ParentOrganizationBusinessNoted_ID == null || organization.ParentOrganizationBusinessNoted_ID == "")
                    throw new ArgumentException("Please input parent Organization business");
                if (query1.STECOrganizationNote_ID == null || organization.STECOrganizationNote_ID == "")
                    throw new ArgumentException("Please input STEC Organization");
                if (query1.OrganizationType_ID == null || organization.OrganizationType_ID == "")
                    throw new ArgumentException("Please input Organization type ID");
                if (query1.OrganizationUnit_ID == null || organization.OrganizationUnit_ID == "")
                    throw new ArgumentException("Please input Organization unit ID");
                if (query1.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (query1.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

                _organizationRepository.Update(organization);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOrganizationType(OrganizationType organizationType)
        {
            try
            {
                OrganizationType query1 = GetOrganizationType(organizationType.OrganizationType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (organizationType.OrganizationType_ID == null || organizationType.OrganizationType_ID == "")
                    throw new ArgumentException("Please input organization type ID");
                if (organizationType.OrganizationTypeName == null || organizationType.OrganizationTypeName == "")
                    throw new ArgumentException("Please input organization type name");

                if (organizationType.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (organizationType.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

                _organizationTypeRepository.Update(organizationType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updateOrganizationUnit(OrganizationUnit organizationUnit)
        {
            try
            {
                OrganizationUnit query1 = GetOrganizationUnit(organizationUnit.OrganizationUnit_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (organizationUnit.OrganizationUnit_ID == null || organizationUnit.OrganizationUnit_ID == "")
                    throw new ArgumentException("Please input organization unit ID");
                if (organizationUnit.OrganizationUnitName == "" || organizationUnit.OrganizationUnitName == "")
                    throw new ArgumentException("Please input organization unit name");

                if (organizationUnit.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (organizationUnit.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

                _organizationUnitRepository.Update(organizationUnit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePosition(Position position)
        {
            try
            {
                Position query1 = GetPosition(position.Position_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (position.Position_ID == null || position.Position_ID == "")
                    throw new ArgumentException("Please input position ID");
                if (position.PositionNameTH == null || position.PositionNameTH == "")
                    throw new ArgumentException("Please input position name (TH)");
                //if (position.PositionNameEN == null || position.PositionNameEN == "")
                //    throw new ArgumentException("Please input position name (TH)");
                if (position.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");
                if (position.EndDate == null)
                    throw new ArgumentException("Please input EndDate");


                _positionRepository.Update(position);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePositionType(PositionType positionType)
        {
            try
            {
                PositionType query1 = GetPositionType(positionType.PositionType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
               

                if (positionType.PositionType_ID == null || positionType.PositionType_ID == "")
                    throw new ArgumentException("Please input position type ID");
                if (positionType.PositionTypeNameTH == null || positionType.PositionTypeNameTH == "")
                    throw new ArgumentException("Please input position type name");
                if (positionType.SalaryFlag == null)
                    throw new ArgumentException("Please input payroll cal");
                if (positionType.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (positionType.EndDate == null)
                    throw new ArgumentException("Please input EndDate");


                _positionTypeRepository.Update(positionType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateShift(Shift shift)
        {
            try
            {
                Shift query1 = GetShift(shift.Shift_ID);
                if (query1 == null)
                    throw new ArgumentException("LeaveTypeID Not value");

                if (shift.Shift_ID == null || shift.Shift_ID == "")
                    throw new ArgumentException("Please input shift ID");

                if (shift.StartTime == null)
                    throw new ArgumentException("Please input Start of time");

                if (shift.EndTime == null)
                    throw new ArgumentException("Please input End of time");

                if (shift.ValidFrom == null)
                    throw new ArgumentException("Please input Validfrom");

                if (shift.EndDate == null)
                    throw new ArgumentException("Please input EndDate");

                _shiftRepository.Update(shift);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        public void RemoveBlood(Blood blood)
        {
            throw new NotImplementedException();
        }

        public Organization GetOrganization(Organization organization)
        {
            return _organizationRepository.GetSingle(e => e.Organization_ID == organization.Organization_ID , e => e.OrganizationUnit);
        }

        public void AddBlood(params Blood[] blood)
        {
            throw new NotImplementedException();
        }

        public void UpdateBlood(Blood blood)
        {
            throw new NotImplementedException();
        }

        public IList<JobTitle> GetAllJobTitles()
        {
            return _jobTitleRepository.GetAll();
        }

        public IList<Person> GetAllPersons()
        {
            return _personRepository.GetAll(e => e.Gender, e => e.Blood, e => e.MaritalStatu, e => e.Nationality, e => e.Race, e => e.Religion,e=>e.TitleName);
        }

        public Person GetPerson(string person)
        {
            return _personRepository.GetSingle(e => e.Person_ID == person, e => e.Gender, e => e.Blood, e => e.MaritalStatu, e => e.Nationality, e => e.Race, e => e.Religion,e=> e.TitleName);
        }

        public IList<Employee> GetPersonHistory(string person_ID)
        {
            return _employeeRepository.GetList(e => e.Person_ID == person_ID, e => e.Person, e => e.Person.TitleName);
        }
     
        public void AddPerson(params Person[] person)
        {
            try
            {
                if (person.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }


                IList<Person> query1 = GetAllPersons();
                foreach (Person c in person)
                {
                    //check if exits.
                    if (query1.Where(e => e.Person_ID == c.Person_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate person ID");
                    }

                    if (c.Person_ID == null || c.Person_ID == "")
                        throw new ArgumentException("Please input Person ID");
                    if (c.TitleName_ID == null || c.TitleName_ID =="")
                        throw new ArgumentException("Please input Title name");
                    if (c.FirstNameTH == null || c.FirstNameTH == "")
                        throw new ArgumentException("Please input Firstname");
                    if (c.LastNameTH == null || c.LastNameTH == "")
                        throw new ArgumentException("Please input Lastname");
                    if(c.Gender_ID == null || c.Gender_ID == "")
                        throw new ArgumentException("Please input Gender");
                }

                _personRepository.Add(person);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePerson(Person person)
        {
            try
            {
                Person query1 = GetPerson(person.Person_ID);
                
                if (query1 == null)
                    throw new ArgumentException("Person Not value");

                
                if (person.Person_ID == null)
                    throw new ArgumentException("Please input person ID");
                if (person.Person_ID == null || person.Person_ID == "")
                    throw new ArgumentException("Please input Person ID");
                if (person.TitleName_ID == null || person.TitleName_ID == "")
                    throw new ArgumentException("Please input Title name");
                if (person.FirstNameTH == null || person.FirstNameTH == "")
                    throw new ArgumentException("Please input Firstname");
                if (person.LastNameTH == null || person.LastNameTH == "")
                    throw new ArgumentException("Please input Lastname");
                if (person.Gender_ID == null || person.Gender_ID == "")
                    throw new ArgumentException("Please input Gender");

                _personRepository.Update(person);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemovePerson(Person person)
        {
            try
            {             
                Person query1 = GetPerson(person.Person_ID);
                if (query1 == null)
                {
                    throw new ArgumentException("Not value");
                }
                else
                {
                    IList<Employee> query2 = GetAllEmployees().Where(x => x.Person_ID == query1.Person_ID && x.EndDate >= DateTime.Now).ToList();
                    if (query2.Count > 0)
                    {
                        throw new ArgumentException(" Can't delete this person because he/she was already in the employee module");
                    }
                }
                    
                _personRepository.Remove(person);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Employee> GetAllEmployees()
        {
            return _employeeRepository.GetAll(e => e.Person, e => e.ShiftControls, e => e.WorkUpCountries, e => e.Person.TitleName);
        }

        public Employee GetEmployeeInformation(string employee)
        {
 
            var x = _employeeRepository.GetSingle(e => e.Employee_ID == employee, e => e.ShiftControls, e => e.Person, e => e.Person.TitleName);
            return _employeeRepository.GetSingle(e => e.Employee_ID == employee, e => e.ShiftControls, e => e.Person, e => e.Person.TitleName);
        }

        public Employee GetEmployeeByEmail(string _email)
        {
            return _employeeRepository.GetSingle(e => e.Email == _email) ;
        }

        public void AddEmployee(params Employee[] employee)
        {
            try
            {
                if (employee.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                IList<Employee> query1 = GetAllEmployees();
                foreach (Employee c in employee)
                {
                    //check if exits.
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate Employee ID");
                    }

                    if (c.Employee_ID == null || c.Employee_ID == "")
                        throw new ArgumentException("Please input employee ID");
                    if (c.FingerScanID == null || c.FingerScanID == "")
                        throw new ArgumentException("Please input fingerScan ID");
                    if (c.Person_ID == null || c.Person_ID == "")
                        throw new ArgumentException("Please input person");

                    if (c.StartDate == null )
                        throw new ArgumentException("Please input start of date");
                    
                }
                _employeeRepository.Add(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEmployee(Employee employee)
        {
            try
            {
                Employee query1 = GetEmployeeInformation(employee.Employee_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (employee.Person_ID == null || employee.Person_ID == "")
                    throw new ArgumentException("Please input person ID");
                if (employee.Employee_ID == null || employee.Employee_ID == "")
                    throw new ArgumentException("Please input employee ID");
                if (employee.FingerScanID == null || employee.FingerScanID == "")
                    throw new ArgumentException("Please input fingerScan ID");
                if (employee.StartDate == null)
                    throw new ArgumentException("Please input Start of date");



                _employeeRepository.Update(employee);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveEmployee(Employee employee)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                //Gender query1 = GetAllGender(gender.Gender_ID);
                //var query1 = from c in GetAllEmployees() select c.Employee_ID;
                Employee query1 = GetEmployeeInformation(employee.Employee_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _employeeRepository.Remove(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        public Employee GetEmployeeByShift(string shift)
        {
            throw new NotImplementedException();
        }

        public IList<Gender> GetAllGender()
        {
            return _genderRepository.GetAll();
        }

        public void AddGender(params Gender[] gender)
        {
            try
            {
                if (gender.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                IList<Gender> query1 = GetAllGender();
                foreach (Gender c in gender)
                {
                    if(query1.Where(e=>e.Gender_ID == c.Gender_ID).Count() >0)
                        throw new ArgumentException("Duplicate value");
                    if (c.Gender_ID == null || c.Gender_ID =="")
                        throw new ArgumentException("Please input Gender ID");
                    if (c.GenderTH == null || c.GenderTH == "")
                        throw new ArgumentException("Please input name(TH)");
                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");
                }
                _genderRepository.Add(gender);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateGender(Gender[] gender)
        {
            try
            {
                //var query1 = from c in GetAllGender() select c.Gender_ID ;
                //if (query1 == null)
                //    throw new ArgumentException("Date Not value");
                IList<Gender> query1 = GetAllGender();

                foreach (Gender c in gender)
                {
                    if (c.Gender_ID == null)
                        throw new ArgumentException("Please input Gender ID");
                    if (query1.Where(e => e.Gender_ID == c.Gender_ID).Count() < 0)
                        throw new ArgumentException("Not value");
                    if (c.GenderTH == null || c.GenderTH == "")
                        throw new ArgumentException("Please input name(TH)");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");
                }

                _genderRepository.Update(gender);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveGender(Gender gender)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                //Gender query1 = GetAllGender(gender.Gender_ID);
                var query1 = GetAllGender().Where(e => e.Gender_ID == gender.Gender_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _genderRepository.Remove(gender);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<TitleName> GetAllTitleName()
        {
            return _titleNameRepository.GetAll();
        }

        public void AddTitleName(params TitleName[] titlename)
        {
            try
            {
                if (titlename.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                foreach (TitleName c in titlename)
                {
                    if (c.TitleName_ID == null || c.TitleName_ID =="")
                        throw new ArgumentException("Please input title ID");
                    if (c.TitleNameTH == null || c.TitleNameTH =="")
                        throw new ArgumentException("Please input name(TH)");

                    if (c.TitleNameInitialTH == null || c.TitleNameInitialTH =="")
                        throw new ArgumentException("Please input  initital name(TH)");
                }
                _titleNameRepository.Add(titlename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTitleName(params TitleName[] titlename)
        {
            try
            {
                //var query1 = from c in GetAllGender() select c.Gender_ID ;
                //if (query1 == null)
                //    throw new ArgumentException("Date Not value");
                IList<TitleName> query1 = GetAllTitleName();
                foreach (TitleName c in titlename)
                {
                    if (c.TitleName_ID == null)
                        throw new ArgumentException("Please input title ID");
                    if(query1.Where(e=>e.TitleName_ID == c.TitleName_ID).Count()<0)
                        throw new ArgumentException("Not value");
                    if (c.TitleNameTH == null || c.TitleNameTH =="")
                        throw new ArgumentException("Please input name(TH)");

                    if (c.TitleNameInitialTH == null || c.TitleNameInitialTH =="")
                        throw new ArgumentException("Please input name(TH) initital");
                }

                _titleNameRepository.Update(titlename);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveTitleName(TitleName titlename)
        {
            try
            {
                //var query1 = from c in GetAllCropSetups() select c.Crop;
                //Gender query1 = GetAllGender(gender.Gender_ID);
                //var query1 = from c in GetAllTitleName() select c.TitleName_ID;
                var query1 = GetAllTitleName().Where(e => e.TitleName_ID == titlename.TitleName_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _titleNameRepository.Remove(titlename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<EmployeePosition> GetAllEmployeePositions()
        {
            return _employeePositionRepository.GetAll(e => e.Employee.Person,e=>e.PositionOrganization,e=>e.PositionOrganization.Position,e=>e.PositionOrganization.PositionType);
            //return _employeePositionRepository.GetAll(e => e.PositionOrganization);
        }

        

        public void AddEmployeePosition(params EmployeePosition[] employeePosition)
        {
            try
            {
                if (employeePosition.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }
                IList<EmployeePosition> query1 = GetAllEmployeePositions();
                foreach (EmployeePosition c in employeePosition)
                {
                    if (c.PositionOrganization_ID == null || c.PositionOrganization_ID == "")
                        throw new ArgumentException("Please input position");
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID && e.PositionOrganization_ID == c.PositionOrganization_ID).Count() > 0)
                        throw new ArgumentException("Duplicate value");
                    if (c.Employee_ID == null || c.Employee_ID == "")
                        throw new ArgumentException("Please input Employee ID");                  
                    if (c.HireDate == null)
                        throw new ArgumentException("Please input HireDate");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");
                    if(c.FingerScanFlag == null )
                        throw new ArgumentException("Please input finger scan flag");

                    IList<EmployeePosition> query2 = GetPositionByEmployee(c.Employee_ID);
                    if (query2.Where(e => e.EndDate >= c.HireDate).Count() > 0)
                        throw new ArgumentException("หากต้องการเพิ่มตำแหน่งใหม่คุณจะต้องทำการสิ้นสุดวันที่สุดท้ายของตำแหน่งงานเดิมก่อน");
                }
                _employeePositionRepository.Add(employeePosition);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEmployeePosition(params EmployeePosition[] employeePosition)
        {
            try
            {
                IList<EmployeePosition> query1 = GetAllEmployeePositions();
                foreach (EmployeePosition c in employeePosition)
                {
                    if (c.PositionOrganization_ID == null || c.PositionOrganization_ID == "")
                        throw new ArgumentException("Please input position");
                    if (query1.Where(e=>e.Employee_ID == c.Employee_ID && e.PositionOrganization_ID == c.PositionOrganization_ID).Count() < 0)
                        throw new ArgumentException("Not value");
                    if (c.Employee_ID == null || c.Employee_ID == "")
                        throw new ArgumentException("Please input Employee ID");                    
                    if (c.HireDate == null)
                        throw new ArgumentException("Please input HireDate");

                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");
                    if (c.FingerScanFlag == null)
                        throw new ArgumentException("Please input finger scan flag");

                    IList<EmployeePosition> query2 = GetPositionByEmployee(c.Employee_ID);
                    if (query2.Where(e => e.EndDate >= c.HireDate && e.PositionOrganization_ID != c.PositionOrganization_ID).Count() > 0)
                        throw new ArgumentException("หากต้องการเพิ่มตำแหน่งใหม่คุณจะต้องทำการสิ้นสุดวันที่สุดท้ายของตำแหน่งงานเดิมก่อน");
                }

                _employeePositionRepository.Update(employeePosition);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveEmployeePosition(EmployeePosition employeePosition)
        {
            try
            {

                var query1 = GetAllEmployeePositions().Where(e => e.PositionOrganization_ID == employeePosition.PositionOrganization_ID && e.Employee_ID == employeePosition.Employee_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _employeePositionRepository.Remove(employeePosition);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ShiftControl> GetAllShiftControls()
        {
            return _shiftControlRepository.GetAll();
        }

        public IList<ShiftControl> GetShiftControlByEmployee(string EmployeeID)
        {
            return _shiftControlRepository.GetList(e => e.Employee_ID == EmployeeID, e => e.Shift);
        }

        public IList<ShiftControl> GetShiftControlByEmployeeAndDate(string EmployeeID, string Shift_ID)
        {
            return _shiftControlRepository.GetList(e => e.Employee_ID == EmployeeID && e.Shift_ID == Shift_ID, e => e.Shift);
        }

        public IList<ShiftControl> GetShiftControlByShiftAndDate(string Shift_ID, DateTime fromDate, DateTime toDate)
        {
            return _shiftControlRepository.GetList(e => e.Shift_ID == Shift_ID && (e.ValidFrom >= fromDate && e.EndDate <= toDate), e => e.Shift, e => e.Employee, e => e.Employee.Person)
                                          .OrderBy(o => o.ValidFrom)
                                          .ThenBy(o => o.Employee_ID)
                                          .ToList();
        }

        public void AddShiftControl(params ShiftControl[] shiftControl)
        {
            try
            {
                if (shiftControl.Count() < 1) throw new ArgumentException("ผิดพลาด ไม่สามารถอัพเดทข้อมูลได้ กรุณาตรวจสอบ");

                foreach (ShiftControl item in shiftControl)
                {

                    if (string.IsNullOrEmpty(item.Shift_ID)) throw new ArgumentException("กรุณาระบุกะที่ต้องการบันทึก");
                    if (string.IsNullOrEmpty(item.Employee_ID)) throw new ArgumentException("กรุณาระบุ employee ID ของพนักงาน");
                    if (item.ValidFrom == null || item.EndDate == null) throw new ArgumentException("กรุณาระบุเวลาให้ถูกต้อง");

                    IList<ShiftControl> query1 = GetShiftControlByEmployee(item.Employee_ID);
                    var isExisted = query1.SingleOrDefault(s => s.ValidFrom == item.ValidFrom);
                    if (isExisted != null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ " + " \n" +
                                                                       "Employee_id : " + isExisted.Employee_ID + " \n" +
                                                                       "Shift_ID : " + isExisted.Shift_ID + " \n" +
                                                                       "ValidFrom : " + isExisted.ValidFrom + " \n" +
                                                                       "มีอยู่ในระบบแล้ว");
                }
                _shiftControlRepository.Add(shiftControl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateShiftControl(params ShiftControl[] shiftControl)
        {
            try
            {             
                IList<ShiftControl> query1 = GetAllShiftControls();
                foreach (ShiftControl c in shiftControl)
                {
                    //IList<ShiftControl> query1 = GetShiftControlByEmployeeAndShift(c.Employee_ID, c.Shift_ID);
                    if (string.IsNullOrEmpty(c.Employee_ID)) throw new ArgumentException("กรุณาระบุ employee ID ของพนักงาน");
                    if (c.ValidFrom == null || c.EndDate == null) throw new ArgumentException("กรุณาระบุเวลาให้ถูกต้อง");
                    if (string.IsNullOrEmpty(c.Shift_ID)) throw new ArgumentException("กรุณาระบุกะที่ต้องการบันทึก");
                    if (query1.Where(e=> e.Employee_ID == c.Employee_ID && 
                                         e.Shift_ID == c.Shift_ID && 
                                         e.ValidFrom == c.ValidFrom).Count() < 0)
                        throw new ArgumentException("ผิดพลาดไม่พบข้อมูลที่ต้องการอัพเดท กรุณาตรวจสอบอีกครั้ง");
                }
                _shiftControlRepository.Update(shiftControl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveShiftControl(ShiftControl shiftControl)
        {
            try
            {
                if (shiftControl == null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                var query1 = GetAllShiftControls().Where(e => e.Employee_ID == shiftControl.Employee_ID && 
                                                              e.Shift_ID == shiftControl.Shift_ID && 
                                                              e.ValidFrom == shiftControl.ValidFrom);
                if (query1 == null) throw new ArgumentException("ผิดพลาดไม่พบข้อมูลที่ต้องการลบ กรุณาตรวจสอบอีกครั้ง");
            
                _shiftControlRepository.Remove(shiftControl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<EmployeePosition> GetPositionByEmployee(string employee)
        {
            //return _employeePositionRepository.GetList(e => e.Employee_ID == employee , e=>e.PositionOrganization,e=>e.PositionOrganization.Position,e=>e.PositionOrganization.Organization.OrganizationUnit,e=>e.PositionType);
            return _employeePositionRepository.GetList(e => e.Employee_ID == employee, 
                e => e.Employee.Person,
            e=>e.PositionOrganization.Position,
            e=>e.PositionOrganization.PositionType,
            e=>e.PositionOrganization.Organization.OrganizationUnit);
        }

        public IList<Leave> GetAllLeaves()
        {
            return _leaveRepository.GetAll(e => e.Employee.Person, e => e.Employee.Person.TitleName,e=>e.LeaveType);
        }
        public IList<Leave> GetLeaveByEmployee(string employee)
        {
            return _leaveRepository.GetList(e => e.Employee_ID == employee, e => e.Employee.Person, e => e.Employee.Person.TitleName, e => e.LeaveType);
        }

        public void AddLeave(params Leave[] leave)
        {
            try
            {
                if (leave.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                //var query1 = from c in GetAllCropSetups() select c.Crop;
                IList<Leave> query1 = GetAllLeaves();
                foreach (Leave c in leave)
                {
                    //check if exits.
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID && e.LeaveDate == c.LeaveDate).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate data");
                    }

                    if (c.LeaveDate == null)
                        throw new ArgumentException("Please input leave date");
                    if (c.LeaveType_ID == null || c.LeaveType_ID == "")
                        throw new ArgumentException("Please input leave type");
                    if (c.NcountLeave == null || c.NcountLeave < 0)
                        throw new ArgumentException("Please input count of leave");

                }
                _leaveRepository.Add(leave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLeave(params Leave[] leave)
        {
            try
            {
                IList<Leave> query1 = GetAllLeaves();
                foreach (Leave c in leave)
                {
                    if (c.LeaveType_ID == null || c.LeaveType_ID =="")
                        throw new ArgumentException("Please input leave type");
                    if (query1.Where(e=>e.Employee_ID == c.Employee_ID && e.LeaveDate == c.LeaveDate && e.LeaveType_ID == e.LeaveType_ID).Count() <0)
                        throw new ArgumentException("Not value");
                    if (c.LeaveDate == null)
                        throw new ArgumentException("Please input leave date");
                    
                    if(c.NcountLeave == null || c.NcountLeave <0)
                        throw new ArgumentException("Please input count of leave");
                }
                _leaveRepository.Update(leave);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveLeave(Leave leave)
        {
            try
            {

                var query1 = GetAllLeaves().Where(e => e.Employee_ID == leave.Employee_ID && e.LeaveDate == leave.LeaveDate && e.LeaveType_ID == leave.LeaveType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _leaveRepository.Remove(leave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkUpCountry> GetAllWorkUpCountry()
        {
            return _workUpCountryRepository.GetAll(e => e.Employee.Person, e => e.Employee.Person.TitleName);
        }

        public IList<WorkUpCountry> GetWorkUpCountryByEmployee(string employee)
        {
            return _workUpCountryRepository.GetList(e => e.Employee_ID == employee, e => e.Employee.Person, e => e.Employee.Person.TitleName);
        }

        

        public void AddWorkUpcountry(params WorkUpCountry[] workUpCountry)
        {
            try
            {
                if (workUpCountry.Count() < 1)
                {
                    throw new ArgumentException("Please input data");

                }

                //var query1 = from c in GetAllCropSetups() select c.Crop;
                IList<WorkUpCountry> query1 = GetAllWorkUpCountry();
                foreach (WorkUpCountry c in workUpCountry)
                {
                    //check if exits.
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID && e.WorkDate == c.WorkDate).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate value");
                    }

                    if (c.WorkDate == null)
                        throw new ArgumentException("Please input work date");
                    if (c.Employee_ID == null)
                        throw new ArgumentException("Please input employee");


                }
                _workUpCountryRepository.Add(workUpCountry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateWorkUpcountry(params WorkUpCountry[] workUpCountry)
        {
            try
            {
                IList<WorkUpCountry> query1 = GetAllWorkUpCountry();
                foreach (WorkUpCountry c in workUpCountry)
                {
                    
                    if (c.WorkDate == null)
                        throw new ArgumentException("Please input work date");
                    if (c.Employee_ID == null || c.Employee_ID == "")
                        throw new ArgumentException("Please input employee");
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID && e.WorkDate == c.WorkDate).Count() < 0)
                        throw new ArgumentException("Not value");
                }
                _workUpCountryRepository.Update(workUpCountry);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveWorkUpcountry(WorkUpCountry workUpCountry)
        {
            try
            {

                //var query1 = from c in GetAllWorkUpCountry() select new { c.WorkDate, c.Employee_ID };
                var query1 = GetAllWorkUpCountry().Where(e => e.Employee_ID == workUpCountry.Employee_ID && e.WorkDate == workUpCountry.WorkDate);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _workUpCountryRepository.Remove(workUpCountry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<OT> GetAllOTs()
        {
            return _otRepository.GetAll(e=>e.Employee, e=>e.Employee.Person , e=>e.Employee.Person.TitleName);
        }

        public IList<OT> GetOTByEmployee(string employee)
        {
            return _otRepository.GetList(e => e.Employee_ID == employee, e => e.Employee.Person, e => e.Employee.Person.TitleName);
        }

        public void AddOT(params OT[] ot)
        {
            try
            {
                if (ot.Count() < 1) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                IList<OT> query1 = GetAllOTs();
                foreach (OT c in ot)
                {
                    //check if exits.
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID && 
                                          e.OTDate == c.OTDate  && 
                                          e.RequestType == c.RequestType &&
                                          e.OTType == c.OTType).Count() > 0)
                    {
                        throw new ArgumentException("ไม่สามารถบันทึกข้อมูลซ้ำได้ ข้อมูลนี้มีอยู่ในระบบแล้ว");
                    }

                    if (c.OTDate == null) throw new ArgumentException("กรุณาตรวจสอบข้อมูล OT date");
                    if (string.IsNullOrEmpty(c.Employee_ID)) throw new ArgumentException("กรุณาตรวจสอบข้อมูล Employee_ID");
                    if(c.NcountOT == null || c.NcountOT < 0) throw new ArgumentException("กรุณาตรวจสอบข้อมูลจำนวน OT");
                }
                _otRepository.Add(ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOT(params OT[] ot)
        {
            try
            {
                if (ot.Count() < 1) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                IList<OT> query1 = GetAllOTs();
                foreach (OT c in ot)
                {
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID &&
                                          e.OTDate == c.OTDate &&
                                          e.OTType == c.OTType &&
                                          e.RequestType == c.RequestType).Count() < 0)
                        throw new ArgumentException("ไม่พบข้อมูลการทำโอทีของพนักงานคนนี้หรือข้อมูลถูกลบจากระบบ กรุณาตรวจสอบข้อมูลอีกครั้ง");
                    if (c.OTDate == null) throw new ArgumentException("กรุณาตรวจสอบข้อมูล OT date");
                    if (string.IsNullOrEmpty(c.Employee_ID)) throw new ArgumentException("กรุณาตรวจสอบข้อมูล Employee_ID");
                    if (c.NcountOT == null || c.NcountOT < 0) throw new ArgumentException("กรุณาตรวจสอบข้อมูลจำนวน OT");
                }
                _otRepository.Update(ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveOT(OT ot)
        {
            try
            {
                if (ot == null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                var query1 = GetAllOTs().Where(e => e.Employee_ID == ot.Employee_ID && 
                                                    e.OTDate == ot.OTDate && 
                                                    e.OTType == ot.OTType &&
                                                    e.RequestType == ot.RequestType);
                if (query1 == null) throw new ArgumentException("ไม่พบข้อมูลการทำโอทีของพนักงานคนนี้หรือข้อมูลถูกลบจากระบบ กรุณาตรวจสอบข้อมูลอีกครั้ง");
                _otRepository.Remove(ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<PositionOrganization> GetAllPositionOrganizations()
        {
            return _positionOrganizationRepository.GetAll(e => e.Organization, e => e.Position, e => e.JobTitle, e => e.Organization.OrganizationUnit, e => e.Organization.OrganizationType, e => e.PositionType);
        }


        public PositionOrganization GetPositionOrganization(string positionOrganization_ID)
        {
            return _positionOrganizationRepository.GetSingle(e => e.PositionOrganization_ID == positionOrganization_ID,e=>e.Position);
        }

        public void AddPositionOrganization(params PositionOrganization[] positionOrganization)

        {
            try
            {
                if (positionOrganization.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }

                IList<PositionOrganization> query1 = GetAllPositionOrganizations();
                foreach (PositionOrganization c in positionOrganization)
                {
                    if (c.PositionOrganization_ID == null || c.PositionOrganization_ID =="")
                        throw new ArgumentException("Please input Organizationposition ID");
                    if (c.Position_ID == null || c.Position_ID == "")
                        throw new ArgumentException("Please input position ID");
                    if (c.Organization_ID == null || c.Organization_ID == "")
                        throw new ArgumentException("Please input organization");
                    //check if exits.
                    if (query1.Where(e => e.PositionOrganization_ID == c.PositionOrganization_ID).Count() > 0)
                        throw new ArgumentException("Duplicate positionOrganization");                    
                    //if(query1.Where(e=>e.Position_ID == c.Position_ID && e.Organization_ID == c.Organization_ID).Count() >0)
                    //    throw new ArgumentException("Duplicate position and organization");
                    if (c.PositionNode == null || c.PositionNode == "")
                        throw new ArgumentException("Please input position node");
                    if (c.ParentPositionBusinessNode_ID == null || c.ParentPositionBusinessNode_ID == "")
                        throw new ArgumentException("Please input parent position node");
                    if (c.STECPositionNode_ID == null || c.STECPositionNode_ID == "")
                        throw new ArgumentException("Please input stec position node");
                    if (c.PositionType_ID == null || c.PositionType_ID == "")
                        throw new ArgumentException("Please input position type");
                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");
                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }
                _positionOrganizationRepository.Add(positionOrganization);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePositionOrganization(params PositionOrganization[] positionOrganization)
        {
            try
            {
                //var query1 = GetAllPositionOrganizations().Where(e => e.PositionOrganization_ID == positionOrganization.PositionOrganization_ID && e.Position_ID == positionOrganization.Position_ID);
                //if (query1 == null)
                //    throw new ArgumentException("Not value");

                IList<PositionOrganization> query1 = GetAllPositionOrganizations();
                foreach (PositionOrganization c in positionOrganization)
                {
                    if (c.PositionOrganization_ID == null || c.PositionOrganization_ID == "")
                        throw new ArgumentException("Please input Organizationposition ID");
                    if (c.Position_ID == null || c.Position_ID == "")
                        throw new ArgumentException("Please input position ID");
                    if (c.Organization_ID == null || c.Organization_ID == "")
                        throw new ArgumentException("Please input organization");
                    //check if exits.
                    if (query1.Where(e => e.PositionOrganization_ID == c.PositionOrganization_ID).Count() < 0)
                        throw new ArgumentException("Not value");
                    //if (query1.Where(e => e.Position_ID == c.Position_ID && e.Organization_ID == c.Organization_ID).Count() > 0)
                    //    throw new ArgumentException("Duplicate position and organization");
                    if (c.PositionNode == null || c.PositionNode == "")
                        throw new ArgumentException("Please input position node");
                    if (c.ParentPositionBusinessNode_ID == null || c.ParentPositionBusinessNode_ID == "")
                        throw new ArgumentException("Please input parent position node");
                    if (c.STECPositionNode_ID == null || c.STECPositionNode_ID == "")
                        throw new ArgumentException("Please input stec position node");
                    if (c.PositionType_ID == null || c.PositionType_ID == "")
                        throw new ArgumentException("Please input positiontype");
                    if (c.ValidFrom == null)
                        throw new ArgumentException("Please input Validfrom");
                    if (c.EndDate == null)
                        throw new ArgumentException("Please input EndDate");

                }

                _positionOrganizationRepository.Update(positionOrganization);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemovePositionOrganization(PositionOrganization positionOrganization)
        {
            try
            {
                var query1 = GetAllPositionOrganizations().Where(e => e.PositionOrganization_ID == positionOrganization.PositionOrganization_ID && e.Position_ID == positionOrganization.Position_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");


                _positionOrganizationRepository.Remove(positionOrganization);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IList<LeaveTypeBenefit> GetLeaveTypeBenefitsByPositionType(string positionType_ID)
        //{
        //    return _leaveTypeBenefitsRepository.GetList(e => e.PositionType_ID == positionType_ID, e=>e.PositionType);
        //}

        //public void AddLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits)
        //{
        //    try
        //    {
        //        if (leaveTypeBenefits.Count() < 1)
        //        {
        //            throw new ArgumentException("Please input data");
        //        }

        //        IList<LeaveTypeBenefit> query1 = GetLeaveTypeBenefitsByPositionType(leaveTypeBenefits.ToString());
        //        foreach (LeaveTypeBenefit c in leaveTypeBenefits)
        //        {
        //            if (c.PositionType_ID == null || c.PositionType_ID == "")
        //                throw new ArgumentException("Please input positionType ID");
        //            if (c.LeaveType_ID == null || c.LeaveType_ID == "")
        //                throw new ArgumentException("Please input leaveType ID");
        //            if (c.MaxNDate == null)
        //                throw new ArgumentException("Please input count of max ");
        //            if (c.EndDate == null)
        //                throw new ArgumentException("Please input EndDate");
        //            //check if exits.
        //            if (query1.Where(e => e.PositionType_ID == c.PositionType_ID && e.LeaveType_ID == c.LeaveType_ID && c.EndDate <= e.EndDate).Count() > 0)
        //                throw new ArgumentException("Duplicate value");                   
        //            if (c.ValidFrom == null)
        //                throw new ArgumentException("Please input Validfrom");
                    

        //        }
        //        _leaveTypeBenefitsRepository.Add(leaveTypeBenefits);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void UpdateLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits)
        //{
        //    try
        //    {
        //        //var query1 = GetAllPositionOrganizations().Where(e => e.PositionOrganization_ID == positionOrganization.PositionOrganization_ID && e.Position_ID == positionOrganization.Position_ID);
        //        //if (query1 == null)
        //        //    throw new ArgumentException("Not value");

        //        IList<LeaveTypeBenefit> query1 = GetLeaveTypeBenefitsByPositionType(leaveTypeBenefits.ToString());
        //        foreach (LeaveTypeBenefit c in leaveTypeBenefits)
        //        {
        //            if (c.PositionType_ID == null || c.PositionType_ID == "")
        //                throw new ArgumentException("Please input positionType ID");
        //            if (c.LeaveType_ID == null || c.LeaveType_ID == "")
        //                throw new ArgumentException("Please input leaveType ID");
        //            if (c.MaxNDate == null)
        //                throw new ArgumentException("Please input count of max ");
        //            if (c.EndDate == null)
        //                throw new ArgumentException("Please input EndDate");
        //            //check if exits.
        //            if (query1.Where(e => e.PositionType_ID == c.PositionType_ID && e.LeaveType_ID == c.LeaveType_ID && c.EndDate <= e.EndDate).Count() > 0)
        //                throw new ArgumentException("Duplicate value");
        //            if (c.ValidFrom == null)
        //                throw new ArgumentException("Please input Validfrom");


        //        }

        //        _leaveTypeBenefitsRepository.Update(leaveTypeBenefits);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void RemoveLeaveTypeBenefits(LeaveTypeBenefit leaveTypeBenefits)
        //{
        //    try
        //    {
        //        var query1 = GetLeaveTypeBenefitsByPositionType(leaveTypeBenefits.ToString()).Where(e => e.PositionType_ID == leaveTypeBenefits.PositionType_ID && e.LeaveType_ID == leaveTypeBenefits.LeaveType_ID && leaveTypeBenefits.EndDate <= e.EndDate);
        //        if (query1 == null)
        //            throw new ArgumentException("Not value");


        //        _leaveTypeBenefitsRepository.Remove(leaveTypeBenefits);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public IList<Leave> GetLeaveByLeaveDate(DateTime LeaveDate)
        {
            return _leaveRepository.GetList(e => e.LeaveDate == LeaveDate);
        }

        public IList<IDCardInfo> GetAllIDCard()
        {
            return _idCardRepository.GetAll();
        }

        public IList<IDCardInfo> GetIDCardInfo(string person_ID)
        {
            return _idCardRepository.GetList(e => e.Person_ID == person_ID,e=>e.Province , e=>e.District );
        }

        

        public void AddIDCardInfo(params IDCardInfo[] idCardInfo)
        {
            try
            {
                if (idCardInfo.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }
                IList<IDCardInfo> query1 = GetAllIDCard();
                foreach (IDCardInfo c in idCardInfo)
                {
                    if (c.Person_ID == null || c.Person_ID == "")
                        throw new ArgumentException("กรุณาระบุ Person Id");
                    if (c.Card_ID == null || c.Card_ID == "")
                        throw new ArgumentException("กรุณาระบุ เลขบัตรประชาชน");
                    //if (c.MaxNDate == null)
                    //    throw new ArgumentException("Please input count of max ");
                    //if (c.EndDate == null)
                    //    throw new ArgumentException("Please input EndDate");
                    //check if exits.
                    if (query1.Where(e => e.Card_ID == c.Card_ID &&  e.ExpiredDate >= c.ExpiredDate ).Count() > 0)
                        throw new ArgumentException("ข้อมูลเลขบัตรประชาชนใหม่ระบุวันที่ไม่ถูกต้องกับข้อมูลเดิมที่มีอยู่แล้ว");
                    
                }
                _idCardRepository.Add(idCardInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateIDCardInfo(params IDCardInfo[] idCardInfo)
        {
            try
            {
                if (idCardInfo.Count() < 1)
                {
                    throw new ArgumentException("Please input data");
                }
                IList<IDCardInfo> query1 = GetAllIDCard();
                foreach (IDCardInfo c in idCardInfo)
                {
                    if (c.Person_ID == null || c.Person_ID == "")
                        throw new ArgumentException("กรุณาระบุ Person ID");
                    if (c.Card_ID == null || c.Card_ID == "")
                        throw new ArgumentException("กรุณาระบุเลขบัตรประชาชน");
                    //if (query1.Where(e => e.Card_ID == c.Card_ID && e.ExpiredDate >= c.ExpiredDate).Count() > 0)
                    //    throw new ArgumentException("ข้อมูลเลขบัตรประชาชนใหม่ระบุวันที่ไม่ถูกต้องกับข้อมูลเดิมที่มีอยู่แล้ว");
                }
                _idCardRepository.Update(idCardInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveIDCardInfo(IDCardInfo idCardInfo)
        {
            try
            {
                var query1 = GetIDCardInfo(idCardInfo.ToString());
                
                if (query1 == null)
                    throw new ArgumentException("Not value");


                _idCardRepository.Remove(idCardInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Province> GetAllProvinces()
        {
            return _provinceRepository.GetAll();
        }

        public void AddProvince(params Province[] province)
        {
            throw new NotImplementedException();
        }

        public void UpdateProvince(params Province[] province)
        {
            throw new NotImplementedException();
        }

        public void RemoveProvince(Province province)
        {
            throw new NotImplementedException();
        }
        public District GetDistrict(string district_ID)
        {
            return _districtRepository.GetSingle(e => e.District_ID == district_ID);
        }
        public IList<District> GetDistrictByProvince(string province_ID)
        {
            return _districtRepository.GetList(e => e.Province_ID == province_ID);
        }

        public void AddDistrict(params District[] district)
        {
            throw new NotImplementedException();
        }

        public void UpdateDistrict(params District[] district)
        {
            throw new NotImplementedException();
        }

        public void RemoveDistrict(District district)
        {
            throw new NotImplementedException();
        }

        public IList<SubDistrict> GetSubDistrictByDistrict(string District_ID)
        {
            return _subDistrictRepository.GetList(e => e.District_ID == District_ID);
        }

        public void AddSubDistrict(params SubDistrict[] subDistrict)
        {
            throw new NotImplementedException();
        }

        public void UpdateSubDistrict(params SubDistrict[] subDistrict)
        {
            throw new NotImplementedException();
        }

        public void RemoveSubDistrict(SubDistrict subDistrict)
        {
            throw new NotImplementedException();
        }

        public IList<Address> GetAddressByPerson(string person_ID)
        {
            return _addressRepository.GetList(e => e.Person_ID == person_ID);
        }

        public void AddAddress(params Address[] address)
        {
            try
            {
                if (address.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                foreach (Address c in address)
                {
   
                    if (c.Person_ID == null)
                        throw new ArgumentException("Please input person ID");

                    if (c.AddressType_ID == null)
                        throw new AggregateException("Please input address type");
                    if (c.HomeNumber == null)
                        throw new AggregateException("Please input home number");
                    if (c.SubDistrict_ID == null)
                        throw new AggregateException("Plese input Sub district");

                }

                _addressRepository.Add(address);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateAddress(Address address)
        {
            try
            {
                if (address == null  )
                {
                    throw new ArgumentException("Please input datas");

                }


                
                    if (address.Person_ID == null)
                        throw new ArgumentException("Please input person ID");

                    if (address.AddressType_ID == null)
                        throw new AggregateException("Please input address type");
                    if (address.HomeNumber == null)
                        throw new AggregateException("Please input home number");
                    if (address.SubDistrict_ID == null)
                        throw new AggregateException("Plese input Sub district");

               

                _addressRepository.Update(address);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveAddress(string addressType, string person_ID)
        {
            try
            {
                if (addressType == null || person_ID == null)
                {
                    throw new ArgumentException("Please input datas");

                }

                _addressRepository.Remove();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Province GetProvince(string province_ID)
        {
            return _provinceRepository.GetSingle(e => e.Province_ID == province_ID);
        }

        public SubDistrict GetSubDistrict(string subDistrict_ID)
        {
            return _subDistrictRepository.GetSingle(e => e.SubDistrict_ID == subDistrict_ID);
        }

        public Country GetCountry(string country_ID)
        {
            return _countryRepository.GetSingle(e => e.Country_ID == country_ID);
        }

        public void AddCountry(params Country[] country)
        {
            throw new NotImplementedException();
        }

        public void UpdateCountry(params Country[] country)
        {
            throw new NotImplementedException();
        }

        public void RemoveCountry(Country country)
        {
            throw new NotImplementedException();
        }

        public IList<Country> GetAllCountrys()
        {
            return _countryRepository.GetAll();
        }

        public IList<ManualWorkDateType> GetAllManualWorkDateTypes()
        {
            return _manualWorkDateTypeRepository.GetAll();
        }

        public IList<ManualWorkDate> GetAllManualWorkDates()
        {
            return _manualWorkDateRepository.GetAll(e => e.Employee ,e=>e.ManualWorkDateType);
        }

        public ManualWorkDate GetManualWorkDateByEmployeeAndType(string _employee_ID, string _manualWorkDateType, DateTime _manualWorkDate)
        {
            return _manualWorkDateRepository.GetSingle(e => e.Employee_ID == _employee_ID && e.ManualWorkDateType_ID == _manualWorkDateType
                && Convert.ToDateTime(e.ManualWorkDateDate) >= _manualWorkDate
                && Convert.ToDateTime(e.ManualWorkDateDate) <= _manualWorkDate, e => e.Employee, e => e.ManualWorkDateType);
        }
        public List<ManualWorkDate> GetManualWorkDateByEmployee(string _employee_ID, DateTime _manualWorkDate)
        {
            return _manualWorkDateRepository.GetList(e => e.Employee_ID == _employee_ID
                && Convert.ToDateTime(e.ManualWorkDateDate) >= _manualWorkDate
                && Convert.ToDateTime(e.ManualWorkDateDate) <= _manualWorkDate,
                e => e.ManualWorkDateType).ToList();
        }

        public void AddManualWorkDate(params ManualWorkDate[] manualWorkDate)
        {
            try
            {
                if (manualWorkDate.Count() < 1) throw new ArgumentException("ไม่สามารถบันทึกค่าว่างได้ กรุณาตรวจสอบ");
                foreach (ManualWorkDate c in manualWorkDate)
                {
                    if (GetManualWorkDateByEmployee(c.Employee_ID, c.ManualWorkDateDate).Where(w => w.ManualWorkDateType_ID == c.ManualWorkDateType_ID).Count() > 0) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบ.");
                    if (c.Employee_ID == null) throw new ArgumentException("ไม่พบค่า Employee ID กรุณาตรวจสอบ");
                    if (c.ManualWorkDateDate == null) throw new AggregateException("ไม่พบค่าวันที่ กรุณาตรวจสอบ");
                    if (c.ManualWorkDateType_ID == null) throw new AggregateException("ไม่พบค่า ManualWorkDate Type กรุณาตรวจสอบ");
                    if (c.Times == null) throw new AggregateException("ไม่พบค่าเวลา กรุราตรวจสอบ");
                    if (c.Status == null) throw new AggregateException("ไม่พบค่า Status กรุราตรวจสอบ");
                }
                _manualWorkDateRepository.Add(manualWorkDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManualWorkDate(params ManualWorkDate[] manualWorkDate)
        {
            try
            {
                if (manualWorkDate == null) throw new ArgumentException("ไม่สามารถบันทึกค่าว่างได้ กรุณาตรวจสอบ");
                foreach (ManualWorkDate c in manualWorkDate)
                {
                    if (c.Employee_ID == null) throw new ArgumentException("ไม่พบค่า Employee ID กรุณาตรวจสอบ");
                    if (c.ManualWorkDateDate == null) throw new AggregateException("ไม่พบค่าวันที่ กรุณาตรวจสอบ");
                    if (c.ManualWorkDateType_ID == null) throw new AggregateException("ไม่พบค่า ManualWorkDate Type กรุณาตรวจสอบ");
                    if (c.Times == null) throw new AggregateException("ไม่พบค่าเวลา กรุราตรวจสอบ");
                    if (c.Status == null) throw new AggregateException("ไม่พบค่า Status กรุราตรวจสอบ");
                }
                _manualWorkDateRepository.Update(manualWorkDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveManualWorkDate(ManualWorkDate manualWorkDate)
        {
            try
            {
                _manualWorkDateRepository.Remove(manualWorkDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<District> GetAllDistrict()
        {
            return _districtRepository.GetAll();
        }

        public IList<TransactionLog> GetAllTransactionLogs()
        {
            return _transactionLogRepository.GetAll();
        }

        public IList<TransactionLog> GetTransactionLogByTable_Id(string table_ID)
        {
            return _transactionLogRepository.GetList(e => e.Table_ID == table_ID, e => e.Table);
        }

        public void AddTransactionLog(params TransactionLog[] transactionLog)
        {
            try
            {
                if (transactionLog.Count() < 1) throw new ArgumentException("Please input datas");

                foreach (TransactionLog c in transactionLog)
                {
                    if (c.TransactionType_ID == null) throw new ArgumentException("กรุณาระบุ Transaction Type");
                    if (c.ModifiedUser == null) throw new AggregateException("กรุณาระบุ user");
                }
                _transactionLogRepository.Add(transactionLog);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Table> GetAllTables()
        {
            return _tableRepository.GetAll();
        }

        public IList<SubDistrict> getAllSubDistrict()
        {
            return _subDistrictRepository.GetAll();
        }

        public Organization GetOrganizationByOrganizationID(string organizationID)
        {
            return _organizationRepository.GetSingle(e => e.Organization_ID == organizationID, e => e.OrganizationUnit);
        }

        public IList<PositionOrganization> GetPositionOrganizationByOrganization_ID(string organization_ID)
        {
            return _positionOrganizationRepository.GetList(e => e.Organization_ID == organization_ID, e => e.Organization, e => e.Organization.OrganizationUnit,e=>e.Position,e=>e.PositionType,e=>e.JobTitle);
        }


        public PositionOrganization GetPositionOrganizationRootNode()
        {
            return _positionOrganizationRepository.GetSingle(e => e.STECPositionNode_ID == null);
        }


        public IList<EmployeePosition> GetEmployeePostionByPositionOrganizationID(string positionOrganization_ID)
        {
            return _employeePositionRepository.GetList(e => e.PositionOrganization_ID == positionOrganization_ID);
        }

      
        void IHRISBusinessLayer.AddAddressType(params AddressType[] addressTypes)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateAddressType(AddressType addressTypes)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveAddressType(AddressType addressType)
        {
            throw new NotImplementedException();
        }


        void IHRISBusinessLayer.AddAddress(params Address[] address)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateAddress(Address address)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveAddress(string addressType, string person_ID)
        {
            throw new NotImplementedException();
        }

      

        void IHRISBusinessLayer.AddBlood(params Blood[] blood)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateBlood(Blood blood)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveBlood(Blood blood)
        {
            throw new NotImplementedException();
        }

       

        void IHRISBusinessLayer.AddCropSetup(params CropSetup[] cropSetup)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateCropSetup(CropSetup cropSetup)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveCropSetup(CropSetup cropSetup)
        {
            throw new NotImplementedException();
        }

     

        void IHRISBusinessLayer.AddHoliday(params Holiday[] holiday)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateHoliday(Holiday holiday)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveHoliday(Holiday holiday)
        {
            throw new NotImplementedException();
        }

       

        void IHRISBusinessLayer.AddLeaveType(params LeaveType[] leaveType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateLeaveType(LeaveType leaveType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveLeaveType(LeaveType leaveType)
        {
            throw new NotImplementedException();
        }

        //IList<LeaveTypeBenefit> IHRISBusinessLayer.GetLeaveTypeBenefitsByPositionType(string positionType_ID)
        //{
        //    throw new NotImplementedException();
        //}

        //void IHRISBusinessLayer.AddLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits)
        //{
        //    throw new NotImplementedException();
        //}

        //void IHRISBusinessLayer.UpdateLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits)
        //{
        //    throw new NotImplementedException();
        //}

        //void IHRISBusinessLayer.RemoveLeaveTypeBenefits(LeaveTypeBenefit leaveTypeBenefits)
        //{
        //    throw new NotImplementedException();
        //}

       

        void IHRISBusinessLayer.AddShift(params Shift[] shift)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateShift(Shift shift)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveShift(Shift shift)
        {
            throw new NotImplementedException();
        }

      

        void IHRISBusinessLayer.AddShiftControl(params ShiftControl[] shiftControl)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateShiftControl(params ShiftControl[] shiftControl)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveShiftControl(ShiftControl shiftControl)
        {
            throw new NotImplementedException();
        }


        void IHRISBusinessLayer.AddBranch(params Branch[] branch)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateBranch(Branch branch)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveBranch(Branch branch)
        {
            throw new NotImplementedException();
        }

       
        void IHRISBusinessLayer.AddJobTitle(params JobTitle[] jobTitle)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateJobTitle(JobTitle jobTitle)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveJobTitle(JobTitle jobTitle)
        {
            throw new NotImplementedException();
        }

      

        void IHRISBusinessLayer.AddPositionType(params PositionType[] positionType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdatePositionType(PositionType positionType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemovePositionType(PositionType positionType)
        {
            throw new NotImplementedException();
        }

       

        void IHRISBusinessLayer.AddPosition(params Position[] position)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdatePosition(Position position)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemovePosition(Position position)
        {
            throw new NotImplementedException();
        }

       

        void IHRISBusinessLayer.AddPositionOrganization(params PositionOrganization[] positionOrganization)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdatePositionOrganization(params PositionOrganization[] positionOrganization)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemovePositionOrganization(PositionOrganization positionOrganization)
        {
            throw new NotImplementedException();
        }

      

        void IHRISBusinessLayer.AddOrganizationUnit(params OrganizationUnit[] organizationUnit)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.updateOrganizationUnit(OrganizationUnit organizationUnit)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveOrganizationUnit(OrganizationUnit organizationUnit)
        {
            throw new NotImplementedException();
        }

        
        void IHRISBusinessLayer.AddOrganizationType(params OrganizationType[] organizationType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateOrganizationType(OrganizationType organizationType)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveOrganizationType(OrganizationType organizationType)
        {
            throw new NotImplementedException();
        }

        
        void IHRISBusinessLayer.AddOrganization(params Organization[] organization)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateOrganization(Organization organization)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveOrganization(Organization organization)
        {
            throw new NotImplementedException();
        }

      

        void IHRISBusinessLayer.AddPerson(params Person[] person)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdatePerson(Person person)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemovePerson(Person person)
        {
            throw new NotImplementedException();
        }

        

        void IHRISBusinessLayer.AddEmployee(params Employee[] employee)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveEmployee(Employee employee)
        {
            throw new NotImplementedException();
        }

       

        void IHRISBusinessLayer.AddEmployeePosition(params EmployeePosition[] employeePosition)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.UpdateEmployeePosition(params EmployeePosition[] employeePosition)
        {
            throw new NotImplementedException();
        }

        void IHRISBusinessLayer.RemoveEmployeePosition(EmployeePosition employeePosition)
        {
            throw new NotImplementedException();
        }


        public OT GetOTByEmployeeOneRow(string employee_ID, DateTime ot_Date, string ot_Type)
        {
            return _otRepository.GetSingle(c => c.Employee_ID == employee_ID &&
                                                c.OTDate >= ot_Date &&
                                                c.OTDate <= ot_Date &&
                                                c.OTType == ot_Type);
        }
        public OT GetOTByEmployeeOneRow(string employee_ID, DateTime ot_Date, string ot_Type, string requestType)
        {
            return _otRepository.GetSingle(c => c.Employee_ID == employee_ID &&
                                                c.OTDate >= ot_Date &&
                                                c.OTDate <= ot_Date &&
                                                c.RequestType == requestType &&
                                                c.OTType == ot_Type);
        }

        public IList<MaritalStatu> GetAllMaritalStatus()
        {
            return _maritalStatusRepository.GetAll();
        }

        public MaritalStatu GetMaritalStatusByID(string maritalStatus_ID)
        {
            return _maritalStatusRepository.GetSingle(c => c.MaritalStatus_ID == maritalStatus_ID);
        }


        public IList<Nationality> GetAllNationality()
        {
            return _nationalityRepository.GetAll();
        }

        public Nationality GetNationalityByID(string nationality_ID)
        {
            return _nationalityRepository.GetSingle(e => e.Nationality_ID == nationality_ID);
        }

        public IList<Race> GetAllRace()
        {
            return _raceRepository.GetAll();
        }

        public Race GetRaceByID(string race_ID)
        {
            return _raceRepository.GetSingle(e => e.Race_ID == race_ID);
        }


        public IList<Religion> GetAllReligion()
        {
           return _religionRepository.GetAll();
        }

        public Religion GetReligionByID(string religion_ID)
        {
            return _religionRepository.GetSingle(e => e.Religion_ID == religion_ID);
        }


        public Blood GetBloodByID(string blood_ID)
        {
            return _bloodRepository.GetSingle(e => e.Blood_ID == blood_ID);
        }

        public IList<Education> GetAllEducations()
        {
            return _educationRepository.GetAll(e=>e.EducationLevel ,e=>e.EducationQualification , e=>e.Person);
        }

        public Education GetEducationByID(string person_ID,string educationLevel_ID,string educationQualification_ID)
        {
            return _educationRepository.GetSingle(e => e.Person_ID == person_ID && e.EducationLevel_ID == educationLevel_ID && e.EducationQualification_ID == educationQualification_ID);
        }

        public void AddEducation(params Education[] education)
        {
            try
            {
                if (education.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<Education> query1 = GetAllEducations();
                foreach (Education c in  education)
                {
                    //check if exits.
                    if (query1.Where(e => e.Person_ID == c.Person_ID  && e.EducationLevel_ID == c.EducationLevel_ID && e.EducationQualification_ID == c.EducationQualification_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate value");
                    }
                    if (c.Person_ID == null || c.Person_ID == "")
                        throw new ArgumentException("Please input person");

                    if (c.EducationLevel_ID == null  ||  c.EducationLevel_ID == "")
                        throw new ArgumentException("Please input education level");

                    if (c.EducationQualification_ID == null || c.EducationQualification_ID == "")                   
                        throw new ArgumentException("Please input education qualification");

                    if (c.AdmissionDate == null)
                        throw new ArgumentException("Please input addmission date");

                    if (c.GraduatedDate == null)
                        throw new ArgumentException("Please input graduated date ");

                    if (c.GPA == null  )
                        throw new ArgumentException("Please input GPA");

                }

                _educationRepository.Add(education);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEducation(Education education)
        {
            try
            {
                Education query1 = GetEducationByID(education.Person_ID, education.EducationLevel_ID, education.EducationQualification_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (education.Person_ID == null || education.Person_ID == "")
                    throw new ArgumentException("Please input person id");
                if (education.EducationLevel_ID == null || education.EducationLevel_ID == "")
                    throw new ArgumentException("Please input education level");
                if (education.EducationQualification_ID == null || education.EducationQualification_ID == "")
                    throw new ArgumentException("Please input education qualification");
                if(education.AdmissionDate == null)
                    throw new ArgumentException("Please input addmission date");
                if(education.GraduatedDate == null)
                    throw new ArgumentException("Please input graduated date");
                if(education.GPA == null )
                    throw new ArgumentException("Please input GPA");
                _educationRepository.Update(education);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveEducation(Education education)
        {
            try
            {
                Education query1 = GetEducationByID(education.Person_ID, education.EducationLevel_ID, education.EducationQualification_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _educationRepository.Remove(education);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<EducationLevel> GetAllEducationLevels()
        {
            return _educationLevelRepository.GetAll();
        }

        public EducationLevel GetEducationLevelByID(string educationLevel_ID)
        {
            return _educationLevelRepository.GetSingle(e => e.EducationLevel_ID == educationLevel_ID);
        }

        public IList<EducationQualification> GetAllEducationQualifications()
        {
            return _educationQualificationRepository.GetAll();
        }

        public EducationQualification GetEducationQualificationByID(string educationQualification_ID)
        {
            return _educationQualificationRepository.GetSingle(e => e.EducationQualification_ID == educationQualification_ID);
        }

        public void AddEducationQualification(params EducationQualification[] educationQualification)
        {
            try
            {
                if (educationQualification.Count()< 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<EducationQualification> query1 = GetAllEducationQualifications();
                foreach (EducationQualification c in educationQualification)
                {
                    //check if exits.
                    if (query1.Where(e => e.EducationQualification_ID == c.EducationQualification_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate value");
                    }
                    if (c.EducationQualification_ID == null || c.EducationQualification_ID == "")
                        throw new ArgumentException("Please input education ID");

                    if (c.EducationQualificaionTH == null || c.EducationQualificaionTH == "")
                        throw new ArgumentException("Please input education name");
                }

                _educationQualificationRepository.Add(educationQualification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEducationQualification(EducationQualification educationQualification)
        {
            try
            {
                EducationQualification query1 = GetEducationQualificationByID(educationQualification.EducationQualificaionTH);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (educationQualification.EducationQualificaionTH == null || educationQualification.EducationQualificaionTH == "")
                    throw new ArgumentException("Please input name ");

                _educationQualificationRepository.Update(educationQualification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveEducationQualification(EducationQualification educationQualification)
        {
            try
            {
                EducationQualification query1 = GetEducationQualificationByID(educationQualification.EducationQualificaionTH);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _educationQualificationRepository.Remove(educationQualification);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<Institute> GetAllInstitutes()
        {
            return _instituteRepository.GetAll();
        }

        public Institute GetInstituteByID(string institute_ID)
        {
            return _instituteRepository.GetSingle(e => e.Institute_ID == institute_ID);
        }

        public IList<Major> GetAllMajors()
        {
            return _majorRepository.GetAll();
        }

        public Major GetMajorByID(string major_ID)
        {
            return _majorRepository.GetSingle(e => e.Major_ID == major_ID);
        }

        public void AddMajor(params Major[] major)
        {
            try
            {
                if (major.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<Major> query1 = GetAllMajors();
                foreach (Major c in major)
                {
                    //check if exits.
                    if (query1.Where(e => e.Major_ID == c.Major_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate value");
                    }
                    if (c.EducationQualification_ID == null || c.EducationQualification_ID == "")
                        throw new ArgumentException("Please input education ID");

                    if(c.Major_ID == null || c.Major_ID == "")
                        throw new ArgumentException("Please input major ID");
                    if (c.MajorTH == null || c.MajorTH == "")
                        throw new ArgumentException("Please input major(TH)");
                }

                _majorRepository.Add(major);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void UpdateMajor(Major major)
        {
            try
            {
                Major query1 = GetMajorByID(major.Major_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (major.MajorTH == null || major.MajorTH == "")
                    throw new ArgumentException("Please input name ");

                _majorRepository.Update(major);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveMajor(Major major)
        {
            try
            {
                Major query1 = GetMajorByID(major.Major_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _majorRepository.Remove(major);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IList<Education> GetEducationByPersonID(string person_Id)
        {
            return _educationRepository.GetList(e => e.Person_ID == person_Id ,e=>e.Person,e=>e.EducationLevel,e=>e.EducationQualification,e=>e.Institute,e=>e.Major).OrderByDescending(e => e.GraduatedDate).ToList();
        }

        
        


        public IList<VitalStau> GetAllVitalStatuss()
        {
            return _vitalStatusRepository.GetAll();
        }

        public VitalStau GetVitalStatusByID(string vitalStatus_ID)
        {
            return _vitalStatusRepository.GetSingle(e => e.VitalStatus_ID == vitalStatus_ID);
        }

        public IList<FamilyMemberType> GetAllFamilyMemberTypes()
        {
            return _familyMemberTypeRepository.GetAll();
        }

        public FamilyMemberType GetFamilyMemberTypeByID(string familyMemberType_ID)
        {
            return _familyMemberTypeRepository.GetSingle(e => e.FamilyMemberType_ID == familyMemberType_ID);
        }

        public void AddFamilyMemberType(params FamilyMemberType[] familyMemeberType)
        {
            try
            {
                if (familyMemeberType.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<FamilyMemberType> query1 = GetAllFamilyMemberTypes();
                foreach (FamilyMemberType c in familyMemeberType)
                {
                    //check if exits.
                    if (query1.Where(e => e.FamilyMemberType_ID == c.FamilyMemberType_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate FamilyMemberType ID");
                    }


                    if (c.FamilyMembers == null)
                        throw new ArgumentException("Please input Family  type name");

                }

                _familyMemberTypeRepository.Add(familyMemeberType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFamilyMemberType(FamilyMemberType familyMemeberType)
        {
            try
            {
                FamilyMemberType query1 = GetFamilyMemberTypeByID(familyMemeberType.FamilyMemberType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (familyMemeberType.FamilyMemberType_ID == null || familyMemeberType.FamilyMemberType_ID == "")
                    throw new ArgumentException("Please input FamlilyType id");
                if (familyMemeberType.FamilyMembers == null )
                    throw new ArgumentException("Please input FamilyMember name");

                _familyMemberTypeRepository.Update(familyMemeberType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveFamilyMemberType(FamilyMemberType familyMemberType)
        {
            try
            {
                FamilyMemberType query1 = GetFamilyMemberTypeByID(familyMemberType.FamilyMemberType_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _familyMemberTypeRepository.Remove(familyMemberType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<FamilyMember> GetAllFamilyMembers()
        {
            return _familyMemberRepository.GetAll(e=>e.FamilyMemberType,e=>e.VitalStau,e=>e.Person,e=>e.TitleName);
        }

        public FamilyMember GetFamilyMemberByID(string familyMember_ID)
        {
            return _familyMemberRepository.GetSingle(e => e.FamilyMember_ID == familyMember_ID);
        }

        public void AddFamilyMember(params FamilyMember[] familyMember)
        {
            try
            {
                if (familyMember.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<FamilyMember> query1 = GetAllFamilyMembers();
                foreach (FamilyMember c in familyMember)
                {
                    //check if exits.
                    if (query1.Where(e=>e.FamilyMember_ID == c.FamilyMember_ID).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate FamilyMember ID");
                    }

                    if (c.FamilyMemberType_ID == null)
                        throw new ArgumentException("Please input Family  type ");
                    if (c.Person_ID == null)
                        throw new AggregateException("Please input Person ");
                    if (c.TitleName_ID == null)
                        throw new AggregateException("Please input Titlename");
                    if (c.FirstName == null)
                        throw new AggregateException("Please input Firstname");
                    if (c.LastName == null)
                        throw new AggregateException("Please input Lastname");

                }

                _familyMemberRepository.Add(familyMember);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFamilyMember(FamilyMember familyMember)
        {
            try
            {
                FamilyMember query1 = GetFamilyMemberByID(familyMember.FamilyMember_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (familyMember.FamilyMemberType_ID == null || familyMember.FamilyMemberType_ID == "")
                    throw new ArgumentException("Please input FamlilyType id");
                if (familyMember.Person_ID == null)
                    throw new ArgumentException("Please input Person");
                if (familyMember.Person_ID == null)
                    throw new AggregateException("Please input Person ");
                if (familyMember.TitleName_ID == null)
                    throw new AggregateException("Please input Titlename");
                if (familyMember.FirstName == null)
                    throw new AggregateException("Please input Firstname");
                if (familyMember.LastName == null)
                    throw new AggregateException("Please input Lastname");

                _familyMemberRepository.Update(familyMember);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveFamilyMember(FamilyMember familyMember)
        {
            try
            {
                FamilyMember query1 = GetFamilyMemberByID(familyMember.FamilyMember_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _familyMemberRepository.Remove(familyMember);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<FamilyMember> GetFamilyMemberByPerson_ID(string person_ID)
        {
            return _familyMemberRepository.GetList(e => e.Person_ID == person_ID, e => e.FamilyMemberType, e => e.VitalStau, e => e.Person, e => e.TitleName);
        }

        public IList<PayrollFlag> GetAllPayrollFlag()
        {
            return _payrollFlagRepository.GetAll();
        }

        public IList<PayrollFlag> GetPayrollFlagByID(string employee_ID)
        {
            return _payrollFlagRepository.GetList(e => e.Employee_ID == employee_ID );
        }

        public PayrollFlag GetPayrollFlagByIDAndvalidFrom(string employee_ID, DateTime validfrom)
        {
            return _payrollFlagRepository.GetSingle(e => e.Employee_ID == employee_ID && e.ValidFrom >= validfrom && e.ValidFrom <= validfrom, e => e.Employee, e => e.Employee.Person);
        }

        public void AddPayrollFlag(params PayrollFlag[] _payrollFlag)
        {
            try
            {
                if (_payrollFlag.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<PayrollFlag> query1 = GetAllPayrollFlag();
                foreach (PayrollFlag c in _payrollFlag)
                {
                    //check if exits.
                    if (query1.Where(e => e.Employee_ID == c.Employee_ID  && e.ValidFrom == c.ValidFrom ).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate employee");
                    }

                    if (c.Employee_ID == null)
                        throw new ArgumentException("Please input Employee ");
                    if (c.ValidFrom == null)
                        throw new AggregateException("Please input validfrom ");
                   
                    
                }
                _payrollFlagRepository.Add(_payrollFlag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePayrollFlag(PayrollFlag _payrollFlag)
        {
            try
            {
                PayrollFlag query1 = GetPayrollFlagByIDAndvalidFrom(_payrollFlag.Employee_ID, _payrollFlag.ValidFrom);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_payrollFlag.Employee_ID == null)
                    throw new ArgumentException("Please input Employee ");
                if (_payrollFlag.ValidFrom == null)
                    throw new AggregateException("Please input validfrom ");
               


                _payrollFlagRepository.Update(_payrollFlag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemovePayrollFlag(PayrollFlag _payrollFlag)
        {
            try
            {
                PayrollFlag query1 = GetPayrollFlagByIDAndvalidFrom(_payrollFlag.Employee_ID, _payrollFlag.ValidFrom);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _payrollFlagRepository.Remove(_payrollFlag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ContactPersonInfo> GetAllContactpersonInfos()
        {
            return _contactPersonInfoRepository.GetAll();
        }

        public IList<ContactPersonInfo> GetContactPersonInfoByPersonID(string personID)
        {
            return _contactPersonInfoRepository.GetList(e => e.Person_ID == personID,  e=>e.TitleName   ) ;
        }

        public ContactPersonInfo GetContactPersonInfoByID(string _contactPersonInfoID)
        {
            return _contactPersonInfoRepository.GetSingle(e => e.ContactPersonInfo_ID == _contactPersonInfoID, e => e.TitleName);
        }

        public void AddContactPersonInfo(params ContactPersonInfo[] _contactPersonInfo)
        {
            try
            {
                if (_contactPersonInfo.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }                                
                foreach (ContactPersonInfo c in _contactPersonInfo)
                   {                         
                       if (c.Person_ID == null)
                           throw new ArgumentException("Please input person ");
                       if (c.Relation == null)
                           throw new AggregateException("Please input relation ");
                       if (c.Title_ID == null)
                           throw new AggregateException("Please input titlename ");
                        if (c.FirstName == null)
                           throw new AggregateException("Please input firstname ");
                        if (c.LastName == null)
                            throw new AggregateException("Please input lastname ");
                        
                   }
                 _contactPersonInfoRepository.Add(_contactPersonInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateContactPersonInfo(ContactPersonInfo _contactPersonInfo)
        {
            try
            {
                ContactPersonInfo query1 = GetContactPersonInfoByID(_contactPersonInfo.ContactPersonInfo_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_contactPersonInfo.Person_ID == null)
                    throw new ArgumentException("Please input person ");
                if (_contactPersonInfo.Relation == null)
                    throw new AggregateException("Please input relation ");
                if (_contactPersonInfo.Title_ID == null)
                    throw new AggregateException("Please input titlename ");
                if (_contactPersonInfo.FirstName == null)
                    throw new AggregateException("Please input firstname ");
                if (_contactPersonInfo.LastName == null)
                    throw new AggregateException("Please input lastname ");
                

                _contactPersonInfoRepository.Update(_contactPersonInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveContactPersonInfo(ContactPersonInfo _contactPersonInfo)
        {
            try
            {
                ContactPersonInfo query1 = GetContactPersonInfoByID(_contactPersonInfo.ContactPersonInfo_ID);

                _contactPersonInfoRepository.Remove(_contactPersonInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<OriginalWorkHistory> GetAllOriginalWorkHistorys()
        {
            return _originalWorkHistoryRepository.GetAll();
        }

        public IList<OriginalWorkHistory> GetOriginalWorkHistoryByPersonID(string personID)
        {
            return _originalWorkHistoryRepository.GetList(e => e.Person_ID == personID);
        }
        public OriginalWorkHistory GetOriginalWorkHistoryByID(string _orginalWorkHistoryID)
        {
            return _originalWorkHistoryRepository.GetSingle(e => e.OrginalWork_ID == _orginalWorkHistoryID);
        }

        public void AddOriginalWorkHistory(params OriginalWorkHistory[] _originalWorkHistory)
        {
            try
            {
                if (_originalWorkHistory.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (OriginalWorkHistory c in _originalWorkHistory)
                {
                    if (c.Person_ID == null)
                        throw new ArgumentException("Please input person ");                    
                    if (c.CompanyName == null)
                        throw new AggregateException("Please input companyname ");
                    if (c.PositionName == null)
                        throw new AggregateException("Please input poitionname ");
                    if (c.JoinDate == null)
                        throw new AggregateException("Please input joindate ");
                    if (c.EndDate == null)
                        throw new AggregateException("Please input enddate ");

                }
                _originalWorkHistoryRepository.Add(_originalWorkHistory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOriginalWorkHistory(OriginalWorkHistory _originalWorkHistory)
        {
            try
            {
                OriginalWorkHistory query1 = GetOriginalWorkHistoryByID(_originalWorkHistory.OrginalWork_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_originalWorkHistory.Person_ID == null)
                    throw new ArgumentException("Please input person ");
                if (_originalWorkHistory.CompanyName == null)
                    throw new AggregateException("Please input companyname ");
                if (_originalWorkHistory.PositionName == null)
                    throw new AggregateException("Please input poitionname ");
                if (_originalWorkHistory.JoinDate == null)
                    throw new AggregateException("Please input joindate ");
                if (_originalWorkHistory.EndDate == null)
                    throw new AggregateException("Please input enddate ");
                _originalWorkHistoryRepository.Update(_originalWorkHistory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveOriginalWorkHistory(OriginalWorkHistory _originalWorkHistory)
        {
            try
            {
                OriginalWorkHistory query1 = GetOriginalWorkHistoryByID(_originalWorkHistory.OrginalWork_ID);

                _originalWorkHistoryRepository.Remove(_originalWorkHistory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public IList<TrainingCourse> GetAllTrainingCourses()
        {
            return _trainingCourseRepository.GetAll();
        }

        public TrainingCourse GetTrainingCourseByID(string _trainingCourse_ID)
        {
            return _trainingCourseRepository.GetSingle(e => e.TrainingCourse_ID == _trainingCourse_ID);
        }

        public void AddTrainingCourse(params TrainingCourse[] _trainingCourse)
        {
            try
            {
                if (_trainingCourse.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (TrainingCourse c in _trainingCourse)
                {
                    if (c.TrainingCourse_ID == null)
                        throw new ArgumentException("Please input tranining course ID ");
                    if (c.TrainingCourseNameTH == null || c.TrainingCourseNameTH == "" )
                        throw new AggregateException("Please input training course name ");
                   

                }
                _trainingCourseRepository.Add(_trainingCourse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTrainingCourse(TrainingCourse _trainingCourse)
        {
            try
            {
                TrainingCourse query1 = GetTrainingCourseByID(_trainingCourse.TrainingCourse_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_trainingCourse.TrainingCourseNameTH == null || _trainingCourse.TrainingCourseNameTH == "")
                    throw new ArgumentException("Please input training course name");
                _trainingCourseRepository.Update(_trainingCourse);                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveTrainingCourse(TrainingCourse _trainingCourse)
        {
            try
            {
                TrainingCourse query1 = GetTrainingCourseByID(_trainingCourse.TrainingCourse_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");              
                _trainingCourseRepository.Remove(_trainingCourse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Training> GetAllTrainings()
        {
            return _trainingRepository.GetAll(e => e.TrainingCourse);
        }

        public IList<Training> GetTrainingByEmployee(string _employeeID)
        {
            return _trainingRepository.GetList(e => e.Employee_ID == _employeeID, e => e.TrainingCourse);
        }

        public IList<Training> GetTrainingByTrainingCourse(string _trainingCourseID , DateTime beginDate , DateTime Enddate)
        {
            return _trainingRepository.GetList(e => e.TrainingCourse_ID == _trainingCourseID && e.BeginDate >= beginDate && e.EndDate <= Enddate , e => e.TrainingCourse, e=>e.Employee.Person , e=>e.Employee);
        }

        public void AddTraining(params Training[] _training)
        {
            try
            {
                if (_training.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (Training c in _training)
                {
                    if (c.TrainingCourse_ID == null)
                        throw new ArgumentException("Please input tranining course ID ");
                    if (c.Employee_ID == null)
                        throw new AggregateException("Please input employee");
                    if (c.BeginDate == null)
                        throw new AggregateException("Please input the begin of this training course");
                    if(c.EndDate == null)
                        throw new AggregateException("Please input the end of this training course");
                    if (Convert.ToDateTime(c.BeginDate) > Convert.ToDateTime(c.EndDate))
                        throw new AggregateException("The range of this training course not match");
                }
                _trainingRepository.Add(_training);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTraining(Training _training)
        {
            try
            {
                
                _trainingRepository.Update(_training);                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveTraining(Training _training)
        {
            try
            {

                _trainingRepository.Remove(_training);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<SSOHospitalList> GetAllSSOHospitalLists()
        {
           return _ssoHospitalListRepository.GetAll();
        }

        public SSOHospitalList GetSSOHospitalListByID(string _ssoHospitallist_ID)
        {
            return _ssoHospitalListRepository.GetSingle(e => e.SSOHospital_ID == _ssoHospitallist_ID);
        }

        public void AddSSOHospitalList(params SSOHospitalList[] _ssoHospitalList)
        {
            try
            {
                if (_ssoHospitalList.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (SSOHospitalList c in _ssoHospitalList)
                {
                    if (c.SSOHospital_ID== null)
                        throw new ArgumentException("Please input hospital list ID ");
                    if (c.HospitalNameTH == null)
                        throw new AggregateException("Please input the name of hospital in THAI");
                    if (c.Telephone == null)
                        throw new AggregateException("Please input the telephone number of this hospital");
                    
                }
                _ssoHospitalListRepository.Add(_ssoHospitalList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSSOHospitalList(SSOHospitalList _ssoHospitalList)
        {
            try
            {
                SSOHospitalList query1 = GetSSOHospitalListByID(_ssoHospitalList.SSOHospital_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_ssoHospitalList.HospitalNameTH == null )
                    throw new AggregateException("Please input the name of hospital in THAI");
                _ssoHospitalListRepository.Update(_ssoHospitalList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveSSOHospitalList(SSOHospitalList _ssoHospitalList)
        {
            try
            {
                SSOHospitalList query1 = GetSSOHospitalListByID(_ssoHospitalList.SSOHospital_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_ssoHospitalList.HospitalNameTH == null)
                    throw new AggregateException("Please input the name of hospital in THAI");
                _ssoHospitalListRepository.Remove(_ssoHospitalList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<SSOHospital> GetAllSSOHospitals()
        {
            return _ssoHospitalRepository.GetAll(e=>e.SSOHospitalList,e=>e.Employee,e=>e.Employee.Person,e=>e.Employee.Person.TitleName);
        }
       
        public IList<SSOHospital> GetSSOHospitaltByID(string _ssoHospital_ID)
        {
            return _ssoHospitalRepository.GetList(e => e.SSOHospital_ID == _ssoHospital_ID, e => e.Employee, e => e.Employee.Person, e => e.Employee.Person.TitleName);
        }

        public SSOHospital GetSSOHospitalBySSOHopsitalIDAndEmployeeID(string _ssoHospital_ID, string _employeeID)
        {
            return _ssoHospitalRepository.GetSingle(e => e.SSOHospital_ID == _ssoHospital_ID && e.Employee_ID == _employeeID, e => e.Employee, e => e.Employee.Person, e => e.Employee.Person.TitleName);
        }

        public void AddSSOHospital(params SSOHospital[] _ssoHospital)
        {
            try
            {
                if (_ssoHospital.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (SSOHospital c in _ssoHospital)
                {
                    if (c.SSOHospital_ID == null)
                        throw new ArgumentException("Please input hospital list ID ");
                    if (c.Employee_ID == null)
                        throw new AggregateException("Please input employee");
                    if (c.ValidFrom == null)
                        throw new AggregateException("Please input the date that temployee start to join this hospital");

                }
                _ssoHospitalRepository.Add(_ssoHospital);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSSOHospital(SSOHospital _ssoHospital)
        {
            try
            {
                SSOHospital query1 = GetSSOHospitalBySSOHopsitalIDAndEmployeeID(_ssoHospital.SSOHospital_ID, _ssoHospital.Employee_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_ssoHospital.Employee_ID == null)
                    throw new AggregateException("Please input employee ID");
                _ssoHospitalRepository.Update(_ssoHospital);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveSSOHospital(SSOHospital _ssoHospital)
        {
            try
            {
                SSOHospital query1 = GetSSOHospitalBySSOHopsitalIDAndEmployeeID(_ssoHospital.SSOHospital_ID, _ssoHospital.Employee_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (_ssoHospital.Employee_ID == null)
                    throw new AggregateException("Please input employee ID");
                _ssoHospitalRepository.Remove(_ssoHospital);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }







        public IList<OTOnlineSetupAdmin> GetAllOTOnlineSetupAdmins()
        {
            return _otOnlineRepository.GetAll(e=>e.Employee, e=>e.Employee.Person);
        }

        public OTOnlineSetupAdmin GetOTonlineSetuAdminByEmployee(string _employee_id)
        {
            return _otOnlineRepository.GetSingle(e => e.Employee_ID == _employee_id, e => e.Employee, e => e.Employee.Person);
        }

        public OTOnlineSetupAdmin GetOTOnlineSetupAdminByAdminID(int _adminID)
        {
            return _otOnlineRepository.GetSingle(e => e.Admin_ID == _adminID, e => e.Employee, e => e.Employee.Person);
        }

        public OTOnlineSetupAdmin GetOTOnlineSetupAdminByADName(string _adName)
        {
            return _otOnlineRepository.GetSingle(e => e.AdName == _adName, e => e.Employee);
        }

        public void AddOTOnlineSetupAdmin(params OTOnlineSetupAdmin[] _ot)
        {
            try
            {
                if (_ot.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (OTOnlineSetupAdmin c in _ot)
                {
                    //if (c.Admin_ID == null) throw new ArgumentException("Please input hospital admin ID ");
                    if (c.Employee_ID == null)
                        throw new AggregateException("Please input employee");
                    if(c.AdName == null)
                        throw new AggregateException("Please input account name ");
                    if (c.ValidFrom == null)
                        throw new AggregateException("Please input the date that start to join");

                }
                _otOnlineRepository.Add(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOTOnlineSetupAdmin( OTOnlineSetupAdmin _ot)
        {
            try
            {
                OTOnlineSetupAdmin query1 = GetOTOnlineSetupAdminByAdminID(_ot.Admin_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");
                if (query1.AdName == null)
                    throw new AggregateException("Please input account name ");

                _otOnlineRepository.Update(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveOTOnlineSetupAdmin(OTOnlineSetupAdmin _ot)
        {
            try
            {
                OTOnlineSetupAdmin query1 = GetOTOnlineSetupAdminByAdminID(_ot.Admin_ID);
                if (query1 == null)
                    throw new ArgumentException("Not value");

                _otOnlineRepository.Remove(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public IList<OTOnlineSetupAdminDetail> GetAllOTOnlineSetupAdminDetails()
        {
            return _otOnlineDetailRepository.GetAll(e => e.OTOnlineSetupAdmin
                , e => e.OTOnlineSetupAdmin.Employee
                , e => e.OTOnlineSetupAdmin.Employee.Person
                //,e=>e.OTOnlineSetupAdmin.Employee.Person.TitleName
                , e => e.Employee, e => e.Employee.Person
                //,e=>e.Employee.Person.TitleName);
                );
        }

        //public IList<OTOnlineSetupAdminDetail> GetOTonlineSetuAdminDetailByEmployee(string _employee_id)
        //{
        //    return _otOnlineDetailRepository.GetList(e=>e.emp)
        //}

        public IList<OTOnlineSetupAdminDetail> GetOTonLineSetupAdminDetailByAdminID(int _adminID)
        {
            return _otOnlineDetailRepository.GetList(e => e.Admin_ID == _adminID);
        }

        public IList<OTOnlineSetupAdminDetail> GetOTOnlineSetupAdminDetailbyReportTo(string _employee_ID)
        {
            return _otOnlineDetailRepository.GetList(e => e.ReportTo == _employee_ID);
        }

        public void AddOTOnlineSetupAdminDetail(params OTOnlineSetupAdminDetail[] _ot)
        {
            try
            {
                if (_ot.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }
                foreach (OTOnlineSetupAdminDetail c in _ot)
                {
                    //if (c.Admin_ID == null) throw new ArgumentException("Please input hospital admin ID ");
                    
                    if (c.ValidFrom == null)
                        throw new AggregateException("Please input the date that start to join");

                }
                _otOnlineDetailRepository.Add(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateOTOnlineSetupAdminDetail(OTOnlineSetupAdminDetail _ot)
        {
            try
            {


                _otOnlineDetailRepository.Update(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveOTOnlineSetupAdminDetail(OTOnlineSetupAdminDetail _ot)
        {
            try
            {


                _otOnlineDetailRepository.Remove(_ot);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public OTOnlineSetupAdminDetail GetOTOnlineSetupAdminDetailByAdminAndDeptID(int _admin, string _deptID)
        {
            return _otOnlineDetailRepository.GetSingle(e => e.Admin_ID == _admin && e.DeptID == _deptID);
        }

        public IDCardInfo GetIDCardByPerson_id(string person_ID)
        {
            return _idCardRepository.GetSingle(e => e.Person_ID == person_ID);
        }
    }
}
