﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;
using DataAccessLayer_HRISSystem;

namespace BusinessLayer_HRISSystem
{
  public  class HRISSTECPayrollBusinessLayer:IHRISSTECPayrollBusinesslayer
    {
      
      private readonly HRISSTECPayrollDataRepository.IOT_DetailRepository _ot_detailRepository;
      private readonly HRISSTECPayrollDataRepository.ILabour_workRepository _labour_workRepository;
      private readonly HRISSTECPayrollDataRepository.IEmployeeSTECPayrollRepository _employeeSTECpayrollRepository;
      private readonly HRISSTECPayrollDataRepository.ILot_NumberRepository _lot_NumberRepository;

      public HRISSTECPayrollBusinessLayer()
        {
            _ot_detailRepository = new HRISSTECPayrollDataRepository.OT_DetailRepository();
            _labour_workRepository = new HRISSTECPayrollDataRepository.Labour_workRepository();
            _employeeSTECpayrollRepository = new HRISSTECPayrollDataRepository.EmployeeSTECPayrollRepository();
            _lot_NumberRepository = new HRISSTECPayrollDataRepository.Lot_NumberRepostory();
        }


      public IList<OT_Detail> GetAllOT_Details()
      {
          return _ot_detailRepository.GetAllSTECPayroll();
      }

      public void AddOT_Detail(params OT_Detail[] ot_Details)
      {
          try
          {
              if ( ot_Details.Count() < 1)
              {
                  throw new ArgumentException("Please input datas");

              }

              IList<OT_Detail> query1 = GetAllOT_Details();
              foreach (OT_Detail c in ot_Details)
              {
                  if (c.Employee_id == null)
                      throw new ArgumentException("Please input employee_ID");

                  if (c.Lot_Month == null)
                      throw new AggregateException("Please input Month and Year");
                  

                  //check if exits.
                  if (query1.Where(e => e.Employee_id == c.Employee_id && e.Lot_Month == c.Lot_Month && e.Lot_Year == c.Lot_Year).Count() > 0)
                  {
                      throw new ArgumentException("Duplicate employee ");
                  }

              }
              _ot_detailRepository.AddSTECPayroll(ot_Details);

          }
          catch (Exception ex)
          {
              throw ex;
          }
      }

      public void RemoveOT_Detail(OT_Detail ot_Detail)
      {
          try
          {
              List<OT_Detail> query1 = GetAllOT_Details().Where(x => x.Employee_id == ot_Detail.Employee_id && x.Lot_Month == ot_Detail.Lot_Month && x.Lot_Year == ot_Detail.Lot_Year).ToList();
              if (query1 == null )
                  throw new ArgumentException("Not value");
              _ot_detailRepository.RemoveSTECPayroll(ot_Detail);
          }
          catch (Exception ex)
          {
              throw ex;
          };
      }

      public IList<Labour_work> GetAllLabour_Works()
      {
          return _labour_workRepository.GetAllSTECPayroll();
      }

      public void AddLabour_work(params Labour_work[] labour_work)
      {
          try
          {
              if ( labour_work.Count() < 1)
              {
                  throw new ArgumentException("Please input datas");

              }

              IList<Labour_work> query1 = GetAllLabour_Works();
              foreach (Labour_work c in labour_work)
              {
                  if (c.Employee_id == null)
                      throw new ArgumentException("Please input employee_ID");

                  if (c.Lot_Month == null)
                      throw new AggregateException("Please input Month and Year");


                  //check if exits.
                  if (query1.Where(e => e.Employee_id == c.Employee_id && e.Lot_Month == c.Lot_Month && e.Lot_Year == c.Lot_Year).Count() > 0)
                  {
                      throw new ArgumentException("Duplicate employee ");
                  }

              }
              _labour_workRepository.AddSTECPayroll(labour_work);

          }
          catch (Exception ex)
          {
              throw ex;
          }
      }

      public void RemoveLabour_work(Labour_work labour_work)
      {
          try
          {
              List<Labour_work> query1 = GetAllLabour_Works().Where(x => x.Employee_id == labour_work.Employee_id && x.Lot_Month == labour_work.Lot_Month && x.Lot_Year == labour_work.Lot_Year).ToList();
              if (query1 == null)
                  throw new ArgumentException("Not value");
              _labour_workRepository.RemoveSTECPayroll(labour_work);
          }
          catch (Exception ex)
          {
              throw ex;
          };
      }

      public IList<EmployeeSTECPayroll> GetAllEmployeeSTECPayrolls()
      {
          return _employeeSTECpayrollRepository.GetAllSTECPayroll();
      }

        public EmployeeSTECPayroll GetEmployeeSTECPayrollsByNoted(string Noted)
        {
            return _employeeSTECpayrollRepository.GetSingleSTECPayroll(g => g.Employee_id == Noted);
        }
      public void AddEmployeeSTEcPayroll(params EmployeeSTECPayroll[] employeeSTECPayroll)
      {
          try
          {
              if (employeeSTECPayroll.Count() < 1)
              {
                  throw new ArgumentException("Please input datas");

              }

              IList<EmployeeSTECPayroll> query1 = GetAllEmployeeSTECPayrolls();
              foreach (EmployeeSTECPayroll c in employeeSTECPayroll)
              {
                  if (c.Employee_id == null)
                      throw new ArgumentException("Please input employee_ID");

                  //if (c.Lot_Month == null)
                  //    throw new AggregateException("Please input Month and Year");


                  //check if exits.
                  if (query1.Where(e => e.Employee_id == c.Employee_id ).Count() > 0)
                  {
                      throw new ArgumentException("Duplicate employee ");
                  }

              }
              _employeeSTECpayrollRepository.AddSTECPayroll(employeeSTECPayroll);

          }
          catch (Exception ex)
          {
              throw ex;
          }
      }

     

      public void RemoveEmployeeSTECPayroll(EmployeeSTECPayroll employeeSTECPayroll)
      {
          try
          {
              List<EmployeeSTECPayroll> query1 = GetAllEmployeeSTECPayrolls().Where(e => e.Employee_id == employeeSTECPayroll.Employee_id).ToList();            
              if (query1 == null)
                  throw new ArgumentException("Not value");
              _employeeSTECpayrollRepository.RemoveSTECPayroll(employeeSTECPayroll);

          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      public void UpdateEmployeeSTECPayroll(params EmployeeSTECPayroll[] employeeSTECPayroll)
      {
          try
          {
              if (employeeSTECPayroll.Count() < 1)
              {
                  throw new ArgumentException("Please input datas");

              }

              IList<EmployeeSTECPayroll> query1 = GetAllEmployeeSTECPayrolls();
              foreach (EmployeeSTECPayroll c in employeeSTECPayroll)
              {
                  if (c.Employee_id == null)
                      throw new ArgumentException("Please input employee_ID");

                  //if (c.Lot_Month == null)
                  //    throw new AggregateException("Please input Month and Year");


                  ////check if exits.
                  //if (query1.Where(e => e.Employee_id == c.Employee_id).Count() > 0)
                  //{
                  //    throw new ArgumentException("Duplicate employee ");
                  //}

              }
              _employeeSTECpayrollRepository.UpdateSTECPayroll(employeeSTECPayroll);

          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


      public EmployeeSTECPayroll GetEmployeeSTECPayroll(string employee_ID)
      {
          return _employeeSTECpayrollRepository.GetSingleSTECPayroll(p => p.Employee_id == employee_ID);
      }

        public List<sp_GetPayrollEmployeeByPersonId_Result> GetEmployeePayrollByPersonID(string PersonID)
        {
            using (STEC_PayrollEntities payroll = new STEC_PayrollEntities())
            {
                return payroll.sp_GetPayrollEmployeeByPersonId(PersonID).ToList();
            }
        }

      public IList<Lot_Number> GetAllLot_Numbers()
      {
          return _lot_NumberRepository.GetAllSTECPayroll();
      }

      public Lot_Number GetLot_Number(string lot_month, string lot_year)
      {
          return _lot_NumberRepository.GetSingleSTECPayroll(p => p.Lot_Month == lot_month && p.Lot_Year == lot_year);
      }

      public Lot_Number GetLotNumberByDate(DateTime  date)
      {
          return _lot_NumberRepository.GetSingleSTECPayroll(p => Convert.ToDateTime( p.Start_date) <= Convert.ToDateTime(date) && Convert.ToDateTime( p.Finish_date) >= Convert.ToDateTime(date));
      }

      public void UpdateLot_Number(params Lot_Number[] lot_Number)
      {
          try
          {
              if (lot_Number.Count() < 1)
              {
                  throw new ArgumentException("Please input datas");

              }

              IList<Lot_Number> query1 = GetAllLot_Numbers();
              foreach (Lot_Number c in lot_Number)
              {
                  if (c.Lot_Month == null && c.Lot_Year == null)
                  {
                      throw new ArgumentException("Please input data");                     
                  }                  
              }

              _lot_NumberRepository.UpdateSTECPayroll(lot_Number);            
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }


        public OT_Detail GetOT_DetailByLot(string lot_Month, string lot_Year, string employee_ID)
        {
            return _ot_detailRepository.GetSingleSTECPayroll(e => e.Lot_Month == lot_Month && e.Lot_Year == lot_Year && e.Employee_id == employee_ID);
        }
        public List<OT_Detail> GetOT_DetailByLot_List(string lot_Month, string lot_Year)
        {
            return _ot_detailRepository.GetAllSTECPayroll().Where(e => e.Lot_Month == lot_Month && e.Lot_Year == lot_Year).ToList();
        }


        public Labour_work GetLabour_workByLot(string lot_Month, string Lot_Year, string employee_ID)
        {
            return _labour_workRepository.GetSingleSTECPayroll(e => e.Lot_Month == lot_Month && e.Lot_Year == Lot_Year && e.Employee_id == employee_ID);
        }
        public List<Labour_work> GetLabour_workByLot_List(string lot_Month, string Lot_Year)
        {
            return _labour_workRepository.GetAllSTECPayroll().Where(e => e.Lot_Month == lot_Month && e.Lot_Year == Lot_Year).ToList();
        }

        public List<BusinessObject.LabourWorkingDateClass> GetLabourWorkingDate(DateTime dateFrom, DateTime dateTo)
      {         
          STECPayrollDataSet.sp_GetWorkingDateToPayrollDataTable sp_GetWorkingDateToPayrollDataTable = new STECPayrollDataSet.sp_GetWorkingDateToPayrollDataTable();
          DataAccessLayer_HRISSystem.STECPayrollDataSetTableAdapters.sp_GetWorkingDateToPayrollTableAdapter sp_GetWorkingDateToPayrollTableAdapter = new DataAccessLayer_HRISSystem.STECPayrollDataSetTableAdapters.sp_GetWorkingDateToPayrollTableAdapter();
          sp_GetWorkingDateToPayrollTableAdapter.Fill(sp_GetWorkingDateToPayrollDataTable, dateFrom, dateTo);
          List<BusinessObject.LabourWorkingDateClass> labourWorkingDateList = new List<BusinessObject.LabourWorkingDateClass>();
          foreach(STECPayrollDataSet.sp_GetWorkingDateToPayrollRow item  in sp_GetWorkingDateToPayrollDataTable)
          {
              labourWorkingDateList.Add(new BusinessObject.LabourWorkingDateClass
              {
                  Employee_ID = item.EMPLOYEE_ID,
                  FingerScanID = Convert.ToString( item.FINGERSCANID),
                  AccCode = item.PAYROLL_ID,
                  TitleNameTH = "",
                  FirstNameTH = item.FNAME,
                  LastNameTH = item.LNAME,
                  NWorking = item.NWORKING_DATE,
                  Payroll_Date = ""
              });
          }
          return labourWorkingDateList;
      }


      public List<BusinessObject.DepartMentFromSTECPayrollClass> GetDepartmentFromSTECPayroll()
      {
          STECPayrollDataSet.sp_GetDepartmentDataTable sp_GetDepartmentDataTable = new STECPayrollDataSet.sp_GetDepartmentDataTable();
          DataAccessLayer_HRISSystem.STECPayrollDataSetTableAdapters.sp_GetDepartmentTableAdapter sp_GetDepartmentTableAdapter = new DataAccessLayer_HRISSystem.STECPayrollDataSetTableAdapters.sp_GetDepartmentTableAdapter();
          sp_GetDepartmentTableAdapter.Fill(sp_GetDepartmentDataTable);
          List<BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessObject.DepartMentFromSTECPayrollClass>();
          foreach (STECPayrollDataSet.sp_GetDepartmentRow item in sp_GetDepartmentDataTable)
          {
              departmentFromSTECPayrollList.Add(new BusinessObject.DepartMentFromSTECPayrollClass
              {
                  DeptCode = item.Dept_code,
                  DeptName = item.Dept_name
              });
          }
          return departmentFromSTECPayrollList;           
      }


      
    }
}
