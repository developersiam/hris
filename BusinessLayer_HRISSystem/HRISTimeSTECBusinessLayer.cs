﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;
using DataAccessLayer_HRISSystem;

namespace BusinessLayer_HRISSystem
{
    
   public class HRISTimeSTECBusinessLayer : IHRISTimeSTECBusinessLayer
    {
        private readonly HRISTimeSTECDataRepository.IUserInfoRepository _userInfoRepository;
        private readonly HRISTimeSTECDataRepository.IDepartmentTimeSTECRepository _departmentTimeSTECRepository;
        private readonly HRISTimeSTECDataRepository.IUserInfo_attareaRepository _userInfo_attareaTimeSTECRepository;
        private readonly HRISTimeSTECDataRepository.ICheckInOutRepository _checkInoutRepository;

        public HRISTimeSTECBusinessLayer()
        {
            _userInfoRepository = new HRISTimeSTECDataRepository.UserInfoRepository();
            _departmentTimeSTECRepository = new HRISTimeSTECDataRepository.DepartmentTimeSTECRepository();
            _userInfo_attareaTimeSTECRepository = new HRISTimeSTECDataRepository.UserInfo_attreaRepository();
            _checkInoutRepository = new HRISTimeSTECDataRepository.CheckInOutRepository();
        }
        public void AddUserInfo(params userinfo[] userInfo)
        {
            try
            {
                if (userInfo.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<userinfo> query1 = GetAllUserInfos();
                foreach (userinfo c in userInfo)
                {
                    //check if exits.
                    if (query1.Where(e => e.badgenumber == c.badgenumber).Count() > 0) throw new ArgumentException("Duplicate fingerScan code");
                    if (c.badgenumber == null) throw new ArgumentException("Please input fingerScan code");
                    if (c.defaultdeptid == null) throw new AggregateException("Please input deptID");
                    if (c.name == null) throw new AggregateException("Please input name");
                }

                _userInfoRepository.AddTimeSTEC(userInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<userinfo> GetAllUserInfos()
        {
            return _userInfoRepository.GetAllTimeSTEC();
        }

        //public IList<userinfo> GetMaxUserInfo()
        //{
        //    //return _userInfoRepository.GetAllTimeSTEC().Max(c=>c.badgenumber);
        //}

        
        public userinfo GetUserInfoByBadGenNumber(int BadGenNumber)
        {
            return _userInfoRepository.GetSingleTimeSTEC(e => Convert.ToInt32( e.badgenumber) ==  BadGenNumber);
        }

        public void UpdateUserInfo(userinfo userInfo)
        {
            try
            {
                userinfo query1 = GetUserInfoByBadGenNumber(Convert.ToInt32( userInfo.badgenumber));
                if (query1 == null)
                    throw new ArgumentException("Not value");

                if (userInfo.badgenumber == null || userInfo.badgenumber == "")
                    throw new ArgumentException("Please input fingerScan code");
                if (userInfo.defaultdeptid == null )
                    throw new ArgumentException("Please input department");
                if (userInfo.name == null || userInfo.name == "")
                    throw new ArgumentException("Please input name");

                _userInfoRepository.UpdateTimeSTEC(userInfo);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public department GetDefaultDeptID(string dept_code)
        {
            return _departmentTimeSTECRepository.GetSingleTimeSTEC(e => e.code == dept_code);
        }

        public void AddUserInfo_att(params userinfo_attarea[] userInfo_attarea)

        {
            try
            {
                if (userInfo_attarea.Count() < 1)
                {
                    throw new ArgumentException("Please input datas");

                }

                IList<userinfo_attarea> query1 = GetAllUserInfo_attarea();
                foreach (userinfo_attarea c in userInfo_attarea)
                {
                    //check if exits.
                    if (query1.Where(e => e.employee_id == c.employee_id).Count() > 0)
                    {
                        throw new ArgumentException("Duplicate userID");
                    }

                    //if (c.area_id )
                    //    throw new ArgumentException("Please input areaID");

                    //if (c.defaultdeptid == null)
                    //    throw new AggregateException("Please input deptID");
                    //if (c.name == null)
                    //    throw new AggregateException("Please input name");

                }

                _userInfo_attareaTimeSTECRepository.AddTimeSTEC(userInfo_attarea);
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
     
        public IList<userinfo_attarea> GetAllUserInfo_attarea()
        {
            return _userInfo_attareaTimeSTECRepository.GetAllTimeSTEC();
        }


        //public List<BusinessObject.LabourWorkClass> GetLabourWork(DateTime dateFrom, DateTime dateTo,string Lot_Month,string Lot_Year)
        //{
        //    HRISSystemDataSet hrisSystemDataSet = new HRISSystemDataSet();
        //    HRISSystemDataSet.sp_GetEmployeeTimeAttendanceByDate1DataTable sp_GetEmployeeTimeAttendanceByDate1DataTable = new HRISSystemDataSet.sp_GetEmployeeTimeAttendanceByDate1DataTable();

        //    DataAccessLayer_HRISSystem.HRISSystemDataSetTableAdapters.sp_GetEmployeeTimeAttendanceByDate1TableAdapter sp_GetEmployeeTimeAttendanceByDate1TableAdapter = new DataAccessLayer_HRISSystem.HRISSystemDataSetTableAdapters.sp_GetEmployeeTimeAttendanceByDate1TableAdapter();
        //    sp_GetEmployeeTimeAttendanceByDate1TableAdapter.Fill(sp_GetEmployeeTimeAttendanceByDate1DataTable, dateFrom, dateTo);



        //    List<BusinessObject.LabourWorkClass> laberWorkList = new List<BusinessObject.LabourWorkClass>();

        //    foreach(HRISSystemDataSet.sp_GetEmployeeTimeAttendanceByDate1Row item in sp_GetEmployeeTimeAttendanceByDate1DataTable)
        //    {
        //        laberWorkList.Add(new BusinessObject.LabourWorkClass
        //        {
        //            Employee_ID = item.Employee_ID,
        //            FingerScanID = Convert.ToString( item.FINGERSCANCODE),
        //            AccCode = item.ACC_ID,
        //            TitleNameTH = item.TITLENAMETH,
        //            FirstNameTH = item.FIRSTNAMETH ,
        //            LastNameTH = item.LASTNAMETH,
        //            LotMonth = Lot_Month,
        //            LotYear = Lot_Year,
        //            NWorking = 0
        //        });
        //    }
        //    return laberWorkList;
        //}
       public userinfo GetUserInfoByFingerID(int fingerScan)
        {
            return _userInfoRepository.GetSingleTimeSTEC(e => Convert.ToInt32( e.badgenumber) == Convert.ToInt32( fingerScan));
        }
       
        public IList<checkinout> getCheckInOut(int _fingerscan ,DateTime datetime)
        {
            // BXUO190360022 IS BACKSCAN * BXUO190360026 IS FRONTSCAN
            return _checkInoutRepository.GetListTimeSTEC(e => e.userid == _fingerscan  && 
                                                              e.checktime >= datetime  && 
                                                              (e.sn_name != "BXUO190360026" && e.sn_name != "BXUO190360022") &&
                                                              e.checktime <  datetime.AddDays(1)).ToList();
        }


        public IList<department> GetAllDepartments()
        {
            return _departmentTimeSTECRepository.GetAllTimeSTEC();
        }

        public department GetDepartmentByCode(string _code)
        {
            return _departmentTimeSTECRepository.GetSingleTimeSTEC(e => e.code == _code);
        }
    }
}
