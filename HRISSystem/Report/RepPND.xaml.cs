﻿using System;
using System.Windows;
using System.Windows.Controls;
using BusinessLayer_HRISSystem.BusinessLogic;
using BusinessLayer_HRISSystem.BusinessObject;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Spire.Xls;
using System.Data;
using OfficeOpenXml;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepPND.xaml
    /// </summary>
    public partial class RepPND : Page
    {
        IEmployeeBL empBL = new EmployeeBL();
        List<PNDViewModel> pndList = new List<PNDViewModel>();
        List<PNDViewModel> pnd3_List = new List<PNDViewModel>();

        public RepPND()
        {
            InitializeComponent();
            BindPayrollLot();
        }

        private void BindPayrollLot()
        {
            try
            {
                var lotnumberList = new Lot_NumberClass().getLot_Number(DateTime.Now.Year);
                if (lotnumberList == null) return;
                PayrollLotCombobox.ItemsSource = lotnumberList;
                PayrollLotCombobox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PayrollLotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                FileUploadTextbox.Text = "";
                if (PayrollLotCombobox.SelectedValue == null) { MessageBox.Show("กรุณาเลือกงวด"); return; }
                pndList = new List<PNDViewModel>();
                pndList = reCalulate(empBL.getPND_by_PayrollLot(PayrollLotCombobox.Text));
                PndDatagrid.ItemsSource = null;
                PndDatagrid.ItemsSource = pndList;
                DataAmountTextbox.Text = pndList == null ? "0" : pndList.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<PNDViewModel> reCalulate(List<PNDViewModel> list)
        {
            if (list == null) return null;
            List<PNDViewModel> collection = new List<PNDViewModel>();
            foreach (var i in list)
            {
                PNDViewModel vmodel = new PNDViewModel();
                vmodel = i;
                vmodel.totalActualSalary = i.actualSalary;
                vmodel.totalIncomeTax = i.incomeTax;
                collection.Add(vmodel);
            }
            return collection;
        }

        private void ExportToTextButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pndList == null || !pndList.Any()) return;

                System.Windows.Forms.SaveFileDialog fileDialog = new System.Windows.Forms.SaveFileDialog();
                fileDialog.Filter = "Data Files (*.txt)|*.txt";
                fileDialog.DefaultExt = "txt";
                fileDialog.AddExtension = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    using (var writer = new StreamWriter(fileDialog.FileName))
                    {
                        foreach (var p in pndList)
                        {
                            string pndText = p.pndIndex + "|" + p.taxType + "|" + p.citizenID + "||" +
                                            p.titleName + "|" + p.employeeName + "|" + p.paidDate.Value.ToString("dd-MM-yyyy") + "|" +
                                            string.Format("{0:n}", p.totalActualSalary.Value) + "|" + string.Format("{0:n}", p.totalIncomeTax.Value) + "|" +
                                            p.revenueCondition;
                            writer.WriteLine(pndText);
                        }
                    }
                    MessageBox.Show("Export ข้อมูลสำเร็จ", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(Path.GetDirectoryName(fileDialog.FileName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportToExcelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pndList == null || !pndList.Any()) return;

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = ExportDetail(folderDlg.SelectedPath, pndList);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export ข้อมูลสำเร็จ.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public string ExportDetail(string folderName, List<PNDViewModel> exportList)
        {
            string name = @"" + folderName + "\\Pnd1-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";

            FileInfo info = new FileInfo(name);
            if (info.Exists) File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ภงด.1");
                ws.Cells["A1"].Value = "Payroll Lot : " + PayrollLotCombobox.SelectedValue;
                ws.Cells["A2"].Value = "Export Date : " + DateTime.Now;
                ws.Cells["A3"].LoadFromCollection(exportList, true);
                ws.DeleteColumn(14); // Remove column เงื่อนไขภาษี
                ws.DeleteColumn(14); // Remove column สถานะ import
                ws.Protection.IsProtected = false;
                using (ExcelRange col = ws.Cells[4, 7, 3 + exportList.Count, 7]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            if (pndList != null) pndList.Clear();
            PndDatagrid.ItemsSource = null;
            DataAmountTextbox.Text = "0";
            FileUploadTextbox.Text = "";
            PayrollLotCombobox.SelectedIndex = -1;

            if (pnd3_List != null) pnd3_List.Clear();
            MisMatchedDatagrid.ItemsSource = null;
            MisMatchedTextbox.Text = "0";
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pndList == null || !pndList.Any()) { MessageBox.Show("กรุณาเลือกข้อมูลจากงวด", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning); return; }

                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileUploadTextbox.Text = fileDialog.FileName;
                    var excelList = ReadExcelFile(fileDialog.FileName);
                    pnd3_List = new List<PNDViewModel>();

                    if (excelList != null || excelList.Count > 0)
                    {
                        foreach (var i in excelList)
                        {
                            var pnd = pndList.FirstOrDefault(f => f.citizenID == i.citizenID);
                            if (pnd != null)
                            {
                                var _totalActualSalary = pnd.actualSalary + i.importedActualSalary;
                                var _totalIncomeTax = pnd.incomeTax + i.importedIncomeTax;

                                pnd.importedActualSalary = i.importedActualSalary;
                                pnd.importedIncomeTax = i.importedIncomeTax;
                                pnd.totalActualSalary = _totalActualSalary;
                                pnd.totalIncomeTax = _totalIncomeTax;
                                pnd.importStatus = true;
                            }
                            else
                            {
                                //Not Matched list
                                pnd3_List.Add(i);
                            }
                        }
                        PndDatagrid.ItemsSource = null;
                        PndDatagrid.ItemsSource = pndList;

                        /////Not Matched list
                        MisMatchedDatagrid.ItemsSource = null;
                        MisMatchedDatagrid.ItemsSource = pnd3_List;
                        MisMatchedTextbox.Text = pnd3_List.Count.ToString();
                    }
                    else { MessageBox.Show("กรุณาปิดไฟล์ Excel ที่กำลัง Upload.", "ไม่สามารถเปิดไฟล์ได้ !!", MessageBoxButton.OK, MessageBoxImage.Warning); return; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Math excel : " + ex.Message, "Alert!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public List<PNDViewModel> ReadExcelFile(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }

        public List<PNDViewModel> DataTableToList(DataTable Datable)
        {
            List<PNDViewModel> PndViewmodelList = new List<PNDViewModel>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row[0].ToString() == "") break;
                PNDViewModel model = new PNDViewModel();
                model.citizenID = row[0].ToString();
                try { model.importedIncomeTax = Convert.ToDecimal(row[1].ToString()); }
                catch (Exception) { model.importedIncomeTax = 0; }
                try { model.importedActualSalary = Convert.ToDecimal(row[2].ToString()); }
                catch (Exception) { model.importedActualSalary = 0; }
                PndViewmodelList.Add(model);
            }
            return PndViewmodelList;
        }

        private void ExportToPND3_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
