﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Data;



namespace DataAccessLayer_HRISSystem
{
    public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    {
        public void Add(params T[] items)
        {
           using (var context = new HRISSystemEntities())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Added;

                }
                context.SaveChanges();
            }
        }

        public IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            List<T> list;
            using (var context = new HRISSystemEntities() )
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList<T>();
            }
            return list;
        }

        public IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            List<T> list;
            using (var context = new HRISSystemEntities())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();
            }
            return list;
        }

        public T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            T item = null;
            using (var context = new HRISSystemEntities())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
            }
            return item;
        }

        public void Remove(params T[] items)
        {
            using (var context = new HRISSystemEntities())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                }
                context.SaveChanges();
            }
        }

        public void Update(params T[] items)
        {
            using (var context = new HRISSystemEntities())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                }
                context.SaveChanges();


                //try
                //{
                //    foreach (T item in items)
                //    {
                //        context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                //    }                     
                //    context.SaveChanges();
                //}
                //catch (DbUpdateConcurrencyException ex)
                //{

                //    // Update the values of the entity that failed to save from the store 
                //    ex.Entries.Single().Reload();
                //   foreach (T item in items)
                //   {
                //       context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                //   }
                //   context.SaveChanges();
                //}



            }
        }
    }
}
