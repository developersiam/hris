﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR020.xaml
    /// </summary>
    public partial class RepRPTHR020 : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private int XFingerScanCode;
        string xIDCard;
        string xPerson_ID;
        public RepRPTHR020()
        {
            InitializeComponent();
        }

        public RepRPTHR020(EmployeeCurrentInformation x3)
        {
            InitializeComponent();
            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                GetEmployeeInformation(x3);
                RdtByEmp.IsChecked = true;

                IDCardInfo idCardInfo = new IDCardInfo();
                idCardInfo = _hrisBusinessLayer.GetIDCardInfo(xPerson_ID).ToList().FirstOrDefault();
                xIDCard = idCardInfo.Card_ID;
            }
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtFirstNameTH.Text = x.xFirstNameTH + " " + x.xLastNameTH;
            XFingerScanCode = Convert.ToInt16(x.xFingerScan_ID);
            xPerson_ID = x.xPerson_ID;                     
        }  

        public RepRPTHR020(string employee_ID)
        {
            InitializeComponent();
            GetDataFromEmployee_ID(employee_ID);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null)
                {
                    NavigationService.RemoveBackEntry();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtFirstNameTH.Text = ex.xFirstNameTH + " " + ex.xLastNameTH;
                XFingerScanCode = Convert.ToInt16(ex.xFingerScan_ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("Report20"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DateFrom.Text == "" || DateTo.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (Convert.ToDateTime(DateFrom.Text) > Convert.ToDateTime(DateTo.Text))
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (RdtByEmp.IsChecked == true && txtEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ต้องการแสดงรายงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }
                _ReportViewer1.Reset();
                HRISReportDataset.sp_GetSSOWalfareDataTable sp_getSSOWalfareDatatable = new HRISReportDataset.sp_GetSSOWalfareDataTable();
                HRISReportDatasetTableAdapters.sp_GetSSOWalfareTableAdapter sp_getSSOWalfareTableAdapter = new HRISReportDatasetTableAdapters.sp_GetSSOWalfareTableAdapter();
                sp_getSSOWalfareTableAdapter.Fill(sp_getSSOWalfareDatatable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateTo.Text));
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource1.Name = "SSOWalfareDataSet";
                

                if (RdtAllEmp.IsChecked == true)
                {
                    reportDataSource1.Value = sp_getSSOWalfareDatatable;
                }
                else if (RdtByEmp.IsChecked == true)
                {
                    DataTable D2 = sp_getSSOWalfareDatatable.Clone();
                    string expression;
                    //expression = "Employee_ID = '" + txtEmployee_ID.Text + "'";
                    expression = "ID_card = '" + xIDCard + "' ";
                    DataRow[] result = sp_getSSOWalfareDatatable.Select(expression);
                    foreach (DataRow d in result)
                    {
                        D2.ImportRow(d);
                    }
                    reportDataSource1.Value = D2;
                }
                

                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR020.rdlc";
                _ReportViewer1.RefreshReport();

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtByEmp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtEmployee_ID.IsEnabled = true;
                txtEmployee_ID.Text = "";
                NavigateToSearchButton.IsEnabled = true;
                txtEmployee_ID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtAllEmp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtEmployee_ID.IsEnabled = false;
                txtEmployee_ID.Text = "";
                txtFirstNameTH.Text = "";
                NavigateToSearchButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
