﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_Organization.xaml
    /// </summary>
    public partial class Organization_Organization : Page

    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string xOrganization_ID ;      
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        TreeViewOrganizationModel TreeViewModel_After;
       
        public Organization_Organization ()
        {
            InitializeComponent();          
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                DateTime xFromDatetime;
                DateTime xEndDatetime;
                ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                xFromDatetime = Convert.ToDateTime( ValidFromDatePicker.Text);
                ValidFromDatePicker.Text = Convert.ToString(DateTime.MaxValue);
                xEndDatetime = Convert.ToDateTime(ValidFromDatePicker.Text);
                             
                Organization[] queryObj = new Organization[1];
                Organization query1 = new Organization();

                double XMaxOrganization_ID;
                List<Organization> XOrg;
                XOrg = _hrisBusinessLayer.GetAllOrganizations().ToList();
                if(XOrg.Count > 0)
                {
                    XMaxOrganization_ID = Convert.ToDouble(XOrg.Max(c => c.Organization_ID)) + 1;
                }
                else
                {
                    XMaxOrganization_ID = 1;
                }

                query1.Organization_ID = XMaxOrganization_ID.ToString() ;
                query1.Branch_ID = (string)cmbBranch.SelectedValue;
                query1.ParentOrganizationNoted_ID = txtParent.Text;
                query1.ParentOrganizationBusinessNoted_ID = txtParent.Text;
                query1.STECOrganizationNote_ID = txtParent.Text;
                query1.OrganizationUnit_ID = (string)cmbOrgUnit.SelectedValue;
                query1.OrganizationType_ID = (string)cmbOrgType.SelectedValue;         
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = xFromDatetime;
                query1.EndDate = xEndDatetime;
                query1.ModifiedDate = DateTime.Now;

                queryObj[0] = query1;
                _hrisBusinessLayer.AddOrganization(queryObj);


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T011";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Oranization_ID";
                transactionLog1.PKFields = XMaxOrganization_ID.ToString();
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //DateTime xFromDatetime;
                //DateTime xEndDatetime;
                //ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                //xFromDatetime = Convert.ToDateTime(ValidFromDatePicker.Text);
                //ValidFromDatePicker.Text = Convert.ToString(DateTime.MaxValue);
                //xEndDatetime = Convert.ToDateTime(ValidFromDatePicker.Text);

                //if (ValidFromDatePicker.Text == "")
                //    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);



               
                //Organization query1 = new Organization();
                //Organization query2 = new Organization();
                //query2 = _hrisBusinessLayer.GetOrganizationByOrganizationID(XParentID);

                //query1.Branch_ID = (string)cmbBranch.SelectedValue;
                //query1.ParentOrganizationNoted_ID = txtParent.Text;
                //query1.ParentOrganizationBusinessNoted_ID = txtParent.Text;
                //query1.STECOrganizationNote_ID = txtParent.Text;
                //query1.OrganizationUnit_ID = (string)cmbOrgUnit.SelectedValue;
                //query1.OrganizationType_ID = (string)cmbOrgType.SelectedValue;
                //query1.CreatedDate = query2.CreatedDate;
                //query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                //query1.EndDate = Convert.ToDateTime(ValidFromDatePicker.Text);
                //query1.ModifiedDate = DateTime.Now;           
                //_hrisBusinessLayer.UpdateOrganization(query1);

                //transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                //if (transactionLog.Count > 0)
                //{
                //    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                //}
                //else
                //{
                //    XMaxTransactionLog_ID = 1;
                //}
                //TransactionLog[] transactionLogObj = new TransactionLog[1];
                //TransactionLog transactionLog1 = new TransactionLog();
                //transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                //transactionLog1.TransactionType_ID = "U";
                //transactionLog1.Table_ID = "T011";
                //transactionLog1.TransactionDate = DateTime.Now;
                //transactionLog1.FieldName = "Oranization_ID";
                //transactionLog1.PKFields = query2.Organization_ID.ToString();
                //transactionLog1.FKFields = "";
                //transactionLog1.OldData = query2.STECOrganizationNote_ID;
                //transactionLog1.NewData = txtParent.Text;
                //transactionLog1.ModifiedDate = DateTime.Now;
                //transactionLog1.ModifiedUser = _singleton.username;
                //transactionLogObj[0] = transactionLog1;
                //_hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                //ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //ให้เช็ค employee in org นี้ว่ามีหรือเปล่า?  ถ้ามีไม่อนุญาตให้ลบข้อมูลจนกว่าจะย้ายพนักงานไปอยู่ org อื่น

                DateTime xFromDatetime;
                DateTime xEndDatetime;
                ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                xFromDatetime = Convert.ToDateTime(ValidFromDatePicker.Text);
                ValidFromDatePicker.Text = Convert.ToString(DateTime.MaxValue);
                xEndDatetime = Convert.ToDateTime(ValidFromDatePicker.Text);

                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
      
                Organization query2 = new Organization();
                query2 = _hrisBusinessLayer.GetOrganizationByOrganizationID(xOrganization_ID);
                _hrisBusinessLayer.RemoveOrganization(query2);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T011";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Oranization_ID";
                transactionLog1.PKFields = query2.Organization_ID.ToString();
                transactionLog1.FKFields = "";
                transactionLog1.OldData = query2.STECOrganizationNote_ID;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearData()
        {
            try
            {
                xOrganization_ID = "";
                AddButton.IsEnabled = true;
                txtParent.Text = "";
                showOrg();               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddBranch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_Branch("Org"));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddType_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_OrganizationType());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddUnit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_organizationUnit());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void showOrg()
        {
            List<Organization> OrgList = new List<Organization>();
            OrgList = _hrisBusinessLayer.GetAllDepartments().ToList();
            var flatList = new List<OrganizationGroup>();
            foreach (Organization i in OrgList)
            {
                flatList.Add(new OrganizationGroup()
                {
                    Organization_ID = i.Organization_ID 
                    ,STECParentID =  i.STECOrganizationNote_ID == null ? 0 : Convert.ToInt32( i.STECOrganizationNote_ID)
                    ,STECOrganizationNote_ID = i.STECOrganizationNote_ID == null ? "" : i.STECOrganizationNote_ID 
                    ,ParentOrganizationBusinessNoted_ID = i.ParentOrganizationBusinessNoted_ID
                    ,ParentOrganizationNoted_ID = i.ParentOrganizationNoted_ID
                    ,OrganizationUnit_ID = i.OrganizationUnit_ID == null ? null : i.OrganizationUnit_ID
                    ,OrgName = i.OrganizationUnit.OrganizationUnitName
                    ,Branch_ID = i.Branch_ID
                    ,OrganizationType_ID = i.OrganizationType_ID
                    ,OrganizationStatus_ID = i.OrganizationStatus_ID
                    ,CreatedDate = i.CreatedDate
                    ,ValidFrom  = i.ValidFrom
                    ,EndDate = i.EndDate
                });
            }
            var tree = flatList.BuildOrganizationTree();
            TreeView1.ItemsSource = tree;
            TreeViewModel_After = new TreeViewOrganizationModel();
            TreeViewModel_After.Items = new List<OrganizationGroup>();
            

            IList<Branch> _branch = _hrisBusinessLayer.GetAllBranchs();
            cmbBranch.ItemsSource = null;
            cmbBranch.ItemsSource = _branch.OrderBy(b => b.BranchTH);

            IList<OrganizationType> _orgType = _hrisBusinessLayer.GetAllOrganizationTypes();
            cmbOrgType.ItemsSource = null;
            cmbOrgType.ItemsSource = _orgType.OrderBy(b => b.OrganizationTypeName);

            IList<OrganizationUnit> _orgUnit = _hrisBusinessLayer.GetAllOrganizationUnits();
            cmbOrgUnit.ItemsSource = null;
            cmbOrgUnit.ItemsSource = _orgUnit.OrderBy(b => b.OrganizationUnitName);
        }

        public TreeViewOrganizationModel TreeModel
        {
            get
            {
                return TreeViewModel_After;
            }
        }

        Point _lastMouseDown;
        OrganizationGroup draggedItem, _target;

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                showOrg();              
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }           
        }

        private void TreeView1_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                var selectedNode = (e.Source as TreeView).SelectedItem as OrganizationGroup;

                xOrganization_ID = selectedNode.Organization_ID.ToString();
                txtParent.Text = Convert.ToInt32(xOrganization_ID) == 0 ? "" : xOrganization_ID;
                GetEmployeeData(txtParent.Text);            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TreeView1_MouseDown(object sender,MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                _lastMouseDown = e.GetPosition(TreeView1);
            }
        }
        private bool CheckGridSplitter(UIElement element)
        {
            if (element is GridSplitter)
            {
                return true;
            }

            GridSplitter GridSplitter = FindParent<GridSplitter>(element);

            if (GridSplitter != null)
            {
                return true;
            }
            return false;

        }
        private void TreeView1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                //if (e.LeftButton == MouseButtonState.Pressed)
                //{
                //    UIElement element = e.OriginalSource as UIElement;
                //    bool bGridSplitter = CheckGridSplitter(element);


                //    Point currentPosition = e.GetPosition(TreeView1);


                //    if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                //        (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                //    {
                //        draggedItem = (OrganizationGroup)TreeView1.SelectedItem;

                //        if ((draggedItem != null) && !bGridSplitter)
                //        {
                //            DragDropEffects finalDropEffect = DragDrop.DoDragDrop(TreeView1, TreeView1.SelectedValue, DragDropEffects.Move);
                //            //Checking target is not null and item is dragging(moving)
                //            if ((finalDropEffect == DragDropEffects.Move) && (_target != null))
                //            {

                //                // A Move drop was accepted
                //                if (draggedItem.Organization_ID != _target.Organization_ID)
                //                {
                //                    CopyItem(draggedItem, _target);
                //                    _target = null;
                //                    draggedItem = null;
                //                }


                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }    
        private void TreeView1_DragOver(object sender,DragEventArgs e)
        {
            try
            {
                Point currentPosition = e.GetPosition(TreeView1);


                if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                    (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                {
                    // Verify that this is a valid drop and then store the drop target
                    OrganizationGroup item = GetNearestContainer(e.OriginalSource as UIElement);
                    if (CheckDropTarget(draggedItem, item))
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;
                    }
                }
                e.Handled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TreeView1_Drop(object sender, DragEventArgs e)
        {
            try
            {

                e.Effects = DragDropEffects.None;
                e.Handled = true;

                // Verify that this is a valid drop and then store the drop target
                OrganizationGroup TargetItem = GetNearestContainer(e.OriginalSource as UIElement);
                if (TargetItem != null && draggedItem != null)
                {
                    _target = TargetItem;
                    e.Effects = DragDropEffects.Move;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TreeView1_DragEnter(object sender,DragEventArgs e){
            e.Effects = DragDropEffects.Move;
        }
        private bool CheckDropTarget(OrganizationGroup _sourceItem, OrganizationGroup _targetItem)
        {
            //Check whether the target item is meeting your condition
            bool _isEqual = false;

            if (_sourceItem.Organization_ID != _targetItem.Organization_ID)
            {
                _isEqual = true;
            }

            return _isEqual;

        }
        private void CopyItem(OrganizationGroup _sourceItem, OrganizationGroup _targetItem)
        {
            //Asking user wether he want to drop the dragged TreeViewItem here or not
            if (MessageBox.Show("Would you like to drop  '" + _sourceItem.OrgName + "'  into '" + _targetItem.OrgName + "'", "Confirmation", MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    _targetItem.Children.Add(_sourceItem);

                    Organization query1 = new Organization();
                    query1.Organization_ID = _sourceItem.Organization_ID;
                    query1.Branch_ID = _sourceItem.Branch_ID;
                    query1.STECOrganizationNote_ID = _targetItem.Organization_ID;
                    query1.ParentOrganizationBusinessNoted_ID = _targetItem.Organization_ID;
                    query1.ParentOrganizationNoted_ID = _targetItem.Organization_ID;
                    query1.OrganizationUnit_ID = _sourceItem.OrganizationUnit_ID;
                    query1.OrganizationType_ID = _sourceItem.OrganizationType_ID;
                    query1.OrganizationStatus_ID = _sourceItem.OrganizationStatus_ID;
                    query1.CreatedDate = _sourceItem.CreatedDate;
                    query1.ValidFrom = _sourceItem.ValidFrom;
                    query1.EndDate = _sourceItem.EndDate;
                    query1.ModifiedDate = DateTime.Now;
                    _hrisBusinessLayer.UpdateOrganization(query1);


                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                    transactionLog1.TransactionType_ID = "U";
                    transactionLog1.Table_ID = "T011";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "Oranization_ID";
                    transactionLog1.PKFields = _sourceItem.Organization_ID;
                    transactionLog1.FKFields = "";
                    transactionLog1.OldData = _sourceItem.STECOrganizationNote_ID + "-" +_sourceItem.ParentOrganizationBusinessNoted_ID + "-" + _sourceItem.ParentOrganizationNoted_ID;
                    transactionLog1.NewData = _targetItem.Organization_ID;
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                    ClearData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private OrganizationGroup GetNearestContainer(UIElement element)
        {
            // Walk up the element tree to the nearest tree view item.
            TreeViewItem UIContainer = FindParent<TreeViewItem>(element);
            OrganizationGroup NVContainer = null;

            if (UIContainer != null)
            {
                NVContainer = UIContainer.DataContext as OrganizationGroup;
            }
            return NVContainer;
        }
        private static Parent FindParent<Parent>(DependencyObject child)
               where Parent : DependencyObject
        {
            DependencyObject parentObject = child;
            parentObject = VisualTreeHelper.GetParent(parentObject);

            //check if the parent matches the type we're looking for
            if (parentObject is Parent || parentObject == null)
            {
                return parentObject as Parent;
            }
            else
            {
                //use recursion to proceed with next level
                return FindParent<Parent>(parentObject);
            }
        }
        private void GetEmployeeData(string xOrganizationUnit_ID)
        {
            try
            {
                EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = employeeCurrentInformation.GetEmployeesCurrentInformationByDeptCode(xOrganizationUnit_ID);
            }            
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnSetupPositionOrg_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xOrganization_ID == "")
                {
                    MessageBox.Show("กรุณาเลือกฝ่าย/แผนกที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                this.NavigationService.Navigate(new Organization_PositionOrganzation(xOrganization_ID));
            }
            catch(Exception ex){
                MessageBox.Show("Error: " + ex.Message,"Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToEmployeeInfo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                if ( query1 == null )
                {
                    MessageBox.Show("กรุณาเลือกรหัสพนักงานที่ต้องการแสดงข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if ( query1.Employee_ID != "")
                {
                    this.NavigationService.Navigate(new Employees.Employees(query1));
                }
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
       

    }

   

        
}
