﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Data;
using System.Threading;
using System.IO;
using BusinessLayer_HRISSystem.BusinessLogic;
using System.Collections;
using BusinessLayer_HRISSystem.BusinessObject;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR026.xaml
    /// </summary>
    public partial class RepRPTHR026 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        private List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass>();

        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private int XFingerScanCode;
        IDCardInfo Xidcardinfo = new IDCardInfo();


        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;

        //private string XlsPath;
        IEmployeeBL empBL = new EmployeeBL();
        string selectedLot, selectedDept;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        public RepRPTHR026()
        {
            InitializeComponent();
        }

        public RepRPTHR026(EmployeeCurrentInformation x3)
        {
            InitializeComponent();
            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                GetEmployeeInformation(x3);
            }
        }

        public RepRPTHR026(string employee_ID)
        {
            InitializeComponent();
            GetEmployeeData();
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtFirstNameTH.Text = x.xFirstNameTH + " " + x.xLastNameTH;
            XFingerScanCode = Convert.ToInt16(x.xFingerScan_ID);
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchButton.IsEnabled = RdtAllEmp.IsChecked.GetValueOrDefault() ? true : false;
                txtEmployee_ID.Text = string.Empty;
                txtEmployee_ID.IsEnabled = RdtByEmp.IsChecked.GetValueOrDefault() ? true : false;
                txtFirstNameTH.Text = string.Empty;
                txtFirstNameTH.IsEnabled = RdtByName.IsChecked.GetValueOrDefault() ? true : false;
                if (RdtByEmp.IsChecked.GetValueOrDefault()) txtEmployee_ID.Focus();
                if (RdtByName.IsChecked.GetValueOrDefault()) txtFirstNameTH.Focus();

                dtgEmployee.ItemsSource = null;
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //BindCombobox
                departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
                CmbDept.ItemsSource = departmentFromSTECPayrollList;
                CmbDept.SelectedIndex = 0;

                lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
                cmbLot.ItemsSource = lot_NumberClassList;
                cmbLot.SelectedIndex = 0;

                SearchButton.IsEnabled = (cmbLot.SelectedIndex >= 0 && CmbDept.SelectedIndex >= 0);

                if(_singleton.username.ToLower() == "m.tanainan")
                {
                    Rdtstafftype2.IsChecked = true;
                    Rdtstafftype1.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetEmployeeData();
            else
            {
                bool enableBtn = string.IsNullOrEmpty(txtEmployee_ID.Text);
                NavigateToSearchButton.IsEnabled = enableBtn;
                SearchButton.IsEnabled = !enableBtn;
            }
        }

        private void txtFirstNameTH_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetEmployeeData();
            else
            {
                bool enableBtn = string.IsNullOrEmpty(txtFirstNameTH.Text);
                NavigateToSearchButton.IsEnabled = enableBtn;
                SearchButton.IsEnabled = !enableBtn;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            GetEmployeeData();
        }

        private void GetEmployeeData()
        {
            try
            {
                selectedLot = cmbLot.SelectedValue.ToString();
                selectedDept = CmbDept.SelectedValue.ToString();
                List<PrintSlipViewModel> result = new List<PrintSlipViewModel>();
                if (!Rdtstafftype1.IsChecked.GetValueOrDefault() && !Rdtstafftype2.IsChecked.GetValueOrDefault()) return;
                string staffType = Rdtstafftype1.IsChecked.GetValueOrDefault() ? "1" : "2";

                result = empBL.GetEmployee(w => w.DepartmentCode == selectedDept && w.Payroll_date == selectedLot);
                if (RdtAllEmp.IsChecked.GetValueOrDefault())
                {
                    if (staffType == "1") result = result.Where(w => w.Staff_type == "1").ToList();
                    else result = result.Where(w => w.Staff_type != "1").ToList();
                }
                else if (RdtByEmp.IsChecked.GetValueOrDefault())
                {
                    if (string.IsNullOrEmpty(txtEmployee_ID.Text)) { MessageBox.Show("กรุณาคีย์รหัสพนักงาน"); return; }
                    result = result.Where(w => w.EmployeeIdHRIS == txtEmployee_ID.Text && 
                                               w.Staff_type == staffType).ToList();
                }
                else if (RdtByName.IsChecked.GetValueOrDefault())
                {
                    if (string.IsNullOrEmpty(txtFirstNameTH.Text)) { MessageBox.Show("กรุณาคีย์ชื่อพนักงาน"); return; }
                    result = result.Where(w => w.FirstNameTH.StartsWith(txtFirstNameTH.Text) && 
                                               w.Staff_type == staffType).ToList();
                }

                if (!result.Any())
                {
                    dtgEmployee.ItemsSource = null;
                    MessageBox.Show("ไม่พบข้อมูล");
                    return;
                }
                dtgEmployee.ItemsSource = result;
                dtgEmployee.Focus();
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("Report26"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            cmbLot.SelectedIndex = 0;
            CmbDept.SelectedIndex = 0;

            txtEmployee_ID.Text = string.Empty;
            txtEmployee_ID.IsEnabled = false;
            txtFirstNameTH.Text = string.Empty;
            txtFirstNameTH.IsEnabled = false;

            SearchButton.IsEnabled = (cmbLot.SelectedIndex >= 0 && CmbDept.SelectedIndex >= 0);
            dtgEmployee.ItemsSource = null;
            PrintItemCount();
        }

        private void dtgEmployee_MouseUp(object sender, MouseButtonEventArgs e)
        {
            PrintItemCount();
        }

        private void dtgEmployee_KeyUp(object sender, KeyEventArgs e)
        {
            PrintItemCount();
        }

        private void PrintItemCount()
        {
            int? selectedCount = dtgEmployee.SelectedItems.Count;
            PrintButton.Content = string.Format("PRINT SLIP ({0})", selectedCount);
            PrintButton.IsEnabled = selectedCount > 0 ? true : false;
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintSlip();
        }

        private void PrintSlip()
        {
            try
            {
                if (_singleton.deptCode != "IT")
                {
                    if (!isPrintPermission(_singleton.username))
                    {
                        MessageBox.Show("ผู้ใช้นี้ไม่มีสิทธิ์เข้างานในส่วนนี้ หากต้องการเพิ่มสิทธิ์ในการปรินท์กรุณาติดต่อแผนกไอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                else if (string.IsNullOrEmpty(selectedLot) || string.IsNullOrEmpty(selectedDept))
                {
                    MessageBox.Show("ไม่สามารถเรียกข้อมูลได้ กรุณาตรวจสอบ {Dept:" + selectedDept + ",Lot:" + selectedLot + "}", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                List<sp_GetPayRollForPrintSlip_Result> selectedSlip = new List<sp_GetPayRollForPrintSlip_Result>();
                var listPaySlip = empBL.GetPaySlip(selectedLot, selectedDept);
                var selectedItem = dtgEmployee.SelectedItems;
                foreach (var item in selectedItem)
                {
                    selectedSlip.AddRange(listPaySlip.Where(w => w.EmployeeIdPayroll == ((PrintSlipViewModel)item).EmployeeIdPayroll));
                }

                if (selectedSlip == null || !selectedSlip.Any()) { MessageBox.Show("ไม่พบข้อมูล"); return; }

                Print(selectedSlip);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool isPrintPermission(string username)
        {
            switch (username.ToLower())
            {
                case "pranee" : return true;
                case "pitchayapa" : return true;
                case "saruta" : return true;
                case "netchanok" : return true;
                case "m.tanainan": return true;
            }
            return false;
        }

        private void Print(List<sp_GetPayRollForPrintSlip_Result> selectedSlip)
        {
            try
            {
                foreach (var i in selectedSlip)
                {
                    //28 Aug 2018 ให้ใช้ slip format ใหม่ เริ่มต้นในงวดการจ่ายเดือนสิงหาคม 2018 เป็นต้นไป
                    excelWorkBook = excelApplication.Workbooks.Open(@"C:\HRIS\Template_Payslip_July2018.xlsx");
                    //string _filePath = System.IO.Path.Combine(Environment.CurrentDirectory, @"Template_Payslip_July2018.xlsx");
                    //excelWorkBook = excelApplication.Workbooks.Open(_filePath);

                    excelWorkSheet = excelWorkBook.ActiveSheet;
                    excelWorkSheet.PageSetup.Zoom = false;
                    excelWorkSheet.PageSetup.FitToPagesWide = 1;
                    excelApplication.Visible = false;

                    PutDataintoCell("E2", i.Salary_paid_date.GetValueOrDefault().ToString("dd/MM/yyyy")); //วันที่จ่าย
                    PutDataintoCell("H2", i.DepartmentName.ToString());//ฝ่าย
                    PutDataintoCell("L2", i.DepartmentName.ToString()); //แผนก

                    PutDataintoCell("D3", i.EmployeeIdPayroll.ToString()); //รหัสพนักงาน
                    PutDataintoCell("F3", i.EmployeeName.ToString());//ชื่อ-สกุลพนักงาน
                    PutDataintoCell("L3", i.POSITIONTH.ToString());//ตำแหน่ง

                    //รายได้
                    PutDataintoCell("D8", i.Working_date.ToString()); //จำนวนวันทำงาน
                    PutDataintoCell("D9", i.Cot_holiday.ToString()); //จำนวนวันทำงานวันหยุด
                    PutDataintoCell("D10", i.Cot_hour.ToString()); //จำนวค่าล่วงเวลา 1.5 เท่า
                    PutDataintoCell("D11", i.Cot_hour_holiday.ToString());  //จำนวนค่าล่วงเวลา 3 เท่า

                    //PutDataintoCell("E8", i.Salary.ToString()); // เงินเดือน/ค่าจ้าง
                    PutDataintoCell("E8", i.NetSalary.ToString()); // เงินเดือน/ค่าจ้าง (ที่ได้ในเดือนนั้นโดยยังไม่รวมโอที)
                    PutDataintoCell("E9", i.SUM_HOLIDAY.ToString()); //เงินค่าทำงานวันหยุด
                    PutDataintoCell("E10", i.SUMOT_HOUR.ToString()); //เงินค่าล่วงเวลา 1.5 เท่า
                    PutDataintoCell("E11", i.SUM_HOUR_HOLIDAY.ToString()); //เงินค่าล่วงเวลา 3เท่า
                    PutDataintoCell("E12", i.BONUSBAHT.ToString()); // เงินโบนัส
                    PutDataintoCell("E13", i.Special.ToString()); //เงินได้อื่นๆ
                    PutDataintoCell("E17", i.Total_Income.ToString()); //รวมเงินได้

                    //รายจ่าย
                    PutDataintoCell("I8", i.SSO.ToString());
                    PutDataintoCell("I9", i.Tax.ToString());
                    PutDataintoCell("I10", i.Cut_wage.ToString());
                    PutDataintoCell("I17", i.SUMDEDUCTS.ToString()); //รวมเงินหัก

                    PutDataintoCell("M5", i.Id_card.ToString()); //เลขผู้เสียภาษี
                    PutDataintoCell("M7", i.Id_card.ToString()); //เลขบัตรประชาชน
                    PutDataintoCell("M8", i.BankAcount.ToString()); //เลขบัญชีธนาคาร

                    PutDataintoCell("M11", i.SUMNET_INCOME.ToString()); //ยอดสะสมรายได้
                    PutDataintoCell("M12", i.SUMTAX.ToString()); //ยอดสะสมภาษี
                    PutDataintoCell("M13", i.SUMSSO.ToString()); //ยอดสะสมประกันสังคม

                    PutDataintoCell("M16", i.Net_Income.ToString()); //รายได้สุทธิ

                    excelWorkSheet.PrintOutEx();
                    excelWorkBook.Close(0);
                    excelApplication.Quit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Pnd1Button_Click(object sender, RoutedEventArgs e)
        {
            if (_singleton.username.ToLower() == "netchanok" || _singleton.deptCode == "IT") this.NavigationService.Navigate(new Report.RepPND());
            else MessageBox.Show("ผู้ใช้นี้ไม่มีสิทธิ์เข้างานในส่วนนี้ หากต้อการเข้าใข้งานกรุณาติดต่อฝ่าย IT", "ปฏิเสธการเข้าถึง!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }
    }
}
