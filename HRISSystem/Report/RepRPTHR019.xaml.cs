﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR019.xaml
    /// </summary>
    public partial class RepRPTHR019 : Page
    {
        public string _trainingCourseID;
        public DateTime _beginDate;
        public DateTime _endDate;
        public RepRPTHR019( string trainingCourseID,DateTime beginDate,DateTime endDate)
        {
            InitializeComponent();
            _trainingCourseID = trainingCourseID;
            _beginDate = beginDate;
            _endDate = endDate;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _ReportViewer1.Reset();
                HRISReportDataset.sp_GetTrainingInformationsDataTable sp_getTrainingInformationDataTable = new HRISReportDataset.sp_GetTrainingInformationsDataTable();
                HRISReportDatasetTableAdapters.sp_GetTrainingInformationsTableAdapter sp_GetTrainingInformationTableAdapter = new HRISReportDatasetTableAdapters.sp_GetTrainingInformationsTableAdapter();
                sp_GetTrainingInformationTableAdapter.Fill(sp_getTrainingInformationDataTable, _trainingCourseID, _beginDate, _endDate);
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource1.Name = "TrainingCourseDataSet";
                reportDataSource1.Value = sp_getTrainingInformationDataTable;
                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR019.rdlc";
                _ReportViewer1.RefreshReport();     

               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
