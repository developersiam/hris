﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for RefEducationQualification.xaml
    /// </summary>
    public partial class RefEducationQualification : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XType;
        private string XPersonID;
        EducationQualification XFromEduRefPage;
        EducationQualification eduEdit = new EducationQualification();

        public RefEducationQualification(string _xType,string _eduID,string _personId,string _employeeID)
        {
            InitializeComponent();
            if (_xType == "Add")
            {
                XType = "Add";
                XPersonID = _personId;
            }
            else if (_xType == "Edit")
            {
                XType  = "Edit";
                XPersonID = _personId;
                ShowEduDetail(_eduID);
            }
        }

        public RefEducationQualification(EducationQualification eX)
        {
            InitializeComponent();
            XFromEduRefPage = eX;
            ShowEduDetail(XFromEduRefPage.EducationQualification_ID);

        }
        private void ShowEduDetail(string eduID)
        {

            eduEdit = _hrisBusinessLayer.GetEducationQualificationByID(eduID);

            TxtID.Text = eduEdit.EducationQualification_ID;
            TxtNameTH.Text = eduEdit.EducationQualificaionTH;
            TxtNameEN.Text = eduEdit.EducationQualificationEN;
            TxtIniNameTH.Text = eduEdit.EducationQualificationInitialTH;
            TxtIniNameEN.Text = eduEdit.EducationQualificationInitialEN;

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (XFromEduRefPage != null)
                    NavigationService.RemoveBackEntry();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //getAll and added 1
                string XId ;
                List<EducationQualification> edu = new List<EducationQualification>();
                edu = _hrisBusinessLayer.GetAllEducationQualifications().OrderBy(b => b.EducationQualification_ID).ToList();
                if (edu.Count() > 0)
                {                   
                    XId = Convert.ToString(Convert.ToDouble(edu.Max(d=>d.EducationQualification_ID)+1));
                }
                else
                {
                    XId = string.Format("{0:0000}", 0001);
                }
                TxtID.Text = XId;


                if (TxtID.Text == "")
                {
                    MessageBox.Show("กรุณาแจ้ง IT ให้ตรวจสอบรหัสนี้");
                    return;
                }

                if (TxtNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชื่อเต็มวุฒิการศึกษาภาษาไทย");
                    TxtNameTH.Focus();
                    return;
                }


                if (XType == "Add")
                {
                    EducationQualification[] educationQualificationObj = new EducationQualification[1];
                    EducationQualification educationQualificationAdd = new EducationQualification();
                    educationQualificationAdd.EducationQualification_ID = TxtID.Text;
                    educationQualificationAdd.EducationQualificaionTH = TxtNameTH.Text;
                    educationQualificationAdd.EducationQualificationEN = TxtNameEN.Text;
                    educationQualificationAdd.EducationQualificationInitialTH = TxtIniNameTH.Text;
                    educationQualificationAdd.EducationQualificationInitialEN = TxtIniNameEN.Text;
                    educationQualificationAdd.ModifiedDate = DateTime.Now;
                    educationQualificationObj[0] = educationQualificationAdd;
                    _hrisBusinessLayer.AddEducationQualification(educationQualificationObj);

                    
                }
                else if (XType == "Edit")
                {
                    if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }

                    eduEdit.EducationQualificaionTH = TxtNameTH.Text;
                    eduEdit.EducationQualificationEN = TxtNameEN.Text;
                    eduEdit.EducationQualificationInitialTH = TxtIniNameTH.Text;
                    eduEdit.EducationQualificationInitialEN = TxtIniNameEN.Text;
                    eduEdit.ModifiedDate = DateTime.Now;
                    _hrisBusinessLayer.UpdateEducationQualification(eduEdit);
                }
                //NavigationService.GoBack();
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupEduQualification","", TxtID.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtNameTH.Text = "";
                TxtNameEN.Text = "";
                TxtIniNameTH.Text = "";
                TxtIniNameEN.Text = "";
                TxtNameTH.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupEduQualification", "", TxtID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
