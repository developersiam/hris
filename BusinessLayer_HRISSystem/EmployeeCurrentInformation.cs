﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem
{
    public class EmployeeCurrentInformation : Employee
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        public string xTitleNameTH { get; set; } //properties
        public string xFirstNameTH { get; set; }
        public string xLastNameTH { get; set; }
        public string xTitleNameEN { get; set; } 
        public string xFirstNameEN { get; set; }
        public string xLastNameEN { get; set; }
        public string xEmail { get; set; }

        //public string IDCard { get; set; }
        public string xPositionTypeNameTH { get; set; }
        public string xPositionOrganizationID { get; set; }
        public string xPosition_ID { get; set; }
        public string xPositionNameTH { get; set; }

        public string xEmployee_ID { get; set; }
        public string xDeptName { get; set; }
        public string xStaffType { get; set; }

        public string xFingerScan_ID { get; set; }
        public string xNoted { get; set; }

        public DateTime xStartDate { get; set; }
        public DateTime xEnddate { get; set; }
        public decimal xBaseSalary { get; set; }
        public string xPerson_ID { get; set; }

        public DateTime xCreatedDateEmployee { get; set; }
        

        public EmployeeCurrentInformation()
        {

        }
        public EmployeeCurrentInformation (string employee_ID)
        {
            Employee employee = new Employee();
            employee = _hrisBusinessLayer.GetEmployeeInformation(employee_ID);
            if (employee != null)
            {
                this.xEmployee_ID = employee.Employee_ID;
                //this.Employee_ID = employee.Employee_ID;
                this.xPerson_ID = employee.Person_ID;
                this.xTitleNameTH = employee.Person.TitleName.TitleNameTH;
                this.xFirstNameTH = employee.Person.FirstNameTH;
                this.xLastNameTH = employee.Person.LastNameTH;
                this.xFingerScan_ID = employee.FingerScanID;
                this.xNoted = employee.Noted;
                this.xStartDate = Convert.ToDateTime( employee.StartDate);
                this.xEnddate = Convert.ToDateTime( employee.EndDate);
                this.xBaseSalary = Convert.ToDecimal(employee.BaseSalary);
                this.xCreatedDateEmployee = Convert.ToDateTime(employee.CreatedDate);
                this.xEmail = employee.Email;
                this.xTitleNameEN = employee.Person.TitleName.TitleNameEN;
                this.xFirstNameEN = employee.Person.FirstNameEN;
                this.xLastNameEN = employee.Person.LastNameEN;

                ////หา positionOrganization ปัจจุบันเพื่อแสดงค่า ตำแหน่งงานปัจจุบัน
                //IList<EmployeePosition> employeePositionList;
                //employeePositionList = _hrisBusinessLayer.GetPositionByEmployee(employee_ID).Where(e => e.EndDate >= DateTime.Now).ToList();
                //if (employeePositionList.Count > 0)
                //{
                //    string positionOrganization_ID = "";
                //    foreach (EmployeePosition c in employeePositionList)
                //    {
                //        positionOrganization_ID = c.PositionOrganization_ID;
                //        this.xPositionOrganizationID = positionOrganization_ID;                       
                //    }

                //    //นำ PostionOrganization ปัจจุุบันไปหาข้อมูลปัจจุบัน
                //    PositionOrganization positionOrganization = new PositionOrganization();
                //    positionOrganization = _hrisBusinessLayer.GetPositionOrganization(positionOrganization.PositionOrganization_ID);
                //    this.xPosition_ID = positionOrganization.Position_ID;
                //    this.xPositionNameTH = positionOrganization.Position.PositionNameTH;

                //    PositionType positionType = new PositionType();
                //    positionType = _hrisBusinessLayer.GetPositionType(positionOrganization.PositionType_ID);
                //    this.xPositionTypeNameTH = positionType.PositionTypeNameTH;
                //}

                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll = _hrisSTECPayrollLayer.GetEmployeeSTECPayroll(employee.Noted);
                if (employeeSTECPayroll != null)
                {
                    if (employeeSTECPayroll.Staff_type == "1")
                    {
                        this.xStaffType = "พนักงานประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "2")
                    {
                        this.xStaffType = "พนักงานรายวันประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "3")
                    {
                        this.xStaffType = "พนักงานรายวัน";
                    }
                }
            }
        }

        public List<EmployeeCurrentInformation> GetAllEmployeeCurrentInformation()  //method
        {
            List<Employee> employeeList = new List<Employee>();
            employeeList = _hrisBusinessLayer.GetAllEmployees().Where(x=>x.EndDate >= DateTime.Now ).ToList();

            List<EmployeePosition> employeePositionList = new List<EmployeePosition>();
            employeePositionList = _hrisBusinessLayer.GetAllEmployeePositions().Where(x=>x.EndDate >= DateTime.Now).ToList();

            var query1 = (from a in employeeList
                          from b in employeePositionList.Where(x => x.Employee_ID == a.Employee_ID).DefaultIfEmpty()
                          select new EmployeeCurrentInformation
                          {

                              xEmployee_ID = a.Employee_ID,
                              //Employee_ID = a.Employee_ID,
                              xTitleNameTH = a.Person.TitleName.TitleNameTH == null ? null : a.Person.TitleName.TitleNameTH,
                              xFirstNameTH = a.Person.FirstNameTH,
                              xLastNameTH = a.Person.LastNameTH,
                              xFingerScan_ID = a.FingerScanID,
                              xNoted = a.Noted,
                              xPositionTypeNameTH = (b == null) ? null : b.PositionOrganization.PositionType.PositionTypeNameTH,
                              xPositionNameTH = (b == null) ? null : b.PositionOrganization.Position.PositionNameTH,
                              xPerson_ID = a.Person_ID,
                              xStartDate = Convert.ToDateTime(a.StartDate),
                              xEnddate = Convert.ToDateTime(a.EndDate),
                              xBaseSalary = Convert.ToDecimal(a.BaseSalary),
                              xCreatedDateEmployee = Convert.ToDateTime(a.CreatedDate)    ,
                              xEmail = a.Email
                          });
  
            return query1.ToList();
        }

        public List<EmployeeCurrentInformation> GetEmployeesCurrentInformationByDeptCode(string xOrganizationUnit)
        {
            List<Employee> employeeList = new List<Employee>();
            employeeList = _hrisBusinessLayer.GetAllEmployees().Where(x => x.EndDate >= DateTime.Now).ToList();

            List<EmployeePosition> employeePositionList = new List<EmployeePosition>();
            employeePositionList = _hrisBusinessLayer.GetAllEmployeePositions().Where(x => x.EndDate >= DateTime.Now).ToList();

            List<PositionOrganization> positionOrganization = new List<PositionOrganization>();
            positionOrganization = _hrisBusinessLayer.GetAllPositionOrganizations().Where(x => x.Organization_ID == xOrganizationUnit).ToList();
       

            var query1 = (
                          //  from a in employeeList
                          //from b in employeePositionList.Where(x=> x.Employee_ID == a.Employee_ID).DefaultIfEmpty()
                          //from c in positionOrganization.Where(x=>x.PositionOrganization_ID == b.PositionOrganization_ID).DefaultIfEmpty()

                          from a in employeeList
                          join b in employeePositionList on a.Employee_ID equals b.Employee_ID 
                          join c in positionOrganization on b.PositionOrganization_ID equals c.PositionOrganization_ID
                          select new EmployeeCurrentInformation
                          {

                              xEmployee_ID = a.Employee_ID,
                              //Employee_ID = a.Employee_ID,
                              xTitleNameTH = a.Person.TitleName.TitleNameTH == null ? null : a.Person.TitleName.TitleNameTH,
                              xFirstNameTH = a.Person.FirstNameTH,
                              xLastNameTH = a.Person.LastNameTH,
                              xFingerScan_ID = a.FingerScanID,
                              xNoted = a.Noted,
                              xPositionTypeNameTH = (b == null) ? null : b.PositionOrganization.PositionType.PositionTypeNameTH,
                              xPositionNameTH = (b == null) ? null : b.PositionOrganization.Position.PositionNameTH,
                              xPerson_ID = a.Person_ID,
                              xStartDate = Convert.ToDateTime(a.StartDate),
                              xEnddate = Convert.ToDateTime(a.EndDate),
                              xBaseSalary = Convert.ToDecimal(a.BaseSalary),
                              xCreatedDateEmployee = Convert.ToDateTime(a.CreatedDate),
                              xEmail = a.Email
                          });


            return query1.ToList();
        }

        
    }
}
