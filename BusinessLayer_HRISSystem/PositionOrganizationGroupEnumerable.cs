﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem
{
    public static class PositionOrganizationGroupEnumerable
    {
        public static IList<PositionOrganizationGroup> BuildPositionTree(this IEnumerable<PositionOrganizationGroup> source)
        {
            var groups = source.GroupBy(i => i.STECPositionParentID); //group data

            var roots = groups.FirstOrDefault(g => g.Key.Value == 0).ToList();  //find the root node  by key.value = 0 
            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddPositionChildren(roots[i], dict);
            }

            //var groups = source.GroupBy(i => i.STECPositionOrganizationID2); //group data

            //var roots = groups.FirstOrDefault(g => g.Key.Value == 0).ToList();  //find the root node  by key.value = 0 
            //if (roots.Count > 0)
            //{
            //    var dict = groups.Where(g => g.Key.HasValue ).ToDictionary(g => g.Key.Value, g => g.ToList());
            //    for (int i = 0; i < roots.Count; i++)
            //        AddPositionChildren(roots[i], dict);
            //}
            return roots;
        }

     
        private static void AddPositionChildren(PositionOrganizationGroup node, IDictionary<long, List<PositionOrganizationGroup>> source)
        {
            if (source.ContainsKey(Convert.ToInt64(node.STECPositionParentID)))
            {
                node.Children = source[Convert.ToInt64(node.STECPositionParentID)];
                for (int i = 0; i < node.Children.Count; i++)
                {
                    AddPositionChildren(node.Children[i], source);
                }
            }
            else
            {
                node.Children = new List<PositionOrganizationGroup>();
            }



            //if (source.ContainsKey(Convert.ToInt64(node.STECPositionOrganizationID2)))
            //{
            //    node.Children = source[Convert.ToInt64(node.STECPositionOrganizationID2)];            
            //    for (int i = 0; i < node.Children.Count; i++)
            //    {
            //        AddPositionChildren(node.Children[i], source);                   
            //    }
            //}
            //else
            //{
            //    node.Children = new List<PositionOrganizationGroup>();
            //}

        }
    }
}
