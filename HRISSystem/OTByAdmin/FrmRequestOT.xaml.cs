﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Globalization;
using System.Threading;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.ComponentModel;


namespace HRISSystem.OTByAdmin
{
    /// <summary>
    /// Interaction logic for FrmRequestOT.xaml
    /// </summary>
    /// 
    
    public partial class FrmRequestOT : Page
    {
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private Double XCurrentOT;
        Boolean XFromOTInformation = false;
        Boolean XchkDate = false;
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private int xFingerScanID;
        private List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> _otOnlineInformationList = new List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation>();
        private BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation otselected;
        //static bool mailSent = false;
        private string XLastManagerEmployee_ID;      
        private OTOnlineSetupAdmin _fromAdmin = new OTOnlineSetupAdmin();
        private string xPayrollLot = "";

        private List<OTOnlineSetupAdminDetail> _fromAdminDetail;


        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _timeSTECBuisnessLayer = new HRISTimeSTECBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();

        public void GetPayrollLot(DateTime otdate)
        {
            try
            {
                DateTime sdate = DateTime.Now;
                sdate = Convert.ToDateTime(sdate.ToString("MM/dd/yyyy"));

                //ถ้าเป็นเดือนที่ผ่านมาแล้ว ไป xpayrolllot เป็นเดือนนี้
                if (Convert.ToInt16(otdate.Month) < Convert.ToInt16(sdate.Month))
                {
                    //get data จาก lot_number from stec_payroll
                    Lot_Number lot_number = new Lot_Number();
                    lot_number = _hrisSTECPayrollLayer.GetLotNumberByDate(Convert.ToDateTime(sdate));
                    xPayrollLot = lot_number.Lot_Month + "/" + lot_number.Lot_Year;
                }
                else
                {
                    //get data จาก lot_number from stec_payroll
                    Lot_Number lot_number = new Lot_Number();
                    lot_number = _hrisSTECPayrollLayer.GetLotNumberByDate(otdate);
                    xPayrollLot = lot_number.Lot_Month + "/" + lot_number.Lot_Year;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        //public void GetPayrollLot(DateTime otdate)
        //{
        //    try
        //    {
        //        //ถ้าเป็นเดือนที่ผ่านมาแล้ว ไป xpayrolllot เป็นเดือนนี้
        //        if (Convert.ToInt16(otdate.Month) <= Convert.ToInt16(DateTime.Now.Month))
        //        {
        //            //get data จาก lot_number from stec_payroll
        //            Lot_Number lot_number = new Lot_Number();
        //            lot_number = _hrisSTECPayrollLayer.GetLotNumberByDate(DateTime.Now);
        //            xPayrollLot = lot_number.Lot_Month + "/" + lot_number.Lot_Year;
        //        }
        //        else
        //        {
        //            //get data จาก lot_number from stec_payroll
        //            Lot_Number lot_number = new Lot_Number();
        //            lot_number = _hrisSTECPayrollLayer.GetLotNumberByDate(otdate);
        //            xPayrollLot = lot_number.Lot_Month + "/" + lot_number.Lot_Year;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}
        public void getOTDetail(string employee_ID, DateTime otDate, string otType) //แสดงรายละเอียด OT เฉพาะรายการนั้น    
        {
            try
            {
                OT ot = new OT();
                ot = _hrisBusinessLayer.GetOTByEmployeeOneRow(employee_ID, otDate, otType);

                OTDate.Text = ot.OTDate.ToString();
                if (otType == "H")
                {
                    List<OTType> otTypeList = new List<OTType>();
                    OTType h4 = new OTType { OTTypeCode = "H", OTTypeName = "Holiday" };
                    otTypeList.Add(h4);
                    cmbOTType.ItemsSource = otTypeList;
                    cmbOTType.SelectedIndex = 0;
                    txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    cmbRequest.SelectedValue = ot.RequestType;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                }
                else if (otType == "S")
                {
                    List<OTType> otTypeList = new List<OTType>();
                    OTType h5 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
                    otTypeList.Add(h5);
                    cmbOTType.ItemsSource = otTypeList;
                    cmbOTType.SelectedIndex = 0;
                    txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    cmbRequest.SelectedValue = ot.RequestType;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                }
                else
                {
                    cmbOTType.SelectedValue = ot.OTType;
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    txtNcountOT.Text = Convert.ToString(XCurrentOT);
                    cmbRequest.SelectedValue = ot.RequestType;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private class StatusFlags
        {
            public string RequestFlag { get; set; }
            public string RequestFlagName { get; set; }
        }
        public FrmRequestOT()
        {
            InitializeComponent();
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            StatusFlags h2 = new StatusFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbRequest.ItemsSource = statusFlagList;
            cmbRequest.SelectedIndex = 1;

            
               
        }

        public FrmRequestOT(EmployeeCurrentInformation x3)
        {
            InitializeComponent();
            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            StatusFlags h2 = new StatusFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };

            statusFlagList.Add(h1);
            statusFlagList.Add(h2);

            cmbRequest.ItemsSource = statusFlagList;
            cmbRequest.SelectedIndex = 1;


            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                GetEmployeeInformation(x3);
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = x3.xFirstNameTH;
                txtSearch.IsEnabled = true;
            }            
        }

        public FrmRequestOT (DateTime otDate, string otType,string employee_ID,Boolean xchkFrom,string xfrom) 
        {
            InitializeComponent();
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            StatusFlags h2 = new StatusFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbRequest.ItemsSource = statusFlagList;
            cmbRequest.SelectedIndex = 1;

            GetDataFromEmployee_ID(employee_ID);
            getOTDetail(employee_ID, otDate, otType);

        }

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
      
        private class OTType
        {
            public string OTTypeCode { get; set; }
            public string OTTypeName { get; set; }
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();


                // Sets the CurrentCulture property to U.S. English.
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                LeaveDateFromSearch.Text = FirstDayOfMonth(DateTime.Now).ToString();
                LeaveDateToSearch.Text = DateTime.Now.ToString();


                DataGrid.ItemsSource = null; //clear 


                cmbRequest.SelectedIndex = 1;
                cmbOTType.SelectedIndex = 0;

                if (XFromOTInformation == true)
                {
                    txtNcountOT.Text = XCurrentOT.ToString();
                    GetDataFromSearch();
                    AddButton.IsEnabled = false;
                }

                
                _fromAdmin = _hrisBusinessLayer.GetOTOnlineSetupAdminByADName(_singleton.username);

                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                //txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
                txtStaffType.Text = ex.xStaffType;
                txtEmployeeAcc.Text = ex.xNoted;
                xFingerScanID = Convert.ToInt16(ex.xFingerScan_ID);

                txtStaffType.Text = "";
                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll = _hrisSTECPayrollLayer.GetEmployeeSTECPayroll(ex.xNoted);
                if (employeeSTECPayroll != null)
                {
                    if (employeeSTECPayroll.Staff_type == "1")
                    {
                        this.txtStaffType.Text = "พนักงานประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "2")
                    {
                        this.txtStaffType.Text = "พนักงานรายวันประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "3")
                    {
                        this.txtStaffType.Text = "พนักงานรายวัน";
                    }
                }

                //find detail userlogin 
                //OTOnlineSetupAdmin _fromAdmin = new OTOnlineSetupAdmin();
                _fromAdmin = _hrisBusinessLayer.GetOTOnlineSetupAdminByADName(_singleton.username);
                if (_fromAdmin == null)
                {
                    MessageBox.Show("กรุณาตรวจสอบการได้รับสิทธิ์ให้บันทึกการขอทำโอทีของคุณ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    _fromAdminDetail = new List<OTOnlineSetupAdminDetail>();
                    _fromAdminDetail = _hrisBusinessLayer.GetOTonLineSetupAdminDetailByAdminID(_fromAdmin.Admin_ID).ToList();

                    //แสดงผู้จัดการที่ทำการขออนุมัติ
                    txtManagerApprove.Text = "";
                    Boolean xChk = false;
                    foreach (OTOnlineSetupAdminDetail c in _fromAdminDetail)
                    {
                        if (c.DeptID == Convert.ToString(txtEmployeeAcc.Text.Substring(2, 3)))
                        {
                            Employee e = new Employee();
                            e = _hrisBusinessLayer.GetEmployeeInformation(c.ReportTo);
                            txtManagerApprove.Text = e.Employee_ID + " " + e.Person.FirstNameTH + " " + e.Person.LastNameTH;
                            XLastManagerEmployee_ID = e.Employee_ID;
                            xChk = true;

                        }
                    }
                    if (xChk == false)
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานให้ตรงกับที่คุณได้รับสิทธิ์ในการขออนุมัติโอที กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                

                

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            //EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            //txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
            txtEmployeeAcc.Text = x.xNoted;
            xFingerScanID = Convert.ToInt16(x.xFingerScan_ID);

            txtStaffType.Text = "";
            EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
            employeeSTECPayroll = _hrisSTECPayrollLayer.GetEmployeeSTECPayroll(x.xNoted);
            if (employeeSTECPayroll != null)
            {
                if (employeeSTECPayroll.Staff_type == "1")
                {
                    this.txtStaffType.Text = "พนักงานประจำ";
                }
                else if (employeeSTECPayroll.Staff_type == "2")
                {
                    this.txtStaffType.Text = "พนักงานรายวันประจำ";
                }
                else if (employeeSTECPayroll.Staff_type == "3")
                {
                    this.txtStaffType.Text = "พนักงานรายวัน";
                }
            }


            
            //find detail userlogin 
            //OTOnlineSetupAdmin _fromAdmin = new OTOnlineSetupAdmin();
            _fromAdmin = _hrisBusinessLayer.GetOTOnlineSetupAdminByADName(_singleton.username);
            if (_fromAdmin == null)
            {
                MessageBox.Show("กรุณาตรวจสอบการได้รับสิทธิ์ให้บันทึกการขอทำโอทีของคุณ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else
            {
                _fromAdminDetail = new List<OTOnlineSetupAdminDetail>();
                _fromAdminDetail = _hrisBusinessLayer.GetOTonLineSetupAdminDetailByAdminID(_fromAdmin.Admin_ID).ToList();

                //แสดงผู้จัดการที่ทำการขออนุมัติ
                txtManagerApprove.Text = "";
                Boolean xChk = false;
                foreach (OTOnlineSetupAdminDetail c in _fromAdminDetail)
                {
                    if (c.DeptID == Convert.ToString(txtEmployeeAcc.Text.Substring(2, 3)))
                    {
                        Employee e = new Employee();
                        e = _hrisBusinessLayer.GetEmployeeInformation(c.ReportTo);
                        txtManagerApprove.Text = e.Employee_ID + " " + e.Person.FirstNameTH + " " + e.Person.LastNameTH;
                        XLastManagerEmployee_ID = e.Employee_ID;
                        xChk = true;

                    }
                }
                if (xChk == false)
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานให้ตรงกับที่คุณได้รับสิทธิ์ในการขออนุมัติโอที กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            
            

        }
        public void ClearData()
        {
            try
            {
                XCurrentOT = 0;
                XFromOTInformation = false;
                AddButton.IsEnabled = true;
                OTDate.IsEnabled = true;
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";
                txtNcountOT.Text = "";
                txtScanIn.Text = "";
                txtScanOut.Text = "";
                txtTotalWork.Text = "";
                txtStaffType.Text = "";
                txtManagerApprove.Text = "";
                XLastManagerEmployee_ID = "";
                txtRemarkFromAdmin.Text = "";


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtManagerApprove.Text == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า manager มี emailที่บันทึกไว้ในหน้า employee หรือยัง 
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeInformation(Convert.ToString(txtManagerApprove.Text.Substring(0, 6)));
                if(e2 == null){
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }else{
                    if(e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                    }
                }

                //if (OTDate.Text == "")
                //{
                //    MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    OTDate.Focus();
                //    return;
                //}
                //else if (ChkDateRange(Convert.ToDateTime(OTDate.Text)) == false)
                //{
                //    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบไม่สามารถคีย์วันที่ทำโอทีของงวดที่แล้วและคีย์ล่วงหน้าได้ !!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                //if (txtScanIn.Text == "" || txtScanOut.Text == "")
                //{
                //    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกคำร้องขออนุมัติโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้วกับฝ่ายบุคคลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                ////ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
                //TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                //if (XchkDate == false)
                //{                 
                //    if (span.Hours <= 8)
                //    {
                //        MessageBox.Show("กรุณาตรวจสอบการข้อมูลการสแกนนิ้วเนื่องจากระบบพบว่ามีการมีการทำงานไม่ครบ 8 ชัวโมง จึงไม่สามารถบันทึกการขอทำโอทีได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        return;
                //    }
                //}

                if (OTDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDateTime(OTDate.Text) > Convert.ToDateTime(DateTime.Now)) //21 June 2018 ไม่อนุญาตให้คีย์ล่วงหน้าได้
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบไม่สามารถคีย์วันที่ทำโอทีล่วงหน้าได้ !!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ChkDateRange(Convert.ToDateTime(OTDate.Text)) == false)
                {
                    if (MessageBox.Show("เนื่องจากคุณระบุวันที่ทำโอทีย้อนหลังโดยระบุก่อนวันที่25 ของเดือนที่แล้ว ต้องการให้รายการคำนวนเงินเดือนในเดือน" + xPayrollLot + " ใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        xPayrollLot = null;
                        return;
                    }
                }


                if (txtScanIn.Text == "" || txtScanOut.Text == "")
                {
                    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้ว ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
                TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                if (XchkDate == false)
                {
                    if (span.Hours < 8)
                    {
                        MessageBox.Show("กรุณาตรวจสอบการข้อมูลการสแกนนิ้วเนื่องจากมีการมีการทำงานน้อยกว่า 8 ชัวโมง จึงไม่สามารถบันทึกการขอทำโอทีได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                List<OT> s = new List<OT>();
                s = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b => b.OTDate == Convert.ToDateTime(OTDate.Text) && b.OTType == (string)cmbOTType.SelectedValue).ToList();
                if (s.Count() > 0)
                {
                    MessageBox.Show("มีข้อมูลวันที่นี้แล้ว กรุณาตรวจสอบอีกครั้ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
               
                if (txtNcountOT.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จำนวนชั่วโมง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    Double remainder;
                    Double ncountOT = Convert.ToDouble(txtNcountOT.Text);
                    remainder = (ncountOT * 10) % 5;
                    
                    if (remainder > 0)
                    {
                        MessageBox.Show("จำนวนชั่วโมงจะต้องเป็นจำนวนเต็มหรือเป็นทศนิยม 0.5 เช่น 1,1.5 เป็นต้น", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    else if (XchkDate == false)
                    {
                        //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                        decimal xMinutesFromTimeScan;
                        xMinutesFromTimeScan = ((span.Hours - 8) * 60) + (span.Minutes);

                        decimal xMinutesFromUser;
                        xMinutesFromUser = (Convert.ToDecimal(txtNcountOT.Text) * 60);
                        //ถ้าโอทีมากกว่าที่ระบบคำนวนได้ เกิน 2 ชั่วโมงขึ้นไป ไม่อนุญาตให้บันทึก ให้แจ้ง HR ให้บันทึกให้
                        if (xMinutesFromUser > (xMinutesFromTimeScan-30))
                        {
                            MessageBox.Show("กรุณาคีย์จำนวนโอทีให้ถูกต้องเนื่องจากพบว่าคีย์จำนวนชั่วโมงOTเกินกว่าที่ระบบคำนวนได้ หากต้องการให้คีย์เกินกว่าที่คำนวนได้ให้แจ้งฝ่าย HR ให้ดำเนินการให้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }
                        //ถ้าโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ ให้ขึ้น message ถามเพื่อยืนยันการบันทึก
                        else if (xMinutesFromUser > xMinutesFromTimeScan)
                        {
                            if (MessageBox.Show("จำนวนโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ คุณต้องการบันทึกข้อมูลต่อใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                            {
                                return;
                            }
                        }
                    }
                }

                if (cmbOTType.SelectedValue.ToString() == "H" && Convert.ToDecimal(txtNcountOT.Text) > 2)
                {
                    MessageBox.Show("คีย์จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                

                if (cmbRequest.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์OT request", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);

                    return;
                }

                //List<OT> s = new List<OT>();
                //s = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b => b.OTDate == Convert.ToDateTime(OTDate.Text) && b.OTType == (string)cmbOTType.SelectedValue).ToList();
                //if (s.Count() > 0)
                //{
                //    MessageBox.Show("มีข้อมูลวันที่นี้แล้ว กรุณาตรวจสอบอีกครั้ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                

                if(txtRemarkFromAdmin.Text == ""){
                    MessageBox.Show("กรุณาระบุหมายเหตุที่ขอทำโอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtRemarkFromAdmin.Focus();
                    return;
                }
                
                

                OT[] otOBJ = new OT[1];
                OT query1 = new OT();  // created 1 instance or object
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.OTDate = Convert.ToDateTime(OTDate.Text);
                query1.OTType = (string)cmbOTType.SelectedValue;
                query1.NcountOT = Convert.ToDecimal(txtNcountOT.Text);
                query1.Status = true;
                query1.ModifiedDate = DateTime.Now;
                query1.RequestType = (string)cmbRequest.SelectedValue;
                query1.ManagerApproveStatus = "P";
                query1.HRApproveStatus = "P";
                query1.RemarkFromAdmin = txtRemarkFromAdmin.Text;
                query1.Admin_ID = _fromAdmin.Admin_ID;
                query1.PayrollLot = xPayrollLot;
                otOBJ[0] = query1;
                _hrisBusinessLayer.AddOT(otOBJ);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T009";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(OTDate.Text);
                transactionLog1.OldData = "";
                transactionLog1.NewData = Convert.ToString(txtNcountOT.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLog1.Noted = "Request OT by Admin " +  " Requesttype " +  (string)cmbRequest.SelectedValue;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);

                LeaveDateFromSearch.Text = OTDate.Text;
                LeaveDateToSearch.Text = OTDate.Text;
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
       
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtManagerApprove.Text == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า manager มี emailที่บันทึกไว้ในหน้า employee หรือยัง 
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeInformation(Convert.ToString(txtManagerApprove.Text.Substring(0, 6)));
                if(e2 == null){
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }else{
                    if(e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                    }
                }

                //if (OTDate.Text == "")
                //{
                //    MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //else if (ChkDateRange(Convert.ToDateTime(OTDate.Text)) == false)
                //{
                //    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบไม่สามารถคีย์วันที่ทำโอทีของงวดที่แล้วและคีย์ล่วงหน้าได้ !!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                //if (txtScanIn.Text == "" || txtScanOut.Text == "")
                //{
                //    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกคำร้องขออนุมัติโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้วกับฝ่ายบุคคลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                ////ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
                //TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                //if (XchkDate == false)
                //{
                //    if (span.Hours <= 8)
                //    {
                //        MessageBox.Show("กรุณาตรวจสอบการข้อมูลการสแกนนิ้วเนื่องจากระบบพบว่ามีการมีการทำงานไม่ครบ 8 ชัวโมง จึงไม่สามารถบันทึกการขอทำโอทีได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        return;
                //    }
                //}


                //if (txtNcountOT.Text == "")
                //{
                //    MessageBox.Show("กรุณาคีย์จำนวนชั่วโมง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //else
                //{
                //    if (XchkDate == false)
                //    {
                //        //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                //        decimal xMinutesFromTimeScan;
                //        xMinutesFromTimeScan = ((span.Hours - 8) * 60) + (span.Minutes);

                //        decimal xMinutesFromUser;
                //        xMinutesFromUser = (Convert.ToDecimal(txtNcountOT.Text) * 60);

                //        if (xMinutesFromUser > xMinutesFromTimeScan)
                //        {
                //            MessageBox.Show("กรุณาคีย์จำนวนโอทีให้ถูกต้อง เนื่องจากพบว่าคีย์จำนวนชั่วโมง OT เกินกว่าที่ระบบคำนวนได้ จะต้องคีย์ไม่เกิน " + (xMinutesFromTimeScan / 60), "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //            return;
                //        }
                //    }
                //}

               
                //if (cmbOTType.SelectedValue == "H" && Convert.ToDecimal(txtNcountOT.Text) > 2)
                //{
                //    MessageBox.Show("คีย์จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                if (OTDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำ OT", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbOTType.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกประเภท OT", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtScanIn.Text == "" || txtScanOut.Text == "")
                {
                    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้ว ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
                TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                if (XchkDate == false)
                {
                    if (span.Hours <= 8)
                    {
                        MessageBox.Show("กรุณาตรวจสอบการข้อมูลการสแกนนิ้วเนื่องจากระบบพบว่ามีการมีการทำงานไม่ครบ 8 ชัวโมง จึงไม่สามารถบันทึกการขอทำโอทีได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }


                if (txtNcountOT.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จำนวนชั่วโมง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    Double remainder;
                    Double ncountOT = Convert.ToDouble(txtNcountOT.Text);
                    remainder = (ncountOT * 10) % 5;

                    if (remainder > 0)
                    {
                        MessageBox.Show("จำนวนชั่วโมงจะต้องเป็นจำนวนเต็มหรือเป็นทศนิยม 0.5 เช่น 1,1.5 เป็นต้น", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    else if (XchkDate == false)
                    {
                        //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                        decimal xMinutesFromTimeScan;
                        xMinutesFromTimeScan = ((span.Hours - 8) * 60) + (span.Minutes);

                        decimal xMinutesFromUser;
                        xMinutesFromUser = (Convert.ToDecimal(txtNcountOT.Text) * 60);
                        //ถ้าโอทีมากกว่าที่ระบบคำนวนได้ เกิน 2 ชั่วโมงขึ้นไป ไม่อนุญาตให้บันทึก ให้แจ้ง HR ให้บันทึกให้
                        if (xMinutesFromUser > (xMinutesFromTimeScan - 30))
                        {
                            MessageBox.Show("กรุณาคีย์จำนวนโอทีให้ถูกต้องเนื่องจากพบว่าคีย์จำนวนชั่วโมงOTเกินกว่าที่ระบบคำนวนได้ หากต้องการให้คีย์เกินกว่าที่คำนวนได้ให้แจ้งฝ่าย HR ให้ดำเนินการให้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }
                        //ถ้าโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ ให้ขึ้น message ถามเพื่อยืนยันการบันทึก
                        else if (xMinutesFromUser > xMinutesFromTimeScan)
                        {
                            if (MessageBox.Show("จำนวนโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ คุณต้องการบันทึกข้อมูลต่อใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                            {
                                return;
                            }
                        }
                    }
                }

                if (cmbOTType.SelectedValue.ToString() == "H" && Convert.ToDecimal(txtNcountOT.Text) > 2)
                {
                    MessageBox.Show("คีย์จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                

                if (cmbRequest.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์OT request", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                List<OT> s = new List<OT>();
                s = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b => b.OTDate == Convert.ToDateTime(OTDate.Text) && b.OTType == (string)cmbOTType.SelectedValue).ToList();
                if (s.Count() == 0)
                {
                    MessageBox.Show("ไม่มีข้อมูลการขอทำโอทีในระบบที่ได้บันทึกไว้ กรุณาตรวจสอบอีกครั้ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                if(txtRemarkFromAdmin.Text == ""){
                    MessageBox.Show("กรุณาระบุหมายเหตุที่ขอทำโอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (otselected.HRApproveStatus.Substring(0,1) == "A")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HRได้อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งฝ่าย HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (otselected.HRApproveStatus.Substring(0, 1) == "N")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HR ไม่อนุมัติการทำโอทีคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งฝ่าย HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (otselected.ManagerApproveStatus.Substring(0, 1) == "A")
                {
                    MessageBox.Show("เนื่องจากผู้จัดการได้อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ผู้จัดการหรือผู้มีสิทธิ์ในการอนุมัติทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (otselected.ManagerApproveStatus.Substring(0, 1) == "N")
                {
                    MessageBox.Show("เนื่องจากผู้จัดการไม่อนุมัติการทำโอทีคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ผู้จัดการหรือผู้มีสิทธิ์ในการอนุมัติทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                
                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลการขออนุมัติโอทีใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                OT query1 = new OT();  // created 1 instance or object  
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.OTDate = Convert.ToDateTime(OTDate.Text);
                query1.OTType = (string)cmbOTType.SelectedValue;
                query1.NcountOT = Convert.ToDecimal(txtNcountOT.Text);
                query1.Status = true;
                query1.ModifiedDate = DateTime.Now;
                query1.RequestType = (string)cmbRequest.SelectedValue;
                query1.ManagerApproveStatus = "P";
                query1.HRApproveStatus = "P";
                query1.RemarkFromAdmin = txtRemarkFromAdmin.Text;
                query1.RemarkFromManager = otselected.RemarkFromManager;
                query1.RemarkFromHR = otselected.RemarkFromHR;
                query1.Admin_ID = _fromAdmin.Admin_ID;
                query1.PayrollLot = xPayrollLot;
                _hrisBusinessLayer.UpdateOT(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T009";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(OTDate.Text);
                transactionLog1.OldData = Convert.ToString(XCurrentOT);
                transactionLog1.NewData = Convert.ToString(txtNcountOT.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                SendEmailForUpdate(Convert.ToString(txtManagerApprove.Text.Substring(0, 6)));
                MessageBox.Show("บันทึกข้อมูลและส่งอีเมลล์ถึงผู้จัดการ/หัวหน้าเพื่อขออนุมัติการแก้ไขการทำโอทีแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);

                LeaveDateFromSearch.Text = OTDate.Text;
                LeaveDateToSearch.Text = OTDate.Text;
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtManagerApprove.Text == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า manager มี emailที่บันทึกไว้ในหน้า employee หรือยัง 
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeInformation(Convert.ToString(txtManagerApprove.Text.Substring(0, 6)));
                if (e2 == null)
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    if (e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                if (OTDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ChkDateRange(Convert.ToDateTime(OTDate.Text)) == false)
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบไม่สามารถคีย์วันที่ทำโอทีของงวดที่แล้วและคีย์ล่วงหน้าได้ !!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtScanIn.Text == "" || txtScanOut.Text == "")
                {
                    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกคำร้องขออนุมัติโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้วกับฝ่ายบุคคลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtNcountOT.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จำนวนชั่วโมง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (Convert.ToDecimal(txtNcountOT.Text) > 12)
                {
                    MessageBox.Show("คีย์จำนวนชั่วโมง OT ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (cmbOTType.SelectedValue.ToString() == "H" && Convert.ToDecimal(txtNcountOT.Text) > 1)
                {
                    MessageBox.Show("คีย์จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbRequest.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์OT request", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                List<OT> s = new List<OT>();
                s = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b => b.OTDate == Convert.ToDateTime(OTDate.Text) && b.OTType == (string)cmbOTType.SelectedValue).ToList();
                if (s.Count() == 0)
                {
                    MessageBox.Show("ไม่มีข้อมูลการขอทำโอทีในระบบที่ได้บันทึกไว้ กรุณาตรวจสอบอีกครั้ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtRemarkFromAdmin.Text == "")
                {
                    MessageBox.Show("กรุณาระบุหมายเหตุที่ขอทำโอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                

                OT query1 = new OT();  // created 1 instance or object  
                if (otselected.OTnormal != 0)
                {
                    query1 = _hrisBusinessLayer.GetOTByEmployeeOneRow(otselected.Employee_ID, Convert.ToDateTime(otselected.OTDATE), "N");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday == 0)
                {
                    query1 = _hrisBusinessLayer.GetOTByEmployeeOneRow(otselected.Employee_ID, Convert.ToDateTime(otselected.OTDATE), "H");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday != 0)
                {
                    this.NavigationService.Navigate(new Walefares.GetOTInformation(Convert.ToDateTime(otselected.OTDATE), Convert.ToDateTime(otselected.OTDATE), otselected.Employee_ID.ToString(), "RequstOTOnline",""));
                }


                if (query1.HRApproveStatus.Substring(0, 1) == "A")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HRได้อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ฝ่าย HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (query1.HRApproveStatus.Substring(0, 1) == "N")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HR ไม่อนุมัติการทำโอทีคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (query1.ManagerApproveStatus.Substring(0, 1) == "A")
                {
                    MessageBox.Show("เนื่องจากผู้จัดการได้อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ผู้จัดการหรือผู้มีสิทธิ์ในการอนุมัติทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (query1.ManagerApproveStatus.Substring(0, 1) == "N")
                {
                    MessageBox.Show("เนื่องจากผู้จัดการไม่อนุมัติการทำโอทีคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งให้ผู้จัดการหรือผู้มีสิทธิ์ในการอนุมัติทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("คุณต้องการลบข้อมูลการขออนุมัติโอทีใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                _hrisBusinessLayer.RemoveOT(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T009";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(OTDate.Text);
                transactionLog1.OldData = Convert.ToString(XCurrentOT);
                transactionLog1.NewData = Convert.ToString(txtNcountOT.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);

                LeaveDateFromSearch.Text = OTDate.Text;
                LeaveDateToSearch.Text = OTDate.Text;
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (LeaveDateFromSearch.Text == "" || LeaveDateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DataGrid.SelectedIndex <= -1)
                    return;

                XFromOTInformation = false;
                AddButton.IsEnabled = false;

                //TestOT query2 = new TestOT();  // created 1 instance or object
                otselected = (BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                if (otselected.OTnormal != 0)
                {
                    GetDataFromEmployee_ID(otselected.Employee_ID);
                    getOTDetail(otselected.Employee_ID.ToString(), Convert.ToDateTime(otselected.OTDATE), "N");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday == 0)
                {
                    GetDataFromEmployee_ID(otselected.Employee_ID);
                    getOTDetail(otselected.Employee_ID.ToString(), Convert.ToDateTime(otselected.OTDATE), "H");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday != 0)
                {
                    this.NavigationService.Navigate(new Walefares.GetOTInformation(Convert.ToDateTime(otselected.OTDATE), Convert.ToDateTime(otselected.OTDATE), otselected.Employee_ID.ToString(),"RequstOTOnline",""));
                }
                ShowCheckInOut();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }       

        //private Boolean ChkDateRange(DateTime _OTdate)
        //{
        //    Boolean xresult = false;
        //    DateTime Currentdate = DateTime.Now;
        //    var beginDate = Currentdate;
        //    var endDate = Currentdate;
        //    //เช็คว่าเป็นเดือน 1 ใช่หรือไม่ ถ้าใช่ปีจะต้องลบไป1
        //    if (Currentdate.Month == 1)
        //    {
        //        beginDate = new DateTime(Currentdate.Year - 1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์OTย้อนหลังได้
        //        endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
        //    }
        //    else
        //    {
        //        beginDate = new DateTime(Currentdate.Year, Currentdate.Month - 1, 26);
        //        endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);
        //    }

        //    //ไม่อนุญาติให้คีย์วันที่ทำโอทีล่วงหน้าได้
        //    if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
        //    {
        //        xresult = false;
        //    }           
        //    else 
        //    {
        //        //เช็คว่าถ้าวันที่น้อยกว่า 25 
        //        if (_OTdate.Day <= 25 && _OTdate.Month != Currentdate.Month)
        //        {
        //            if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate))
        //            {
        //                xresult = false;
        //            }
        //            else if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(endDate))
        //            {
        //                xresult = false;
        //            }
        //            else
        //            {
        //                xresult = true;
        //            }
        //        }
        //        else if (Convert.ToInt16(_OTdate.Day) <= 25 && _OTdate.Month == Currentdate.Month)
        //        {
        //            if (Convert.ToDateTime(_OTdate) <= Convert.ToDateTime(endDate))
        //            {
        //                xresult = true;
        //            }                    
        //            else
        //            {
        //                xresult = false;
        //            }
        //        }
        //        else //เช็คว่าถ้าวันที่มากกว่า 25 จะต้องใช้งวดถัดไป
        //        {

        //            if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate) && _OTdate.Month != Currentdate.Month)
        //            {
        //                xresult = false;
        //            }
        //            else if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(endDate) && _OTdate.Month == Currentdate.Month)
        //            {
        //                xresult = false;
        //            }
        //            else
        //            {
        //                xresult = true;
        //            }
        //        }               
        //    }
        //    return xresult;
            
        //    ////ไม่อนุญาตให้คีย์ล่วงหน้า
        //    //if(Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
        //    //{
        //    //    xresult = false;
        //    //}
        //    //else if ((Currentdate.Year == _OTdate.Year) && (Currentdate.Month == _OTdate.Month))//ถ้าเป็นปีและเดือนเดียวกัน  
        //    //{

        //    //}else if(_OTdate.Month < endPreviouseMonth.Month) //ถ้าวันที่โอทีน้อยกว่าวันที่ 26 ของเดือนที่แล้ว

        //    ////ไม่อนุญาตให้คีย์วันทีทำโอทีล่วงหน้า
        //    //if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
        //    //{
        //    //    xresult = false;
        //    //}
        //    //else if (Convert.ToDateTime(_OTdate) <= Convert.ToDateTime(lastCurrentMonth))
        //    //{
        //    //    if (Convert.ToInt16(_OTdate.Month) < Convert.ToInt16(lastCurrentMonth.Month))
        //    //    {
        //    //        xresult = false;
        //    //    }
        //    //    else
        //    //    {
        //    //        xresult = true;
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    xresult = true;
        //    //}

        //    ////1.เช็คว่าถ้าเป็นปีและเดือนเดียวกัน
        //    //if ((Currentdate.Year == _OTdate.Year && Currentdate.Month == _OTdate.Month))
        //    //{
        //    //    //1.1 ถ้าวันที่โอทีน้อยกว่าวันที่ปัจจุบัน และ วันที่โอทีมากกว่าวันที่ 25 
        //    //    if (_OTdate.Date <= Currentdate.Date && _OTdate <= Convert.ToDateTime(lastCurrentMonth))
        //    //    {
        //    //        xresult = true;
        //    //    }
        //    //    else
        //    //    {
        //    //        xresult = false;
        //    //    }
        //    //}//2.เช็คว่าถ้าไม่ใช่เดือนเดียวกัน
        //    //else if (_OTdate < Convert.ToDateTime(endPreviouseMonth))
        //    //{
        //    //    xresult = false;
        //    //}
        //    //else if (_OTdate > Convert.ToDateTime(lastCurrentMonth) && _OTdate > Currentdate)
        //    //{
        //    //    xresult = false;
        //    //}

        //    //else
        //    //{
        //    //    xresult = true;
        //    //}


            
        //}
        private Boolean ChkDateRange(DateTime _OTdate)
        {
            Boolean xresult = false;
            DateTime Currentdate = DateTime.Now;
            var beginDate = Currentdate;
            var endDate = Currentdate;
            //เช็คว่าเป็นเดือน 1 ใช่หรือไม่ ถ้าใช่ปีจะต้องลบไป1
            if (Currentdate.Month == 1)
            {
                beginDate = new DateTime(Currentdate.Year - 1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์OTย้อนหลังได้
                endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            else
            {
                beginDate = new DateTime(Currentdate.Year, Currentdate.Month - 1, 26);
                endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);
            }

            //ไม่อนุญาติให้คีย์วันที่ทำโอทีล่วงหน้าได้
            if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
            {
                xresult = true;
            }
            else
            {
                //เช็คว่าถ้าวันที่น้อยกว่า 25 
                if (_OTdate.Day <= 25 && _OTdate.Month != Currentdate.Month)
                {
                    if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate))
                    {
                        xresult = false;
                    }
                    else if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(endDate))
                    {
                        xresult = false;
                    }
                    else
                    {
                        xresult = true;
                    }
                }
                else if (Convert.ToInt16(_OTdate.Day) <= 25 && _OTdate.Month == Currentdate.Month)
                {
                    if (Convert.ToDateTime(_OTdate) <= Convert.ToDateTime(endDate))
                    {
                        xresult = true;
                    }
                    else
                    {
                        xresult = false;
                    }
                }
                else //เช็คว่าถ้าวันที่มากกว่า 25 จะต้องใช้งวดถัดไป
                {

                    if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate) && _OTdate.Month != Currentdate.Month)
                    {
                        xresult = false;
                    }
                    else if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(endDate) && _OTdate.Month == Currentdate.Month)
                    {
                        xresult = false;
                    }
                    else
                    {
                        xresult = true;
                    }
                }
            }
            return xresult;
        }

        private List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> GetOTOnlineInformation(DateTime dateFrom, DateTime dateTo)
        {
            Report.HRISReportDataset.sp_GetOTOnlineInformationsDataTable sp_GetOTOnlineInformationDataTable = new Report.HRISReportDataset.sp_GetOTOnlineInformationsDataTable();
            Report.HRISReportDatasetTableAdapters.sp_GetOTOnlineInformationsTableAdapter sp_GetOTOnlineInformationsTableAdapter = new Report.HRISReportDatasetTableAdapters.sp_GetOTOnlineInformationsTableAdapter();
            sp_GetOTOnlineInformationsTableAdapter.Fill(sp_GetOTOnlineInformationDataTable, dateFrom, dateTo);
            List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> OTOnlineInformationList = new List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation>();
            foreach (Report.HRISReportDataset.sp_GetOTOnlineInformationsRow item in sp_GetOTOnlineInformationDataTable)
            {
                OTOnlineInformationList.Add(new BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation
                {
                    //Admin_ID = item.Admin_ID == null ? 0 : item.Admin_ID
                    Admin_ID = item.Admin_ID
                    //this.Blood_Name = person.Blood_ID != null ? person.Blood.BloodGroupTH : "";
                    ,                  
                    
                    AdminEmployee_ID = item.AdminEmployeeID == "" ? "" : item.AdminEmployeeID
                    ,
                    AdminFirstName = item.AdminFirstName == null ? "" : item.AdminFirstName
                    ,
                    AdminLastName = item.AdminLastName == null ? "" : item.AdminLastName
                    ,
                    AdminACCCode = item.AdminACCCode == null ? "" : item.AdminACCCode
                    ,
                    Employee_ID = item.Employee_ID
                    ,
                    EMPACCCode = item.EMPACCcode
                    ,
                    FingerScanID = item.FingerScanID
                    ,
                    EMPTitlename = item.EMPTitlename
                    ,
                    EMPFirstName = item.EMPFistName
                    ,
                    EMPlastName = item.EMPLastName
                    ,
                    OTDATE = item.OTDate
                    ,
                    RequestType = item.RequestType == null ? "" : item.RequestType
                    ,
                    OTnormal = item.OTNormal
                    ,
                    Holiday = item.Holiday
                    ,
                    OTHoliday = item.OTHoliday
                    ,
                    DeptID = item.DeptID == null ? "" : item.DeptID 
                    ,
                    managerEmployee_ID = item.ManagerID == null ? "" : item.ManagerID
                    ,
                    ManagerACCCode = item.ManagerACCCode == null ? "" : item.ManagerACCCode
                    ,
                    ManagerFirstName = item.ManagerFirstName == null ? "" : item.ManagerFirstName
                    ,
                    ManagerLastName = item.ManagerLastName == null ? "" : item.ManagerLastName
                    ,
                    ManagerApproveStatus = item.ManagerApproveStatus == null ? "" : item.ManagerApproveStatus
                    ,
                    HRApproveStatus = item.HRApproveStatus == null ? "" : item.HRApproveStatus
                    ,
                    RemarkFromAdmin = item.RemarkFromAdmin == null ? "" : item.RemarkFromAdmin
                    ,
                    RemarkFromManager = item.RemarkFromManager == null ? "" : item.RemarkFromManager
                    ,
                    RemarkFromHR = item.RemarkFromHR == null ? "" : item.RemarkFromHR 
                    ,PayrollLot = item.PayrollLot == null ? "" : item.PayrollLot
                    

                });
            }
            return OTOnlineInformationList;
        }
        private void GetDataFromSearch()
        {
            try
            {

                DataGrid.ItemsSource = null;
                if ((LeaveDateFromSearch.Text != "") && (LeaveDateToSearch.Text != ""))
                {
                    _otOnlineInformationList = GetOTOnlineInformation(Convert.ToDateTime(LeaveDateFromSearch.Text), Convert.ToDateTime(LeaveDateToSearch.Text)).ToList();
                    if (ChkAllSearch.IsChecked == true)
                    {
                        if (_fromAdmin != null)
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.Admin_ID == _fromAdmin.Admin_ID).ToList();
                        }
                    }
                    else if (ChkFilterSearch.IsChecked == true)
                    {
                        if (_fromAdmin != null)
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.Admin_ID == _fromAdmin.Admin_ID && b.EMPFirstName.Contains(txtSearch.Text)).ToList();
                        }
                        else
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.EMPFirstName.Contains(txtSearch.Text)).ToList();
                        }
                    }
                    if (RdtPending.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Pending").ToList();
                    }
                    else if (RdtApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Approve").ToList();
                    }
                    else if (RdtUnApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Not Approve").ToList();
                    }

                    else if (RdtHRUnApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Not Approve").ToList();
                    }
                    else if (RdtHRPending.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Pending").ToList();
                    }
                    else if (RdtHRApprove.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Approve").ToList();
                    }
                    if (_otOnlineInformationList.Count <= 0)
                    {
                        MessageBox.Show("ไม่มีข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    DataGrid.ItemsSource = _otOnlineInformationList;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowCheckInOut()
        {
            userinfo u;
            u = _timeSTECBuisnessLayer.GetUserInfoByFingerID(xFingerScanID);
            txtTotalWork.Text = "";

            //เช็ค Time In จาก manualworkdate
            ManualWorkDate w = new ManualWorkDate();
            w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A01", Convert.ToDateTime(OTDate.Text));
            if (w != null)
            {
                txtScanIn.Text = Convert.ToString(w.Times);
            }
            else
            {
                //เช็คจาก TimeScan
                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
                if (_checkInout.Count > 0)
                {
                    txtScanIn.Text = Convert.ToString(_checkInout.Min(c => c.checktime.TimeOfDay));

                }
                else
                {
                    txtScanIn.Text = "";
                }
            }

            //เช็ค Time Out จาก manualworkdate
            w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A02", Convert.ToDateTime(OTDate.Text));
            if (w != null)
            {
                txtScanOut.Text = Convert.ToString(w.Times);
            }
            else
            {
                //เช็คจาก TimeScan
                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
                if (_checkInout.Count > 0)
                {
                    txtScanOut.Text = Convert.ToString(_checkInout.Max(c => c.checktime.TimeOfDay));
                }
                else
                {
                    txtScanOut.Text = "";
                }
            }

            if (txtScanIn.Text != "" && txtScanOut.Text != "")
            {
                TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                txtTotalWork.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
            }


        }
        //private void ShowCheckInOut()
        //{
        //    userinfo u;
        //    u = _timeSTECBuisnessLayer.GetUserInfoByFingerID(xFingerScanID);
        //    txtTotalWork.Text = "";

        //    //เช็ค Time In จาก manualworkdate
        //    ManualWorkDate w = new ManualWorkDate();
        //    w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A01", Convert.ToDateTime(OTDate.Text));
        //    if (w != null)
        //    {
        //        txtScanIn.Text = Convert.ToString(w.Times);
        //    }
        //    else
        //    {
        //        //เช็คจาก TimeScan
        //        List<checkinout> _checkInout;
        //        _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
        //        if (_checkInout.Count > 0)
        //        {
        //            txtScanIn.Text = Convert.ToString(_checkInout.Min(c => c.checktime.TimeOfDay));

        //        }
        //        else
        //        {
        //            txtScanIn.Text = "";
        //        }
        //    }

        //    //เช็ค Time Out จาก manualworkdate
        //    w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A02", Convert.ToDateTime(OTDate.Text));
        //    if (w != null)
        //    {
        //        txtScanOut.Text = Convert.ToString(w.Times);
        //    }
        //    else
        //    {
        //        //เช็คจาก TimeScan
        //        List<checkinout> _checkInout;
        //        _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
        //        if (_checkInout.Count > 0)
        //        {
        //            txtScanOut.Text = Convert.ToString(_checkInout.Max(c => c.checktime.TimeOfDay));
        //        }
        //        else
        //        {
        //            txtScanOut.Text = "";
        //        }
        //    }

        //    if (txtScanIn.Text != "" && txtScanOut.Text != "")
        //    {
        //        TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
        //        txtTotalWork.Text = string.Format("{0} hours , {1} minutes",span.Hours, span.Minutes);
        //    }
            
           
        //}

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("OTRequest"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SearchNameButton.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void OTDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (XFromOTInformation == false)
                {
                    GetPayrollLot(Convert.ToDateTime(OTDate.Text));
                    ShowCheckInOut();

                    if (IsWeekend(Convert.ToDateTime(OTDate.Text)) == true)
                    {
                        txtChkWeekend.Text = "วันอาทิตย์";
                        txtChkWeekend.Foreground = Brushes.Red;
                        XchkDate = true;

                        List<OTType> otTypeList = new List<OTType>();
                        OTType h4 = new OTType { OTTypeCode = "H", OTTypeName = "Holiday" };
                        OTType h5 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
                        otTypeList.Add(h4);
                        otTypeList.Add(h5);
                        cmbOTType.ItemsSource = otTypeList;
                        cmbOTType.SelectedIndex = 0;
                    }
                    else
                    {
                        //เช็คว่าเป็น holiday ที่กำหนดไว้หรือไม่  
                        Holiday h = _hrisBusinessLayer.GetHoliday(Convert.ToDateTime(OTDate.Text));
                        if (h == null)
                        {
                            txtChkWeekend.Text = "วันธรรมดา";
                            txtChkWeekend.Foreground = Brushes.Black;
                            XchkDate = false;
                            List<OTType> otTypeList = new List<OTType>();
                            OTType h3 = new OTType { OTTypeCode = "N", OTTypeName = "OT-normal" };
                            otTypeList.Add(h3);
                            cmbOTType.ItemsSource = otTypeList;
                            cmbOTType.SelectedIndex = 0;
                        }
                        else
                        {
                            txtChkWeekend.Text = h.HolidayNameTH.ToString();
                            txtChkWeekend.Foreground = Brushes.Blue;
                            XchkDate = true;

                            List<OTType> otTypeList = new List<OTType>();
                            OTType h4 = new OTType { OTTypeCode = "H", OTTypeName = "Holiday" };
                            OTType h5 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
                            otTypeList.Add(h4);
                            otTypeList.Add(h5);
                            cmbOTType.ItemsSource = otTypeList;
                            cmbOTType.SelectedIndex = 0;
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbOTType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbOTType.SelectedValue.ToString() == "H")
                {
                    txt1.Text = "แรง";
                }
                else
                {
                    txt1.Text = "ชั่วโมง";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtNcountOT_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                bool approveDecimalPoint = false;
                if (e.Text == ".")
                    approveDecimalPoint = true;


                if (!char.IsDigit(e.Text, e.Text.Length - 1) && !(approveDecimalPoint))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SendEmailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("คุณต้องการส่งอีเมลล์ถึงผู้จัดการ/หัวหน้า เพื่อขออนุมัติโอทีใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                if (Convert.ToString(txtManagerApprove.Text.Substring(0, 6)) == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบรายชื่อผู้อนุมัติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                SendEmailForApprove(Convert.ToString(txtManagerApprove.Text.Substring(0, 6)));
                MessageBox.Show("ส่งอีเมลล์ถึงผู้จัดการ/หัวหน้าเพื่อขออนุมัติการทำโอทีแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void SendEmailForApprove(string _toEmployee_ID)
        {

            Employee e = new Employee();
            e = _hrisBusinessLayer.GetEmployeeInformation(_toEmployee_ID);
            string toName =  e.Person.FirstNameTH + " " + e.Person.LastNameTH;
            string toEmail = e.Email;          

            string fromPerson = "it-info@siamtobacco.com";
            string fromPassword = "Up2uy@dmin$tecE!";

            MailMessage mailmessage = new MailMessage();

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(fromPerson, fromPassword);
            client.Port = 587;
            client.Host = "smtp.emailsrvr.com";

            mailmessage.From = new MailAddress("it-info@siamtobacco.com");
            mailmessage.To.Add(toEmail); // to manager
            mailmessage.CC.Add(_singleton.emailFrom);
            mailmessage.Subject = "The request OT online for approve from HRIS system.";
            string messages = "";
             messages = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียน  คุณ " + toName + " </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจากอีเมลล์ " + _singleton.emailFrom + "  กรุณาตรวจสอบข้อมูลและอนุมัติการขอทำโอทีของพนักงานในฝ่าย/แผนกของท่านในโปรแกรม HRIS </td>" +
                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +

                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear Khun " + toName +"</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>You have received a new request OT online approve from " + _singleton.emailFrom + " and could you please consider in the HRIS system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;
            //mailmessage.
            mailmessage.Body = messages;
            mailmessage.IsBodyHtml = true;
            try
            {
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void SendEmailForUpdate(string _toEmployee_ID)
        {

            Employee e = new Employee();
            e = _hrisBusinessLayer.GetEmployeeInformation(_toEmployee_ID);
            string toName = e.Person.FirstNameTH + " " + e.Person.LastNameTH;
            string toEmail = e.Email;

            string fromPerson = "it-info@siamtobacco.com";
            string fromPassword = "Up2uy@dmin$tecE!";

            MailMessage mailmessage = new MailMessage();

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(fromPerson, fromPassword);
            client.Port = 587;
            client.Host = "smtp.emailsrvr.com";

            mailmessage.From = new MailAddress("it-info@siamtobacco.com");
            mailmessage.To.Add(toEmail); // to manager
            mailmessage.CC.Add(_singleton.emailFrom);
            mailmessage.Subject = "The request modified OT online for approve from HRIS system.";
            string messages = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียน  คุณ " + toName + " </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจากอีเมลล์ " + _singleton.emailFrom + " เนื่องจากมีการแก้ไขข้อมูลการขออนุมัติการทำโอที  กรุณาตรวจสอบข้อมูลและอนุมัติการขอทำโอทีของพนักงานในฝ่าย/แผนกของท่านในโปรแกรม HRIS </td>" +
                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +

                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear Khun " + toName + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>You have received a new request OT online for approve from " + _singleton.emailFrom + " and could you please consider on the HRIS system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;
            //mailmessage.
            mailmessage.Body = messages;
            mailmessage.IsBodyHtml = true;
            try
            {
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtPending_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtUnApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRPending_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRApprove_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRUnApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
