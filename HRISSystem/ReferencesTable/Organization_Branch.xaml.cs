﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_Branch.xaml
    /// </summary>

    public partial class Organization_Branch : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XFromPage;
        public Organization_Branch()
        {
            InitializeComponent();
        }

        public Organization_Branch(string _xfromPage)
        {        
            InitializeComponent();
            XFromPage = _xfromPage;

        }

        public void ClearData()
        {
            try
            {
                txtBranchID.IsReadOnly = false;
                txtBranchID.Background = new SolidColorBrush(Colors.White);
                AddButton.IsEnabled = true;
                txtBranchID.Text = "";
                txtBranchNameTH.Text = "";
                txtBranchNameEN.Text = "";
                txtAddress.Text = "";
                txtPhoneNumber1.Text = "";
                txtPhoneNumber2.Text = "";
                txtFaxNumber.Text = "";
            
                DataGridBranch.ItemsSource = null; //clear 
                DataGridBranch.ItemsSource = _hrisBusinessLayer.GetAllBranchs();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBranch.ItemsSource = _hrisBusinessLayer.GetAllBranchs();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                Branch[] branchObj = new Branch[1];
                Branch branch1 = new Branch();
                branch1.Branch_ID = txtBranchID.Text;
                branch1.BranchTH = txtBranchNameTH.Text;
                branch1.BranchEN = txtBranchNameEN.Text;
                branch1.Address = txtAddress.Text;
                branch1.PhoneNumber1 = txtPhoneNumber1.Text;
                branch1.PhoneNumber2 = txtPhoneNumber2.Text;
                branch1.FaxNumber = txtFaxNumber.Text;
                branch1.CreatedDate = DateTime.Now;
                branch1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                branch1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                branch1.ModifiedDate = DateTime.Now;
                branchObj[0] = branch1;
                _hrisBusinessLayer.AddBranch(branchObj);
                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                Branch branch1 = new Branch();
                Branch query2 = new Branch();
                query2 = (Branch)DataGridBranch.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                branch1.Branch_ID = txtBranchID.Text;
                branch1.BranchTH = txtBranchNameTH.Text;
                branch1.BranchEN = txtBranchNameEN.Text;
                branch1.Address = txtAddress.Text;
                branch1.PhoneNumber1 = txtPhoneNumber1.Text;
                branch1.PhoneNumber2 = txtPhoneNumber2.Text;
                branch1.FaxNumber = txtFaxNumber.Text;
                branch1.CreatedDate = query2.CreatedDate;
                branch1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                branch1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                branch1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateBranch(branch1);
                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Branch branch1 = new Branch();
                branch1 = (Branch)DataGridBranch.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
             
                _hrisBusinessLayer.RemoveBranch(branch1);
                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridBranch_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Branch branch1 = new Branch();
                branch1 = (Branch)DataGridBranch.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                if (branch1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtBranchID.IsReadOnly = false;
                    txtBranchID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtBranchID.IsReadOnly = true;
                    txtBranchID.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
