﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem.BusinessObject;

namespace BusinessLayer_HRISSystem
{
    public class PositionOrganizationGroup : PositionOrganization
    {
        public Int64? STECPositionParentID { get; set; }
        public string OrganizationUnit_ID { get; set; }
        public string OrganizationName { get; set; }
        public string PositionName { get; set; }
        public List<PositionOrganizationGroup> Children { get; set; }
        public bool ShowPositionOrganization { get; set; }
        public PositionOrganizationGroup()
        {
            ShowPositionOrganization = true;
        }
    }
    //public class PositionOrganizationGroup : BusinessLayer_HRISSystem.BusinessObject.PositionOrganizationTreeView
    //{        
    //    public List<PositionOrganizationGroup> Children { get; set; }
    //    public bool ShowPositionOrganization { get; set; }
    //    public PositionOrganizationGroup()
    //    {
    //        ShowPositionOrganization = true;
    //    }
    //}
}
