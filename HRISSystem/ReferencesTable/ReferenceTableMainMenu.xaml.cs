﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for ReferenceTableMainMenu.xaml
    /// </summary>
    public partial class ReferenceTableMainMenu : Page
    {
        public ReferenceTableMainMenu()
        {
            InitializeComponent();
        }

        private void NavigateToManageCropButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HumanResource_CropSetup());
        }

        private void NavigateToManageBranchButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_Branch());
        }

        private void NavigateToLeaveTypeButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HumanResource_LeaveType());
        }

        private void NavigateToShiftButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HumanResource_Shift());
        }

        private void NavigateToJobTitleButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_JobTitle());
        }

        private void NavigateToOrganizationButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_Organization());
        }

        private void NavigateToPositionTypeButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_PositionType());
        }

        private void NavigateToPositionButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_Position());
        }

        //private void NavigateToOrganizationTypeButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.NavigationService.Navigate(new Organization_OrganizationType());
        //}

        //private void NavigateToOrganizationUnitButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.NavigationService.Navigate(new Organization_organizationUnit());
        //}

        //private void NavigateToPersonButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.NavigationService.Navigate(new Person_Person());
        //}

        private void NavigateToPositionOrganizationButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Organization_PositionOrganzation());
        }
    }
}
