﻿using BusinessLayer_HRISSystem;
using BusinessLayer_HRISSystem.BusinessLogic;
using BusinessLayer_HRISSystem.BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for ShiftControl.xaml
    /// </summary>
    public partial class ShiftControl : Page
    {
        HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new HRISSTECPayrollBusinessLayer();
        HRISBusinessLayer _hrisBusinessLayer = new HRISBusinessLayer();
        List<ShiftControlViewModel> deptToShiftList = new List<ShiftControlViewModel>();
        List<ShiftControlViewModel> sourceShiftList = new List<ShiftControlViewModel>();
        List<ShiftControlViewModel> destinationShiftList = new List<ShiftControlViewModel>();
        Lot_NumberClass lot_numberList = new Lot_NumberClass();

        public ShiftControl()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BindLot();
        }

        private void BindLot()
        {
            try
            {
                var lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
                shiftLotCombobox.ItemsSource = null;
                shiftLotCombobox.ItemsSource = lot_NumberClassList;
                destinationLotCombobox.ItemsSource = null;
                destinationLotCombobox.ItemsSource = lot_NumberClassList;
                string currentLot = lot_numberList.CurrentLot();
                var selectedLot = lot_NumberClassList.SingleOrDefault(w => w.LotName == currentLot);
                if (selectedLot != null)
                {
                    shiftLotCombobox.SelectedValue = currentLot;
                    shiftLotCombobox_Change();
                    destinationLotCombobox.SelectedValue = currentLot;
                    destinationLotCombobox_Change();
                }
                else MessageBox.Show("ไม่พบงวดที่ตรงกับวันที่ปัจจุบัน กรุณาตรวจสอบ", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //  DAEPARTMENT TO SHIFT CONTROL
        private void EmployeeToShiftTab_Loaded(object sender, RoutedEventArgs e)
        {
            deptToShiftList.Clear();
            //Bind Department
            departmentCombobox.ItemsSource = null;
            var departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
            departmentCombobox.ItemsSource = departmentFromSTECPayrollList;
            //Bind All Shift
            shiftCombobox.ItemsSource = null;
            shiftCombobox.ItemsSource = _shiftList();
        }

        private void tabItem1_Clicked(object sender, MouseButtonEventArgs e)
        {
            ClearMoveToShift();
            ClearShiftControl();
        }

        private List<ShiftViewModel> _shiftList()
        {
            var shiftList = _hrisBusinessLayer.GetAllShifts().ToList()
                                              .Select(s => new ShiftViewModel
                                              {
                                                  Shift_ID = s.Shift_ID,
                                                  Shift_Time = s.StartTime.GetValueOrDefault().ToString() + " - " + s.EndTime.GetValueOrDefault().ToString()
                                              }).OrderBy(o => o.Shift_Time).ToList();
            return shiftList;
        }

        private void shiftLotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            shiftLotCombobox_Change();
        }

        private void shiftLotCombobox_Change()
        {
            ClearShiftControl();
            var selectedLot = (Lot_NumberClass)shiftLotCombobox.SelectedItem;
            if (selectedLot != null)
            {
                var startDate = selectedLot.Start_date;
                var finishDate = selectedLot.Finish_date;
                shiftLotFromDatePicker.SelectedDate = startDate;
                shiftLotToDatePicker.SelectedDate = finishDate;
                fromDatePicker.DisplayDateStart = startDate;
                toDatePicker.DisplayDateStart = startDate;
                fromDatePicker.DisplayDateEnd = finishDate;
                toDatePicker.DisplayDateEnd = finishDate;
            }
        }

        private void departmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            GetEmployeeData();
        }

        private void searchNameTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetEmployeeData();
        }

        private void searchNameButton_Click(object sender, RoutedEventArgs e)
        {
            GetEmployeeData();
        }

        private void GetEmployeeData()
        {
            try
            {
                if (string.IsNullOrEmpty(departmentCombobox.Text)) return;
                var hrEmpList = _hrisBusinessLayer.GetAllEmployees().ToList();
                var prEmpList = _hrisSTECPayrollBusinessLayer.GetAllEmployeeSTECPayrolls().Where(w => w.Dept_code == departmentCombobox.SelectedValue.ToString() &&
                                                                                                      w.Staff_status == "1");

                var joinedEmp = from hr in hrEmpList
                                join pr in prEmpList on hr.Noted equals pr.Employee_id
                                select new ShiftControlViewModel
                                {
                                    Employee_ID = hr.Employee_ID,
                                    Name = pr.Fname + " " + pr.Lname,
                                    Staff_type = pr.Staff_type == "1" ? "พนักงานประจำ" : (pr.Staff_type == "2" ? "รายวันประจำ" : "รายวัน"),
                                    Staff_status = pr.Staff_status == "1" ? "ทำงาน" : "สิ้นสุด"
                                };
                if (!string.IsNullOrEmpty(searchNameTextbox.Text))
                {
                    joinedEmp = joinedEmp.Where(w => w.Name.Contains(searchNameTextbox.Text));
                }
                EmployeeDataGrid.ItemsSource = joinedEmp;
                CountMoveToShift();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CountMoveToShift();
        }

        private void EmployeeDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            CountMoveToShift();
        }

        private void CountMoveToShift()
        {
            int? selectedCount = EmployeeDataGrid.SelectedItems.Count;
            moveToShiftButton.Content = string.Format("Add >> ({0})", selectedCount);
            moveToShiftButton.IsEnabled = selectedCount > 0 ? true : false;
        }

        private void moveToShiftButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(shiftCombobox.Text) || string.IsNullOrEmpty(fromDatePicker.Text) || string.IsNullOrEmpty(toDatePicker.Text))
            {
                MessageBox.Show("กรุณาเลือกข้อมูลปลายทาง", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var selectedItem = EmployeeDataGrid.SelectedItems;
            foreach (var item in selectedItem)
            {
                if ((!deptToShiftList.Any()))
                {
                    deptToShiftList.Add((ShiftControlViewModel)item);
                }
                else
                {
                    //if not existed
                    //var isExisted = deptToShiftList.SingleOrDefault(s => s.Employee_ID == ((ShiftControlViewModel)item).Employee_ID);
                    if (!deptToShiftList.Where(w => w.Employee_ID == ((ShiftControlViewModel)item).Employee_ID).Any())
                    {
                        deptToShiftList.Add((ShiftControlViewModel)item);
                    }
                }

            }
            ShiftControlDataGrid.ItemsSource = null;
            ShiftControlDataGrid.ItemsSource = deptToShiftList;
            CountShiftControlSaveButton();
        }

        private void removeFromEmptoShiftButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedLot = (Lot_NumberClass)shiftLotCombobox.SelectedItem;

                if (selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (selectedLot.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Warining !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedItem = ShiftControlDataGrid.SelectedItems;

                if (ShiftControlDataGrid.SelectedItems.Count < 1) return;
                if (MessageBox.Show("ยืนยันการลบ (" + ShiftControlDataGrid.SelectedItems.Count + ") ข้อมูลหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                foreach (var item in selectedItem)
                {
                    var i = (ShiftControlViewModel)item;
                    // Remove only existed shift control
                    if (i.ValidFrom != null && i.EndDate != null)
                    {
                        var shiftControl = new DomainModel_HRISSystem.ShiftControl
                        {
                            Shift_ID = (string)shiftCombobox.SelectedValue,
                            Employee_ID = i.Employee_ID,
                            ValidFrom = i.ValidFrom.Value,
                            EndDate = i.EndDate,
                            Noted = "Delete from department to shift.",
                            CreatedDate = DateTime.Now
                        };
                        _hrisBusinessLayer.RemoveShiftControl(shiftControl);
                    }
                }
                MessageBox.Show("ลบข้อมูลสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                GetShiftControlDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Warning: " + ex.Message, "ไม่สามารถลบข้อมูลได้", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShiftControlDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CountRemoveFromShift();
        }

        private void ShiftControlDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            CountRemoveFromShift();
        }

        private void CountRemoveFromShift()
        {
            int? selectedCount = ShiftControlDataGrid.SelectedItems.Count;
            var selectedLot = (Lot_NumberClass)shiftLotCombobox.SelectedItem;
            var isEnabled = (selectedCount > 0) && selectedLot.Lock_Acc != "2" && selectedLot.Lock_Acc_Labor != "2" && selectedLot.Lock_Hr_Labor != "2";
            removeFromEmptoShiftButton.Content = string.Format("Remove ({0})", selectedCount);
            removeFromEmptoShiftButton.IsEnabled = isEnabled;
        }

        private void CountShiftControlSaveButton()
        {
            var selectedLot = (Lot_NumberClass)shiftLotCombobox.SelectedItem;
            var isEnabled = (deptToShiftList.Count() > 0) && selectedLot.Lock_Acc != "2" && selectedLot.Lock_Acc_Labor != "2" && selectedLot.Lock_Hr_Labor != "2";
            exportShiftButton.IsEnabled = deptToShiftList.Count() > 0;
            SaveButton.IsEnabled = isEnabled;
            SaveButton.Content = string.Format("Save ({0})", deptToShiftList.Count());
        }

        private void EmptoShift_CalendarClosed(object sender, RoutedEventArgs e)
        {
            GetShiftControlDataGrid();
        }

        private void shiftCombobox_DropDownClosed(object sender, EventArgs e)
        {
            GetShiftControlDataGrid();
        }

        private void GetShiftControlDataGrid()
        {
            if (string.IsNullOrEmpty(shiftCombobox.Text)) return;
            if (string.IsNullOrEmpty(fromDatePicker.Text)) return;
            if (string.IsNullOrEmpty(toDatePicker.Text)) return;

            deptToShiftList.Clear();
            var shiftControlList = _hrisBusinessLayer.GetShiftControlByShiftAndDate(shiftCombobox.SelectedValue.ToString(), fromDatePicker.SelectedDate.Value, toDatePicker.SelectedDate.Value).ToList();
            if (shiftControlList.Count > 0)
            {
                foreach (var item in shiftControlList)
                {
                    deptToShiftList.Add(new ShiftControlViewModel
                    {
                        Employee_ID = item.Employee_ID,
                        Name = item.Employee.Person.FirstNameTH + " " + item.Employee.Person.LastNameTH,
                        ValidFrom = item.ValidFrom,
                        EndDate = item.EndDate
                    });
                }
            }
            ShiftControlDataGrid.ItemsSource = null;
            ShiftControlDataGrid.ItemsSource = deptToShiftList;
            CountRemoveFromShift();
            CountShiftControlSaveButton();
        }

        private void ShiftControlDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            // Null is first sorting it will be "Ascending"
            var directionDatagrid = e.Column.SortDirection == null ? ListSortDirection.Ascending : e.Column.SortDirection;
            var columnName = e.Column.SortMemberPath;

            if (!deptToShiftList.Any()) return;
            if (string.IsNullOrEmpty(columnName)) return;

            e.Handled = true; //Take event control

            if (columnName == "Employee_ID")
            {
                if (directionDatagrid == ListSortDirection.Ascending) deptToShiftList = deptToShiftList.OrderBy(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
                else deptToShiftList = deptToShiftList.OrderByDescending(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "Name")
            {
                if (directionDatagrid == ListSortDirection.Ascending) deptToShiftList = deptToShiftList.OrderBy(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
                else deptToShiftList = deptToShiftList.OrderByDescending(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "ValidFrom")
            {
                if (directionDatagrid == ListSortDirection.Ascending) deptToShiftList = deptToShiftList.OrderBy(o => o.ValidFrom).ThenBy(o => o.Employee_ID).ToList();
                else deptToShiftList = deptToShiftList.OrderByDescending(o => o.ValidFrom).ThenBy(o => o.Name).ToList();
            }
            else if (columnName == "EndDate")
            {
                if (directionDatagrid == ListSortDirection.Ascending) deptToShiftList = deptToShiftList.OrderBy(o => o.EndDate).ThenBy(o => o.Employee_ID).ToList();
                else deptToShiftList = deptToShiftList.OrderByDescending(o => o.EndDate).ThenBy(o => o.Name).ToList();
            }

            ShiftControlDataGrid.ItemsSource = null;
            ShiftControlDataGrid.ItemsSource = deptToShiftList;

            // Add new direction to column
            if (directionDatagrid == ListSortDirection.Ascending) e.Column.SortDirection = ListSortDirection.Descending;
            else e.Column.SortDirection = ListSortDirection.Ascending;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทึกข้อมูลหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                if (fromDatePicker.SelectedDate > toDatePicker.SelectedDate)
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedLot = (Lot_NumberClass)shiftLotCombobox.SelectedItem;

                if (selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (selectedLot.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedItem = ShiftControlDataGrid.Items;
                if (selectedItem.Count < 1) return;

                var validFrom = Convert.ToDateTime(fromDatePicker.Text);
                var endDate = Convert.ToDateTime(toDatePicker.Text);

                var shiftControlList = new List<DomainModel_HRISSystem.ShiftControl>();
                foreach (var selected in selectedItem)
                {
                    var item = (ShiftControlViewModel)selected;
                    // Insert only new shiftcontrol
                    if (item.ValidFrom == null && item.EndDate == null)
                    {
                        for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                        {
                            var shiftControl = new DomainModel_HRISSystem.ShiftControl
                            {
                                Shift_ID = (string)shiftCombobox.SelectedValue,
                                Employee_ID = item.Employee_ID,
                                ValidFrom = day,
                                EndDate = day,
                                Noted = "Insert from department to shift.",
                                CreatedDate = DateTime.Now
                            };
                            shiftControlList.Add(shiftControl);
                        }
                    }
                }

                if (shiftControlList.Any())
                {
                    DomainModel_HRISSystem.ShiftControl[] shiftControlArray = shiftControlList.ToArray();
                    _hrisBusinessLayer.AddShiftControl(shiftControlArray);
                }

                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                GetShiftControlDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Waring !!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void exportShiftButton_Click(object sender, RoutedEventArgs e)
        {
            ExcelExport(deptToShiftList, shiftCombobox.Text, fromDatePicker.Text, toDatePicker.Text);
        }

        private void clearmoveToShiftButton_Click(object sender, RoutedEventArgs e)
        {
            ClearMoveToShift();
        }

        private void ClearMoveToShift()
        {
            searchNameTextbox.Text = "";
            departmentCombobox.SelectedIndex = -1;
            EmployeeDataGrid.ItemsSource = null;
            CountMoveToShift();
        }

        private void clearShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            ClearShiftControl();
        }

        private void ClearShiftControl()
        {
            deptToShiftList.Clear();
            shiftCombobox.SelectedIndex = -1;
            fromDatePicker.Text = "";
            toDatePicker.Text = "";
            ShiftControlDataGrid.ItemsSource = null;
            CountShiftControlSaveButton();
            CountRemoveFromShift();
        }

        //  SHIFT CONTROL TO SHIFT CONTROL
        private void ShiftToShiftTab_Loaded(object sender, RoutedEventArgs e)
        {
            destinationShiftList.Clear();
            sourceShiftList.Clear();
            sourceShiftCombobox.ItemsSource = null;
            sourceShiftCombobox.ItemsSource = _shiftList();
            destinationShiftCombobox.ItemsSource = null;
            destinationShiftCombobox.ItemsSource = _shiftList();
        }

        private void tabItem2_Clicked(object sender, MouseButtonEventArgs e)
        {
            ClearSourceShiftControl();
            ClearDestinationShiftControl();
        }

        private void destinationLotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            destinationLotCombobox_Change();
        }

        private void destinationLotCombobox_Change()
        {
            ClearDestinationShiftControl();
            var selectedLot = (Lot_NumberClass)destinationLotCombobox.SelectedItem;
            if (selectedLot != null)
            {
                var startDate = selectedLot.Start_date;
                var finishDate = selectedLot.Finish_date;
                destinationLotFromDatePicker.SelectedDate = startDate;
                destinationLotToDatePicker.SelectedDate = finishDate;
                destinationFromDatePicker.DisplayDateStart = startDate;
                destinationToDatePicker.DisplayDateStart = startDate;
                destinationFromDatePicker.DisplayDateEnd = finishDate;
                destinationToDatePicker.DisplayDateEnd = finishDate;
            }
        }

        private void sourceShiftCombobox_DropDownClosed(object sender, EventArgs e)
        {
            Get_SourceShiftControl();
        }

        private void sourceFromDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            Get_SourceShiftControl();
        }

        private void sourceToDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            Get_SourceShiftControl();
        }

        private void searchSourceButton_Click(object sender, RoutedEventArgs e)
        {
            Get_SourceShiftControl();
        }

        private void Get_SourceShiftControl()
        {
            if (string.IsNullOrEmpty(sourceShiftCombobox.Text)) return;
            if (string.IsNullOrEmpty(sourceFromDatePicker.Text)) return;
            if (string.IsNullOrEmpty(sourceToDatePicker.Text)) return;

            sourceShiftList.Clear();
            var _shiftControlList = _hrisBusinessLayer.GetShiftControlByShiftAndDate(sourceShiftCombobox.SelectedValue.ToString(), sourceFromDatePicker.SelectedDate.Value, sourceToDatePicker.SelectedDate.Value).ToList();
            if (_shiftControlList.Count > 0)
            {
                foreach (var item in _shiftControlList)
                {
                    sourceShiftList.Add(new ShiftControlViewModel
                    {
                        Employee_ID = item.Employee_ID,
                        Name = item.Employee.Person.FirstNameTH + " " + item.Employee.Person.LastNameTH,
                        ValidFrom = item.ValidFrom,
                        EndDate = item.EndDate
                    });
                }
            }
            sourceShiftControlDataGrid.ItemsSource = null;
            sourceShiftControlDataGrid.ItemsSource = sourceShiftList;
            CountMoveSourceShift();
        }

        private void sourceShiftControlDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            var directionDatagrid = e.Column.SortDirection == null ? ListSortDirection.Ascending : e.Column.SortDirection;
            var columnName = e.Column.SortMemberPath;

            if (!sourceShiftList.Any()) return;
            if (string.IsNullOrEmpty(columnName)) return;

            e.Handled = true; //Take event control

            if (columnName == "Employee_ID")
            {
                if (directionDatagrid == ListSortDirection.Ascending) sourceShiftList = sourceShiftList.OrderBy(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
                else sourceShiftList = sourceShiftList.OrderByDescending(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "Name")
            {
                if (directionDatagrid == ListSortDirection.Ascending) sourceShiftList = sourceShiftList.OrderBy(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
                else sourceShiftList = sourceShiftList.OrderByDescending(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "ValidFrom")
            {
                if (directionDatagrid == ListSortDirection.Ascending) sourceShiftList = sourceShiftList.OrderBy(o => o.ValidFrom).ThenBy(o => o.Employee_ID).ToList();
                else sourceShiftList = sourceShiftList.OrderByDescending(o => o.ValidFrom).ThenBy(o => o.Name).ToList();
            }
            else if (columnName == "EndDate")
            {
                if (directionDatagrid == ListSortDirection.Ascending) sourceShiftList = sourceShiftList.OrderBy(o => o.EndDate).ThenBy(o => o.Employee_ID).ToList();
                else sourceShiftList = sourceShiftList.OrderByDescending(o => o.EndDate).ThenBy(o => o.Name).ToList();
            }

            sourceShiftControlDataGrid.ItemsSource = null;
            sourceShiftControlDataGrid.ItemsSource = sourceShiftList;

            // Add new direction to column
            if (directionDatagrid == ListSortDirection.Ascending) e.Column.SortDirection = ListSortDirection.Descending;
            else e.Column.SortDirection = ListSortDirection.Ascending;
        }

        private void sourceShiftControlDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CountMoveSourceShift();
        }

        private void sourceShiftControlDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            CountMoveSourceShift();
        }

        private void CountMoveSourceShift()
        {
            int? selectedCount = sourceShiftControlDataGrid.SelectedItems.Count;
            sourceMoveButton.Content = string.Format("Add >> ({0})", selectedCount);
            sourceMoveButton.IsEnabled = selectedCount > 0 ? true : false;
        }

        private void sourceClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearSourceShiftControl();
        }

        private void ClearSourceShiftControl()
        {
            sourceShiftCombobox.SelectedIndex = -1;
            sourceFromDatePicker.Text = "";
            sourceToDatePicker.Text = "";
            sourceShiftList.Clear();
            sourceShiftControlDataGrid.ItemsSource = null;
            CountMoveSourceShift();
        }

        private void sourceMoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(destinationShiftCombobox.Text) || string.IsNullOrEmpty(destinationFromDatePicker.Text) || string.IsNullOrEmpty(destinationToDatePicker.Text))
            {
                MessageBox.Show("กรุณาเลือกข้อมูลปลายทาง", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var selectedItem = sourceShiftControlDataGrid.SelectedItems;
            foreach (var item in selectedItem)
            {
                var selectedShift = (ShiftControlViewModel)item;
                var newShift = new ShiftControlViewModel { Employee_ID = selectedShift.Employee_ID, Name = selectedShift.Name };

                if ((!destinationShiftList.Any()))
                {
                    destinationShiftList.Add(newShift);
                }
                else
                {
                    //if not existed
                    if (!destinationShiftList.Where(w => w.Employee_ID == (newShift).Employee_ID).Any())
                    {
                        destinationShiftList.Add(newShift);
                    }
                }

            }
            destinationShiftControlDataGrid.ItemsSource = null;
            destinationShiftControlDataGrid.ItemsSource = destinationShiftList;
            CountDestinaionSaveButton();
        }

        private void destinationShiftCombobox_DropDownClosed(object sender, EventArgs e)
        {
            Get_DestinationShiftControl();
        }

        private void destinationFromDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            Get_DestinationShiftControl();
        }

        private void destinationToDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            Get_DestinationShiftControl();
        }

        private void Get_DestinationShiftControl()
        {
            if (string.IsNullOrEmpty(destinationShiftCombobox.Text)) return;
            if (string.IsNullOrEmpty(destinationFromDatePicker.Text)) return;
            if (string.IsNullOrEmpty(destinationToDatePicker.Text)) return;

            destinationShiftList.Clear();
            var shiftControlList = _hrisBusinessLayer.GetShiftControlByShiftAndDate(destinationShiftCombobox.SelectedValue.ToString(), destinationFromDatePicker.SelectedDate.Value, destinationToDatePicker.SelectedDate.Value).ToList();
            if (shiftControlList.Count > 0)
            {
                foreach (var item in shiftControlList)
                {
                    destinationShiftList.Add(new ShiftControlViewModel
                    {
                        Employee_ID = item.Employee_ID,
                        Name = item.Employee.Person.FirstNameTH + " " + item.Employee.Person.LastNameTH,
                        ValidFrom = item.ValidFrom,
                        EndDate = item.EndDate
                    });
                }
            }
            destinationShiftControlDataGrid.ItemsSource = null;
            destinationShiftControlDataGrid.ItemsSource = destinationShiftList;
            CountDestinaionSaveButton();
            CountRemoveDestinationShift();
        }

        private void destinationShiftControlDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            var directionDatagrid = e.Column.SortDirection == null ? ListSortDirection.Ascending : e.Column.SortDirection;
            var columnName = e.Column.SortMemberPath;

            if (!destinationShiftList.Any()) return;
            if (string.IsNullOrEmpty(columnName)) return;

            e.Handled = true; //Take event control

            if (columnName == "Employee_ID")
            {
                if (directionDatagrid == ListSortDirection.Ascending) destinationShiftList = destinationShiftList.OrderBy(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
                else destinationShiftList = destinationShiftList.OrderByDescending(o => o.Employee_ID).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "Name")
            {
                if (directionDatagrid == ListSortDirection.Ascending) destinationShiftList = destinationShiftList.OrderBy(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
                else destinationShiftList = destinationShiftList.OrderByDescending(o => o.Name).ThenBy(o => o.ValidFrom).ToList();
            }
            else if (columnName == "ValidFrom")
            {
                if (directionDatagrid == ListSortDirection.Ascending) destinationShiftList = destinationShiftList.OrderBy(o => o.ValidFrom).ThenBy(o => o.Employee_ID).ToList();
                else destinationShiftList = destinationShiftList.OrderByDescending(o => o.ValidFrom).ThenBy(o => o.Name).ToList();
            }
            else if (columnName == "EndDate")
            {
                if (directionDatagrid == ListSortDirection.Ascending) destinationShiftList = destinationShiftList.OrderBy(o => o.EndDate).ThenBy(o => o.Employee_ID).ToList();
                else destinationShiftList = destinationShiftList.OrderByDescending(o => o.EndDate).ThenBy(o => o.Name).ToList();
            }

            destinationShiftControlDataGrid.ItemsSource = null;
            destinationShiftControlDataGrid.ItemsSource = destinationShiftList;

            // Add new direction to column
            if (directionDatagrid == ListSortDirection.Ascending) e.Column.SortDirection = ListSortDirection.Descending;
            else e.Column.SortDirection = ListSortDirection.Ascending;
        }

        private void CountDestinaionSaveButton()
        {
            var selectedLot = (Lot_NumberClass)destinationLotCombobox.SelectedItem;
            var isEnabled = (destinationShiftList.Count() > 0) && selectedLot.Lock_Acc != "2" && selectedLot.Lock_Acc_Labor != "2" && selectedLot.Lock_Hr_Labor != "2";
            destinationSaveButton.IsEnabled = isEnabled;
            exportDestinationButton.IsEnabled = destinationShiftList.Count() > 0;
            destinationSaveButton.Content = string.Format("Save ({0})", destinationShiftList.Count());
        }

        private void destinationShiftControlDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CountRemoveDestinationShift();
        }

        private void destinationShiftControlDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            CountRemoveDestinationShift();
        }

        private void CountRemoveDestinationShift()
        {
            int? selectedCount = destinationShiftControlDataGrid.SelectedItems.Count;
            var selectedLot = (Lot_NumberClass)destinationLotCombobox.SelectedItem;
            var isEnabled = (selectedCount > 0) && selectedLot.Lock_Acc != "2" && selectedLot.Lock_Acc_Labor != "2" && selectedLot.Lock_Hr_Labor != "2";
            destinationRemoveButton.IsEnabled = isEnabled;
            destinationRemoveButton.Content = string.Format("Remove ({0})", selectedCount);
        }

        private void destinationRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedLot = (Lot_NumberClass)destinationLotCombobox.SelectedItem;

                if (selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (selectedLot.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Waring !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedItem = destinationShiftControlDataGrid.SelectedItems;

                if (destinationShiftControlDataGrid.SelectedItems.Count < 1) return;
                if (MessageBox.Show("ยืนยันการลบ (" + destinationShiftControlDataGrid.SelectedItems.Count + ") ข้อมูลหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                foreach (var item in selectedItem)
                {
                    var i = (ShiftControlViewModel)item;
                    // Remove only existed shift control
                    if (i.ValidFrom != null && i.EndDate != null)
                    {
                        var shiftControl = new DomainModel_HRISSystem.ShiftControl
                        {
                            Shift_ID = (string)shiftCombobox.SelectedValue,
                            Employee_ID = i.Employee_ID,
                            ValidFrom = i.ValidFrom.Value,
                            EndDate = i.EndDate,
                            Noted = "Delete from department to shift.",
                            CreatedDate = DateTime.Now
                        };
                        _hrisBusinessLayer.RemoveShiftControl(shiftControl);
                    }
                }
                MessageBox.Show("ลบข้อมูลสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                Get_DestinationShiftControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "ไม่สามารถลบข้อมูลได้", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void destinationSaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทึกข้อมูลหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                if (destinationFromDatePicker.SelectedDate > destinationToDatePicker.SelectedDate)
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedLot = (Lot_NumberClass)destinationLotCombobox.SelectedItem;

                if (selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (selectedLot.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var selectedItem = destinationShiftControlDataGrid.Items;
                if (selectedItem.Count < 1) return;

                var validFrom = Convert.ToDateTime(destinationFromDatePicker.Text);
                var endDate = Convert.ToDateTime(destinationToDatePicker.Text);

                var shiftControlList = new List<DomainModel_HRISSystem.ShiftControl>();
                foreach (var selected in selectedItem)
                {
                    var item = (ShiftControlViewModel)selected;
                    // Insert only new shiftcontrol
                    if (item.ValidFrom == null && item.EndDate == null)
                    {
                        for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                        {
                            var shiftControl = new DomainModel_HRISSystem.ShiftControl
                            {
                                Shift_ID = (string)destinationShiftCombobox.SelectedValue,
                                Employee_ID = item.Employee_ID,
                                ValidFrom = day,
                                EndDate = day,
                                Noted = "Insert from department to shift.",
                                CreatedDate = DateTime.Now
                            };
                            shiftControlList.Add(shiftControl);
                        }
                    }
                }

                if (shiftControlList.Any())
                {
                    DomainModel_HRISSystem.ShiftControl[] shiftControlArray = shiftControlList.ToArray();
                    _hrisBusinessLayer.AddShiftControl(shiftControlArray);
                }

                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                Get_DestinationShiftControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Waring !!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void destinationClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDestinationShiftControl();
        }

        private void ClearDestinationShiftControl()
        {
            destinationShiftList.Clear();
            destinationShiftCombobox.SelectedIndex = -1;
            destinationFromDatePicker.Text = "";
            destinationToDatePicker.Text = "";
            destinationShiftControlDataGrid.ItemsSource = null;
            CountDestinaionSaveButton();
            CountRemoveDestinationShift();
        }

        private void exportDestinationButton_Click(object sender, RoutedEventArgs e)
        {
            ExcelExport(destinationShiftList, destinationShiftCombobox.Text, destinationFromDatePicker.Text, destinationToDatePicker.Text);
        }

        private void ExcelExport(List<ShiftControlViewModel> exportList, string shiftTime, string fromDate, string toDate)
        {
            try
            {
                if (exportList.Count() < 1)
                {
                    MessageBox.Show("ไม่พบข้อมูลที่ต้องการ Export", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = ExportShiftControlDetail(folderDlg.SelectedPath, exportList, shiftTime, fromDate, toDate);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public string ExportShiftControlDetail(string folderName, List<ShiftControlViewModel> exportList, string shiftTime, string fromDate, string toDate)
        {
            string name = @"" + folderName + "\\HRIS-Shift-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";

            FileInfo info = new FileInfo(name);
            if (info.Exists) File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                //ExcelWorksheet ws = pck.Workbook.Worksheets.Add(destinationFromDatePicker.SelectedDate.Value.ToString("0:d") + " - " + destinationToDatePicker.SelectedDate.Value.ToString("0:d"));
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Shift Control");
                ws.Cells["A1"].Value = "Shift : " + shiftTime;
                ws.Cells["A2"].Value = "Date : " + fromDate + " - " + toDate;
                ws.Cells["A3"].LoadFromCollection(exportList, true);
                ws.DeleteColumn(3); // Remove column Staff_type
                ws.DeleteColumn(3); // Remove column Staff_status
                ws.Protection.IsProtected = false;
                // (fromRow, fromColumn, toColumn, intoColumn)
                using (ExcelRange col = ws.Cells[4, 3, 3 + exportList.Count, 3]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[4, 4, 3 + exportList.Count, 4]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }
    }
}
