﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for HumanResource_WorkUpCountry.xaml
    /// </summary>
    public partial class HumanResource_WorkUpCountry : Page
    {
        public List<WorkUpCountry> ObjList;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();


        private class StatusFlags
        {
            public bool StatusFlag { get; set; }
            public string StatusFlagName { get; set; }
        }

        private class NotedFlags
        {
            public int NoteFlag { get; set; }
            public string NoteFlagName { get; set; }
        }

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public HumanResource_WorkUpCountry()
        {
            InitializeComponent();

            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };

            statusFlagList.Add(h1);
            statusFlagList.Add(h2);

            cmbStatus.ItemsSource = statusFlagList;
            cmbStatus.SelectedIndex = 0;

            List<NotedFlags> notedFlagList = new List<NotedFlags>();
            NotedFlags n1 = new NotedFlags { NoteFlag = 1, NoteFlagName = "ปฏิบัติงานต่างจังหวัด(Workup country)" };
            NotedFlags n2 = new NotedFlags { NoteFlag = 2, NoteFlagName = "ปฏิบัติงานนอกสถานที่(Off-site work)" };

            notedFlagList.Add(n1);
            notedFlagList.Add(n2);
            txtNoted.ItemsSource = notedFlagList;
            txtNoted.SelectedIndex = 1;


            ObjList = new List<WorkUpCountry>();

            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        public HumanResource_WorkUpCountry(EmployeeCurrentInformation x3)
        {

            InitializeComponent();

            List<StatusFlags> statusFlagList = new List<StatusFlags>();
            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbStatus.ItemsSource = statusFlagList;
            cmbStatus.SelectedIndex = 0;

            List<NotedFlags> notedFlagList = new List<NotedFlags>();
            NotedFlags n1 = new NotedFlags { NoteFlag = 1, NoteFlagName = "ปฏิบัติงานต่างจังหวัด(Workup country)" };
            NotedFlags n2 = new NotedFlags { NoteFlag = 2, NoteFlagName = "ปฏิบัติงานนอกสถานที่(Off-site work)" };
            notedFlagList.Add(n1);
            notedFlagList.Add(n2);
            txtNoted.ItemsSource = notedFlagList;
            txtNoted.SelectedIndex = 1;

            ObjList = new List<WorkUpCountry>();
            xFromEmployeeCurrentInformation = x3;
            if (x3 != null)
            {
                GetEmployeeInformation(x3);
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = x3.xFirstNameTH ;
                txtSearch.IsEnabled = true;
            }

            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            //EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
        }
        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                WorkDate.IsEnabled = true;
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";
                txtNoted.Text = "";

                //DataGrid.ItemsSource = null; //clear 
                //DataGrid.ItemsSource = _hrisBusinessLayer.GetAllWorkUpCountry();
                
            }
           catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void GetDataFromSearch()
        {
            try
            {
                ObjList = _hrisBusinessLayer.GetAllWorkUpCountry().ToList();

                DataGrid.ItemsSource = null;
                if ((LeaveDateFromSearch.Text != "") && (LeaveDateToSearch.Text != ""))
                {
                    if (ChkAllSearch.IsChecked == true)
                    {
                        DataGrid.ItemsSource = ObjList.Where(p => p.WorkDate >= Convert.ToDateTime(LeaveDateFromSearch.Text) && p.WorkDate <= Convert.ToDateTime(LeaveDateToSearch.Text));
                    }
                    else if (ChkFilterSearch.IsChecked == true)
                    {
                        DataGrid.ItemsSource = ObjList.Where(p => p.Employee.Person.FirstNameTH.Contains(txtSearch.Text) && p.WorkDate >= Convert.ToDateTime(LeaveDateFromSearch.Text) && p.WorkDate <= Convert.ToDateTime(LeaveDateToSearch.Text));
                    }                   
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();

                LeaveDateFromSearch.Text = FirstDayOfMonth(DateTime.Now).ToString();


               
                LeaveDateToSearch.Text = DateTime.Now.ToString();

                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = ObjList;


                lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
                cmbLot.ItemsSource = null;
                cmbLot.ItemsSource = lot_NumberClassList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private Boolean ChkDateRange(DateTime _date)//สามารถลงวันที่ย้อนหลังได้ไม่เกินวันที่ 26 ของเดือนที่แล้ว
        {
            Boolean xresult = false;
            DateTime date = DateTime.Now;
            //var endPreviouseMonth = new DateTime(date.Year, date.Month - 1, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
            //var lastCurrentMonth = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้

            var endPreviouseMonth = date;
            //var lastCurrentMonth = date;
            //var lastCurrentMonth2 = date;
            //ถ้าเป็นเดือนมกราคม ปีจะต้องลดไป1
            if (date.Month == 1)
            {
                endPreviouseMonth = new DateTime(date.Year - 1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                //lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                //lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            else
            {
                endPreviouseMonth = new DateTime(date.Year, date.Month - 1, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                //lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                //lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            if (_date < Convert.ToDateTime(endPreviouseMonth))
            {
                xresult = false;
            }
            else
            {
                xresult = true;
            }
            return xresult;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (DateFromSearch.Text == "" || DateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (WorkDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำงานต่างจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (Convert.ToDateTime(WorkDate.Text) < Convert.ToDateTime(DateFromSearch.Text))
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Convert.ToDateTime(WorkDate.Text) > Convert.ToDateTime(DateToSearch.Text))
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //else if (ChkDateRange(Convert.ToDateTime(WorkDate.Text)) == false)
                //{
                //    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วจนถึงวันที่ 25 ของเดือนนี้เท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //else if (Convert.ToDateTime(WorkDate.Text).Year > DateTime.Now.Year + 1 || Convert.ToDateTime(WorkDate.Text).Year < DateTime.Now.Year - 1)
                //{
                //    MessageBox.Show("กรุณาคีย์วันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                Boolean XChkDayOfWeek2 = IsWeekend(Convert.ToDateTime(WorkDate.Text));
                if (XChkDayOfWeek2)
                {
                    MessageBox.Show("วันที่นี้เป็นวันอาทิตย์ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbStatus.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์Status", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                WorkUpCountry[] workUpCountryObj = new WorkUpCountry[1];
                WorkUpCountry query1 = new WorkUpCountry();  // created 1 instance or object

                query1.Employee_ID = txtEmployee_ID.Text;
                query1.WorkDate = Convert.ToDateTime(WorkDate.Text);                
                query1.Status = (Boolean)cmbStatus.SelectedValue;
                query1.Noted = txtNoted.Text;
                query1.ModifiedDate = DateTime.Now;

                workUpCountryObj[0] = query1;
                _hrisBusinessLayer.AddWorkUpcountry(workUpCountryObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T010";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(WorkDate.Text);
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtNoted.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                LeaveDateFromSearch.Text = WorkDate.Text;
                LeaveDateToSearch.Text = WorkDate.Text;
                GetDataFromSearch();
                //ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (DateFromSearch.Text == "" || DateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (WorkDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำงานต่างจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Convert.ToDateTime(WorkDate.Text) < Convert.ToDateTime(DateFromSearch.Text))
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Convert.ToDateTime(WorkDate.Text) > Convert.ToDateTime(DateToSearch.Text))
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                Boolean XChkDayOfWeek2 = IsWeekend(Convert.ToDateTime(WorkDate.Text));
                if (XChkDayOfWeek2)
                {
                    MessageBox.Show("วันที่นี้เป็นวันอาทิตย์ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbStatus.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์Status", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                WorkUpCountry query1 = new WorkUpCountry();  // created 1 instance or object

                query1.Employee_ID = txtEmployee_ID.Text;
                query1.WorkDate = Convert.ToDateTime(WorkDate.Text);
                query1.Status = (Boolean)cmbStatus.SelectedValue;
                query1.Noted = txtNoted.Text;
                query1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateWorkUpcountry(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T010";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(WorkDate.Text);
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtNoted.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetDataFromSearch();
                //ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                WorkUpCountry query1 = new WorkUpCountry();  // created 1 instance or object
                query1 = (WorkUpCountry)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                _hrisBusinessLayer.RemoveWorkUpcountry(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T010";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(WorkDate.Text);
                transactionLog1.OldData = txtNoted.Text;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                WorkUpCountry query1 = new WorkUpCountry();  // created 1 instance or object
                query1 = (WorkUpCountry)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                    WorkDate.IsEnabled = true;

                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtEmployee_ID.IsReadOnly = true;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.LightGray);
                    WorkDate.IsEnabled = false;

                    GetDataFromEmployee_ID(query1.Employee_ID);
                }
                //GetEmployeeInformation();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(LeaveDateFromSearch.Text == "" || LeaveDateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("WorkUpCountry"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;

                DateFromSearch.IsEnabled = false;
                DateToSearch.IsEnabled = false;
                DateFromSearch.Text = lot_numberList.Start_date.ToString();
                DateToSearch.Text = lot_numberList.Finish_date.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
