﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace DataAccessLayer_HRISSystem
{
    public interface IGenericTimeSTECDataRepository<T> where T : class
    {
        IList<T> GetAllTimeSTEC(params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetListTimeSTEC(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingleTimeSTEC(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void AddTimeSTEC(params T[] items);
        void UpdateTimeSTEC(params T[] items);
        void RemoveTimeSTEC(params T[] items);
    }
}
