﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.ToPayroll
{
    /// <summary>
    /// Interaction logic for ToPayrollMenu.xaml
    /// </summary>
    public partial class ToPayrollMenu : Page
    {
        public ToPayrollMenu()
        {
            InitializeComponent();
        }

        private void NavigateToOTButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ToPayroll.OTToPayroll());
        }

        private void NavigateToWorkDateButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ToPayroll.LabourWorkingToPayroll());
        }

        private void NavigateToHRLockButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ToPayroll.HRToPayrollSetup());
        }

        private void NavigateToPrintPaySlipButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Report.RepRPTHR026());
        }
    }
}
