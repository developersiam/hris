﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for WalefareMenu.xaml
    /// </summary>
    public partial class WalefareMenu : Page
    {
        public WalefareMenu()
        {
            InitializeComponent();
        }

    
        private void NavigateToWorkUpCountryButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Walefares.HumanResource_WorkUpCountry());
        }

        private void NavigateToLeaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Walefares.HumanResource_Leave());
        }

        private void NavigateToOTButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Walefares.HumanResource_OT());
        }
      
        private void NavigateToSocialWalfareButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Walefares.SocialWalfare());
        }
    }
}
