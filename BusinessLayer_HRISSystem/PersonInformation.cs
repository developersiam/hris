﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem
{
    public class PersonInformation : Person
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public string TitleNameTH { get; set; } //properties
        //public string FirstNameTH { get; set; } //properties
        //public string LastNameTH { get; set; } //properties
        public string IDCard { get; set; } //properties
        public string GenDer_Name { get; set; }        
        public string MaritalStatus_Name { get; set; }
        public string Nationality_Name { get; set; }
        public string Race_Name { get; set; }
        public string Religion_Name { get; set; }
        public string Blood_Name { get; set; }
        public DateTime IDCardIssuedDate { get; set; } //properties
        public DateTime IDCardExpiredDate { get; set; } //properties
        public string IDCardIssuedAtProvince { get; set; } //properties
        public string IDCardIssuedAtDistrict { get; set; } //properties

        public string AddressByPermanentHomeNumber { get; set; }
        public int AddressByPermanentMoo { get; set; }
        public string AddressByPermanentHousingProject { get; set; }
        public string AddressByPermanentStreet { get; set; }
        public int AddressByPermanentSoi { get; set; }
        public string AddressByPermanentHomePhoneNumber { get; set; }
        public string AddressByPermanentSubDistrict { get; set; }
        public string AddressByPermanentDistrict { get; set; }
        public string AddressByPermanentProvince { get; set; }
        public decimal AddressByPermanentPostcode { get; set; }
        public string AddressByPermanentCountry { get; set; }

        public string AddressByIDCARDHomeNumber { get; set; }
        public int AddressByIDCARDMoo { get; set; }
        public string AddressByIDCARDHousingProject { get; set; }
        public string AddressByIDCARDStreet { get; set; }
        public int AddressByIDCARDSoi { get; set; }
        public string AddressByIDCARDHomePhoneNumber { get; set; }
        public string AddressByIDCARDSubDistrict { get; set; }
        public string AddressByIDCARDDistrict { get; set; }
        public string AddressByIDCARDProvince { get; set; }
        public decimal AddressByIDCARDPostcode { get; set; }
        public string AddressByIDCARDCountry { get; set; }

        public string AddressByCurrentHomeNumber { get; set; }
        public int AddressByCurrentMoo { get; set; }
        public string AddressByCurrentHousingProject { get; set; }
        public string AddressByCurrentStreet { get; set; }
        public int AddressByCurrentSoi { get; set; }
        public string AddressByCurrentHomePhoneNumber { get; set; }
        public string AddressByCurrentSubDistrict { get; set; }
        public string AddressByCurrentDistrict { get; set; }
        public string AddressByCurrentProvince { get; set; }
        public decimal AddressByCurrentPostcode { get; set; }
        public string AddressByCurrentCountry { get; set; }
        public string xPersonID { get; set; }
        public PersonInformation(string person_ID)
        {
            Person person = new Person();
            person = _hrisBusinessLayer.GetPerson(person_ID);
            if (person != null)
            {
                this.xPersonID = person.Person_ID;
                this.TitleNameTH = person.TitleName.TitleNameTH;
                this.FirstNameTH = person.FirstNameTH;
                this.FirstNameEN = person.FirstNameEN;
                this.LastNameTH = person.LastNameTH;
                this.LastNameEN = person.LastNameEN;
                this.MidNameTH = person.MidNameTH;
                this.MidNameEN = person.MidNameEN;
                this.DateOfBirth = person.DateOfBirth;
                this.Email = person.Email;
                this.Weight = person.Weight;
                this.Height = person.Height;
                this.Blood_ID = person.Blood_ID;
                this.Blood_Name = person.Blood_ID != null ? person.Blood.BloodGroupTH : "";
                this.Gender_ID = person.Gender_ID;
                this.GenDer_Name = person.Gender_ID != null ? person.Gender.GenderTH : "";
                this.MaritalStatus_ID = person.MaritalStatus_ID;
                this.MaritalStatus_Name = person.MaritalStatus_ID != null ? person.MaritalStatu.MaritalStatusName:"";
                this.Nationality_ID = person.Nationality_ID;
                this.Nationality_Name = person.Nationality_ID != null ? person.Nationality.NationalityTH:"";
                this.Race_ID = person.Race_ID;
                this.Race_Name = person.Race_ID != null ? person.Race.RaceTH : "";
                this.Religion_ID = person.Religion_ID;
                this.Religion_Name = person.Religion_ID != null ? person.Religion.ReligionTH : "";
                



                IDCardInfo idCardInfo = _hrisBusinessLayer.GetIDCardInfo(person_ID).OrderByDescending(c=>c.ExpiredDate).FirstOrDefault();
                if (idCardInfo != null)
                {
                    this.IDCard = idCardInfo.Card_ID;
                    this.IDCardIssuedDate = Convert.ToDateTime(idCardInfo.IssuedDate);
                    this.IDCardExpiredDate = Convert.ToDateTime(idCardInfo.ExpiredDate);
                    if (idCardInfo.Province != null)
                        this.IDCardIssuedAtProvince = idCardInfo.Province.ProvinceNameTH;

                    if (idCardInfo.District != null)
                        this.IDCardIssuedAtDistrict = idCardInfo.District.DistrictNameTH;
                }
                //foreach (IDCardInfo c in idCardInfo)
                //{
                //    if (c.Card_ID != null)
                //    {
                //        this.IDCard = c.Card_ID;
                //        this.IDCardIssuedDate = Convert.ToDateTime(c.IssuedDate);
                //        this.IDCardExpiredDate = Convert.ToDateTime(c.ExpiredDate);
                //        if (c.Province != null)
                //            this.IDCardIssuedAtProvince = c.Province.ProvinceNameTH;

                //        if (c.District != null)
                //            this.IDCardIssuedAtDistrict = c.District.DistrictNameTH;
                //    }
                //}


                IList<Address> addressByPermanent = _hrisBusinessLayer.GetAddressByPerson(person.Person_ID);
                foreach (Address c in addressByPermanent)
                {
                    if(c.AddressType_ID == "1")//ตามบัตรประชาชน
                    {
                        this.AddressByIDCARDHomeNumber = c.HomeNumber;
                        this.AddressByIDCARDMoo = Convert.ToInt16(c.Moo);
                        this.AddressByIDCARDHousingProject = c.HousingProject;
                        this.AddressByIDCARDStreet = c.Street;
                        this.AddressByIDCARDSoi = Convert.ToInt16(c.Soi);

                        SubDistrict subDistrict = _hrisBusinessLayer.GetSubDistrict(c.SubDistrict_ID);
                        this.AddressByIDCARDSubDistrict = subDistrict.SubDistrictNameTH;

                        District district = _hrisBusinessLayer.GetDistrict(subDistrict.District_ID);
                        this.AddressByIDCARDDistrict = district.DistrictNameTH;

                        Province province = _hrisBusinessLayer.GetProvince(district.Province_ID);
                        this.AddressByIDCARDProvince = province.ProvinceNameTH;

                        this.AddressByIDCARDPostcode = Convert.ToDecimal(c.Postcode);

                        Country country = _hrisBusinessLayer.GetCountry(c.Country_ID);
                        this.AddressByIDCARDCountry = country.CountryNameTH;

                        this.AddressByIDCARDHomePhoneNumber = c.HomePhoneNumber;
                    }
                    else if(c.AddressType_ID == "2")//ตามทะเบียนบ้าน
                    {
                        this.AddressByPermanentHomeNumber = c.HomeNumber;
                        this.AddressByPermanentMoo = Convert.ToInt16(c.Moo);
                        this.AddressByPermanentHousingProject = c.HousingProject;
                        this.AddressByPermanentStreet = c.Street;
                        this.AddressByPermanentSoi = Convert.ToInt16(c.Soi);

                        SubDistrict subDistrict = _hrisBusinessLayer.GetSubDistrict(c.SubDistrict_ID);
                        this.AddressByPermanentSubDistrict = subDistrict.SubDistrictNameTH;

                        District district = _hrisBusinessLayer.GetDistrict(subDistrict.District_ID);
                        this.AddressByPermanentDistrict = district.DistrictNameTH;

                        Province province = _hrisBusinessLayer.GetProvince(district.Province_ID);
                        this.AddressByPermanentProvince = province.ProvinceNameTH;

                        this.AddressByPermanentPostcode = Convert.ToDecimal(c.Postcode);

                        Country country = _hrisBusinessLayer.GetCountry(c.Country_ID);
                        this.AddressByPermanentCountry = country.CountryNameTH;

                        this.AddressByIDCARDHomePhoneNumber = c.HomePhoneNumber;

                        
                    }
                    else if (c.AddressType_ID == "3")//ที่อยู่ปัจจุบัน
                    {
                        this.AddressByCurrentHomeNumber = c.HomeNumber;
                        this.AddressByCurrentMoo = Convert.ToInt16(c.Moo);
                        this.AddressByCurrentHousingProject = c.HousingProject;
                        this.AddressByCurrentStreet = c.Street;
                        this.AddressByCurrentSoi = Convert.ToInt16(c.Soi);

                        SubDistrict subDistrict = _hrisBusinessLayer.GetSubDistrict(c.SubDistrict_ID);
                        this.AddressByCurrentSubDistrict = subDistrict.SubDistrictNameTH;

                        District district = _hrisBusinessLayer.GetDistrict(subDistrict.District_ID);
                        this.AddressByCurrentDistrict = district.DistrictNameTH;

                        Province province = _hrisBusinessLayer.GetProvince(district.Province_ID);
                        this.AddressByCurrentProvince = province.ProvinceNameTH;

                        this.AddressByCurrentPostcode = Convert.ToDecimal(c.Postcode);

                        Country country = _hrisBusinessLayer.GetCountry(c.Country_ID);
                        this.AddressByCurrentCountry = country.CountryNameTH;

                        this.AddressByCurrentHomePhoneNumber = c.HomePhoneNumber;
                    }

                    
                }

            }
        }

       
    }
}
