﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Security.Principal;
using MahApps.Metro.Controls.Dialogs;
using DomainModel_HRISSystem;

namespace HRISSystem
{
    /// <summary>
    /// Interaction logic for LoginForm.xaml
    /// </summary>
    public partial class LoginForm : Window
    {
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        PrincipalContext myDomain;
        UserPrincipal user;

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                txtUsername.Text = string.Empty;
                txtPassword.Password = string.Empty;

                txtUsername.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            try
            {
                //txtUsername.Text = "Saruta";
                //txtPassword.Password = "Saru0898537733";

                if (string.IsNullOrEmpty(txtUsername.Text))
                {
                    MessageBox.Show("กรุณาระบุ Username และPassword", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //--test
                //_singleton.username = txtUsername.Text;
                //_singleton.LoggedIn = DateTime.Now;
                //MainWindow p1 = new MainWindow(txtUsername.Text);
                //p1.Show();
                //--test


                //--live
                //PrincipalContext ctx = new PrincipalContext(ContextType.Domain,null, txtUsername.Text, txtPassword.Password);
                //UserPrincipal userIT = UserPrincipal.FindByIdentity(ctx, txtUsername.Text);
                //GroupPrincipal groupIT = GroupPrincipal.FindByIdentity(ctx, "IT");


                //if (userIT != null)
                //{
                //    if (userIT.IsMemberOf(groupIT))
                //    {
                //        _singleton.username = txtUsername.Text;
                //        _singleton.LoggedIn = DateTime.Now;
                //        _singleton.emailFrom = userIT.EmailAddress;
                //        _singleton.deptCode = GetOU(_singleton.username);


                //        MainWindow p1 = new MainWindow(txtUsername.Text);
                //        p1.Show();
                //    }
                //    else
                //    {
                //        UserPrincipal userHRM = UserPrincipal.FindByIdentity(ctx, txtUsername.Text);
                //        GroupPrincipal groupHRM = GroupPrincipal.FindByIdentity(ctx, "HRM");
                //        if (userHRM != null)
                //        {
                //            if (userHRM.IsMemberOf(groupHRM))
                //            {
                //                _singleton.username = txtUsername.Text;
                //                _singleton.LoggedIn = DateTime.Now;

                //                MainWindow p1 = new MainWindow(txtUsername.Text);
                //                p1.Show();
                //            }

                //        }
                //    }
                //}
                //else
                //{

                //    MessageBox.Show("คุณไม่สามารถล๊อกอินเข้าระบบได้ กรุณาตรวจสอบสิทธิ์การใช้งาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //--live

                //live




                //Getting the domain;
                myDomain = new PrincipalContext(ContextType.Domain, null, txtUsername.Text, txtPassword.Password);

                _singleton.username = txtUsername.Text;
                _singleton.LoggedIn = DateTime.Now;
                _singleton.emailFrom = GetOU(txtUsername.Text, "Email");
                _singleton.deptCode = GetOU(txtUsername.Text, "OU");



                MainWindow p1 = new MainWindow(txtUsername.Text, _singleton.deptCode);

                if (_singleton.deptCode != "IT" && _singleton.deptCode != "HRM")
                {
                    //ให้เช็คต่อว่าได้รับสิทธิ์ให้สามารถคีย์ request OT ได้หรือไม่

                    p1.ShowDialog();

                }
                else if (_singleton.deptCode == "IT" || _singleton.deptCode == "HRM" || _singleton.deptCode == "ACC")
                {
                    HRISSystem.ShowNotScanInAndOut p2 = new ShowNotScanInAndOut(_singleton.username, _singleton.deptCode);
                    p2.ShowDialog();
                    //p1.Show();
                }

                //--live
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string GetOU(string username,string findType)
        {

            string result = string.Empty;
            
            //Finding the user
            user = UserPrincipal.FindByIdentity(myDomain, username);
            if (user != null)
            {
                if (findType == "Email")
                {
                    result = user.EmailAddress;
                }
                else if(findType =="OU")
                {
                    //Getting the DirectoryEntry
                    DirectoryEntry directoryEntry = (user.GetUnderlyingObject() as DirectoryEntry);
                    //if the directoryEntry is not null
                    if (directoryEntry != null)
                    {
                        //Getting the directoryEntry's path and spliting with the "," character
                        string[] directoryEntryPath = directoryEntry.Path.Split(',');
                        //Getting the each items of the array and splitting again with the "=" character
                        foreach (var splitedPath in directoryEntryPath)
                        {
                            string[] elements = splitedPath.Split('=');
                            //If the 1st element of the array is "OU" string then get the 2nd element
                            if (elements[0].Trim() == "OU")
                            {
                                result = elements[1].Trim();
                                break;
                            }
                        }
                    }

                }               
            }
            return result;          
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtUsername.Text = "";
                txtPassword.Password = "";
                txtUsername.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Login();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Login();
        }
    }
}
