﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace DataAccessLayer_HRISSystem
{
    public class HRISSTECPayrollDataRepository
    {
        public interface IOT_DetailRepository : IGenericSTECPayrollDataRepository<OT_Detail>
        {

        }

        public class OT_DetailRepository : GenericSTECPayrollDataRepository<OT_Detail>, IOT_DetailRepository
        {

        }

        public interface ILabour_workRepository : IGenericSTECPayrollDataRepository<Labour_work>
        {

        }

        public class Labour_workRepository : GenericSTECPayrollDataRepository<Labour_work>, ILabour_workRepository
        {

        }

        public interface IEmployeeSTECPayrollRepository : IGenericSTECPayrollDataRepository<EmployeeSTECPayroll>
        {

        }

        public class EmployeeSTECPayrollRepository : GenericSTECPayrollDataRepository<EmployeeSTECPayroll>, IEmployeeSTECPayrollRepository
        {

        }

        public interface ILot_NumberRepository : IGenericSTECPayrollDataRepository<Lot_Number>
        {

        }
        public class Lot_NumberRepostory : GenericSTECPayrollDataRepository<Lot_Number>, ILot_NumberRepository
        {

        }
    }
}
