﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem
{
   public interface IHRISTimeSTECBusinessLayer
    {
        //UserInfo
        IList<userinfo> GetAllUserInfos();
        userinfo GetUserInfoByFingerID(int fingerScan);
        //IList< userinfo> GetMaxUserInfo();

        userinfo GetUserInfoByBadGenNumber(int BadGenNumber);
        void AddUserInfo(params userinfo[] userInfo);
        void UpdateUserInfo(userinfo userInfo);

        department GetDefaultDeptID(string dept_code);
        void AddUserInfo_att(params userinfo_attarea[] userInfo_attarea);
        IList< userinfo_attarea> GetAllUserInfo_attarea();


        IList<checkinout> getCheckInOut(int _fingerscan,DateTime datetime);


        IList<department> GetAllDepartments();
        department GetDepartmentByCode(string _code);
    }
}
