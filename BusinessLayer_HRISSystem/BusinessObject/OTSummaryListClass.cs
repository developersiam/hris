﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem.BusinessObject
{
   public class OTSummaryListClass
    {      
       private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

       public string Employee_ID { get; set; }
       public string AccCode { get; set; }
       public string FingerScanID { get; set; }
       public string TitleNameTH { get; set; }
       public string FirstNameTH { get; set; }
       public string LastNameTH { get; set; }
       public Nullable<decimal> OTnormal { get; set; }
       public Nullable<decimal> Holiday { get; set; }
        public Nullable<decimal> OTHoliday { get; set; }
        public Nullable<decimal> OTSpecial { get; set; }

        public List<OTSummaryListClass> getTestOTByPayrollLot(string PayrollLot)
       {
           List<Employee> oE = new List<Employee>();
           oE = _hrisBusinessLayer.GetAllEmployees().ToList();

           List<OT> OjAll = new List<OT>();
           OjAll = _hrisBusinessLayer.GetAllOTs().Where(p => p.PayrollLot == PayrollLot && 
                                                       (p.Remark == "Create by HRM" ||
                                                       (p.Remark == "Create from OT Online" &&
                                                        p.AdminApproveStatus == "A" &&
                                                        p.ManagerApproveStatus == "A" &&
                                                        p.FinalApproverStatus == "A" &&
                                                        p.HRApproveStatus == "A"))).ToList();
            // where Admin, Approver, Final, HR status if Create from OT Online
            var query = (
                           from a in oE
                           join b in OjAll on a.Employee_ID equals b.Employee_ID
                           group new { a, b } by new
                           {
                               a.Employee_ID,
                               a.Noted
                               ,
                               a.FingerScanID,
                               a.Person.TitleName.TitleNameInitialTH
                               ,
                               a.Person.FirstNameTH,
                               a.Person.LastNameTH
                           }
                               into g
                               select new OTSummaryListClass
                               {
                                   Employee_ID = g.Key.Employee_ID,
                                   AccCode = g.Key.Noted,
                                   FingerScanID = g.Key.FingerScanID,
                                   TitleNameTH = g.Key.TitleNameInitialTH,
                                   FirstNameTH = g.Key.FirstNameTH,
                                   LastNameTH = g.Key.LastNameTH,
                                   OTnormal = g.Where(e => e.b.OTType == "N").Sum(e => e.b.NcountOT),
                                   Holiday = g.Where(e => e.b.OTType == "H").Sum(e => e.b.NcountOT),
                                   OTHoliday = g.Where(e => e.b.OTType == "S").Sum(e => e.b.NcountOT),
                                   OTSpecial = g.Where(e => e.b.OTType == "SP").Sum(e => e.b.NcountOT)
                               });
           return query.ToList();
       }  
    

       public List<OTSummaryListClass> getTestOT(DateTime _xstartdate, DateTime _xendDate)
       {
           List<Employee> oE = new List<Employee>();
           oE = _hrisBusinessLayer.GetAllEmployees().ToList();

           List<OT> OjAll = new List<OT>();
           OjAll = _hrisBusinessLayer.GetAllOTs().Where(p => p.OTDate >= _xstartdate && p.OTDate <= _xendDate).ToList();

           var query = (
                           from a in oE
                           join b in OjAll on a.Employee_ID equals b.Employee_ID
                           group new { a, b } by new
                           {
                               a.Employee_ID,
                               a.Noted
                               ,
                               a.FingerScanID,
                               a.Person.TitleName.TitleNameInitialTH
                               ,
                               a.Person.FirstNameTH,
                               a.Person.LastNameTH
                           }
                               into g
                               select new OTSummaryListClass
                               {
                                   Employee_ID = g.Key.Employee_ID,
                                   AccCode = g.Key.Noted,
                                   FingerScanID = g.Key.FingerScanID,
                                   TitleNameTH = g.Key.TitleNameInitialTH,
                                   FirstNameTH = g.Key.FirstNameTH,
                                   LastNameTH = g.Key.LastNameTH,
                                   OTnormal = g.Where(e => e.b.OTType == "N").Sum(e => e.b.NcountOT),
                                   Holiday = g.Where(e => e.b.OTType == "H").Sum(e => e.b.NcountOT),
                                   OTHoliday = g.Where(e => e.b.OTType == "S").Sum(e => e.b.NcountOT)
                               });
           return query.ToList();
       }  
    }
}
