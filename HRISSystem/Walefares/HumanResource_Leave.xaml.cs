﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for HumanResource_Leave.xaml
    /// </summary>
    public partial class HumanResource_Leave : Page

    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        public List<Leave> ObjList;
        private Boolean XSelectedRow = false;
        Holiday holiday = new Holiday();
        Boolean XChkWeekend = false;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private double XNcountLeave;
        public List<TransactionLog> transactionLog;
        public List<TransactionLog> transactionLogByTable_ID;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        private class StatusFlags
        {
            public bool StatusFlag { get; set; }
            public string StatusFlagName { get; set; }
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetNcountLeave()
        {
            double i;
            TimeSpan ts = (Convert.ToDateTime(LeaveDateTo.Text) - Convert.ToDateTime(LeaveDateFrom.Text));
            i = ts.TotalDays + 1;

            DateTime xLeaveDate = Convert.ToDateTime(LeaveDateFrom.Text);
            for (int Xi = 1; Xi <= i; Xi++)
            {
                //ไม่นับวันอาทิตย์และไม่นับวันหยุด
                XChkWeekend = IsWeekend(Convert.ToDateTime(xLeaveDate.AddDays(Xi - 1)));
                holiday = _hrisBusinessLayer.GetHoliday(xLeaveDate.AddDays(Xi - 1)); //get holiday from holiday table       
                if (XChkWeekend == true || holiday != null)
                {
                    i--;
                }
            }
            txtNcountLeave.Text = Convert.ToString(i);
        }

        public HumanResource_Leave()
        {
            InitializeComponent();

            IList<LeaveType> leaveType = _hrisBusinessLayer.GetAllLeaveTypes();
            cmbLeaveType.ItemsSource = leaveType;

            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbStatus.ItemsSource = statusFlagList;
            cmbStatus.SelectedIndex = 0;

            ObjList = new List<Leave>();

          
        }

        public HumanResource_Leave(EmployeeCurrentInformation x3)
        {
            InitializeComponent();

            IList<LeaveType> leaveType = _hrisBusinessLayer.GetAllLeaveTypes();
            cmbLeaveType.ItemsSource = leaveType;

            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();

            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbStatus.ItemsSource = statusFlagList;
            cmbStatus.SelectedIndex = 0;

            ObjList = new List<Leave>();

            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                GetEmployeeInformation(x3);
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = x3.xFirstNameTH;
                txtSearch.IsEnabled = true;
            }

        }
        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            //EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
        }
        public void ClearData()
        {
            try
            {
                XNcountLeave = 0;
                AddButton.IsEnabled = true;
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                LeaveDateFrom.IsEnabled = true;
                LeaveDateTo.IsEnabled = true;
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";
                txtPositionNameTH.Text = "";
                txtPositionTypeNameTH.Text = "";
                txtNoted.Text = "";
                XSelectedRow = false;
                cmbLeaveType.IsEnabled = true;

                //ObjList = _hrisBusinessLayer.GetAllLeaves().ToList();
                //GetDataFromSearch();
                //DataGrid.ItemsSource = null; //clear 
                //DataGrid.ItemsSource = _hrisBusinessLayer.GetAllLeaves();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void GetDataFromSearch()
        {
            try
            {
                ObjList = _hrisBusinessLayer.GetAllLeaves().ToList();
                DataGrid.ItemsSource = null;
                if ((LeaveDateFromSearch.Text != "") && (LeaveDateToSearch.Text != ""))
                {
                    if(ChkAllSearch.IsChecked == true)
                    {                      
                        DataGrid.ItemsSource = ObjList.Where(p => p.LeaveDate >= Convert.ToDateTime(LeaveDateFromSearch.Text) && p.LeaveDate <= Convert.ToDateTime(LeaveDateToSearch.Text));

                    }
                    else if(ChkFilterSearch.IsChecked == true)
                    {
                       
                       DataGrid.ItemsSource = ObjList.Where(p => p.Employee.Person.FirstNameTH.Contains(txtSearch.Text) && p.LeaveDate >= Convert.ToDateTime(LeaveDateFromSearch.Text) && p.LeaveDate <= Convert.ToDateTime(LeaveDateToSearch.Text));
                        
                    } 
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();

                LeaveDateFromSearch.Text = FirstDayOfMonth(DateTime.Now).ToString();
                LeaveDateToSearch.Text = DateTime.Now.ToString();

                //ObjList = _hrisBusinessLayer.GetAllLeaves().ToList();
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = ObjList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Leave query1 = new Leave();  // created 1 instance or object
                query1 = (Leave)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    XNcountLeave = 0;
                    AddButton.IsEnabled = true;
                    LeaveDateFrom.IsEnabled = true;
                    LeaveDateTo.IsEnabled = true;
                    XSelectedRow = false;
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                    cmbLeaveType.IsEnabled = true;
                }
                else
                {
                    XNcountLeave = Convert.ToDouble( query1.NcountLeave);
                    AddButton.IsEnabled = false;
                    LeaveDateFrom.IsEnabled = false;
                    LeaveDateTo.IsEnabled = false;
                    XSelectedRow = true;
                    txtEmployee_ID.IsReadOnly = true;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.LightGray);
                    cmbLeaveType.IsEnabled = false;

                    GetDataFromEmployee_ID(query1.Employee_ID);
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private Boolean ChkDateRange(DateTime _date)//สามารถลงวันที่ลาย้อนหลังได้ไม่เกินวันที่ 26 ของเดือนที่แล้ว
        {
            Boolean xresult = false;
            DateTime date = DateTime.Now;

            var endPreviouseMonth = date;
            //var lastCurrentMonth = date;
            //var lastCurrentMonth2 = date;
            //ถ้าเป็นเดือนมกราคม ปีจะต้องลดไป1
            if (date.Month == 1)
            {
                endPreviouseMonth = new DateTime(date.Year - 1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                //lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                //lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            else
            {
                endPreviouseMonth = new DateTime(date.Year, date.Month - 1, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                //lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                //lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }


            //var endPreviouseMonth = new DateTime(date.Year, date.Month - 1, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
            //var lastCurrentMonth = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            if (_date < Convert.ToDateTime(endPreviouseMonth))
            {
                xresult = false;
            }            
            else
            {
                xresult = true;
            }
            return xresult;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (LeaveDateFrom.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ขอลา ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //else if (ChkDateRange(Convert.ToDateTime(LeaveDateFrom.Text)) == false)
                //{
                //    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วจนถึงวันที่ 25 ของเดือนนี้เท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                
               
                if (LeaveDateTo.Text == "")
                {
                    LeaveDateTo.Text = LeaveDateFrom.Text;
                }
                else if (Convert.ToDateTime(LeaveDateTo.Text).Year > DateTime.Now.Year+1 || Convert.ToDateTime(LeaveDateTo.Text).Year < DateTime.Now.Year - 1)
                {
                    MessageBox.Show("กรุณาคีย์วันที่ขอลา End date ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if(cmbStatus.Text =="")
                {
                    MessageBox.Show("กรุณาคีย์สถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                DateTime xLeaveDate = Convert.ToDateTime(LeaveDateFrom.Text);
                holiday = _hrisBusinessLayer.GetHoliday(xLeaveDate); //get holiday from holiday table                
                Decimal nCountLeave = Convert.ToDecimal(txtNcountLeave.Text);

                //เช็คจำนวนวันที่ต้องจอง Array
                Decimal j;
                TimeSpan ts = (Convert.ToDateTime(LeaveDateTo.Text) - Convert.ToDateTime(LeaveDateFrom.Text));
                j = Convert.ToDecimal( ts.TotalDays + 1);

                //ถ้าเป็นวันเดียวกัน 
                if (j ==1)
                {
                    //จะคีย์เกินเลข 1 ไม่ได้เนื่องจากไม่มีทางที่จะลาเกิน 1
                    if (Convert.ToDecimal(txtNcountLeave.Text) > 1)
                    {
                        MessageBox.Show("คีย์จำนวนวัน/ชั่วโมงไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    //เลือกวันหยุดบริษัทไม่ได้                    
                    if(holiday!= null && holiday.Date == Convert.ToDateTime(LeaveDateFrom.Text))
                    {
                        MessageBox.Show("วันที่ระบุตรงกับวันหยุดบริษัท กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                }else
                {

                    nCountLeave = 1;
                }


                //1.ให้เช็คใน Lotnumber  form STEC_payroll ว่าวันที่ที่คีย์อยู่ใน lot ใด
                //2.ถ้า acc ยังไม่ได้ lock หรือไม่มีข้อมูลใน lot_number อนุญาตให้บันทึกได้
                //3.ถ้า acc lock ระบบอนุญาตให้บันทึก แล้วให้โชว์ message ว่าการลงวันลาจะไม่มีผลย้อนหลังกับการจ่ายเงินเดือน ระบบจะยึดตามการจ่ายเงินเดือน 
                Lot_Number lot_number = new Lot_Number();                
                for(int Xnum = 0; Xnum < j; Xnum++)
                { //วนวันแรกจนถึงวันสุดท้ายที่ระบุ
                    lot_number = _hrisSTECPayrollBusinessLayer.GetLotNumberByDate(xLeaveDate.AddDays(Xnum));
                    if (lot_number != null && lot_number.Lock_Acc == "2")
                    {
                        //ถ้าคีย์ย้อนหลัง หลังจากที่บัญชีปิดงวดไปแล้วจะต้องคีย์ทีละ1 เท่านั้น
                        if (j > 1)
                        {
                            MessageBox.Show("การคีย์ข้อมูลย้อนหลังหลังจากฝ่ายบัญชีปิดยอดไปแล้ว คุณจะต้องคีย์1วันต่อ1รายการเท่านั้น ไม่สามารถเลือกเป็นช่วงวันที่ได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }
                        
                        if (MessageBox.Show("เนื่องจากฝ่ายบัญชีได้ปิดยอดและคำนวนเงินเดือนของเดือนนี้แล้ว ดังนั้นการบันทึกข้อมูลการลารายการนี้จะไม่มีผลย้อนหลังกับเรื่องเงินเดือน คุณต้องการยืนยันการบันทึกข้อมูลใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        {                            
                            return;
                        }
                        txtNoted.Text = "บันทึกข้อมูลหลังจากบัญชีปิดงวดแล้ว";
                    }
                }
                                                                         
                Leave[] leaveObj = new Leave[Convert.ToInt16(j)];
                Leave query1 = new Leave();  // created 1 instance or object  
                for(int Xnum = 0; Xnum < j; Xnum++)
                {
                    XChkWeekend = IsWeekend(Convert.ToDateTime(xLeaveDate.AddDays(Xnum)));
                    holiday = _hrisBusinessLayer.GetHoliday(xLeaveDate.AddDays(Xnum)); //get holiday from holiday table       
                    //ถ้าไม่ใช่วันอาทิตย์และไม่ใช่วันหยุดบริษัท  ให้บันทึกข้อมูล
                    if (XChkWeekend == false && holiday == null )
                    {                      
                        query1.LeaveDate = xLeaveDate.AddDays(Xnum);
                        query1.Employee_ID = txtEmployee_ID.Text;
                        query1.LeaveType_ID = (string)cmbLeaveType.SelectedValue;
                        query1.NcountLeave = nCountLeave;
                        query1.Status = (Boolean)cmbStatus.SelectedValue;
                        query1.Noted = txtNoted.Text;
                        query1.ModifiedDate = DateTime.Now;
                        leaveObj[Xnum] = query1;
                        _hrisBusinessLayer.AddLeave(leaveObj[Xnum]);

                        transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                        if (transactionLog.Count > 0)
                        {
                            XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                        }
                        else
                        {
                            XMaxTransactionLog_ID = 1;
                        }
                        TransactionLog[] transactionLogObj = new TransactionLog[1];
                        TransactionLog transactionLog1 = new TransactionLog();
                        transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                        transactionLog1.TransactionType_ID = "A";
                        transactionLog1.Table_ID = "T008";
                        transactionLog1.TransactionDate = DateTime.Now;
                        transactionLog1.FieldName = "Employee_ID";
                        transactionLog1.PKFields = txtEmployee_ID.Text;
                        transactionLog1.FKFields = Convert.ToString( xLeaveDate.AddDays(Xnum));
                        transactionLog1.OldData = "";
                        transactionLog1.NewData = (string)cmbLeaveType.SelectedValue;
                        transactionLog1.Noted = txtNoted.Text;
                        transactionLog1.ModifiedDate = DateTime.Now;
                        transactionLog1.ModifiedUser = _singleton.username;
                        transactionLogObj[0] = transactionLog1;
                        _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                    }
                }
               
                LeaveDateFromSearch.Text = LeaveDateFrom.Text;
                LeaveDateToSearch.Text = LeaveDateTo.Text;
                GetDataFromSearch();
                txtNoted.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (LeaveDateFrom.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ขอลา ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (LeaveDateTo.Text == "")
                {
                    LeaveDateTo.Text = LeaveDateFrom.Text;
                }
                if (cmbStatus.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์สถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                DateTime xLeaveDate = Convert.ToDateTime(LeaveDateFrom.Text);
                holiday = _hrisBusinessLayer.GetHoliday(xLeaveDate); //get holiday from holiday table                
                Decimal nCountLeave = Convert.ToDecimal(txtNcountLeave.Text);

                //เช็คจำนวนวันที่ต้องจอง Array
                Decimal j;
                TimeSpan ts = (Convert.ToDateTime(LeaveDateTo.Text) - Convert.ToDateTime(LeaveDateFrom.Text));
                j = Convert.ToDecimal(ts.TotalDays + 1);

                //ถ้าเป็นวันเดียวกัน 
                if (j == 1)
                {
                    //จะคีย์เกินเลข 1 ไม่ได้เนื่องจากไม่มีทางที่จะลาเกิน 1
                    if (Convert.ToDecimal(txtNcountLeave.Text) > 1)
                    {
                        MessageBox.Show("คีย์จำนวนวัน/ชั่วโมงไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    //เลือกวันหยุดบริษัทไม่ได้                    
                    if (holiday != null && holiday.Date == Convert.ToDateTime(LeaveDateFrom.Text))
                    {
                        MessageBox.Show("วันที่ระบุตรงกับวันหยุดบริษัท กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                }
                else
                {

                    nCountLeave = 1;
                }

                //1.ให้เช็คใน Lotnumber  form STEC_payroll ว่าวันที่ที่คีย์อยู่ใน lot ใด
                //2.ถ้า acc ยังไม่ได้ lock หรือไม่มีข้อมูลใน lot_number อนุญาตให้บันทึกได้
                //3.ถ้า acc lock ระบบอนุญาตให้บันทึก แล้วให้โชว์ message ว่าการลงวันลาจะไม่มีผลย้อนหลังกับการจ่ายเงินเดือน ระบบจะยึดตามการจ่ายเงินเดือน 
                Lot_Number lot_number = new Lot_Number();
                for (int Xnum = 0; Xnum < j; Xnum++)
                { //วนวันแรกจนถึงวันสุดท้ายที่ระบุ
                    lot_number = _hrisSTECPayrollBusinessLayer.GetLotNumberByDate(xLeaveDate.AddDays(Xnum));
                    if (lot_number != null && lot_number.Lock_Acc == "2")
                    {
                        //ถ้าคีย์ย้อนหลัง หลังจากที่บัญชีปิดงวดไปแล้วจะต้องคีย์ทีละ1 เท่านั้น
                        if (j > 1)
                        {
                            MessageBox.Show("การแก้ไขข้อมูลย้อนหลังหลังจากฝ่ายบัญชีปิดยอดไปแล้ว คุณจะต้องคีย์1วันต่อ1รายการเท่านั้น ไม่สามารถเลือกเป็นช่วงวันที่ได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }

                        if (MessageBox.Show("เนื่องจากฝ่ายบัญชีได้ปิดยอดและคำนวนเงินเดือนของเดือนนี้แล้ว ดังนั้นการบันทึกข้อมูลการลารายการนี้จะไม่มีผลย้อนหลังกับเรื่องเงินเดือน คุณต้องการยืนยันการบันทึกข้อมูลใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        {
                            return;
                        }
                        txtNoted.Text = "บันทึกข้อมูลหลังจากบัญชีปิดงวดแล้ว";
                    }
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                Leave query1 = new Leave();  // created 1 instance or object

                query1.Employee_ID = txtEmployee_ID.Text;
                query1.LeaveDate = Convert.ToDateTime(LeaveDateFrom.Text);
                query1.LeaveType_ID = (string)cmbLeaveType.SelectedValue;
                query1.NcountLeave = nCountLeave;
                query1.Status = (Boolean)cmbStatus.SelectedValue;
                query1.Noted = txtNoted.Text;
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateLeave(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T008";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(LeaveDateFrom.Text);
                transactionLog1.OldData = (string) cmbLeaveType.SelectedValue + " " +  XNcountLeave ;
                transactionLog1.NewData = (string)cmbLeaveType.SelectedValue + " " + txtNcountLeave.Text;
                transactionLog1.Noted = txtNoted.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //1.ให้เช็คใน Lotnumber  form STEC_payroll ว่าวันที่ที่คีย์อยู่ใน lot ใด
                //2.ถ้า acc ยังไม่ได้ lock หรือไม่มีข้อมูลใน lot_number อนุญาตให้บันทึกได้
                //3.ถ้า acc lock ระบบอนุญาตให้บันทึก แล้วให้โชว์ message ว่าการลงวันลาจะไม่มีผลย้อนหลังกับการจ่ายเงินเดือน ระบบจะยึดตามการจ่ายเงินเดือน 

                Lot_Number lot_number = new Lot_Number();               
                lot_number = _hrisSTECPayrollBusinessLayer.GetLotNumberByDate(Convert.ToDateTime(LeaveDateFrom.Text));
                if (lot_number != null && lot_number.Lock_Acc == "2")
                {
                    if(MessageBox.Show("เนื่องจากฝ่ายบัญชีได้ปิดยอดและคำนวนเงินเดือนของเดือนนี้แล้ว การลบข้อมูลอาจมีผลกระทบต่อข้อมูลการจ่ายเงิน คุณต้องการยืนยันการบันทึกข้อมูลใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }
                }

                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
               
                Leave query1 = new Leave();  // created 1 instance or object
                query1 = (Leave)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                _hrisBusinessLayer.RemoveLeave(query1);


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T008";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(LeaveDateFrom.Text);
                transactionLog1.OldData = "";
                transactionLog1.NewData = (string)cmbLeaveType.SelectedValue;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        
        private void txtNcountLeave_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {      
                //ถ้า start date  == to date จะอนุญาตให้คีย์ข้อมูล  แต่ถ้าไม่เท่ากันจะไม่ให้คีย์โดยระบบจะทำการคำนวนจำนวนให้
                if(Convert.ToDateTime(LeaveDateFrom.Text) == Convert.ToDateTime(LeaveDateTo.Text))
                {
                    bool approveDecimalPoint = false;
                    if (e.Text == ".")
                        approveDecimalPoint = true;


                    if (!char.IsDigit(e.Text, e.Text.Length - 1) && !(approveDecimalPoint))
                    {
                        e.Handled = true;
                    }

                }else if(Convert.ToDateTime(LeaveDateFrom.Text) != Convert.ToDateTime(LeaveDateTo.Text))
                {
                    e.Handled = true;
                    GetNcountLeave();
                }
                          
                         
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }           
        }
      

        private void LeaveDateTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                
                if (LeaveDateFrom.Text != "" && LeaveDateTo.Text != "" && XSelectedRow == false)
                {
                    Boolean XChkDayOfWeek2 = IsWeekend(Convert.ToDateTime(LeaveDateFrom.Text));
                    if (XChkDayOfWeek2)
                    {
                        MessageBox.Show("วันที่นี้เป็นวันอาทิตย์ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (Convert.ToDateTime(LeaveDateFrom.Text) > Convert.ToDateTime(LeaveDateTo.Text))
                    {
                        MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    GetNcountLeave();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //try
            //{
            //    Leave query1 = new Leave();  // created 1 instance or object
            //    query1 = (Leave)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

            //    if (query1 == null)
            //    {
            //        AddButton.IsEnabled = true;
            //        txtEmployee_ID.IsReadOnly = false;
            //        txtEmployee_ID.Background = new SolidColorBrush(Colors.White);

            //    }
            //    else
            //    {
            //        AddButton.IsEnabled = false;
            //        txtEmployee_ID.IsReadOnly = true;
            //        txtEmployee_ID.Background = new SolidColorBrush(Colors.LightGray);
            //    }
            //    //GetEmployeeInformation();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (LeaveDateFromSearch.Text == "" || LeaveDateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("Leave"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SearchNameButton.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
