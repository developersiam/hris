﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR015.xaml
    /// </summary>
    public partial class RepRPTHR015 : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private int XFingerScanCode;
        private string XEmployee_Id;
        private DateTime XDateFrom;
        private DateTime XDateTo;
        public RepRPTHR015()
        {
            InitializeComponent();
        }

        public RepRPTHR015(string _employee_id, DateTime _dateFrom, DateTime _dateTo)
        {
            InitializeComponent();
            XEmployee_Id = _employee_id;
            XDateFrom = _dateFrom;
            XDateTo = _dateTo;

            GetDataFromEmployee_ID(XEmployee_Id);
            DateFrom.Text = XDateFrom.ToString();
            DateTo.Text = XDateTo.ToString();

            GetData();
        }
        public RepRPTHR015(EmployeeCurrentInformation x3)
        {
            InitializeComponent();
            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                GetEmployeeInformation(x3);               
            }
        }

        public RepRPTHR015(string employee_ID)
        {
            InitializeComponent();
            GetDataFromEmployee_ID(employee_ID);
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtFirstNameTH.Text = x.xFirstNameTH + " " + x.xLastNameTH;
            XFingerScanCode = Convert.ToInt16( x.xFingerScan_ID);

        }   

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null)
                {
                    NavigationService.RemoveBackEntry();
                }                                 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetData()
        {

            try
            {
                if (DateFrom.Text == "" || DateTo.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (Convert.ToDateTime(DateFrom.Text) > Convert.ToDateTime(DateTo.Text))
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                _ReportViewer1.Reset();
                HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable sp_GetEmployeeTimeAttendanceByDateAndEmployeeDataTable = new HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable();
                HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter sp_GetEmployeeTimeAttendanceByDateAndEmployeeTableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter();
                sp_GetEmployeeTimeAttendanceByDateAndEmployeeTableAdapter.Fill(sp_GetEmployeeTimeAttendanceByDateAndEmployeeDataTable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateTo.Text), XFingerScanCode);
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource1.Name = "GetEmployeeTimeAttendanceByDateAndEmployee1DataSet";
                reportDataSource1.Value = sp_GetEmployeeTimeAttendanceByDateAndEmployeeDataTable;

                HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable = new HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable();
                HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee2TableAdapter sp_GetEmployeeTimeAttendanceByDateAndEmployee2TableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee2TableAdapter();
                sp_GetEmployeeTimeAttendanceByDateAndEmployee2TableAdapter.Fill(sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateTo.Text), Convert.ToString(txtEmployee_ID.Text));
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource2.Name = "GetEmployeeTimeAttendanceByDateAndEmployee2DataSet";
                reportDataSource2.Value = sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable;

                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
                this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR015.rdlc";



                _ReportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtFirstNameTH.Text = ex.xFirstNameTH + " " + ex.xLastNameTH;
                XFingerScanCode = Convert.ToInt16( ex.xFingerScan_ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("Report1"));            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
