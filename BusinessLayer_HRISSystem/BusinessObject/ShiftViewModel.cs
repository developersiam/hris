﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class ShiftViewModel
    {
        public string Shift_ID { get; set; }
        public string Shift_Time { get; set; }
    }
}
