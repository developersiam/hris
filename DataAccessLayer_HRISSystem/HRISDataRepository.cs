﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace DataAccessLayer_HRISSystem
{
    public class HRISDataRepository
    {
        public interface ISSOHospitalListRepository : IGenericDataRepository<SSOHospitalList>
        {

        }
        public class SSOHospitalListRepository : GenericDataRepository<SSOHospitalList>, ISSOHospitalListRepository
        {

        }

        public interface ISSOHospitalRepository : IGenericDataRepository<SSOHospital>
        {

        }
        public class SSOHospitalRepository : GenericDataRepository<SSOHospital>, ISSOHospitalRepository
        {

        }
        public interface ISSOTypeRepository : IGenericDataRepository<SSOType>
        {

        }
        public class SSOTypeRepository : GenericDataRepository<SSOType>, ISSOTypeRepository
        {

        }
        public interface ISSORepository : IGenericDataRepository<SSO>
        {

        }
        public class SSORepository : GenericDataRepository<SSO>, ISSORepository
        {

        }

        public interface ITrainingCourseRepository : IGenericDataRepository<TrainingCourse>
        {

        }

        public class TrainingCourseRepository : GenericDataRepository<TrainingCourse>, ITrainingCourseRepository
        {

        }
        public interface ITrainingRepository : IGenericDataRepository<Training>
        {

        }

        public class TrainingRepository : GenericDataRepository<Training>, ITrainingRepository
        {

        }
        
        public interface IContactPersonInfoRepository : IGenericDataRepository<ContactPersonInfo>
        {

        }

        public class ContactPersonInfoRepository : GenericDataRepository<ContactPersonInfo>,IContactPersonInfoRepository
        {

        }
        public interface IOriginalWorkHistoryRepository : IGenericDataRepository<OriginalWorkHistory>
        {

        }
        public class OriginalWorkHistoryRepository : GenericDataRepository<OriginalWorkHistory>, IOriginalWorkHistoryRepository
        {

        }

        public interface IPayrollFlagRepository : IGenericDataRepository<PayrollFlag>
        {

        }
        public class PayrollFlagRepository : GenericDataRepository<PayrollFlag>,IPayrollFlagRepository
        {

        }
        public interface IAddressRepository : IGenericDataRepository<Address>
        {

        }

        public class AddressRepository : GenericDataRepository<Address>,IAddressRepository
        {

        }

        public interface IAddressTypeRepository :IGenericDataRepository<AddressType>
        {

        }

        public class AddressTypeRepository : GenericDataRepository<AddressType>,IAddressTypeRepository
        {

        }

        public interface IBloodRepository : IGenericDataRepository<Blood>
        {

        }

        public class BloodRepository : GenericDataRepository<Blood>, IBloodRepository
        {

        }

        public interface ICropSetupRepository : IGenericDataRepository<CropSetup>
        {

        }

        public class CropSetupRepository : GenericDataRepository<CropSetup>,ICropSetupRepository
        {

        }

        public interface IHolidayRepository : IGenericDataRepository<Holiday>
        {

        }

        public class HolidayRepository : GenericDataRepository<Holiday>,IHolidayRepository
        {

        }

        public interface ILeaveTypeRepository : IGenericDataRepository<LeaveType>
        {

        }

        public class LeaveTypeRepository : GenericDataRepository<LeaveType>, ILeaveTypeRepository
        {

        }

        //public interface ILeaveTypeBenefitsRepository : IGenericDataRepository<LeaveTypeBenefit>
        //{

        //}
        //public class LeaveTypeBenefitsRepository : GenericDataRepository<LeaveTypeBenefit>, ILeaveTypeBenefitsRepository
        //{

        //}

        public interface IShiftRepository : IGenericDataRepository<Shift>
        {

        }

        public class ShiftRepository : GenericDataRepository<Shift>, IShiftRepository
        {

        }
        
        public interface IShiftControlRepository : IGenericDataRepository<ShiftControl>
        {

        }

        public class ShiftControlRepository : GenericDataRepository<ShiftControl>, IShiftControlRepository
        {

        }

        public interface IBranchRepository : IGenericDataRepository<Branch>
        {

        }
        public class BranchRepository : GenericDataRepository<Branch>,IBranchRepository
        {

        }

        public interface IJobTitleRepository : IGenericDataRepository<JobTitle>
        {

        }
        public class JobTitleRepository : GenericDataRepository<JobTitle>, IJobTitleRepository
        {

        }

        public interface IOrganizationRepository : IGenericDataRepository<Organization>
        {

        }
        public class OrganizationRepository : GenericDataRepository<Organization>, IOrganizationRepository
        {

        }

        public interface IOrganizationTypeRepository : IGenericDataRepository<OrganizationType>
        {

        }
        public class OrganizationTypeRepository : GenericDataRepository<OrganizationType>,IOrganizationTypeRepository
        {

        }

        public interface IorganizationUnitRepository : IGenericDataRepository<OrganizationUnit>
        {

        }
        public class OrganizationUnitRepository : GenericDataRepository<OrganizationUnit>, IorganizationUnitRepository
        {

        }

        public interface IPositionRepository : IGenericDataRepository<Position>
        {

        }
        public class PositionRepository : GenericDataRepository<Position>, IPositionRepository
        {

        }

        public interface IPositionOrgnizationRepository : IGenericDataRepository<PositionOrganization>
        {

        }
        public class PositionOrganizationRepository : GenericDataRepository<PositionOrganization>, IPositionOrgnizationRepository
        {

        }

        public interface IPositionTypeRepository : IGenericDataRepository<PositionType>
        {

        }
        public class PositionTypeRepository : GenericDataRepository<PositionType>, IPositionTypeRepository
        {

        }

        public interface IPersonRepository : IGenericDataRepository<Person>
        {

        }

        public class PersonRepository : GenericDataRepository<Person>, IPersonRepository
        {

        }

        public interface IEmployeeRepository : IGenericDataRepository<Employee>
        {

        }
        public class EmployeeRepository : GenericDataRepository<Employee>, IEmployeeRepository
        {

        }

        public interface IEmployeePositionRepository : IGenericDataRepository<EmployeePosition>
        {

        }
        public class EmployeePositionRepository : GenericDataRepository<EmployeePosition>, IEmployeePositionRepository
        {

        }

        public interface ITitleNameRepository : IGenericDataRepository<TitleName>
        {

        }
        public class TitleNameRepository : GenericDataRepository<TitleName>, ITitleNameRepository
        {

        }

        public interface IGenderRepository : IGenericDataRepository<Gender>
        {

        }
        public class GenderRepository : GenericDataRepository<Gender>, IGenderRepository
        {

        }

        //public interface IMaritalStatusRepository : IGenericDataRepository<MaritalStatu>
        //{

        //}
        //public class MaritalStatusRepository : GenericDataRepository<MaritalStatu>, IMaritalStatusRepository
        //{

        //}

        public interface INationalityRepository : IGenericDataRepository<Nationality>
        {

        }
        public class NationalityRepository : GenericDataRepository<Nationality>, INationalityRepository
        {

        }

        public interface IRaceRepository : IGenericDataRepository<Race>
        {

        }
        public class RaceRepository : GenericDataRepository<Race>, IRaceRepository
        {

        }

        public interface IReligionRepository : IGenericDataRepository<Religion>
        {

        }
        public class ReligionRepository : GenericDataRepository<Religion>, IReligionRepository
        {

        }
        public interface IWorkUpCountryRepository : IGenericDataRepository<WorkUpCountry>
        {

        }
        public class WorkUpCountryRepository : GenericDataRepository<WorkUpCountry>, IWorkUpCountryRepository
        {

        }

        public interface ILeaveRepository : IGenericDataRepository<Leave>
        {

        }
        public class LeaveRepository : GenericDataRepository<Leave>, ILeaveRepository
        {

        }

        public interface IOTRepository : IGenericDataRepository<OT>
        {

        }
        public class OTRepository : GenericDataRepository<OT>, IOTRepository
        {

        }
        public interface IOTOnlineRepository : IGenericDataRepository<OTOnlineSetupAdmin>
        {

        }
        public class OTOnlineRepository : GenericDataRepository<OTOnlineSetupAdmin>, IOTOnlineRepository
        {

        }
        public interface IOTOnlineDetailRepository : IGenericDataRepository<OTOnlineSetupAdminDetail>
        {

        }
        public class OTOnlineDetailRepository : GenericDataRepository<OTOnlineSetupAdminDetail>, IOTOnlineDetailRepository
        {

        }


        public interface IIDCardInfoRepository : IGenericDataRepository<IDCardInfo>
        {

        }
        public class IDCardRepository : GenericDataRepository<IDCardInfo>, IIDCardInfoRepository
        {

        }

        public interface IProvinceRepository : IGenericDataRepository<Province>
        {

        }
        public class ProvinceRepository : GenericDataRepository<Province>, IProvinceRepository
        {

        }

        public interface IDistrictRepository : IGenericDataRepository<District>
        {

        }
        public class DistrictRepository : GenericDataRepository<District>, IDistrictRepository
        {

        }

        public interface ISubDistrictRepository : IGenericDataRepository<SubDistrict>
        {

        }

        public class SubDistrictRepository : GenericDataRepository<SubDistrict>, ISubDistrictRepository
        {

        }

       public interface ICountryRepository : IGenericDataRepository<Country>
        {

        }
        public class CountryRepository : GenericDataRepository<Country>, ICountryRepository
        {

        }

        public interface IManualWorkDateTypeRepository : IGenericDataRepository<ManualWorkDateType>
        {

        }
        public class ManualWorkDateTypeRepository : GenericDataRepository<ManualWorkDateType>, IManualWorkDateTypeRepository
        {

        }

        public interface IManualWorkDateRepository : IGenericDataRepository<ManualWorkDate>
        {

        }

        public class ManualWorkDateRepository : GenericDataRepository<ManualWorkDate>, IManualWorkDateRepository
        {

        }

        public interface ITransactionLogRepository : IGenericDataRepository<TransactionLog>
        {

        }

        public class TransactionLogRepository : GenericDataRepository<TransactionLog>, ITransactionLogRepository
        {

        }

        public interface ITablesRepository : IGenericDataRepository<Table>
        {

        }
        public class TableRepository : GenericDataRepository<Table>, ITablesRepository
        {

        }

        public interface IMaritalStatusRepository : IGenericDataRepository<MaritalStatu>
        {

        }
        public class MaritalStatusRepository : GenericDataRepository<MaritalStatu>, IMaritalStatusRepository
        {

        }

        public interface IEducationRepository : IGenericDataRepository<Education>
        {

        }
        public class EducationRepository : GenericDataRepository<Education>,IEducationRepository
        {

        }

        public interface IEducationQualificationRepository : IGenericDataRepository<EducationQualification>
        {

        }
        public class EducationQualificationRepository : GenericDataRepository<EducationQualification>, IEducationQualificationRepository
        {

        }


        public interface IEducationLevelRepository : IGenericDataRepository<EducationLevel>
        {

        }
        public class EducationLevelRepository : GenericDataRepository<EducationLevel>, IEducationLevelRepository
        {

        }
        public interface IInstituteRepository : IGenericDataRepository<Institute>
        {

        }
        public class InstituteRepository : GenericDataRepository<Institute>, IInstituteRepository
        {

        }

        public interface IMajorRepository : IGenericDataRepository<Major>{

        }
        public class MajorRepository : GenericDataRepository<Major>,IMajorRepository{

        }

        public interface IVitalStatusRepository : IGenericDataRepository<VitalStau>
        {

        }
        public class VitalStatusRepository : GenericDataRepository<VitalStau>, IVitalStatusRepository
        {

        }
        public interface IFamilyMemberTypeReposotory : IGenericDataRepository<FamilyMemberType>
        {

        }
        public class FamilyMemberTypeRepository : GenericDataRepository<FamilyMemberType>, IFamilyMemberTypeReposotory
        {

        }
        public interface IFamilyMemberRepository : IGenericDataRepository<FamilyMember>
        {

        }
        public class FamilyMemberRepository : GenericDataRepository<FamilyMember>, IFamilyMemberRepository
        {

        }
   

        
    }
}
