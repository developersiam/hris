﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.ComponentModel;


namespace HRISSystem.Employees
{
    /// <summary>
    /// Interaction logic for Employees.xaml
    /// </summary>

    public partial class Employees : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _hrisTimeSTECBusinessLayer = new BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();


        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        public List<Employee> employeeList;
        public string xPersonID;
        public DateTime xCreatedDate;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private EmployeeCurrentInformation employeeCurrentInformation;
        private EmployeeResignInformation xFromEmployeeResignInformation;
        private PersonInformation xPersonInformation;
        public List<TransactionLog> transactionLog;
        Decimal XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        string xEmployeeType;


        private class FingerScanFlage
        {
            public bool FingerScanFlag { get; set; }
            public string FingerScanFlagName { get; set; }
        }

        private class ManualWorkDateStatus
        {
            public bool ManualWorkDateStatusID { get; set; }
            public string ManualWorkDateStatusName { get; set; }
        }
       

        public Employees()
        {
            InitializeComponent();
            employeeList = new List<Employee>();

            List<FingerScanFlage> fingerScanFlagList = new List<FingerScanFlage>();
            FingerScanFlage h1 = new FingerScanFlage { FingerScanFlag = true, FingerScanFlagName = "สแกน" };
            FingerScanFlage h2 = new FingerScanFlage { FingerScanFlag = false, FingerScanFlagName = "ไม่สแกน" };
            fingerScanFlagList.Add(h1);
            fingerScanFlagList.Add(h2);
            cmbFingerScanFlag.ItemsSource = fingerScanFlagList;
           
            IList<Shift> shift = _hrisBusinessLayer.GetAllShifts();
            cmbShiftControlShift.ItemsSource = shift.OrderBy(b => b.StartTime);

            IList<Organization> organizationUnit = _hrisBusinessLayer.GetAllOrganizations();
            cmbOrganizationUnit_ID.ItemsSource = organizationUnit.OrderBy(b => b.OrganizationUnit.OrganizationUnitName);

            IList<ManualWorkDateType> manualWorkDateType = _hrisBusinessLayer.GetAllManualWorkDateTypes();
            cmbManualWorkDateType_ID.ItemsSource = manualWorkDateType.OrderBy(b => b.ManualWorkDateType_ID);

            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }
        public Employees(PersonInformation x4) //fromPersonPage
        {
            InitializeComponent();
            employeeList = new List<Employee>();

            List<FingerScanFlage> fingerScanFlagList = new List<FingerScanFlage>();

            FingerScanFlage h1 = new FingerScanFlage { FingerScanFlag = true, FingerScanFlagName = "สแกน" };
            FingerScanFlage h2 = new FingerScanFlage { FingerScanFlag = false, FingerScanFlagName = "ไม่สแกน" };

            fingerScanFlagList.Add(h1);
            fingerScanFlagList.Add(h2);

            cmbFingerScanFlag.ItemsSource = fingerScanFlagList;

            xPersonInformation = x4;
            if (x4 != null)
            {
                //GetPersonInformation(x4);
                xPersonID = x4.xPersonID;
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }

            IList<Shift> shift = _hrisBusinessLayer.GetAllShifts();
            cmbShiftControlShift.ItemsSource = shift.OrderBy(b => b.StartTime);

            IList<Organization> organizationUnit = _hrisBusinessLayer.GetAllOrganizations();
            cmbOrganizationUnit_ID.ItemsSource = organizationUnit.OrderBy(b => b.OrganizationUnit.OrganizationUnitName);

            IList<ManualWorkDateType> manualWorkDateType = _hrisBusinessLayer.GetAllManualWorkDateTypes();
            cmbManualWorkDateType_ID.ItemsSource = manualWorkDateType.OrderBy(b => b.ManualWorkDateType_ID);


            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        public Employees(EmployeeCurrentInformation x3)
        {
            InitializeComponent();


            employeeList = new List<Employee>();

            List<FingerScanFlage> fingerScanFlagList = new List<FingerScanFlage>();

            FingerScanFlage h1 = new FingerScanFlage { FingerScanFlag = true, FingerScanFlagName = "สแกน" };
            FingerScanFlage h2 = new FingerScanFlage { FingerScanFlag = false, FingerScanFlagName = "ไม่สแกน" };

            fingerScanFlagList.Add(h1);
            fingerScanFlagList.Add(h2);

            cmbFingerScanFlag.ItemsSource = fingerScanFlagList;

            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                //GetEmployeeInformation(x3);
                txtCEmployee_ID.Text = x3.xEmployee_ID;
            }

            IList<Shift> shift = _hrisBusinessLayer.GetAllShifts();
            cmbShiftControlShift.ItemsSource = shift.OrderBy(b => b.StartTime);

            IList<Organization> organizationUnit = _hrisBusinessLayer.GetAllOrganizations();
            cmbOrganizationUnit_ID.ItemsSource = organizationUnit.OrderBy(b => b.OrganizationUnit.OrganizationUnitName);

            IList<ManualWorkDateType> manualWorkDateType = _hrisBusinessLayer.GetAllManualWorkDateTypes();
            cmbManualWorkDateType_ID.ItemsSource = manualWorkDateType.OrderBy(b => b.ManualWorkDateType_ID);

            List<ManualWorkDateStatus> manualWorkDateStatus = new List<ManualWorkDateStatus>();
            ManualWorkDateStatus S1 = new ManualWorkDateStatus { ManualWorkDateStatusID =true, ManualWorkDateStatusName = "Approve" };
            ManualWorkDateStatus S2 = new ManualWorkDateStatus { ManualWorkDateStatusID = false, ManualWorkDateStatusName = "Not Approve" };
            manualWorkDateStatus.Add(S1);
            manualWorkDateStatus.Add(S2);
            cmbManualWorkDateStatus.ItemsSource = manualWorkDateStatus;

            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
            // Disable change start date for current empployee
            StartDate.IsEnabled = false;
        }

        public Employees(EmployeeResignInformation x3)
        {
            InitializeComponent();


            employeeList = new List<Employee>();

            List<FingerScanFlage> fingerScanFlagList = new List<FingerScanFlage>();

            FingerScanFlage h1 = new FingerScanFlage { FingerScanFlag = true, FingerScanFlagName = "สแกน" };
            FingerScanFlage h2 = new FingerScanFlage { FingerScanFlag = false, FingerScanFlagName = "ไม่สแกน" };

            fingerScanFlagList.Add(h1);
            fingerScanFlagList.Add(h2);

            cmbFingerScanFlag.ItemsSource = fingerScanFlagList;

            xFromEmployeeResignInformation = x3;

            if (x3 != null)
            {
                //GetEmployeeInformation(x3);
                txtCEmployee_ID.Text = x3.xEmployee_ID;
            }

            IList<Shift> shift = _hrisBusinessLayer.GetAllShifts();
            cmbShiftControlShift.ItemsSource = shift.OrderBy(b => b.StartTime);

            IList<Organization> organizationUnit = _hrisBusinessLayer.GetAllOrganizations();
            cmbOrganizationUnit_ID.ItemsSource = organizationUnit.OrderBy(b => b.OrganizationUnit.OrganizationUnitName);

            IList<ManualWorkDateType> manualWorkDateType = _hrisBusinessLayer.GetAllManualWorkDateTypes();
            cmbManualWorkDateType_ID.ItemsSource = manualWorkDateType.OrderBy(b => b.ManualWorkDateType_ID);

            List<ManualWorkDateStatus> manualWorkDateStatus = new List<ManualWorkDateStatus>();
            ManualWorkDateStatus S1 = new ManualWorkDateStatus { ManualWorkDateStatusID = true, ManualWorkDateStatusName = "Approve" };
            ManualWorkDateStatus S2 = new ManualWorkDateStatus { ManualWorkDateStatusID = false, ManualWorkDateStatusName = "Not Approve" };
            manualWorkDateStatus.Add(S1);
            manualWorkDateStatus.Add(S2);
            cmbManualWorkDateStatus.ItemsSource = manualWorkDateStatus;

            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
            // Disable change start date for resigned empployee
            StartDate.IsEnabled = false;
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x )
        {   
            txtCEmployee_ID.Text = x.xEmployee_ID;
            txtCTitleNameTH.Text = x.xTitleNameTH;
            txtCFirstNameTH.Text = x.xFirstNameTH;
            txtCLastNameTH.Text = x.xLastNameTH;
            txtCFingerScan_ID.Text = x.xFingerScan_ID;           
            txtCStartDate.Text = Convert.ToString( x.xStartDate);
            txtCEndDate.Text = Convert.ToString(x.xEnddate);
            txtCBaseSalary.Text = Convert.ToString(x.xBaseSalary);
            txtCNoted.Text = x.xNoted;

            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtTitleNameEN.Text = x.xTitleNameEN;
            txtFirstNameEN.Text = x.xFirstNameEN;
            txtLastNameEN.Text = x.xLastNameEN;

            txtFingerScan_ID.Text = x.xFingerScan_ID;           
            StartDate.Text = Convert.ToString(x.xStartDate);
            EndDate.Text = Convert.ToString(x.xEnddate);
            txtBaseSalary.Text = Convert.ToString(x.xBaseSalary);
            txtNoted.Text = x.xNoted;
            txtEmail.Text = x.xEmail;

            xPersonID = x.xPerson_ID;
            xCreatedDate = x.xCreatedDateEmployee;

            //get staff type from stec_payroll
            EmployeeSTECPayroll queryZ = new EmployeeSTECPayroll();
            queryZ = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);
            //queryZ = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll("18ITC001");
            if (queryZ != null)
            {
                txtBankAccount.Text = queryZ.Bank_acc;
                if (queryZ.Staff_type == "1")//ประจำ
                {
                    RdtEmp1.IsChecked = true;
                }
                else if (queryZ.Staff_type == "2")//รายวันประจำ
                {
                    RdtEmp2.IsChecked = true;
                }
                else if (queryZ.Staff_type == "3")//รายวัน
                {
                    RdtEmp3.IsChecked = true;
                }
                else//นอกระบบ เช่น รปภ  พยาบาล นศ.ฝึกงาน
                {
                    RdtEmp4.IsChecked = true;
                }
            }
        }

        public void GetEmployeeInformation(EmployeeResignInformation x)
        {
            txtCEmployee_ID.Text = x.xEmployee_ID;
            txtCTitleNameTH.Text = x.xTitleNameTH;
            txtCFirstNameTH.Text = x.xFirstNameTH;
            txtCLastNameTH.Text = x.xLastNameTH;
            txtCFingerScan_ID.Text = x.xFingerScan_ID;
            txtCStartDate.Text = Convert.ToString(x.xStartDate);
            txtCEndDate.Text = Convert.ToString(x.xEnddate);
            txtCBaseSalary.Text = Convert.ToString(x.xBaseSalary);
            txtCNoted.Text = x.xNoted;

            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtFingerScan_ID.Text = x.xFingerScan_ID;
            StartDate.Text = Convert.ToString(x.xStartDate);
            EndDate.Text = Convert.ToString(x.xEnddate);
            txtBaseSalary.Text = Convert.ToString(x.xBaseSalary);
            txtNoted.Text = x.xNoted;
            txtEmail.Text = x.xEmail;

            xPersonID = x.xPerson_ID;
            xCreatedDate = x.xCreatedDateEmployee;

            //get staff type from stec_payroll
            EmployeeSTECPayroll queryZ = new EmployeeSTECPayroll();
            queryZ = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);
            if (queryZ != null)
            {
                if (queryZ.Staff_type == "1")//ประจำ
                {
                    RdtEmp1.IsChecked = true;
                }
                else if (queryZ.Staff_type == "2")//รายวันประจำ
                {
                    RdtEmp2.IsChecked = true;
                }
                else if (queryZ.Staff_type == "3")//รายวัน
                {
                    RdtEmp3.IsChecked = true;
                }
                else//นอกระบบ เช่น รปภ  พยาบาล นศ.ฝึกงาน
                {
                    RdtEmp4.IsChecked = true;
                }
            }
        }

        public void GetPersonInformation(PersonInformation x)
        {
            
            txtCTitleNameTH.Text = x.TitleNameTH;
            txtCFirstNameTH.Text = x.FirstNameTH;
            txtCLastNameTH.Text = x.LastNameTH;
           
            txtTitleNameTH.Text = x.TitleNameTH;
            txtFirstNameTH.Text = x.FirstNameTH;
            txtLastNameTH.Text = x.LastNameTH;

            txtFirstNameEN.Text = x.FirstNameEN;
            txtLastNameEN.Text = x.LastNameEN;
           
            xPersonID = x.xPersonID;

            //ตรวจสอบว่าพนักงานนี้มีข้อมูลการสแกนนิ้วหรือยัง  ถ้ามีและยังไม่ได้ลบออกจาก TimeSTEC จะทำการ get ค่า fingerScan_code ออกมาแสดง
            List<Employee> e1 = employeeList.Where(b => b.Person_ID == xPersonID).OrderBy(b => b.EndDate).ToList();
            foreach (Employee e2 in e1)
            {
                userinfo query1 = _hrisTimeSTECBusinessLayer.GetUserInfoByBadGenNumber(Convert.ToInt32(e2.FingerScanID));
                if (query1 != null) //ถ้าพบใน TimeSTEC ให้ Get FingerScan เดิมที่ค้างอยู่  แต่ถ้าไม่มีตอนบันทึกจะทำการ gen ค่าใหม่ให้
                {
                    txtFingerScan_ID.Text = e2.FingerScanID;
                }               
            }
            var pempList = _hrisSTECPayrollBusinessLayer.GetEmployeePayrollByPersonID(xPersonID);
            var groupAcc = pempList.GroupBy(g => g.Bank_acc).Select(s => new { BankAcc = s.Key }).ToList();
            if (groupAcc.Count == 1)
            {
                txtCBackAccount.Text = groupAcc.FirstOrDefault().BankAcc.ToString();
            }
            else
            {
                txtCBackAccount.Text = "";
            }
            //BANKACC ON LEFT
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if(xFromEmployeeCurrentInformation != null || xFromEmployeeResignInformation != null || xPersonInformation !=null)
                NavigationService.RemoveBackEntry();

            // Sets the CurrentCulture property to U.S. English.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            employeeList = _hrisBusinessLayer.GetAllEmployees().ToList();

            if (xPersonInformation != null)
            {
                xPersonInformation = new PersonInformation(xPersonID);
                GetPersonInformation(xPersonInformation);
                txtNoted.IsEnabled = true;
                ChkRenewAccCode.IsEnabled = false;
            }
            else
            {
                employeeCurrentInformation = new EmployeeCurrentInformation(txtCEmployee_ID.Text);
                GetEmployeeInformation(employeeCurrentInformation);
            }

            

            DataGridShiftControl.ItemsSource = null;
            DataGridShiftControl.ItemsSource = _hrisBusinessLayer.GetShiftControlByEmployee(txtCEmployee_ID.Text);
           
            DataGridPosition.ItemsSource = null;
            DataGridPosition.ItemsSource = _hrisBusinessLayer.GetPositionByEmployee(txtCEmployee_ID.Text);

            DataGridManualWorkDate.ItemsSource = null;
            DataGridManualWorkDate.ItemsSource = _hrisBusinessLayer.GetAllManualWorkDates().Where(b => b.Employee_ID == txtCEmployee_ID.Text).OrderByDescending(b => b.ManualWorkDateDate);

            DataGridToPayroll.ItemsSource = null;            
            DataGridToPayroll.ItemsSource = _hrisBusinessLayer.GetPayrollFlagByID(txtEmployee_ID.Text);

            DataGridTraining.ItemsSource = null;
            DataGridTraining.ItemsSource = _hrisBusinessLayer.GetTrainingByEmployee(txtEmployee_ID.Text);
        }
      
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการแก้ไขข้อมูลพนักงานรายนี้ใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                if (StartDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่เริ่มงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //if (StartDate.Text == "")
                //    StartDate.Text = Convert.ToString(DateTime.MinValue);
                //if (EndDate.Text == "")
                //    EndDate.Text = Convert.ToString(DateTime.MaxValue);
                //if (txtBaseSalary.Text == "")
                //    txtBaseSalary.Text = Convert.ToString(0);

                if (txtNoted.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ใช้กับระบบบัญชี", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                int XCount = 0;
                XCount = employeeList.Where(b => b.Noted == txtNoted.Text && b.Noted.Length == 8 && b.Employee_ID != txtEmployee_ID.Text ).Count();
                if (ChkRenewAccCode.IsChecked.GetValueOrDefault()) //Edit Account code
                {
                    if (XCount != 0 || _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayrollsByNoted(txtNoted.Text) != null)
                    {
                        MessageBox.Show("รหัสพนักงานที่ใช้กับระบบบัญชีนี้มีแล้ว กรุณาตรวจสอบความถูกต้องของข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                XCount = 0;
                XCount = employeeList.Where(b => b.Person_ID == xPersonID && b.Employee_ID != txtEmployee_ID.Text && Convert.ToDateTime(b.EndDate) >= Convert.ToDateTime(DateTime.Now)).Count();
                if (XCount != 0)
                {
                    MessageBox.Show("รหัส Person นี้ถูกใช้และยังไม่ได้อัพเดทวันสิ้นสุดทำงาน กรุณาตรวจสอบความถูกต้องของข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                

                //get person information by person_ID
                xPersonInformation = new PersonInformation(xPersonID);
                GetPersonInformation(xPersonInformation);       
                //22-04-2017 ถ้าเช็คแล้วพบว่ายังไม่มีข้อมูลเลขบัตรประชาชน จะไม่อนุญาตให้บันทึกข้อมูล
                if (xPersonInformation.IDCard == "" || xPersonInformation.IDCard == null)
                {
                    MessageBox.Show("พนักงานรายนี้ยังไม่มีข้อมูลเลขบัตรประชาชนในโมดูล Person กรุณาระบุข้อมูลก่อนทำการบันทึกรหัสพนักงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (xPersonInformation.AddressByIDCARDHomeNumber == "" || xPersonInformation.AddressByIDCARDHomeNumber == null)
                {
                    MessageBox.Show("พนักงานรายนี้ยังไม่มีข้อมูลที่อยู่ตามบัตรประชาชนในโมดูล Person กรุณาระบุข้อมูลก่อนทำการบันทึกรหัสพนักงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //หากพนักงานสิ้นสุดงานจะต้องตรวจสอบว่ามีข้อมูลโอทีวันที่ทำโอทีหลังจากวันนี้อยู่หรือไม่  หากมีจะต้องแจ้งให้ทราบ
                if (ChkEndDate.IsChecked == true)
                {
                    List<OT> t = new List<OT>();
                    t = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b=>Convert.ToDateTime(b.OTDate) > Convert.ToDateTime(EndDate.Text)).ToList();
                    if (t.Count() > 0)
                    {
                        MessageBox.Show("พบข้อมูลการบันทึกโอทีหลังวันสิ้นสุดการทำงาน กรุณาตรวจสอบการทำโอทีก่อนสิ้นสุดการทำงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    
                }

                if (RdtEmp1.IsChecked == false && RdtEmp2.IsChecked == false && RdtEmp3.IsChecked == false && RdtEmp4.IsChecked == false)
                {
                    MessageBox.Show("กรุณาระบุประเภทพนักงาน(Type)", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    if (RdtEmp1.IsChecked == true)
                    {
                        xEmployeeType = "1";
                    }
                    else if (RdtEmp2.IsChecked == true)
                    {
                        xEmployeeType = "2";
                    }
                    else if (RdtEmp3.IsChecked == true)
                    {
                        xEmployeeType = "3";
                    }
                    else if (RdtEmp4.IsChecked == true)
                    {
                        xEmployeeType = "4";
                    }
                }

                //get ค่า defaultDeptID ---->ตัวนี้ทำไว้จนกว่า organization ใน HRISsystemจะสมบูรณ์
                //ถ้ามีให้ get ค่าออกมา แต่ถ้าไม่มีให้ default เป็น 70 คือ HumanResource แล้วค่อยไปแก้ไขใน 223 
                int xDefaultDeptID;
                if (txtNoted.Text.Length == 8)
                {
                    department queryDepartment = _hrisTimeSTECBusinessLayer.GetDefaultDeptID(txtNoted.Text.Substring(2, 3));
                    if (queryDepartment != null)
                    {
                        xDefaultDeptID = queryDepartment.DeptID;
                    }
                    else
                    {
                        xDefaultDeptID = 70;
                    }
                }
                else
                {
                    xDefaultDeptID = 70;
                }

                //get current userid from userinfo
                userinfo queryBadgenumber = new userinfo();
                queryBadgenumber = _hrisTimeSTECBusinessLayer.GetUserInfoByBadGenNumber(Convert.ToInt32( txtFingerScan_ID.Text));
                
                //ถ้ามีการเปลี่ยน Acc code จะต้อง update defaultDeptID ใน TimeSTEC ด้วย
                userinfo query2 = new userinfo();
                query2.change_time = DateTime.Now;
                query2.create_time = DateTime.Now;
                query2.status = queryBadgenumber.status;
                query2.userid = queryBadgenumber.userid;
                query2.badgenumber = queryBadgenumber.badgenumber;
                query2.defaultdeptid = xDefaultDeptID;
                query2.name = queryBadgenumber.name;
                query2.Card = queryBadgenumber.Card;
                query2.SEP = queryBadgenumber.SEP;
                query2.OffDuty = queryBadgenumber.OffDuty;
                query2.DelTag = queryBadgenumber.DelTag;
                query2.AutoSchPlan = queryBadgenumber.AutoSchPlan;
                query2.MinAutoSchInterval = queryBadgenumber.MinAutoSchInterval;
                query2.RegisterOT = queryBadgenumber.RegisterOT;
                query2.set_valid_time = queryBadgenumber.set_valid_time;
                query2.isatt = queryBadgenumber.isatt;
                query2.AccGroup = queryBadgenumber.AccGroup;
                query2.TimeZones = queryBadgenumber.TimeZones;
                query2.photo = queryBadgenumber.photo;
                query2.ATT = queryBadgenumber.ATT;
                query2.OverTime = queryBadgenumber.OverTime;
                query2.Holiday = queryBadgenumber.Holiday;
                query2.INLATE = queryBadgenumber.INLATE;
                query2.OutEarly = queryBadgenumber.OutEarly;
                query2.Hiredday = queryBadgenumber.Hiredday;
                query2.Lunchduration = queryBadgenumber.Lunchduration;
                query2.Privilege = queryBadgenumber.Privilege;
                _hrisTimeSTECBusinessLayer.UpdateUserInfo(query2);

                Employee query1 = new Employee();  // created 1 instance or object
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.Person_ID = xPersonID;
                query1.FingerScanID = txtFingerScan_ID.Text;
                query1.CreatedDate = xCreatedDate;
                query1.StartDate = Convert.ToDateTime(StartDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.BaseSalary = Convert.ToDecimal(txtBaseSalary.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.Noted = txtNoted.Text;
                query1.Email = txtEmail.Text;
                _hrisBusinessLayer.UpdateEmployee(query1);

                //----employee in STEC_PAYROLL เฉพาะ xemployeeTyep = 2  รายวันประจำ, 3 รายวัน
                //----23/10/2017 แก้ไขให้พนักงานประจำ type = 1 ก็บันทึกใน stec payroll เช่นกัน
                if (xEmployeeType == "1" ||  xEmployeeType == "2" || xEmployeeType == "3")
                {
                    string _pAddress;
                    string _pMoo;
                    string _pTambon;
                    string _pAmphur;
                    string _pProvince;
                    string _pZipcode;
                    
                            
                    if (xPersonInformation.AddressByIDCARDHomeNumber == null  )
                    {
                        _pAddress = "250";
                        _pMoo = "3";
                        _pTambon = "ยางเนิ้ง";
                        _pAmphur = "สารภี";
                        _pProvince = "เชียงใหม่";
                        _pZipcode = "50140";
                    }
                    else
                    {
                        _pAddress = xPersonInformation.AddressByIDCARDHomeNumber;
                        _pMoo = xPersonInformation.AddressByIDCARDMoo.ToString();
                        _pTambon = xPersonInformation.AddressByIDCARDSubDistrict;
                        _pAmphur = xPersonInformation.AddressByIDCARDDistrict;
                        _pProvince = xPersonInformation.AddressByIDCARDProvince;
                        _pZipcode = xPersonInformation.AddressByIDCARDPostcode.ToString();
                    }

                    if (ChkEndDate.IsChecked == true)
                    {
                        EmployeeSTECPayroll queryZ = new EmployeeSTECPayroll();
                        queryZ = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);
                        if (queryZ != null)
                        {
                            //14 Nov 2017 แก้ไข โดยเช็คเพิ่มว่า หากเป็นพนักงานประจำ staff_type = 1 
                            //ให้ถามก่อนว่า ต้องการให้คงบัญชีถึงสิ้นเดือนหรือไม่ หากให้คงบัญชีถึงสิ้นเดือนระบบเงินเดือนจะคำนวนเงินเดือนให้
                            //ให้กำหนด staff_status = 3(คงบัญชีถึงส้ินเดือน) เนื่องจากยังต้องจ่ายเงินเดือนอยู่
                            if (xEmployeeType == "1")
                            {
                                MessageBoxResult result1 = MessageBox.Show("ต้องการให้คงบัญชีถึงสิ้นเดือนใช่หรือไม่ ? (คงบัญชีถึงสิ้นเดือนหมายถึง ระบบเงินเดือนจะคำนวนเงินเดือนตั้งแต่วันที่ 1-สิ้นเดือนที่มาทำงานจริงรวมทั้งคำนวนวันทำโอทีตั้งแต่ 26-25 ให้ หากไม่แน่ใจกรุณาตรวจสอบกับฝ่ายบัญชี(ป้าณี) อีกครั้ง) หากต้องการให้คงบัญชีถึงสิ้นเดือนให้กดปุ่ม Yes , หากไม่ต้องการให้คงบัญชีถึงส้ินเดือนให้กดปุ่ม No , หากไม่แน่ใจและต้องการยกเลิกเพื่อสอบถามบัญชีให้กดปุ่ม Cancel", "Important Question",MessageBoxButton.YesNoCancel);
                                //ต้องการให้คงบัญชีถึงสิ้นเดือน
                                if(result1 == MessageBoxResult.Yes){
                                    //ให้สิ้นสุดรหัสเดิม โดยกำหนด staff_sttus = 2
                                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                                    queryX.Employee_id = txtCNoted.Text;
                                    queryX.Dept_code = txtCNoted.Text.Substring(2, 3);
                                    queryX.Tname = txtTitleNameTH.Text;
                                    queryX.Fname = txtFirstNameTH.Text;
                                    queryX.Lname = txtLastNameTH.Text;
                                    queryX.Fname_Eng = txtFirstNameEN.Text;
                                    queryX.Lname_Eng = txtLastNameEN.Text;
                                    queryX.Birth = xPersonInformation.DateOfBirth;
                                    queryX.Id_card = xPersonInformation.IDCard;
                                    queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                                    queryX.Bank_acc = queryZ.Bank_acc;
                                    queryX.Staff_status = "3";
                                    queryX.Staff_type = xEmployeeType;
                                    queryX.Address = _pAddress;
                                    queryX.Moo = _pMoo;
                                    queryX.Tambon = _pTambon;
                                    queryX.Amphur = _pAmphur;
                                    queryX.Province = _pProvince;
                                    queryX.Zipcode = _pZipcode;
                                    queryX.Shift = 7;
                                    queryX.Wage = queryZ.Wage;
                                    queryX.Salary = queryZ.Salary;
                                    queryX.NetSalary = queryZ.NetSalary;
                                    queryX.Tax = queryZ.Tax;
                                    queryX.Ot_hour_rate = queryZ.Ot_hour_rate;
                                    queryX.Ot_holiday_rate = queryZ.Ot_holiday_rate;
                                    queryX.Ot_hour_holiday_rate = queryZ.Ot_hour_holiday_rate;
                                    queryX.Ot_special_rate = queryZ.Ot_special_rate;
                                    employeeSTECObj[0] = queryX;
                                    _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj); 
                                }else if(result1 == MessageBoxResult.No){
                                    //ไม่ต้องการให้คงบัญชีถึงสิ้นเดือน
                                    //ให้สิ้นสุดรหัสเดิม โดยกำหนด staff_sttus = 2
                                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                                    queryX.Employee_id = txtCNoted.Text;
                                    queryX.Dept_code = txtCNoted.Text.Substring(2, 3);
                                    queryX.Tname = txtTitleNameTH.Text;
                                    queryX.Fname = txtFirstNameTH.Text;
                                    queryX.Lname = txtLastNameTH.Text;
                                    queryX.Fname_Eng = txtFirstNameEN.Text;
                                    queryX.Lname_Eng = txtLastNameEN.Text;
                                    queryX.Birth = xPersonInformation.DateOfBirth;
                                    queryX.Id_card = xPersonInformation.IDCard;
                                    queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                                    queryX.Bank_acc = queryZ.Bank_acc;
                                    queryX.Staff_status = "2";
                                    queryX.Staff_type = xEmployeeType;
                                    queryX.Address = _pAddress;
                                    queryX.Moo = _pMoo;
                                    queryX.Tambon = _pTambon;
                                    queryX.Amphur = _pAmphur;
                                    queryX.Province = _pProvince;
                                    queryX.Zipcode = _pZipcode;
                                    queryX.Shift = 7;
                                    queryX.Wage = queryZ.Wage;
                                    queryX.Salary = queryZ.Salary;
                                    queryX.NetSalary = queryZ.NetSalary;
                                    queryX.Tax = queryZ.Tax;
                                    queryX.Ot_hour_rate = queryZ.Ot_hour_rate;
                                    queryX.Ot_holiday_rate = queryZ.Ot_holiday_rate;
                                    queryX.Ot_hour_holiday_rate = queryZ.Ot_hour_holiday_rate;
                                    queryX.Ot_special_rate = queryZ.Ot_special_rate;
                                    employeeSTECObj[0] = queryX;
                                    _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj); 
                                }else if (result1 == MessageBoxResult.Cancel){
                                    //ต้องการ cancel ไปก่อนเพื่อปรึกษาบัญชี
                                    return;
                                }                                
                          }
                            else
                            {
                                
                                //ให้สิ้นสุดรหัสเดิม โดยกำหนด staff_sttus = 2
                                EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                                EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                                queryX.Employee_id = txtCNoted.Text;
                                queryX.Dept_code = txtCNoted.Text.Substring(2, 3);
                                queryX.Tname = txtTitleNameTH.Text;
                                queryX.Fname = txtFirstNameTH.Text;
                                queryX.Lname = txtLastNameTH.Text;
                                queryX.Fname_Eng = txtFirstNameEN.Text;
                                queryX.Lname_Eng = txtLastNameEN.Text;
                                queryX.Birth = xPersonInformation.DateOfBirth;
                                queryX.Id_card = xPersonInformation.IDCard;
                                queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                                queryX.Bank_acc = queryZ.Bank_acc;
                                queryX.Staff_status = "2";
                                queryX.Staff_type = xEmployeeType;
                                queryX.Address = _pAddress;
                                queryX.Moo = _pMoo;
                                queryX.Tambon = _pTambon;
                                queryX.Amphur = _pAmphur;
                                queryX.Province = _pProvince;
                                queryX.Zipcode = _pZipcode;
                                queryX.Shift = 7;
                                queryX.Wage = queryZ.Wage;
                                queryX.Salary = queryZ.Salary;
                                queryX.NetSalary = queryZ.NetSalary;
                                queryX.Tax = queryZ.Tax;
                                queryX.Ot_hour_rate = queryZ.Ot_hour_rate;
                                queryX.Ot_holiday_rate = queryZ.Ot_holiday_rate;
                                queryX.Ot_hour_holiday_rate = queryZ.Ot_hour_holiday_rate;
                                queryX.Ot_special_rate = queryZ.Ot_special_rate;
                                employeeSTECObj[0] = queryX;
                                _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj); 
                            }

                                                       
                        }
                        
                    }
                    else if (ChkEndDate.IsChecked == false)
                    {
                        //ถ้ามีการเปลี่ยนรหัส Acc code ใหม่ ให้ staff_type = 2 ในรหัสเดิม และเพิ่มรหัสใหม่
                        if (ChkRenewAccCode.IsChecked == true)
                        {
                            EmployeeSTECPayroll queryZ = new EmployeeSTECPayroll();
                            queryZ = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);
                            if (queryZ != null)
                            {
                                //ให้สิ้นสุดรหัสเดิม โดยกำหนด staff_sttus = 2
                                EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                                EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                                queryX.Employee_id = txtCNoted.Text;
                                queryX.Dept_code = txtCNoted.Text.Substring(2, 3);
                                queryX.Tname = txtTitleNameTH.Text;
                                queryX.Fname = txtFirstNameTH.Text;
                                queryX.Lname = txtLastNameTH.Text;
                                queryX.Fname_Eng = txtFirstNameEN.Text;
                                queryX.Lname_Eng = txtLastNameEN.Text;
                                queryX.Birth = xPersonInformation.DateOfBirth;
                                queryX.Id_card = xPersonInformation.IDCard;
                                queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                                queryX.Bank_acc = queryZ.Bank_acc;
                                queryX.Staff_status = "2";
                                queryX.Staff_type = xEmployeeType;
                                queryX.Address = _pAddress;
                                queryX.Moo = _pMoo;
                                queryX.Tambon = _pTambon;
                                queryX.Amphur = _pAmphur;
                                queryX.Province = _pProvince;
                                queryX.Zipcode = _pZipcode;
                                queryX.Shift = 7;
                                queryX.Wage = queryZ.Wage;
                                queryX.Salary = queryZ.Salary;
                                queryX.Tax = queryZ.Tax;
                                queryX.NetSalary = queryZ.NetSalary;
                                queryX.Ot_hour_rate = queryZ.Ot_hour_rate;
                                queryX.Ot_holiday_rate = queryZ.Ot_holiday_rate;
                                queryX.Ot_hour_holiday_rate = queryZ.Ot_hour_holiday_rate;
                                queryX.Ot_special_rate = queryZ.Ot_special_rate;
                                employeeSTECObj[0] = queryX;
                                _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObj);

                                //ให้กำหนดรหัสใหม่
                                EmployeeSTECPayroll[] employeeSTECObI = new EmployeeSTECPayroll[1];
                                EmployeeSTECPayroll queryy = new EmployeeSTECPayroll();
                                queryy.Employee_id = txtNoted.Text;
                                queryy.Dept_code = txtNoted.Text.Substring(2, 3);
                                queryy.Tname = txtTitleNameTH.Text;
                                queryy.Fname = txtFirstNameTH.Text;
                                queryy.Lname = txtLastNameTH.Text;
                                queryy.Fname_Eng = txtFirstNameEN.Text;
                                queryy.Lname_Eng = txtLastNameEN.Text;
                                queryy.Birth = xPersonInformation.DateOfBirth;
                                queryy.Id_card = xPersonInformation.IDCard;
                                queryy.Start_date = Convert.ToDateTime(StartDate.Text);
                                queryy.Bank_acc = queryZ.Bank_acc;
                                queryy.Staff_status = "1";
                                queryy.Staff_type = xEmployeeType;
                                queryy.Address = _pAddress;
                                queryy.Moo = _pMoo;
                                queryy.Tambon = _pTambon;
                                queryy.Amphur = _pAmphur;
                                queryy.Province = _pProvince;
                                queryy.Zipcode = _pZipcode;
                                queryy.Shift = 7;
                                queryy.Bank_acc = queryZ.Bank_acc;
                                queryy.Wage = queryZ.Wage;
                                queryy.Salary = queryZ.Salary;
                                queryy.Tax = queryZ.Tax;
                                queryy.NetSalary = queryZ.NetSalary;
                                queryy.Ot_hour_rate = queryZ.Ot_hour_rate;
                                queryy.Ot_holiday_rate = queryZ.Ot_holiday_rate;
                                queryy.Ot_hour_holiday_rate = queryZ.Ot_hour_holiday_rate;
                                queryy.Ot_special_rate = queryZ.Ot_special_rate;

                                employeeSTECObI[0] = queryy;
                                _hrisSTECPayrollBusinessLayer.AddEmployeeSTEcPayroll(employeeSTECObI);
                            }                           
                        }
                        //ถ้าไม่ใช่การกำหนดรหัส acc code ใหม่ ให้ update 
                        if (ChkRenewAccCode.IsChecked == false)
                        {
                            EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                            employeeSTECPayroll = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);

                            EmployeeSTECPayroll[] employeeSTECObZ = new EmployeeSTECPayroll[1];
                            EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                            queryX.Employee_id = txtCNoted.Text;
                            queryX.Dept_code = txtCNoted.Text.Substring(2, 3);
                            queryX.Tname = txtTitleNameTH.Text;
                            queryX.Fname = txtFirstNameTH.Text;
                            queryX.Lname = txtLastNameTH.Text;
                            queryX.Fname_Eng = txtFirstNameEN.Text;
                            queryX.Lname_Eng = txtLastNameEN.Text;
                            queryX.Birth = xPersonInformation.DateOfBirth;
                            queryX.Id_card = xPersonInformation.IDCard;
                            queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                            queryX.Bank_acc = employeeSTECPayroll.Bank_acc;
                            if (ChkEndDate.IsChecked == true)//สิ้นสุด
                            {
                                queryX.Staff_status = "2";
                            }
                            else if (ChkEndDate.IsChecked == false)//ยังทำงาน
                            {
                                queryX.Staff_status = "1";
                            }
                            
                            queryX.Staff_type = xEmployeeType;
                            queryX.Address = _pAddress;
                            queryX.Moo = _pMoo;
                            queryX.Tambon = _pTambon;
                            queryX.Amphur = _pAmphur;
                            queryX.Province = _pProvince;
                            queryX.Zipcode = _pZipcode;
                            queryX.Shift = 7;
                            queryX.Wage = employeeSTECPayroll.Wage;
                            queryX.Tax = employeeSTECPayroll.Tax;
                            queryX.Salary = employeeSTECPayroll.Salary;
                            queryX.NetSalary = employeeSTECPayroll.NetSalary;
                            queryX.Ot_hour_rate = employeeSTECPayroll.Ot_hour_rate;
                            queryX.Ot_holiday_rate = employeeSTECPayroll.Ot_holiday_rate;
                            queryX.Ot_hour_holiday_rate = employeeSTECPayroll.Ot_hour_holiday_rate;
                            queryX.Ot_special_rate = employeeSTECPayroll.Ot_special_rate;                            
                            employeeSTECObZ[0] = queryX;
                            _hrisSTECPayrollBusinessLayer.UpdateEmployeeSTECPayroll(employeeSTECObZ);
                        }
                       
                        
                    }                   
                }

                

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T006";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = xPersonID;
                transactionLog1.OldData = txtCFingerScan_ID.Text + " " + txtCStartDate.Text + " " + txtCEndDate.Text + " " + txtCNoted.Text;
                transactionLog1.NewData = txtFingerScan_ID.Text + " " + Convert.ToString(StartDate.Text) + " " + Convert.ToString(EndDate.Text) + " " + txtNoted.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //SendEmailForApprove(txtEmployee_ID.Text,"Edit");
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว");

                ClearData(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void SendEmailForApprove(string employee_id,string type)
        {

            Employee e = new Employee();
            e = _hrisBusinessLayer.GetEmployeeInformation(employee_id);

            string xstaff_type = "";
            if(RdtEmp1.IsChecked==true){
                xstaff_type ="พนักงานประจำ";
            }else if(RdtEmp2.IsChecked==true){
                xstaff_type ="พนักงานรายวันประจำ";
            }else if(RdtEmp3.IsChecked==true){
                xstaff_type ="พนักงานรายวัน";
            }

            
            string toName = "ฝ่ายบัญชีและการเงิน";
            //string toEmail = e.Email;

            string fromPerson = "it-info@siamtobacco.com";
            string fromPassword = "Up2uy@dmin$tecE!";

            MailMessage mailmessage = new MailMessage();

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(fromPerson, fromPassword);
            client.Port = 587;
            client.Host = "smtp.emailsrvr.com";
           

            mailmessage.From = new MailAddress("it-info@siamtobacco.com");
            mailmessage.To.Add("saruta@siamtobacco.com");
            mailmessage.To.Add("pitchayapa@siamtobacco.com");
            if ( RdtEmp1.IsChecked==true ) //if พนักงานประจำ จะส่งอีเมลล์แจ้งพี่เด
            {
                mailmessage.To.Add("dachar@siamtobacco.com");
            }
            mailmessage.To.Add("pranee@siamtobacco.com");
            mailmessage.To.Add("m.tanainan@siamtobacco.com");
            mailmessage.To.Add("pennapa@siamtobacco.com");
            mailmessage.CC.Add(_singleton.emailFrom);
            

            string messages = "";
            if (type == "Add")
            {
                mailmessage.Subject = "The new employee from HRIS system.";
                messages   = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียน " + toName + " </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>ฝ่าย HR เพิ่มข้อมูลรหัสพนักงาน " + e.Employee_ID + " รหัสบัญชี คือ " + e.Noted + "</td>" +                                                   
                                                "</tr>" +
                                                "<tr>" +
                                                     "<td>พนักงานชื่อ " + e.Person.FirstNameTH + " " + e.Person.LastNameTH + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                     "<td>ประเภท " + xstaff_type + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                     "<td>เริ่มงานวันที่ " + e.StartDate + "</td>" +
                                                "</tr>" +

                                                "<tr>" +
                                                    "<td>" + "  กรุณาตรวจสอบข้อมูลและบันทึกข้อมูลค่าแรง/เงินเดือน และเลขบัญชีธนาคารในโปรแกรม Payroll </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear " + toName + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>HRM department add new employee information and could you please consider in the payroll system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;           
            }
            else if (type == "Delete")
            {
                mailmessage.Subject = "Delete employee from HRIS system.";
                messages = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียน " + toName + " </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>ฝ่าย HR ได้ลบข้อมูลรหัสพนักงาน " + e.Employee_ID + " รหัสบัญชี คือ " + e.Noted + " พนักงานชื่อ " + e.Person.FirstNameTH + " " + e.Person.LastNameTH + "  กรุณาตรวจสอบข้อมูลและบันทึกข้อมูลค่าแรง/เงินเดือน และเลขบัญชีธนาคารในโปรแกรม Payroll </td>" +
                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +

                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear " + toName + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>HRM department delete employee information and could you please consider in the payroll system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;       
            }
            else if (type == "Edit")
            {
                mailmessage.Subject = "Edit employee from HRIS system.";
              messages   = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียน " + toName + " </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +                                                   
                                                    "<td>ฝ่าย HR ได้มีการแก้ไขข้อมูลรหัสพนักงาน " + e.Employee_ID + " รหัสบัญชี คือ " + e.Noted + "</td>" +                        
                                                "</tr>" +
                                                    
                                                "<tr>" +
                                                    "<td>พนักงานชื่อ " + e.Person.FirstNameTH + " " + e.Person.LastNameTH + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>ประเภท " + xstaff_type + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>เริ่มงานวันที่ " + e.StartDate + "</td>" +
                                                "</tr>" +

                                                "<tr>" +
                                                    "<td>" + "  กรุณาตรวจสอบข้อมูลและบันทึกข้อมูลค่าแรง/เงินเดือน และเลขบัญชีธนาคารในโปรแกรม Payroll </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear " + toName + "</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>HRM department edit employee information and could you please consider in the payroll system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;
            }
            
            //mailmessage.
            mailmessage.Body = messages;
            mailmessage.IsBodyHtml = true;
            try
            {
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearData(int TabIndex)
        {
            try
            {
                if (TabIndex == 0)
                {
                    employeeCurrentInformation = new EmployeeCurrentInformation(txtEmployee_ID.Text);
                    GetEmployeeInformation(employeeCurrentInformation);

                    employeeList = _hrisBusinessLayer.GetAllEmployees().ToList();                 
                }
                else if (TabIndex == 1)
                {
                    AddShiftControlButton.IsEnabled = true;
                    cmbShiftControlShift.Text = "";
                    cmbShiftControlShift.IsEnabled = true;
                    txtShiftControlNoted.Text = "";
                    ShiftControlValidFrom.Text = "";
                    ShiftControlValidFrom.IsEnabled = true;
                    ShiftControlEndDate.Text = "";
                    EditShiftControlButton.IsEnabled = false;
                    DeleteShiftControlButton.IsEnabled = false;

                    DataGridShiftControl.ItemsSource = null;
                    DataGridShiftControl.ItemsSource = _hrisBusinessLayer.GetShiftControlByEmployee(txtCEmployee_ID.Text);
                   
                }
                else if (TabIndex == 2)
                {
                    AddPositionButton.IsEnabled = true;
                    //Tab Position
                    DataGridPosition.ItemsSource = null;
                    DataGridPosition.ItemsSource = _hrisBusinessLayer.GetPositionByEmployee(txtCEmployee_ID.Text);
                   
                }else if (TabIndex == 3)
                {
                    cmbManualWorkDateType_ID.IsEnabled = true;
                    AddManualWorkdateButton.IsEnabled = true;
                    ManualWorkDateDate.IsEnabled = true;
                    ManualWorkDateDateTo.IsEnabled = true;

                    DataGridManualWorkDate.ItemsSource = null;
                    DataGridManualWorkDate.ItemsSource = _hrisBusinessLayer.GetAllManualWorkDates().Where(b => b.Employee_ID == txtCEmployee_ID.Text && b.ManualWorkDateDate >= Convert.ToDateTime(DateFromSearch.Text) && b.ManualWorkDateDate <= Convert.ToDateTime(DateToSearch.Text)).OrderByDescending(b => b.ManualWorkDateDate);

                    //DataGridManualWorkDate.ItemsSource = null;
                    //DataGridManualWorkDate.ItemsSource = _hrisBusinessLayer.GetAllManualWorkDates().Where(b => b.Employee_ID == txtCEmployee_ID.Text).OrderByDescending(b => b.ManualWorkDateDate);
                }
                else if (TabIndex == 5)
                {
                    DataGridToPayroll.ItemsSource = null;
                    DataGridToPayroll.ItemsSource = _hrisBusinessLayer.GetPayrollFlagByID(txtCEmployee_ID.Text);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StartDate.Text == "")
                    StartDate.Text = Convert.ToString(DateTime.MinValue);
                if (EndDate.Text == "")
                    EndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (txtBaseSalary.Text == "")
                    txtBaseSalary.Text = Convert.ToString(0);

            
                if (MessageBox.Show("Do you want to delete ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                //SendEmailForApprove(txtEmployee_ID.Text, "Delete");

                Employee query1 = new Employee();
                query1 = _hrisBusinessLayer.GetEmployeeInformation(txtCEmployee_ID.Text);
                if (query1 != null)
                {
                    _hrisBusinessLayer.RemoveEmployee(query1);
                }    
                

                EmployeeSTECPayroll queryx = new EmployeeSTECPayroll();
                queryx = _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayroll(txtCNoted.Text);
                if (queryx != null)
                {
                    _hrisSTECPayrollBusinessLayer.RemoveEmployeeSTECPayroll(queryx);
                }
               

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T006";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = xPersonID;
                transactionLog1.OldData = txtCFingerScan_ID.Text + " " + txtCStartDate.Text + " " + txtCEndDate.Text + " " + txtCNoted.Text;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว");

                ClearData(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(0);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TcEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    // ... Get TabControl reference.
            //    var item = sender as TabControl;
            //    // ... Set Title to selected tab header.
            //    var selected = item.SelectedItem as TabItem;
            //    this.Title = selected.Header.ToString();
            //    int TcEmployeeTabIndex = TcEmployee.SelectedIndex;
            //    if((sender as TabControl).SelectedIndex == 1)
            //    {
            //        ClearData(1);
            //    }
                

            //    if (TcEmployeeTabIndex == 2)
            //    {
            //        ClearData(2);
            //    }

            //    if(TcEmployeeTabIndex == 3)
            //    {
            //        ClearData(3);                   
            //    }               
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        private void AddShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (ShiftControlValidFrom.Text == "") ShiftControlValidFrom.Text = Convert.ToString(DateTime.MinValue);
                if (string.IsNullOrEmpty(ShiftControlEndDate.Text)) ShiftControlEndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (string.IsNullOrEmpty(ShiftControlValidFrom.Text) || string.IsNullOrEmpty(ShiftControlEndDate.Text))
                {
                    MessageBox.Show("กรุณาเลือกวันที่ให้ถูกต้อง"); return;
                }
                else if (Convert.ToDateTime(ShiftControlValidFrom.Text) > Convert.ToDateTime(ShiftControlEndDate.Text))
                {
                    MessageBox.Show("กรุณาเลือกวันที่ให้ถูกต้อง"); return;
                }

                ShiftControl[] shiftControlObj = new ShiftControl[1];
                ShiftControl query1 = new ShiftControl();  // created 1 instance or object

                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.Shift_ID = (string)cmbShiftControlShift.SelectedValue;
                query1.Noted = txtShiftControlNoted.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ShiftControlValidFrom.Text);
                query1.EndDate = Convert.ToDateTime(ShiftControlEndDate.Text);
                query1.ModifiedDate = DateTime.Now;

                shiftControlObj[0] = query1;
                _hrisBusinessLayer.AddShiftControl(shiftControlObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T004";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = (string)cmbShiftControlShift.SelectedValue;
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (ShiftControlValidFrom.Text == "")
                //    ShiftControlValidFrom.Text = Convert.ToString(DateTime.MinValue);

                if (MessageBox.Show("ยืนยันการแก้ไขข้อมูลหรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                if (string.IsNullOrEmpty(ShiftControlEndDate.Text)) ShiftControlEndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (string.IsNullOrEmpty(ShiftControlValidFrom.Text) || string.IsNullOrEmpty(ShiftControlEndDate.Text))
                {
                    MessageBox.Show("กรุณาเลือกวันที่ให้ถูกต้อง"); return;
                }
                else if (Convert.ToDateTime(ShiftControlValidFrom.Text) > Convert.ToDateTime(ShiftControlEndDate.Text))
                {
                    MessageBox.Show("กรุณาเลือกวันที่ให้ถูกต้อง"); return;
                }

                ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                ShiftControl query2 = new ShiftControl();  // created 1 instance or object

                query2 = (ShiftControl)DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.Shift_ID = (string)cmbShiftControlShift.SelectedValue;
                query1.Noted = txtShiftControlNoted.Text;
                query1.CreatedDate = query2.CreatedDate;
                query1.ValidFrom = Convert.ToDateTime(ShiftControlValidFrom.Text);
                query1.EndDate = Convert.ToDateTime(ShiftControlEndDate.Text);
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateShiftControl(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T004";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = (string)cmbShiftControlShift.SelectedValue;
                transactionLog1.OldData = Convert.ToString(query2.ValidFrom) + " " + Convert.ToString(query2.EndDate);
                transactionLog1.NewData = (string)ShiftControlValidFrom.Text + " " + (string)ShiftControlEndDate.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData(1);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                query1 = (ShiftControl)DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveShiftControl(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T004";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = (string)cmbShiftControlShift.SelectedValue;
                transactionLog1.OldData = Convert.ToString(query1.ValidFrom) + " " + Convert.ToString(query1.EndDate);
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData(1);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearShiftControlButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridShiftControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ShiftControl query1 = new ShiftControl();  // created 1 instance or object
                query1 = (ShiftControl)DataGridShiftControl.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                bool isnullShiftData = query1 == null;
                AddShiftControlButton.IsEnabled = isnullShiftData;
                cmbShiftControlShift.IsEnabled = isnullShiftData;
                EditShiftControlButton.IsEnabled = !isnullShiftData;
                DeleteShiftControlButton.IsEnabled = !isnullShiftData;
                // Cannot edit Validfrom bcuz it is PK
                ShiftControlValidFrom.IsEnabled = isnullShiftData;
                if (!isnullShiftData)
                {
                    cmbShiftControlShift.SelectedValue = query1.Shift_ID;
                    txtShiftControlNoted.Text = query1.Noted;
                    ShiftControlValidFrom.SelectedDate = query1.ValidFrom;
                    ShiftControlEndDate.SelectedDate = query1.EndDate.GetValueOrDefault();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbOrganizationUnit_ID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                String XOrganization_ID = (String)cmbOrganizationUnit_ID.SelectedValue;
                IList<PositionOrganization> positionOrganization = _hrisBusinessLayer.GetAllPositionOrganizations().Where(p => p.Organization_ID == XOrganization_ID).ToList();
                cmbPostion_ID.ItemsSource = positionOrganization.OrderBy(b => b.Position.PositionNameTH);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PositionHireDate.Text == "")
                    PositionHireDate.Text = Convert.ToString(DateTime.MinValue);
                if (PositionEndDate.Text == "")
                    PositionEndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (cmbFingerScanFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก use finger scan condition", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                EmployeePosition[] employeePositonObj = new EmployeePosition[1];
                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object
                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.PositionOrganization_ID = (string)cmbPostion_ID.SelectedValue;
                query1.CreatedDate = DateTime.Now;
                query1.HireDate = Convert.ToDateTime(PositionHireDate.Text);
                query1.EndDate = Convert.ToDateTime(PositionEndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.FingerScanFlag = (Boolean)cmbFingerScanFlag.SelectedValue;
                employeePositonObj[0] = query1;
                _hrisBusinessLayer.AddEmployeePosition(employeePositonObj);

                //transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                //if (transactionLog.Count > 0)
                //{
                //    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                //}
                //else
                //{
                //    XMaxTransactionLog_ID = 1;
                //}
                //TransactionLog[] transactionLogObj = new TransactionLog[1];
                //TransactionLog transactionLog1 = new TransactionLog();
                //transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                //transactionLog1.TransactionType_ID = "U";
                //transactionLog1.Table_ID = "T005";
                //transactionLog1.TransactionDate = DateTime.Now;
                //transactionLog1.FieldName = "Employee_ID";
                //transactionLog1.PKFields = txtCEmployee_ID.Text;
                //transactionLog1.FKFields = (string)cmbShiftControlShift.SelectedValue;
                //transactionLog1.OldData = "";
                //transactionLog1.NewData = "";
                //transactionLog1.ModifiedDate = DateTime.Now;
                //transactionLog1.ModifiedUser = _singleton.username;
                //transactionLogObj[0] = transactionLog1;
                //_hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PositionHireDate.Text == "")
                    PositionHireDate.Text = Convert.ToString(DateTime.MinValue);
                if (cmbFingerScanFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก use finger scan condition", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object
                EmployeePosition query2 = new EmployeePosition();  // created 1 instance or object

                query2 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.PositionOrganization_ID = (string)cmbPostion_ID.SelectedValue;
                query1.CreatedDate = query2.CreatedDate;
                query1.HireDate = Convert.ToDateTime(PositionHireDate.Text);
                query1.EndDate = Convert.ToDateTime(PositionEndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.FingerScanFlag = (Boolean)cmbFingerScanFlag.SelectedValue;
                //query1.PositionType_ID = (string)cmbPositionType_ID.SelectedValue;


                _hrisBusinessLayer.UpdateEmployeePosition(query1);
                ClearData(2);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeletePositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object

                query1 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveEmployeePosition(query1);
                ClearData(2);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridPosition_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EmployeePosition query1 = new EmployeePosition();  // created 1 instance or object
                query1 = (EmployeePosition)DataGridPosition.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddPositionButton.IsEnabled = true;
                }
                else
                {
                    AddPositionButton.IsEnabled = false;
                    //cmbPostion_ID.SelectedValuePath = query1.PositionOrganization_ID;
                    String XOrganization_ID = (String)cmbOrganizationUnit_ID.SelectedValue;
                    IList<PositionOrganization> positionOrganization = _hrisBusinessLayer.GetAllPositionOrganizations().Where(p => p.Organization_ID == query1.PositionOrganization.Organization_ID).ToList();
                    cmbPostion_ID.ItemsSource = positionOrganization.OrderBy(b => b.Position.PositionNameTH);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {                              
                if (StartDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่เริ่มงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                    //StartDate.Text = Convert.ToString(DateTime.MinValue);

                if (EndDate.Text == "") EndDate.Text = Convert.ToString(DateTime.MaxValue);
                if (txtBaseSalary.Text == "") txtBaseSalary.Text = Convert.ToString(0);                 

                if (txtNoted.Text == "" )
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ใช้กับระบบบัญชี", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                int XCount = 0;
                XCount = employeeList.Where(b => b.Noted == txtNoted.Text && b.Noted.Length == 8).Count();
                if (XCount != 0 || _hrisSTECPayrollBusinessLayer.GetEmployeeSTECPayrollsByNoted(txtNoted.Text) != null)
                {
                    MessageBox.Show("รหัสพนักงานที่ใช้กับระบบบัญชีนี้มีแล้ว กรุณาตรวจสอบความถูกต้องของข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (RdtEmp1.IsChecked == false && RdtEmp2.IsChecked == false && RdtEmp3.IsChecked == false && RdtEmp4.IsChecked == false)
                {
                    MessageBox.Show("กรุณาระบุประเภทพนักงาน(Type)", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    if (RdtEmp1.IsChecked == true)
                    {
                        xEmployeeType = "1";
                    }else if (RdtEmp2.IsChecked == true){
                        xEmployeeType = "2";
                    }else if(RdtEmp3.IsChecked==true){
                        xEmployeeType  = "3";
                    }else if(RdtEmp4.IsChecked ==true){
                        xEmployeeType = "4";
                    }
                }

                XCount = 0;
                XCount = employeeList.Where(b => b.Person_ID == xPersonID && Convert.ToDateTime(b.EndDate) >= Convert.ToDateTime(DateTime.Now)).Count();
                if (XCount != 0)
                {
                    MessageBox.Show("รหัส Person นี้ถูกใช้และยังไม่ได้อัพเดทวันสิ้นสุดทำงาน กรุณาตรวจสอบความถูกต้องของข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //22-04-2017 ถ้าเช็คแล้วพบว่ายังไม่มีข้อมูลเลขบัตรประชาชน จะไม่อนุญาตให้บันทึกข้อมูล
                if (xPersonInformation.IDCard == "" || xPersonInformation.IDCard == null)
                {
                    MessageBox.Show("พนักงานรายนี้ยังไม่มีข้อมูลเลขบัตรประชาชนในโมดูล Person กรุณาระบุข้อมูลก่อนทำการบันทึกรหัสพนักงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (xPersonInformation.AddressByIDCARDHomeNumber == "" || xPersonInformation.AddressByIDCARDHomeNumber == null)
                {
                    MessageBox.Show("พนักงานรายนี้ยังไม่มีข้อมูลที่อยู่ตามบัตรประชาชนในโมดูล Person กรุณาระบุข้อมูลก่อนทำการบันทึกรหัสพนักงาน", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //get ค่า defaultDeptID ---->ตัวนี้ทำไว้จนกว่า organization ใน HRISsystemจะสมบูรณ์
                //ถ้ามีให้ get ค่าออกมา แต่ถ้าไม่มีให้ default เป็น 70 คือ HumanResource แล้วค่อยไปแก้ไขใน 223 
                int xDefaultDeptID;
                if (txtNoted.Text.Length == 8)
                {
                    department queryDepartment = _hrisTimeSTECBusinessLayer.GetDefaultDeptID(txtNoted.Text.Substring(2, 3));                    
                    if (queryDepartment != null)
                    {
                        xDefaultDeptID = queryDepartment.DeptID;
                    }
                    else
                    {
                        xDefaultDeptID = 70;
                    }
                }
                else
                {
                    xDefaultDeptID = 70;   
                }                
                

                //ถ้า FingerScan ไม่พบใน TimeSTEC แล้วให้ทำการ gen Finger ใหม่และ insert ใน TimeSTEC แต่ยังต้องกดปุ่มOK ใน 223 อยู่ เพราะไม่ sync ลงเครื่องสแกนค่ะ
                if (txtFingerScan_ID.Text == "")
                {         
                    //----userinfo in 223       
                    //getmax badgenumber or fingerscan_ID
                    int xBadgenumber = 0;
                    int xUserID = 0;
                    List<userinfo> e1 = _hrisTimeSTECBusinessLayer.GetAllUserInfos().OrderBy(b => b.badgenumber).ToList();
                    foreach (userinfo e2 in e1)
                    {
                        xBadgenumber = Convert.ToInt32( e2.badgenumber);
                        xUserID = e2.userid;
                    }
                    if(xBadgenumber > 0)
                    {
                        xBadgenumber = xBadgenumber + 1;
                        xUserID = xUserID + 1;
                    }
                    txtFingerScan_ID.Text = Convert.ToString(xBadgenumber);

                    userinfo[] userinfoObj = new userinfo[1];
                    userinfo queryUserInfo = new userinfo();
                    queryUserInfo.change_time = DateTime.Now;
                    queryUserInfo.create_time = DateTime.Now;                
                    queryUserInfo.status = 0;
                    queryUserInfo.userid = xUserID;
                    queryUserInfo.badgenumber = txtFingerScan_ID.Text.PadLeft(13, '0');
                    queryUserInfo.defaultdeptid = xDefaultDeptID;
                    queryUserInfo.name = txtCFirstNameTH.Text;
                    queryUserInfo.Card = "";
                    queryUserInfo.SEP = 1;
                    queryUserInfo.OffDuty = 0;
                    queryUserInfo.DelTag = 0;
                    queryUserInfo.AutoSchPlan = 1;
                    queryUserInfo.MinAutoSchInterval = 24;
                    queryUserInfo.RegisterOT = 1;
                    queryUserInfo.set_valid_time = false;
                    queryUserInfo.isatt = true;
                    queryUserInfo.AccGroup = 1;
                    queryUserInfo.TimeZones = "0001000100000000";
                    queryUserInfo.photo = "";
                    queryUserInfo.ATT = false;
                    queryUserInfo.OverTime = false;
                    queryUserInfo.Holiday = false;
                    queryUserInfo.INLATE = 0;
                    queryUserInfo.OutEarly = 0;
                    queryUserInfo.Hiredday = DateTime.Now;
                    queryUserInfo.Lunchduration = 1;
                    queryUserInfo.Privilege = 0;
                    userinfoObj[0] = queryUserInfo;
                    _hrisTimeSTECBusinessLayer.AddUserInfo(userinfoObj);

                    //get max id from UserInfo_attarea
                    int XqueryUserInfo_attarea = 0;
                    List<userinfo_attarea> Le1 = _hrisTimeSTECBusinessLayer.GetAllUserInfo_attarea().OrderBy(b => b.id).ToList();
                    foreach (userinfo_attarea e2 in Le1)
                    {
                        XqueryUserInfo_attarea = e2.id;
                    }
                    if (XqueryUserInfo_attarea > 0)
                    {
                        XqueryUserInfo_attarea = XqueryUserInfo_attarea + 1;
                    }

                    //get current userid from userinfo
                    userinfo queryBadgenumber = new userinfo();
                    queryBadgenumber = _hrisTimeSTECBusinessLayer.GetUserInfoByBadGenNumber(xBadgenumber);

                    userinfo_attarea[] userInfo_attareaObj = new userinfo_attarea[1];
                    userinfo_attarea queryUserInfo_attarea = new userinfo_attarea();
                    queryUserInfo_attarea.id = XqueryUserInfo_attarea;
                    queryUserInfo_attarea.employee_id = queryBadgenumber.userid;
                    queryUserInfo_attarea.area_id = 1; //default = 1 คือ สำนักงานใหญ่
                    userInfo_attareaObj[0] = queryUserInfo_attarea;
                    _hrisTimeSTECBusinessLayer.AddUserInfo_att(userInfo_attareaObj);

                }

                //---employee in HRIS
                Employee[] employeeObj = new Employee[1];
                Employee query1 = new Employee();  // created 1 instance or object
                string XmaxEmployee_Id = "";
                string xCurrentYear = DateTime.Now.ToString("dd/MM/yy").Substring(6, 2);  //2หลักสุดท้ายของปี ค.ศ.
                if (employeeList.Where(c => c.Employee_ID.Substring(0, 2) == xCurrentYear).Count() > 0)
                {
                    XmaxEmployee_Id = Convert.ToString(Convert.ToDouble(employeeList.Where(c => c.Employee_ID.Substring(0, 2) == xCurrentYear).Max(d => d.Employee_ID)) + 1);
                }
                else
                {
                    XmaxEmployee_Id = xCurrentYear + string.Format("{0:0000}", 0000);
                }
                txtEmployee_ID.Text = XmaxEmployee_Id;
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.Person_ID = xPersonID;
                query1.FingerScanID = txtFingerScan_ID.Text;
                query1.CreatedDate = DateTime.Now;
                query1.StartDate = Convert.ToDateTime(StartDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.BaseSalary = Convert.ToDecimal(txtBaseSalary.Text);
                query1.ModifiedDate = DateTime.Now;
                query1.Noted = txtNoted.Text;
                query1.Email = txtEmail.Text;
                employeeObj[0] = query1;
                _hrisBusinessLayer.AddEmployee(employeeObj);


                //----employee in STEC_PAYROLL เฉพาะ xemployeeTyep = 2  รายวันประจำ, 3 รายวัน
                //----23/10/2017  แก้ไข พนักงานประจำ type = 1 ก็ให้บันทึกใน stec_payroll ด้วย
                if (xEmployeeType =="1" || xEmployeeType == "2" || xEmployeeType == "3")
                {
                    string _pAddress;
                    string _pMoo;
                    string _pTambon;
                    string _pAmphur;
                    string _pProvince;
                    string _pZipcode;
                    if (xPersonInformation.AddressByIDCARDHomeNumber == null)
                    {
                        _pAddress = "250";
                        _pMoo = "3";
                        _pTambon = "ยางเนิ้ง";
                        _pAmphur = "สารภี";
                        _pProvince = "เชียงใหม่";
                        _pZipcode = "50140";
                    }
                    else
                    {
                        _pAddress = xPersonInformation.AddressByIDCARDHomeNumber;
                        _pMoo = xPersonInformation.AddressByIDCARDMoo.ToString();
                        _pTambon = xPersonInformation.AddressByIDCARDSubDistrict;
                        _pAmphur = xPersonInformation.AddressByIDCARDDistrict;
                        _pProvince = xPersonInformation.AddressByIDCARDProvince;
                        _pZipcode = xPersonInformation.AddressByIDCARDPostcode.ToString();
                    }

                    EmployeeSTECPayroll[] employeeSTECObj = new EmployeeSTECPayroll[1];
                    EmployeeSTECPayroll queryX = new EmployeeSTECPayroll();
                    queryX.Employee_id = txtNoted.Text;
                    queryX.Dept_code = txtNoted.Text.Substring(2, 3);
                    queryX.Tname = txtTitleNameTH.Text;
                    queryX.Fname = txtFirstNameTH.Text;
                    queryX.Lname = txtLastNameTH.Text;
                    queryX.Fname_Eng = txtFirstNameEN.Text;
                    queryX.Lname_Eng = txtLastNameEN.Text;
                    queryX.Birth = xPersonInformation.DateOfBirth;
                    queryX.Id_card = xPersonInformation.IDCard;
                    queryX.Start_date = Convert.ToDateTime(StartDate.Text);
                    queryX.Bank_acc = txtCBackAccount.Text;
                    queryX.Staff_status = "1";
                    queryX.Staff_type = xEmployeeType;
                    queryX.Address = _pAddress;
                    queryX.Moo = _pMoo;
                    queryX.Tambon = _pTambon;
                    queryX.Amphur = _pAmphur;
                    queryX.Province = _pProvince;
                    queryX.Zipcode = _pZipcode;
                    queryX.Shift = 7;
                    employeeSTECObj[0] = queryX;
                    _hrisSTECPayrollBusinessLayer.AddEmployeeSTEcPayroll(employeeSTECObj);
                    
                }
                

                //-----transaction log 
                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T006";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = xPersonID;
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                //SendEmailForApprove(txtEmployee_ID.Text, "Add");
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว");

                ClearData(0);
                ChkRenewAccCode.IsEnabled = true;
                txtNoted.IsEnabled = false;
                AddButton.IsEnabled = false;
                EditButton.IsEnabled = true;
                DeleteButton.IsEnabled = false;                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private Boolean ChkDateRange(DateTime _date,int _xtype,string _xfrom)//สามารถลงวันที่ย้อนหลังได้ไม่เกินวันที่ 26 ของเดือนที่แล้ว
        {
            Boolean xresult = false;
            DateTime date = DateTime.Now;

            var endPreviouseMonth = date;
            var lastCurrentMonth = date;
            var lastCurrentMonth2 = date;
            //ถ้าเป็นเดือนมกราคม ปีจะต้องลดไป1
            if (date.Month == 1)
            {
                endPreviouseMonth = new DateTime(date.Year-1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            else
            {
                 endPreviouseMonth = new DateTime(date.Year, date.Month - 1, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์ย้อนหลังได้
                 lastCurrentMonth = date;//วันที่ปัจจุบันคือวันที่สามารถคีย์ manual workdate ได้
                 lastCurrentMonth2 = new DateTime(date.Year, date.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            
            if (_xtype == 1)//1 คือ เช็ค endpreviouse
            {
                if (_date < Convert.ToDateTime(endPreviouseMonth))
                {
                    xresult = false;
                }
                else
                {
                    xresult = true;
                }
            }
            else if (_xtype == 2 && _xfrom =="ManualWorkdate") //2 คือ lastcurrentmonth
            {
                //if (_date > Convert.ToDateTime(lastCurrentMonth))
                if (_date >= Convert.ToDateTime(endPreviouseMonth))
                  {
                      xresult = true;
                  }
                  else
                  {
                      xresult = false;
                  }
            }
            else if (_xtype == 2 && _xfrom == "ToPayrollFlag") //2 คือ lastcurrentmonth
            {
                if (_date > Convert.ToDateTime(lastCurrentMonth2))
                {
                    xresult = false;
                }
                else
                {
                    xresult = true;
                }
            }
            
            return xresult;
        }

        private void AddManualWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if(DateFromSearch.Text == "" || DateToSearch.Text =="" ) {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                

                if (cmbManualWorkDateType_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเภท(Type)", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ManualWorkDateDate.Text == "" || ManualWorkDateDateTo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                if (ManualWorkDateDate.Text != "")
                {
                    if (Convert.ToDateTime(ManualWorkDateDate.Text) < Convert.ToDateTime(DateFromSearch.Text))
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (Convert.ToDateTime(ManualWorkDateDateTo.Text) > Convert.ToDateTime(DateToSearch.Text))
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    //if (ChkDateRange(Convert.ToDateTime(ManualWorkDateDate.Text), 1, "ManualWorkdate") == false)
                    //  {
                    //      MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //      return;
                    //  }

                }
                if(Convert.ToDateTime(ManualWorkDateDate.Text) > Convert.ToDateTime(ManualWorkDateDateTo.Text)){
                    MessageBox.Show("กรุณาระบุช่วงวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //if (ManualWorkDateDateTo.Text != "")
                //{
                //    if (ChkDateRange(Convert.ToDateTime(ManualWorkDateDateTo.Text), 2, "ManualWorkdate") == false)
                //    {
                //        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์ Date to คือวันทำงานปัจจุบันเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        return;
                //    }
                //}
                if (txtManualWorkDateTime.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเวลา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbManualWorkDateStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุStatus ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                

                ManualWorkDate[] manualWorkDateObj = new ManualWorkDate[1];
                ManualWorkDate query1 = new ManualWorkDate();  // created 1 instance or object

                DateTime XDate = new DateTime();
                XDate = Convert.ToDateTime(ManualWorkDateDate.Text);



                //เช็คจำนวนวันที่ต้องจอง Array
                Decimal j;
                TimeSpan ts = (Convert.ToDateTime(ManualWorkDateDateTo.Text) - Convert.ToDateTime(ManualWorkDateDate.Text));
                j = Convert.ToDecimal(ts.TotalDays + 1);
                for (int Xnum = 0; Xnum < j; Xnum++)
                {
                    query1.ManualWorkDateDate = XDate.AddDays(Xnum);
                    query1.ManualWorkDateType_ID = (string)cmbManualWorkDateType_ID.SelectedValue;
                    query1.Employee_ID = txtCEmployee_ID.Text;
                    query1.Times = TimeSpan.Parse(txtManualWorkDateTime.Text);
                    query1.Status = (Boolean)cmbManualWorkDateStatus.SelectedValue;
                    query1.ModifiedDate = DateTime.Now;

                    manualWorkDateObj[0] = query1;
                    _hrisBusinessLayer.AddManualWorkDate(manualWorkDateObj);

                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                    transactionLog1.TransactionType_ID = "A";
                    transactionLog1.Table_ID = "T007";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "Employee_ID";
                    transactionLog1.PKFields = txtCEmployee_ID.Text;
                    transactionLog1.FKFields = (String)cmbManualWorkDateType_ID.SelectedValue;
                    transactionLog1.OldData = "";
                    transactionLog1.NewData = Convert.ToString( XDate.AddDays(Xnum));
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                }
              
                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditManualWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (DateFromSearch.Text == "" || DateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุLot", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbManualWorkDateType_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเภท(Type)", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ManualWorkDateDate.Text == "" || ManualWorkDateDateTo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ManualWorkDateDate.Text != "")
                {
                    //if (ChkDateRange(Convert.ToDateTime(ManualWorkDateDate.Text), 2, "ManualWorkdate") == false)
                    //{
                    //    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //    return;
                    //}

                    //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
                    lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                    if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                    {
                        MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (lot_numberList.Lock_Hr_Labor == "2")
                    {
                        MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (Convert.ToDateTime(ManualWorkDateDate.Text) < Convert.ToDateTime(DateFromSearch.Text))
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    if (Convert.ToDateTime(ManualWorkDateDateTo.Text) > Convert.ToDateTime(DateToSearch.Text))
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากเกินจาก lot ที่เลือกไว้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                if (Convert.ToDateTime(ManualWorkDateDate.Text) > Convert.ToDateTime(ManualWorkDateDateTo.Text))
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ManualWorkDateDateTo.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ManualWorkDateDateTo.Text), 2, "ManualWorkdate") == false)
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์ Date to คือวันทำงานปัจจุบันเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                if (txtManualWorkDateTime.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเวลา", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (cmbManualWorkDateStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุStatus ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ManualWorkDate[] manualWorkDateObj = new ManualWorkDate[1];
                ManualWorkDate query1 = new ManualWorkDate();  // created 1 instance or object
                query1.ManualWorkDateDate = Convert.ToDateTime(ManualWorkDateDate.Text);                
                query1.ManualWorkDateType_ID = (string)cmbManualWorkDateType_ID.SelectedValue;
                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.Times = TimeSpan.Parse(txtManualWorkDateTime.Text);
                query1.Status = (Boolean)cmbManualWorkDateStatus.SelectedValue;
                query1.ModifiedDate = DateTime.Now;
                manualWorkDateObj[0] = query1;
                _hrisBusinessLayer.UpdateManualWorkDate(manualWorkDateObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T007";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = (String)cmbManualWorkDateType_ID.SelectedValue;
                transactionLog1.OldData = "";
                transactionLog1.NewData = Convert.ToString(ManualWorkDateDate.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteManualWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ManualWorkDateDate.Text == "" || ManualWorkDateDateTo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ManualWorkDateDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ManualWorkDateDate.Text), 2, "ManualWorkdate") == false)
                    {
                        MessageBox.Show("ไม่สามารถลบวันที่ดังกล่าวเนื่องจากระบบกำหนดให้สามารถลบวันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                

                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                ManualWorkDate query1 = new ManualWorkDate();  // created 1 instance or object
                query1 = (ManualWorkDate)DataGridManualWorkDate.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveManualWorkDate(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T007";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = (String)cmbManualWorkDateType_ID.SelectedValue;
                transactionLog1.OldData = Convert.ToString(ManualWorkDateDate.Text);
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData(3);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearManualWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData(3);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridManualWorkDate_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {

                ManualWorkDate queryX = new ManualWorkDate();  // created 1 instance or object
                queryX = (ManualWorkDate)DataGridManualWorkDate.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (queryX == null)
                {
                    AddManualWorkdateButton.IsEnabled = true;
                    cmbManualWorkDateType_ID.IsEnabled = true;
                    ManualWorkDateDate.IsEnabled = true;
                    ManualWorkDateDateTo.IsEnabled = true;

                }
                else
                {
                    AddManualWorkdateButton.IsEnabled = false;
                    cmbManualWorkDateType_ID.IsEnabled = false;
                    ManualWorkDateDate.IsEnabled = false;
                    ManualWorkDateDateTo.IsEnabled = false;

                    ManualWorkDateDate.Text = queryX.ManualWorkDateDate.ToString();
                    ManualWorkDateDateTo.Text = queryX.ManualWorkDateDate.ToString();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkEndDate_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (ChkEndDate.IsChecked == true)
                {
                    EndDate.IsEnabled = true;

                }
                else if (ChkEndDate.IsChecked == false)
                {
                    EndDate.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkRenewAccCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChkRenewAccCode.IsChecked == true)
                {
                    txtNoted.IsEnabled = true;
                }
                else if (ChkRenewAccCode.IsChecked == false)
                {
                    txtNoted.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void WorkingDateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new Report.RepRPTHR015(txtCEmployee_ID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridContractDate_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void AddContractButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void EditContractButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DeleteContractButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ClearContractButton_Click(object sender, RoutedEventArgs e)
        {

        }

        

        private void AddToPayrollButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtCEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบการเลือกพนักงานก่อนบันทึกข้อมูลในหมวดนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ToPayrollValidFromDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ToPayrollValidFromDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ToPayrollValidFromDate.Text), 1,"ToPayrollFlag") == false)
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                if (ToPayrollEndDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ToPayrollEndDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ToPayrollEndDate.Text), 2, "ToPayrollFlag") == false)
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์ได้จนถึงวันที่ 25 ของเดือนนี้เท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                if (RdtToPayroll1.IsChecked == false )
                {
                    MessageBox.Show("กรุณาเลือกสถานะ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                

                bool xtoPayrollFlag;
                if (RdtToPayroll1.IsChecked == true)
                {
                    xtoPayrollFlag = true;
                }else{
                    xtoPayrollFlag = false;
                }
                DateTime xDate;
                xDate = Convert.ToDateTime(ToPayrollValidFromDate.Text);

                //เช็คจำนวนวันที่ต้องจอง Array
                int j;
                TimeSpan ts = (Convert.ToDateTime(ToPayrollEndDate.Text) - Convert.ToDateTime(ToPayrollValidFromDate.Text));
                j = Convert.ToInt16(ts.TotalDays + 1);
                PayrollFlag[] payrollFlagObj = new PayrollFlag[j];
                for (int Xnum = 0; Xnum < j; Xnum++)
                {
                    
                    PayrollFlag query1 = new PayrollFlag();  // created 1 instance or object
                    query1.Employee_ID = txtCEmployee_ID.Text;
                    query1.ValidFrom = xDate.AddDays(Xnum);
                    query1.ToPayrollFlag = xtoPayrollFlag;
                    query1.ModifiledDate = DateTime.Now;
                    payrollFlagObj[Xnum] = query1;
                    _hrisBusinessLayer.AddPayrollFlag(payrollFlagObj[Xnum]);

                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                    transactionLog1.TransactionType_ID = "A";
                    transactionLog1.Table_ID = "T019";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "Employee_ID";
                    transactionLog1.PKFields = txtCEmployee_ID.Text;
                    transactionLog1.FKFields = ToPayrollValidFromDate.Text;
                    transactionLog1.OldData = "";
                    transactionLog1.NewData = Convert.ToString(EndDate.Text) + " " + Convert.ToString(xtoPayrollFlag);
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                }                              
                
                ClearData(5);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditToPayrollButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ToPayrollValidFromDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ToPayrollValidFromDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ToPayrollValidFromDate.Text), 1, "ToPayrollFlag") == false)
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์วันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                if (ToPayrollEndDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ToPayrollEndDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ToPayrollEndDate.Text), 2, "ToPayrollFlag") == false)
                    {
                        MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบกำหนดให้คีย์ได้จนถึงวันที่ 25 ของเดือนนี้เท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                bool xtoPayrollFlag;
                if (RdtToPayroll1.IsChecked == true)
                {
                    xtoPayrollFlag = true;
                }
                else
                {
                    xtoPayrollFlag = false;
                }

                PayrollFlag query1 = new PayrollFlag();  // created 1 instance or object
                query1 = (PayrollFlag)DataGridToPayroll.SelectedItem;
                query1.Employee_ID = txtCEmployee_ID.Text;
                query1.ValidFrom = Convert.ToDateTime(ToPayrollValidFromDate.Text);                
                query1.ToPayrollFlag = xtoPayrollFlag;
                query1.ModifiledDate = DateTime.Now;
                _hrisBusinessLayer.UpdatePayrollFlag(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T019";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = ToPayrollValidFromDate.Text;
                transactionLog1.OldData = Convert.ToString(query1.ToPayrollFlag);
                transactionLog1.NewData = ToPayrollEndDate.Text + " " + Convert.ToString(xtoPayrollFlag);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData(5);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteToPayrollButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ToPayrollValidFromDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ToPayrollValidFromDate.Text != "")
                {
                    if (ChkDateRange(Convert.ToDateTime(ToPayrollValidFromDate.Text), 1, "ToPayrollFlag") == false)
                    {
                        MessageBox.Show("ไม่สามารถลบวันที่ดังกล่าวเนื่องจากระบบกำหนดให้ลบวันที่ได้ตั้งแต่วันที่ 26 ของเดือนที่แล้วเท่านั้น!!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                if (ToPayrollEndDate.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ที่ต้องการกำหนด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                

                PayrollFlag query1 = new PayrollFlag();  // created 1 instance or object
                query1 = (PayrollFlag)DataGridToPayroll.SelectedItem;
                _hrisBusinessLayer.RemovePayrollFlag(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = XMaxTransactionLog_ID;
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T019";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtCEmployee_ID.Text;
                transactionLog1.FKFields = ToPayrollValidFromDate.Text;
                transactionLog1.OldData = Convert.ToString(query1.ToPayrollFlag);
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData(5);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridToPayroll_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PayrollFlag queryX = new PayrollFlag();
                queryX = (PayrollFlag)DataGridToPayroll.SelectedItem;
                if (queryX != null)
                {
                    if (queryX.ToPayrollFlag == true)
                    {
                        RdtToPayroll1.IsChecked = true;
                    }
                    
                }

                if (queryX == null)
                {
                    AddToPayrollButton.IsEnabled = true;

                    //AddManualWorkdateButton.IsEnabled = true;
                    //cmbManualWorkDateType_ID.IsEnabled = true;
                    //ManualWorkDateDate.IsEnabled = true;
                    //ManualWorkDateDateTo.IsEnabled = true;

                }
                else
                {
                    AddToPayrollButton.IsEnabled = false;

                    //AddManualWorkdateButton.IsEnabled = false;
                    //cmbManualWorkDateType_ID.IsEnabled = false;
                    //ManualWorkDateDate.IsEnabled = false;
                    //ManualWorkDateDateTo.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearToPayrollButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                AddToPayrollButton.IsEnabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddTrainingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                this.NavigationService.Navigate(new ReferencesTable.RefTraining(txtCEmployee_ID.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
               
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;

                DateFromSearch.IsEnabled = false;
                DateToSearch.IsEnabled = false;
                DateFromSearch.Text = lot_numberList.Start_date.ToString();
                DateToSearch.Text = lot_numberList.Finish_date.ToString();

                DataGridManualWorkDate.ItemsSource = null;
                DataGridManualWorkDate.ItemsSource = _hrisBusinessLayer.GetAllManualWorkDates().Where(b => b.Employee_ID == txtCEmployee_ID.Text && b.ManualWorkDateDate >= Convert.ToDateTime(DateFromSearch.Text) && b.ManualWorkDateDate <= Convert.ToDateTime(DateToSearch.Text)).OrderByDescending(b => b.ManualWorkDateDate);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       
    }
}
