﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for HumanResource_LeaveType.xaml
    /// </summary>
    public partial class HumanResource_LeaveType : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public HumanResource_LeaveType()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //ValidFromDatePicker.Text = Convert.ToString( DateTime.MinValue);
                //EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllLeaveTypes();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDatePicker.Text == "")
                    EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                LeaveType[] leaveTypeObj = new LeaveType[1];
                LeaveType leaveType1 = new LeaveType();
                leaveType1.LeaveType_ID = TxtLeaveType_ID.Text;
                leaveType1.LeaveTypeNameTH = TxtLeaveTypeNameTH.Text;
                leaveType1.LeaveTypeNameEN = TxtLeaveTypeNameEN.Text;
                leaveType1.CreatedDate = DateTime.Now;
                leaveType1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                leaveType1.EndDate = Convert.ToDateTime(EndDatePicker.Text);
                leaveType1.ModifiedDate = DateTime.Now;
                leaveTypeObj[0] = leaveType1;
                _hrisBusinessLayer.AddLeaveType(leaveTypeObj);

                ClearData();
                TxtLeaveType_ID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDatePicker.Text == "")
                    EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                               
                LeaveType leaveType1 = new LeaveType();
                LeaveType query2 = new LeaveType();

                query2 = (LeaveType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้


                leaveType1.LeaveType_ID = TxtLeaveType_ID.Text;
                leaveType1.LeaveTypeNameTH = TxtLeaveTypeNameTH.Text;
                leaveType1.LeaveTypeNameEN = TxtLeaveTypeNameEN.Text;
                leaveType1.CreatedDate = query2.CreatedDate;
                leaveType1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                leaveType1.EndDate = Convert.ToDateTime(EndDatePicker.Text);
                leaveType1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateLeaveType(leaveType1);

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

               
                LeaveType leaveType = new LeaveType();
                leaveType = (LeaveType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้


                _hrisBusinessLayer.RemoveLeaveType(leaveType);
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                TxtLeaveType_ID.IsReadOnly = false;
                TxtLeaveType_ID.Background = new SolidColorBrush(Colors.White);
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllLeaveTypes();
                TxtLeaveType_ID.Text = "";
                TxtLeaveTypeNameTH.Text = "";
                TxtLeaveTypeNameEN.Text = "";
                ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                LeaveType leaveType = new LeaveType();
                leaveType = (LeaveType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                if (leaveType == null)
                {
                    AddButton.IsEnabled = true;
                    TxtLeaveType_ID.IsReadOnly = false;
                    TxtLeaveType_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    TxtLeaveType_ID.IsReadOnly = true;
                    TxtLeaveType_ID.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
