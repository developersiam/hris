//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer_HRISSystem.Payroll
{
    using System;
    using System.Collections.Generic;
    
    public partial class Payroll
    {
        public string Payroll_date { get; set; }
        public Nullable<System.DateTime> Salary_paid_date { get; set; }
        public Nullable<System.DateTime> Ot_paid_date { get; set; }
        public string Employee_id { get; set; }
        public string Tname { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Id_card { get; set; }
        public string Bank_acc { get; set; }
        public string Dept_code { get; set; }
        public string Staff_type { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public Nullable<decimal> Wage { get; set; }
        public Nullable<double> Working_date { get; set; }
        public Nullable<decimal> Ot_hour_rate { get; set; }
        public Nullable<decimal> Ot_holiday_rate { get; set; }
        public Nullable<decimal> Ot_hour_holiday_rate { get; set; }
        public Nullable<decimal> Ot_special_rate { get; set; }
        public Nullable<double> Cot_hour { get; set; }
        public Nullable<double> Cot_holiday { get; set; }
        public Nullable<double> Cot_hour_holiday { get; set; }
        public Nullable<decimal> Special { get; set; }
        public Nullable<decimal> Total_Income { get; set; }
        public Nullable<decimal> Total_Income_Labour { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> Tax_Labour { get; set; }
        public Nullable<decimal> SSO { get; set; }
        public Nullable<decimal> Cut_wage { get; set; }
        public Nullable<decimal> Net_Income { get; set; }
        public string Username { get; set; }
        public Nullable<System.DateTime> Ddate { get; set; }
        public Nullable<decimal> NetSalary { get; set; }
    }
}
