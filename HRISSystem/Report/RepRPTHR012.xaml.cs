﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR012.xaml
    /// </summary>
    public partial class RepRPTHR012 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass>();


        public RepRPTHR012()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DateFrom.Text == "" || DateTo.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (Convert.ToDateTime(DateFrom.Text) > Convert.ToDateTime(DateTo.Text))
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (RdtByDept.IsChecked == true && CmbDept.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกแผนก/ฝ่าย", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (RdtAllDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();
                    HRISReportDataset.sp_GetTimeAttendanceDataTable sp_GetTimeAttendanceDataTable = new HRISReportDataset.sp_GetTimeAttendanceDataTable();
                    HRISReportDatasetTableAdapters.sp_GetTimeAttendanceTableAdapter sp_GetTimeAttendanceTableAdapter = new HRISReportDatasetTableAdapters.sp_GetTimeAttendanceTableAdapter();
                    sp_GetTimeAttendanceTableAdapter.Fill(sp_GetTimeAttendanceDataTable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateTo.Text));
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource1.Name = "GetTimeAttendanceDataSet";

                    if (RdtStaffTypeAll.IsChecked == true)
                    {
                        reportDataSource1.Value = sp_GetTimeAttendanceDataTable;
                    }
                    else if (RdtStaffType1.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceDataTable.Select("Staff_type = 1");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;
                    }
                    else if (RdtStaffType2.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceDataTable.Select("Staff_type = 2");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;
                    }
                    else if (RdtStaffType3.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceDataTable.Select("Staff_type = 3");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;
                    }
                    else if (RdtStaffType4.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceDataTable.Select("Staff_type is null1");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;
                    }
                    
                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR012.rdlc";                   
                    _ReportViewer1.RefreshReport();
                }
                else if (RdtByDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();

                    HRISReportDataset.sp_GetTimeAttendanceByDeptDataTable sp_GetTimeAttendanceByDeptDataTable = new HRISReportDataset.sp_GetTimeAttendanceByDeptDataTable();
                    HRISReportDatasetTableAdapters.sp_GetTimeAttendanceByDeptTableAdapter sp_GetTimeAttendanceByDeptTableAdapter = new HRISReportDatasetTableAdapters.sp_GetTimeAttendanceByDeptTableAdapter();
                    sp_GetTimeAttendanceByDeptTableAdapter.Fill(sp_GetTimeAttendanceByDeptDataTable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateTo.Text), CmbDept.SelectedValue.ToString());

                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource2.Name = "GetTimeAttendanceByDeptDataSet";
                    if (RdtStaffTypeAll.IsChecked == true)
                    {
                        reportDataSource2.Value = sp_GetTimeAttendanceByDeptDataTable;
                    }
                    else if (RdtStaffType1.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceByDeptDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceByDeptDataTable.Select("Staff_type = 1 ");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }

                        reportDataSource2.Value = D2;
                    }
                    else if (RdtStaffType2.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceByDeptDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceByDeptDataTable.Select("Staff_type = 2 ");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }

                        reportDataSource2.Value = D2;
                    }
                    else if (RdtStaffType3.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceByDeptDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceByDeptDataTable.Select("Staff_type = 3 ");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }

                        reportDataSource2.Value = D2;
                    }
                    else if (RdtStaffType4.IsChecked == true)
                    {
                        DataTable D2 = sp_GetTimeAttendanceByDeptDataTable.Clone();
                        DataRow[] result = sp_GetTimeAttendanceByDeptDataTable.Select("Staff_type is null ");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }

                        reportDataSource2.Value = D2;
                    }
                   
                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR014.rdlc";
                    _ReportViewer1.RefreshReport();                  
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //private bool _isReportViewerLoaded;

        //private void ReportViewer_Load(object sender, EventArgs e)
        //{
           
                
    
        //}

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //get department into combobox
                departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
                CmbDept.ItemsSource = departmentFromSTECPayrollList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtByDept_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RdtByDept.IsChecked == true)
                {
                    CmbDept.IsEnabled = true;
                    CmbDept.Focus();
                }
                else if (RdtByDept.IsChecked == false)
                {
                    CmbDept.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
