//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel_HRISSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public Employee()
        {
            this.EmployeeContracts = new HashSet<EmployeeContract>();
            this.EmployeePositions = new HashSet<EmployeePosition>();
            this.EmployeeStatusProbations = new HashSet<EmployeeStatusProbation>();
            this.Leaves = new HashSet<Leave>();
            this.ManualWorkDates = new HashSet<ManualWorkDate>();
            this.Notices = new HashSet<Notice>();
            this.OTs = new HashSet<OT>();
            this.SSOes = new HashSet<SSO>();
            this.SSOHospitals = new HashSet<SSOHospital>();
            this.Trainings = new HashSet<Training>();
            this.TreatmentRooms = new HashSet<TreatmentRoom>();
            this.UniformReceivedAndReturns = new HashSet<UniformReceivedAndReturn>();
            this.WorkUpCountries = new HashSet<WorkUpCountry>();
            this.OTOnlineSetupAdmins = new HashSet<OTOnlineSetupAdmin>();
            this.OTOnlineSetupAdminDetails = new HashSet<OTOnlineSetupAdminDetail>();
            this.PayrollFlags = new HashSet<PayrollFlag>();
            this.ShiftControls = new HashSet<ShiftControl>();
        }
    
        public string Employee_ID { get; set; }
        public string Person_ID { get; set; }
        public string FingerScanID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<decimal> BaseSalary { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Noted { get; set; }
        public string Email { get; set; }
        public string DeptCode { get; set; }
        public Nullable<int> Staff_status { get; set; }
        public Nullable<int> Staff_type { get; set; }
        public string Position_ID { get; set; }
    
        public virtual ICollection<EmployeeContract> EmployeeContracts { get; set; }
        public virtual ICollection<EmployeePosition> EmployeePositions { get; set; }
        public virtual ICollection<EmployeeStatusProbation> EmployeeStatusProbations { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<Leave> Leaves { get; set; }
        public virtual ICollection<ManualWorkDate> ManualWorkDates { get; set; }
        public virtual ICollection<Notice> Notices { get; set; }
        public virtual ICollection<OT> OTs { get; set; }
        public virtual ICollection<SSO> SSOes { get; set; }
        public virtual ICollection<SSOHospital> SSOHospitals { get; set; }
        public virtual ICollection<Training> Trainings { get; set; }
        public virtual ICollection<TreatmentRoom> TreatmentRooms { get; set; }
        public virtual ICollection<UniformReceivedAndReturn> UniformReceivedAndReturns { get; set; }
        public virtual ICollection<WorkUpCountry> WorkUpCountries { get; set; }
        public virtual ICollection<OTOnlineSetupAdmin> OTOnlineSetupAdmins { get; set; }
        public virtual ICollection<OTOnlineSetupAdminDetail> OTOnlineSetupAdminDetails { get; set; }
        public virtual ICollection<PayrollFlag> PayrollFlags { get; set; }
        public virtual ICollection<ShiftControl> ShiftControls { get; set; }
    }
}
