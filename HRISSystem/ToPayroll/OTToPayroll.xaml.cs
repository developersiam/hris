﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ToPayroll
{
    /// <summary>
    /// Interaction logic for OTToPayroll.xaml
    /// </summary>
  
    public partial class OTToPayroll : Page
    {
         
        private List<BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass> otSummaryListClass = new List<BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass>();
        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();
          
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
   
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
 
        public OTToPayroll()
        {
            InitializeComponent();
            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        private void CallDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Convert.ToDateTime(DateToSearch.Text) < Convert.ToDateTime(DateFromSearch.Text))
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //GetDataFromSearch(Convert.ToDateTime(DateFromSearch.Text), Convert.ToDateTime(DateToSearch.Text));
                GetDataFromPayrollLot(Convert.ToString(cmbLot.Text));
                SendDataButton.Content = string.Format("Send Data ({0})", otSummaryListClass.Count());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SendDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (otSummaryListClass == null || otSummaryListClass.Count < 1 || !otSummaryListClass.Any())
                {
                    MessageBox.Show("กรุณาเรียกข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                //if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                //{
                //    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //if (lot_numberList.Lock_Hr_Labor == "2")
                //{
                //    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                var otDetail_by_Lot = _hrisSTECPayrollBusinessLayer.GetOT_DetailByLot_List(lot_numberList.Lot_month, lot_numberList.Lot_year);
                foreach (var item in otDetail_by_Lot)
                {   //ให้ลบข้อมูลใน OT_Detail ในแต่ละรายการ
                    OT_Detail ot_Detail = new OT_Detail();
                    ot_Detail = _hrisSTECPayrollBusinessLayer.GetOT_DetailByLot(lot_numberList.Lot_month, lot_numberList.Lot_year, item.Employee_id);
                    if (ot_Detail != null)
                    {
                        _hrisSTECPayrollBusinessLayer.RemoveOT_Detail(ot_Detail);
                    }
                }
                foreach (BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass item in otSummaryListClass)
                {   //add data to  database. ในแต่ละรายการ
                    _hrisSTECPayrollBusinessLayer.AddOT_Detail(new OT_Detail
                    {
                        Lot_Month = lot_numberList.Lot_month,
                        Lot_Year = lot_numberList.Lot_year,
                        Employee_id = item.AccCode,
                        Ot_hour = Convert.ToDouble(item.OTnormal),
                        Ot_holiday = Convert.ToDouble(item.Holiday),
                        Ot_hour_holiday = Convert.ToDouble(item.OTHoliday),
                        Username = _singleton.username,
                        Ddate = DateTime.Now,
                        Ot_Special = Convert.ToDouble(item.OTSpecial)
                    });
                }

                //foreach (BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass item in otSummaryListClass)
                //{
                //    //ให้ลบข้อมูลใน OT_Detail ในแต่ละรายการ
                //    OT_Detail ot_Detail = new OT_Detail();
                //    ot_Detail = _hrisSTECPayrollBusinessLayer.GetOT_DetailByLot(lot_numberList.Lot_month, lot_numberList.Lot_year, item.AccCode);
                //    if (ot_Detail != null)
                //    {
                //        _hrisSTECPayrollBusinessLayer.RemoveOT_Detail(ot_Detail);
                //    }



                //    //add data to  database. ในแต่ละรายการ
                //    _hrisSTECPayrollBusinessLayer.AddOT_Detail(new OT_Detail
                //    {
                //        Lot_Month = lot_numberList.Lot_month,
                //        Lot_Year = lot_numberList.Lot_year,
                //        Employee_id = item.AccCode,
                //        Ot_hour = Convert.ToDouble( item.OTnormal),
                //        Ot_holiday = Convert.ToDouble(item.Holiday),
                //        Ot_hour_holiday = Convert.ToDouble(item.OTHoliday),
                //        Username = _singleton.username,
                //        Ddate = DateTime.Now
                //    });                 
                //}
                MessageBox.Show("Import ข้อมูลการทำโอที  " + otSummaryListClass.Count() + " รายการ เข้าสู่ระบบ Payroll เรียบร้อยแล้ว", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
      
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DateFromSearch.Text =  FirstDayOfMonth(DateTime.Now).ToString();
                DateToSearch.Text = DateTime.Now.ToString();
                CallDataButton.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromPayrollLot(string lotno)
        {
            try
            {
                BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass ot2 = new BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass();
                otSummaryListClass = ot2.getTestOTByPayrollLot(Convert.ToString(cmbLot.Text));
                DataGrid.ItemsSource = otSummaryListClass;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //private void GetDataFromSearch(DateTime _fromDate,DateTime _toDate)
        //{
        //    try
        //    {          
        //        BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass ot2 = new BusinessLayer_HRISSystem.BusinessObject.OTSummaryListClass();
                
        //        otSummaryListClass = ot2.getTestOT(Convert.ToDateTime(DateFromSearch.Text), Convert.ToDateTime(DateToSearch.Text)).OrderBy(x => x.FirstNameTH).ToList();
        //        DataGrid.ItemsSource = otSummaryListClass;               
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = null;

                
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;

                DateFromSearch.IsEnabled = false;
                DateToSearch.IsEnabled = false;
                DateFromSearch.Text = lot_numberList.Start_date.ToString();
                DateToSearch.Text = lot_numberList.Finish_date.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ViewReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NavigationService.Navigate(new Report.RepRPTHR013( lot_numberList.Lot_month,lot_numberList.Lot_year));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    
    }
}
