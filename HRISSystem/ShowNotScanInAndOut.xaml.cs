﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;
using System.Threading;
using BusinessLayer_HRISSystem;

namespace HRISSystem
{
    /// <summary>
    /// Interaction logic for ShowNotScanInAndOut.xaml
    /// </summary>
    public partial class ShowNotScanInAndOut : Window
    {
        string Xusername;
        string XDeptCode;

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails> NotScanInAndOutDetailList = new List<BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails>();        
        BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails n;

        public ShowNotScanInAndOut()
        {
            InitializeComponent();
        }

        public ShowNotScanInAndOut(string _xusername, string _xdeptcode)
        {
            InitializeComponent();
            Xusername = _xusername;
            XDeptCode = _xdeptcode;
        }

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // Sets the CurrentCulture property to U.S. English.
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                FromDate.Text = FirstDayOfMonth(DateTime.Now).ToString();
                Todate.Text = Convert.ToString(DateTime.Now);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }       
       
        private List<BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails> GetNotScanInAndOutDetail(DateTime dateFrom, DateTime dateTo)
        {
            Report.HRISReportDataset.sp_GetEmployeeNOTScanInAndOutByDateRangeDataTable sp_GetEmployeeNotScanInAndOutBydateRangeDataTable = new Report.HRISReportDataset.sp_GetEmployeeNOTScanInAndOutByDateRangeDataTable();
            Report.HRISReportDatasetTableAdapters.sp_GetEmployeeNOTScanInAndOutByDateRangeTableAdapter sp_GetEmployeeNotScanInAndOutByDateRangeTableAdapter = new Report.HRISReportDatasetTableAdapters.sp_GetEmployeeNOTScanInAndOutByDateRangeTableAdapter();
            sp_GetEmployeeNotScanInAndOutByDateRangeTableAdapter.Fill(sp_GetEmployeeNotScanInAndOutBydateRangeDataTable, dateFrom, dateTo);
            List<BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails> NotScanInAndOutDetailsList = new List<BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails>();
            string xEmployee_id = "";
            string xACC_Id = "";
            string xTitleName = "";
            string xFirstname = "";
            string xLastname = "";
            string xFingerScanID = "";
            string xStaff_Type = "";
            var y = sp_GetEmployeeNotScanInAndOutBydateRangeDataTable;
            foreach (Report.HRISReportDataset.sp_GetEmployeeNOTScanInAndOutByDateRangeRow item in y)
            {
                Report.HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable = new Report.HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable();
                Report.HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter = new Report.HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter();
                sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter.Fill(sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable, dateFrom, dateTo, item.FINGERSCANCODE);
                //วนลูปเพื่อเช็คว่าไม่มาสแกนน้ิวติดต่อกันตั้งแต่ 5 วัน ใช่หรือไม่ ถ้าใช่ ให้เก็บลง object NotScanInAndOutDetailsList
                int xCount = 0;
                var x = sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable;
                foreach (Report.HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1Row item2 in x)
                {

                    if (item2.IsCHECKINNull())
                     {
                         xCount = xCount + 1;

                         xEmployee_id = item2.Employee_ID == null ? "" : item2.Employee_ID;
                         //xACC_Id = item2.ACC_ID == null ? "" : item2.ACC_ID;
                        //xFingerScanID = item2.FINGERSCANCODE == null ? "" : item2.FINGERSCANCODE.ToString();
                        xFingerScanID = item2.FINGERSCANCODE.ToString();
                        xTitleName = item2.TITLENAMETH == null ? "" : item2.TITLENAMETH;
                         xFirstname = item2.FIRSTNAMETH == null ? "" : item2.FIRSTNAMETH;
                         xLastname = item2.LASTNAMETH == null ? "" : item2.LASTNAMETH;
                         xStaff_Type = item2.IsStaff_typeNull() ? "" : item2.Staff_type == "1" ? "พนักงานประจำ" : item2.Staff_type == "2" ? "พนักงานรายวันประจำ" : "พนักงานรายวัน";
                     }
                     else
                     {
                         xCount = 0;
                     }                                   
                }

                //ถ้าไม่มีการสแกนนิ้วตั้งแต่ 5 วันติดต่อกันให้เพิ่มเข้าไปใน Object NotScanInAndOutDetailList
                if (xCount >= 5)
                {
                    NotScanInAndOutDetailsList.Add(new BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails
                    {
                        Employee_ID = xEmployee_id == null ? "" : xEmployee_id
                        ,
                        EMPACCCode = xACC_Id == null ? "" : xACC_Id
                        ,
                        FingerScanID = xFingerScanID == null ? "" : xFingerScanID
                        ,
                        EMPTitlename = xTitleName == null ? "" : xTitleName
                        ,
                        EMPFirstName = xFirstname == null ? "" : xFirstname
                        ,
                        EMPlastName = xLastname== null ? "" : xLastname
                        ,
                        StaffType = xStaff_Type == null ? "" : xStaff_Type
                        ,
                        NCount = xCount

                    });
                }
            }
            return NotScanInAndOutDetailsList;

        }
       
        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (n == null)
                {
                    MessageBox.Show("กรุณาเลือกแถวที่ต้องการจากตาราง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (n.Employee_ID != null)
                {
                    Report.RepRPTHR015 rep1 = new Report.RepRPTHR015(n.Employee_ID, Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(Todate.Text));
                    this.Content = rep1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FromDate.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่ทำงานต่างจังหวัด", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                else if (Convert.ToDateTime(FromDate.Text) > Convert.ToDateTime(Todate.Text))
                {
                    MessageBox.Show("กรุณาคีย์ช่วงวันที่ให้ถูกต้อง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (RdtStaff1.IsChecked == false && RdtStaff2.IsChecked == false && RdtStaff3.IsChecked == false && RdtStaff4.IsChecked == false)
                {
                    MessageBox.Show("กรุณาเลือกประเภทพนักงาน เช่น พนักงานประจำ รายวันประจำ รายวัน หรือทั้งหมด ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                DataGrid.ItemsSource = null;
                NotScanInAndOutDetailList = GetNotScanInAndOutDetail(Convert.ToDateTime(FromDate.Text) , Convert.ToDateTime(Todate.Text));
                TotalTextBox.Text = string.Format("ทั้งหมด {0} ข้อมูล", NotScanInAndOutDetailList.Count);

                if (RdtStaff1.IsChecked == true)
                {
                    NotScanInAndOutDetailList = NotScanInAndOutDetailList.Where(b => b.StaffType == "พนักงานประจำ").ToList();
                }
                else if (RdtStaff2.IsChecked == true)
                {
                    NotScanInAndOutDetailList = NotScanInAndOutDetailList.Where(b => b.StaffType == "พนักงานรายวันประจำ").ToList();
                }
                else if (RdtStaff3.IsChecked == true)
                {
                    NotScanInAndOutDetailList = NotScanInAndOutDetailList.Where(b => b.StaffType == "พนักงานรายวัน").ToList();
                }
                

                if (NotScanInAndOutDetailList.Count <= 0)
                {
                    MessageBox.Show("ไม่มีข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                DataGrid.ItemsSource = NotScanInAndOutDetailList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DataGrid.SelectedIndex <= -1)
                    return;

                n = (BusinessLayer_HRISSystem.BusinessObject.NotScanInAndOutDetails)DataGrid.SelectedItem;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow p1 = new MainWindow(Xusername, XDeptCode);
                Close(); // Close windows before show menu windows
                p1.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
