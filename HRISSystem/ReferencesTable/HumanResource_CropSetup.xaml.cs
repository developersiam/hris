﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for HumanResource_CropSetup.xaml
    /// </summary>
    public partial class HumanResource_CropSetup : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private int cropTxt;
        CropSetup cropSetup1 = new CropSetup();

        public HumanResource_CropSetup()
        {
            
            InitializeComponent();
            DataGridCropSetup.ItemsSource = _hrisBusinessLayer.GetAllCropSetups();
        }

     
        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                TxtCrop.Text = "";
                TxtCrop.IsReadOnly = false;
                TxtCrop.Background = new SolidColorBrush(Colors.White);
                //ValidFromDatePicker.Text = Convert.ToString( DateTime.Now);
                //EndDateDatePicker.Text = Convert.ToString(DateTime.Now);   
                DataGridCropSetup.ItemsSource = null; //clear 
                DataGridCropSetup.ItemsSource = _hrisBusinessLayer.GetAllCropSetups();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

            ShowGridHolidayAndToTal();
            
        }

        private void ShowGridHolidayAndToTal()
        {
            List<Holiday> holidayShow = new List<Holiday>();
            holidayShow = _hrisBusinessLayer.GetHolidayByCrop(cropTxt).ToList();

            DataGridHoliday.ItemsSource = null;
            DataGridHoliday.ItemsSource = holidayShow;

            
            //holiday = cropSetup1.Holidays.ToList();
            
            txtAnnualHoliday.Text = Convert.ToString(holidayShow.Where(b => b.HolidayFlag == true).Count());
            txtSpecialHoliday.Text = Convert.ToString(holidayShow.Where(b => b.HolidayFlag == false).Count());
        }

        private void DataGridCropSetup_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                cropSetup1 = (CropSetup)DataGridCropSetup.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                cropTxt = cropSetup1.Crop;
                ShowGridHolidayAndToTal();

                
                if (cropSetup1 == null)
                {
                    AddButton.IsEnabled = true;
                    TxtCrop.IsReadOnly = false;
                    TxtCrop.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    TxtCrop.IsReadOnly = true;
                    TxtCrop.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    
     
     
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CropSetup[] cropSetupObj = new CropSetup[1];
                CropSetup cropSetup1 = new CropSetup();  // created 1 instance or object
                                                         //CropSetup cropSetup2 = new CropSetup { Crop = Convert.ToInt16(TxtCrop.Text),CreatedDate = Convert.ToDateTime(DateTime.Now),ValidFrom= Convert.ToDateTime(ValidFromDatePicker.Text),EndDate = Convert.ToDateTime(EndDateDatePicker.Text),}
                cropSetup1.Crop = Convert.ToInt16(TxtCrop.Text);
                cropSetup1.CreatedDate = DateTime.Now;
                cropSetup1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                cropSetup1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                cropSetup1.ModifiedDate = DateTime.Now;

                cropSetupObj[0] = cropSetup1;
                _hrisBusinessLayer.AddCropSetup(cropSetupObj);
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                CropSetup cropSetup1 = new CropSetup();
                CropSetup query2 = new CropSetup();  // created 1 instance or object

                query2 = (CropSetup)DataGridCropSetup.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้


                cropSetup1.Crop = Convert.ToInt16(TxtCrop.Text);
                cropSetup1.CreatedDate = query2.CreatedDate;
                cropSetup1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                cropSetup1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                cropSetup1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateCropSetup(cropSetup1);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                CropSetup cropSetup1 = new CropSetup();
                cropSetup1 = (CropSetup)DataGridCropSetup.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                //cropSetup1.Crop = Convert.ToInt16(TxtCrop.Text);
                //cropSetup1.CreatedDate = DateTime.Now;
                //cropSetup1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                //cropSetup1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                //cropSetup1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.RemoveCropSetup(cropSetup1);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TxtCrop_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //key numberic only
            try
            {
                if (!char.IsDigit(e.Text, e.Text.Length - 1))
                    e.Handled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void HolidayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //CropSetup cropSetup1 = new CropSetup();
                //cropSetup1 = (CropSetup)DataGridCropSetup.SelectedItem;
                //this.NavigationService.Navigate(new HumanResource_HolidaySetup(cropSetup1));

                CropSetup cropSetup1 = new CropSetup();
                cropSetup1 = (CropSetup)DataGridCropSetup.SelectedItem;
                this.NavigationService.Navigate(new HumanResource.HolidaySetup(cropSetup1, "Add", DateTime.Now));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CropSetup cropSetup1 = new CropSetup();
                cropSetup1 = _hrisBusinessLayer.GetCropSetup(Convert.ToInt16( TxtCrop.Text));

                Holiday holidayEdit = new Holiday();
                holidayEdit = (Holiday)DataGridHoliday.SelectedItem;
                this.NavigationService.Navigate(new HumanResource.HolidaySetup(cropSetup1, "Edit",holidayEdit.Date ));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Holiday holidayEdit = new Holiday();
                holidayEdit = (Holiday)DataGridHoliday.SelectedItem;

                if (MessageBox.Show("Do you want to delete " + Convert.ToString(holidayEdit.Date.ToString("dd/MM/yyyy")) + " ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                

                _hrisBusinessLayer.RemoveHoliday(holidayEdit);

                ShowGridHolidayAndToTal();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
