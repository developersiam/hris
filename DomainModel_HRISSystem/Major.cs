//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel_HRISSystem
{
    using System;
    using System.Collections.Generic;
    
    public partial class Major
    {
        public Major()
        {
            this.Educations = new HashSet<Education>();
        }
    
        public string Major_ID { get; set; }
        public string EducationQualification_ID { get; set; }
        public string MajorTH { get; set; }
        public string MajorEN { get; set; }
        public string MajorInitialTH { get; set; }
        public string MajorInitialEN { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual ICollection<Education> Educations { get; set; }
        public virtual EducationQualification EducationQualification { get; set; }
    }
}
