﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for RefTrainingCourse.xaml
    /// </summary>
    public partial class RefTrainingCourse : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XFromPage;
        private string XEmployee_ID;
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        public RefTrainingCourse()
        {
            InitializeComponent();
        }
        public RefTrainingCourse(string _xfromPage,string _employee_ID)
        {
            InitializeComponent();
            XFromPage = _xfromPage;
            XEmployee_ID = _employee_ID;
        }

        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                txtID.Text = "";
                txtNameTH.Text = "";
                txtNameEN.Text = "";

                //ValidFromDatePicker.Text = Convert.ToString( DateTime.Now);
                //EndDateDatePicker.Text = Convert.ToString(DateTime.Now);   
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllTrainingCourses();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllTrainingCourses();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private static string SubRight(string value, int length)
        {
            return value.Substring(value.Length - length);
        }
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                string X_ID;
                List<TrainingCourse> Xvalue;
                Xvalue = _hrisBusinessLayer.GetAllTrainingCourses().ToList();
                if (Xvalue.Count > 0)
                {
                    X_ID = Convert.ToString(Convert.ToInt32(SubRight(Xvalue.Max(c => c.TrainingCourse_ID), 4)) + 1);
                    X_ID = "T" + string.Format("{0:0000}", Convert.ToInt32(X_ID));

                }
                else
                {
                    X_ID = "T0001";
                }
                txtID.Text = X_ID;

                TrainingCourse [] queryObj = new TrainingCourse[1];
                TrainingCourse query1 = new TrainingCourse();  // created 1 instance or object

                query1.TrainingCourse_ID = txtID.Text;
                query1.TrainingCourseNameTH = txtNameTH.Text;
                query1.TrainingCourseNameEN = txtNameEN.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;
                queryObj[0] = query1;
                _hrisBusinessLayer.AddTrainingCourse(queryObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T022";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "TrainingCourse_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllTrainingCourses();

                if (XEmployee_ID != "")
                {
                    this.NavigationService.Navigate(new ReferencesTable.RefTraining(query1, XEmployee_ID));
                }
                else
                {
                    this.NavigationService.Navigate(new ReferencesTable.RefTraining(query1));
                }

                //ClearData();

                //if (XFromPage != "")
                //    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                TrainingCourse query1 = new TrainingCourse();  // created 1 instance or object
                TrainingCourse query2 = new TrainingCourse();  // created 1 instance or object

                query2 = (TrainingCourse)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.TrainingCourse_ID = txtID.Text;
                query1.TrainingCourseNameTH = txtNameTH.Text;
                query1.TrainingCourseNameEN = txtNameEN.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdateTrainingCourse(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T022";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "TrainingCourse_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = query2.TrainingCourseNameTH;
                transactionLog1.NewData =txtNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                TrainingCourse query2 = new TrainingCourse();  // created 1 instance or object
                query2 = (TrainingCourse)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                _hrisBusinessLayer.RemoveTrainingCourse(query2);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T022";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "TrainingCourse_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = txtNameTH.Text;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TrainingCourse query1 = new TrainingCourse();  // created 1 instance or object
                query1 = (TrainingCourse)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    
                }
                else
                {
                    AddButton.IsEnabled = false;
                   
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtID.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก Training course ที่ต้องการจากตารางหรือกำหนดใหม่ก่อนทำการ Add employee กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                TrainingCourse query1 = new TrainingCourse();  // created 1 instance or object                   
                query1 = (TrainingCourse)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (XEmployee_ID != "")
                {
                    this.NavigationService.Navigate(new ReferencesTable.RefTraining(query1, XEmployee_ID));
                }
                else
                {
                    this.NavigationService.Navigate(new ReferencesTable.RefTraining(query1));
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
