﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;

namespace HRISSystem.HumanResource
{
    /// <summary>
    /// Interaction logic for HolidaySetup.xaml
    /// </summary>
    public partial class HolidaySetup : Page
    {
        
        private class HolidayFlage
        {
            public bool HolidayFlag { get; set; }
            public string HolidayFlagName { get; set; }
        }


        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        string XType2;
        CropSetup CropHoliday;
        DateTime XHoliday2;
        Holiday holidayEdit = new Holiday();

        //List<Holiday> holiday = new List<Holiday>();

        public HolidaySetup(CropSetup crop , string xType , DateTime xholiday)
        {
            InitializeComponent();
            XType2 = xType;
            CropHoliday = crop;
            XHoliday2 = xholiday;
            TxtCrop.Text = CropHoliday.Crop.ToString();

            GBHeader.Header = XType2 +" " + "holiday information";
            

            //Create view model class for holiday flag combo box.
            List<HolidayFlage> holidayFlagList = new List<HolidayFlage>();

            HolidayFlage h1 = new HolidayFlage { HolidayFlag = false, HolidayFlagName = "วันหยุดพิเศษ" };
            HolidayFlage h2 = new HolidayFlage { HolidayFlag = true, HolidayFlagName = "วันหยุดตามประกาศฯบริษัท" };

            holidayFlagList.Add(h1);
            holidayFlagList.Add(h2);

            cmbHolidayFlag.ItemsSource = holidayFlagList;
  

            if (xType == "Edit")
            {
                ShowHolidayDetail();
            }
        }

        private void ShowHolidayDetail()
        {

            holidayEdit = _hrisBusinessLayer.GetHoliday(XHoliday2);

            Date.Text = Convert.ToString(holidayEdit.Date);
            TxtHolidayNameTH.Text = holidayEdit.HolidayNameTH;
            TxtHolidayNameEN.Text = holidayEdit.HolidayNameEN;
            ValidFromDatePicker.Text = Convert.ToString(holidayEdit.ValidFrom);
            EndDateDatePicker.Text = Convert.ToString(holidayEdit.EndDate);
            TxtNote.Text = holidayEdit.Noted;
            if(holidayEdit.HolidayFlag == true)
            {
                cmbHolidayFlag.SelectedIndex = 1;
            }else if (holidayEdit.HolidayFlag == false)
            {
                cmbHolidayFlag.SelectedIndex = 0;
            }
        }

        public void ClearData()
        {
            try
            {
                TxtHolidayNameEN.Text = "";
                TxtHolidayNameTH.Text = "";
                TxtNote.Text = "";  
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (Date.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่");
                    return;
                }

                if (ValidFromDatePicker.Text == "")
                {
                    ValidFromDatePicker.Text = Convert.ToString(CropHoliday.ValidFrom);
                }

                if (EndDateDatePicker.Text == "")
                {
                    EndDateDatePicker.Text = Convert.ToString(CropHoliday.EndDate);
                }


                if (Convert.ToDateTime(Date.Text) < CropHoliday.ValidFrom)
                {
                    MessageBox.Show("วันที่จะต้องไม่น้อยกว่าวันที่เริ่ม Crop นี้ ");
                    return;
                }
                else if (Convert.ToDateTime(Date.Text) > CropHoliday.EndDate)
                {
                    MessageBox.Show("วันที่ จะต้องไม่เกินวันที่สิ้นสุดของ Crop นี้ ");
                    return;
                }

                if (IsWeekend(Convert.ToDateTime(Date.Text)))
                {
                    MessageBox.Show("ระบบไม่อนุญาตให้บันทึกวันอาทิตย์ เนื่องจากถูกกำหนดให้เป็นวันหยุดอยู่แล้ว");
                    return;
                }

                IList<Leave> chkLeave = _hrisBusinessLayer.GetLeaveByLeaveDate(Convert.ToDateTime(Date.Text));
                if (chkLeave.Count > 0 && (Boolean)cmbHolidayFlag.SelectedValue == true) 
                {
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้เนื่องจากมีการบันทึกการลาของพนักงานในวันดังกล่าวแล้ว  หากต้องการบันทึกข้อมูลนี้จะต้องทำลบข้อมูลการลาก่อน");
                    return;
                }

                if (Convert.ToDateTime(ValidFromDatePicker.Text) < CropHoliday.ValidFrom)
                {
                    MessageBox.Show("วันที่ Valid from จะต้องไม่น้อยกว่าวันที่เริ่ม Crop นี้ ");
                    return;
                }
                else if (Convert.ToDateTime(ValidFromDatePicker.Text) > CropHoliday.EndDate)
                {
                    MessageBox.Show("วันที่ Valid from จะต้องไม่เกินวันที่สิ้นสุดของ Crop นี้ ");
                    return;
                }


                if (Convert.ToDateTime(EndDateDatePicker.Text) < CropHoliday.ValidFrom)
                {
                    MessageBox.Show("วันที่ End date จะต้องไม่น้อยกว่าวันที่เริ่ม Crop นี้ ");
                    return;
                }
                else if (Convert.ToDateTime(EndDateDatePicker.Text) > CropHoliday.EndDate)
                {
                    MessageBox.Show("วันที่ End date จะต้องไม่เกินวันที่สิ้นสุดของ Crop นี้ ");
                    return;
                }

                if (cmbHolidayFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก Holiday flag");
                    return;
                }

                if(XType2 == "Add")
                {
                    Holiday[] holidayObj = new Holiday[1];
                    Holiday holidayAdd = new Holiday(); //created  1 instanct or object
                    holidayAdd.Crop = Convert.ToInt16(TxtCrop.Text);
                    holidayAdd.Date = Convert.ToDateTime(Date.Text);
                    holidayAdd.HolidayNameTH = TxtHolidayNameTH.Text;
                    holidayAdd.HolidayNameEN = TxtHolidayNameEN.Text;
                    holidayAdd.CreatedDate = DateTime.Now;
                    holidayAdd.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                    holidayAdd.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                    holidayAdd.ModifiedDate = DateTime.Now;
                    holidayAdd.HolidayFlag = (Boolean)cmbHolidayFlag.SelectedValue;
                    holidayAdd.Noted = TxtNote.Text;

                    holidayObj[0] = holidayAdd;
                    _hrisBusinessLayer.AddHoliday(holidayObj);
                }
                else if (XType2 == "Edit")
                {
                    if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }



                    holidayEdit.Crop = Convert.ToInt16(TxtCrop.Text);
                    holidayEdit.Date = Convert.ToDateTime(Date.Text).Date;
                    holidayEdit.HolidayNameTH = TxtHolidayNameTH.Text;
                    holidayEdit.HolidayNameEN = TxtHolidayNameEN.Text;
                    //holiday1.CreatedDate = XHoliday2.CreatedDate;
                    holidayEdit.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                    holidayEdit.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                    holidayEdit.ModifiedDate = DateTime.Now;
                    holidayEdit.HolidayFlag = (Boolean)cmbHolidayFlag.SelectedValue;
                    holidayEdit.Noted = TxtNote.Text;
                    _hrisBusinessLayer.UpdateHoliday(holidayEdit);
                }
                NavigationService.GoBack();

                //ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
