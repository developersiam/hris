﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Globalization;
using System.Threading;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.ComponentModel;


namespace HRISSystem.OTByAdmin
{
    /// <summary>
    /// Interaction logic for FrmApproveOTByManager.xaml
    /// </summary>
    public partial class FrmApproveOTByManager : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _timeSTECBuisnessLayer = new HRISTimeSTECBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();       
        private List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> _otOnlineInformationList = new List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation>();
        private BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation otselected;
        //private OTOnlineSetupAdmin _fromAdmin = new OTOnlineSetupAdmin();

        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private Double XCurrentOT;
        Boolean XFromOTInformation = false;
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private int xFingerScanID;
        private string XType;
        //private OTOnlinesObj otselected;
       
        private List<OTOnlineSetupAdminDetail> _fromManager = new List<OTOnlineSetupAdminDetail>();
        private Employee e2 = new Employee();

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        private class OTType
        {
            public string OTTypeCode { get; set; }
            public string OTTypeName { get; set; }
        }

        private class StatusFlags
        {
            public string RequestFlag { get; set; }
            public string RequestFlagName { get; set; }
        }

        private class ManagerApproveStatusFlags
        {
            public string ApproveFlag { get; set; }
            public string ApproveFlagName { get; set; }
        }

        public FrmApproveOTByManager()
        {
            InitializeComponent();
            XType = "Add";
        }

        public FrmApproveOTByManager(DateTime otDate, string otType, string employee_ID, Boolean xchkFrom, string xfrom,string _xtype) 
        {
            InitializeComponent();
            //List<StatusFlags> statusFlagList = new List<StatusFlags>();

            //StatusFlags h1 = new StatusFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            //StatusFlags h2 = new StatusFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };
            //statusFlagList.Add(h1);
            //statusFlagList.Add(h2);
            //cmbRequest.ItemsSource = statusFlagList;
            //cmbRequest.SelectedIndex = 1;

            GetDataFromEmployee_ID(employee_ID);
            getOTDetail(employee_ID, otDate, otType);
            XType = _xtype;

        }


        private List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> GetOTOnlineInformation(DateTime dateFrom, DateTime dateTo)
        {
            Report.HRISReportDataset.sp_GetOTOnlineInformationsDataTable sp_GetOTOnlineInformationDataTable = new Report.HRISReportDataset.sp_GetOTOnlineInformationsDataTable();
            Report.HRISReportDatasetTableAdapters.sp_GetOTOnlineInformationsTableAdapter sp_GetOTOnlineInformationsTableAdapter = new Report.HRISReportDatasetTableAdapters.sp_GetOTOnlineInformationsTableAdapter();
            sp_GetOTOnlineInformationsTableAdapter.Fill(sp_GetOTOnlineInformationDataTable, dateFrom, dateTo);
            List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation> OTOnlineInformationList = new List<BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation>();
            foreach (Report.HRISReportDataset.sp_GetOTOnlineInformationsRow item in sp_GetOTOnlineInformationDataTable)
            {
                OTOnlineInformationList.Add(new BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation
                {
                    Admin_ID = item.Admin_ID
                        //this.Blood_Name = person.Blood_ID != null ? person.Blood.BloodGroupTH : "";
                    ,

                    AdminEmployee_ID = item.AdminEmployeeID == "" ? "" : item.AdminEmployeeID
                    ,
                    AdminFirstName = item.AdminFirstName == null ? "" : item.AdminFirstName
                    ,
                    AdminLastName = item.AdminLastName == null ? "" : item.AdminLastName
                    ,
                    AdminACCCode = item.AdminACCCode == null ? "" : item.AdminACCCode
                    ,
                    Employee_ID = item.Employee_ID
                    ,
                    EMPACCCode = item.EMPACCcode
                    ,
                    FingerScanID = item.FingerScanID
                    ,
                    EMPTitlename = item.EMPTitlename
                    ,
                    EMPFirstName = item.EMPFistName
                    ,
                    EMPlastName = item.EMPLastName
                    ,
                    OTDATE = item.OTDate
                    ,
                    RequestType = item.RequestType == null ? "" : item.RequestType
                    ,
                    OTnormal = item.OTNormal
                    ,
                    Holiday = item.Holiday
                    ,
                    OTHoliday = item.OTHoliday
                    ,
                    DeptID = item.DeptID == null ? "" : item.DeptID
                    ,
                    managerEmployee_ID = item.ManagerID == null ? "" : item.ManagerID
                    ,
                    ManagerACCCode = item.ManagerACCCode == null ? "" : item.ManagerACCCode
                    ,
                    ManagerFirstName = item.ManagerFirstName == null ? "" : item.ManagerFirstName
                    ,
                    ManagerLastName = item.ManagerLastName == null ? "" : item.ManagerLastName
                    ,
                    ManagerApproveStatus = item.ManagerApproveStatus == null ? "" : item.ManagerApproveStatus
                    ,
                    HRApproveStatus = item.HRApproveStatus == null ? "" : item.HRApproveStatus
                    ,
                    RemarkFromAdmin = item.RemarkFromAdmin == null ? "" : item.RemarkFromAdmin
                    ,
                    RemarkFromManager = item.RemarkFromManager == null ? "" : item.RemarkFromManager
                    ,
                    RemarkFromHR = item.RemarkFromHR == null ? "" : item.RemarkFromHR
                    ,PayrollLot = item.PayrollLot == null ? "" : item.PayrollLot

                });
            }
            return OTOnlineInformationList;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();
                

                // Sets the CurrentCulture property to U.S. English.
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                LeaveDateFromSearch.Text = FirstDayOfMonth(DateTime.Now).ToString();
                LeaveDateToSearch.Text = DateTime.Now.ToString();


                DataGrid.ItemsSource = null; //clear 

                //Create view model class for holiday flag combo box.
                //List<StatusFlags> RequestFlagList = new List<StatusFlags>();

                //StatusFlags h1 = new StatusFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
                //StatusFlags h2 = new StatusFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };

                //RequestFlagList.Add(h1);
                //RequestFlagList.Add(h2);

                //cmbRequest.ItemsSource = RequestFlagList;
                //cmbRequest.SelectedIndex = 0;

                //ottype
                //List<OTType> otTypeList = new List<OTType>();
                //OTType T1 = new OTType { OTTypeCode = "N", OTTypeName = "OT-normal" };
                //OTType T2 = new OTType { OTTypeCode = "H", OTTypeName = "Holiday" };
                //OTType T3 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
                //otTypeList.Add(T1);
                //otTypeList.Add(T2);
                //otTypeList.Add(T3);
                //cmbOTType.ItemsSource = otTypeList;
                //cmbOTType.SelectedIndex = 0;


                //Create view model class for holiday flag combo box.
                List<ManagerApproveStatusFlags> ManagerApproveFlagList = new List<ManagerApproveStatusFlags>();

                ManagerApproveStatusFlags M1 = new ManagerApproveStatusFlags { ApproveFlag = "P", ApproveFlagName = "Pending" };
                ManagerApproveStatusFlags M2 = new ManagerApproveStatusFlags { ApproveFlag = "A", ApproveFlagName = "Approve" };
                ManagerApproveStatusFlags M3 = new ManagerApproveStatusFlags { ApproveFlag = "N", ApproveFlagName = "Not Approve" };

                ManagerApproveFlagList.Add(M1);
                ManagerApproveFlagList.Add(M2);
                ManagerApproveFlagList.Add(M3);

                cmbManagerApproveStatus.ItemsSource = ManagerApproveFlagList;
                cmbManagerApproveStatus.SelectedIndex = 1;

                if (XFromOTInformation == true)
                {
                    txtNcountOT.Text = XCurrentOT.ToString();
                    GetDataFromSearch();
                    AddButton.IsEnabled = false;
                }


                //ตรวจสอบว่า manager มี emailที่บันทึกไว้ในหน้า employee หรือยัง 

                e2 = _hrisBusinessLayer.GetEmployeeByEmail(_singleton.emailFrom);
                if (e2 == null)
                {
                    MessageBox.Show("กรุณาตรวจสอบอีเมลล์ผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    if (e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                //ตรวจสอบว่า manager นี้มีสิทธิ์ในการ approve ข้อมูลหรือไม่ โดยเช็คจาก otonlinesetupadmindetail
                _fromManager = _hrisBusinessLayer.GetOTOnlineSetupAdminDetailbyReportTo(_singleton.singleton_employee_ID).ToList();
                if (_fromManager == null)
                {
                    MessageBox.Show("คุณไม่มีสิทธิ์ในการอนุมัติการทำโอทีของพนักงาน หากต้องการเพิ่มสิทธิ์หรือต้องการตรวจสอบข้อมูลกรณาแจ้งฝ่ายบุคคล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DataGrid.SelectedIndex <= -1)
                    return;

                XFromOTInformation = false;
                XType = "Edit";
                //AddButton.IsEnabled = false;

                //TestOT query2 = new TestOT();  // created 1 instance or object
                otselected = (BusinessLayer_HRISSystem.BusinessObject.OTOnlineInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                if (otselected.OTnormal != 0)
                {
                    GetDataFromEmployee_ID(otselected.Employee_ID);
                    getOTDetail(otselected.Employee_ID.ToString(), Convert.ToDateTime(otselected.OTDATE), "N");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday == 0)
                {
                    GetDataFromEmployee_ID(otselected.Employee_ID);
                    getOTDetail(otselected.Employee_ID.ToString(), Convert.ToDateTime(otselected.OTDATE), "H");
                }
                else if (otselected.Holiday != 0 && otselected.OTHoliday != 0)
                {
                    this.NavigationService.Navigate(new Walefares.GetOTInformation(Convert.ToDateTime(otselected.OTDATE), Convert.ToDateTime(otselected.OTDATE), otselected.Employee_ID.ToString(), "ManagerApproveOTOnline",XType));
                }
                cmbManagerApproveStatus.SelectedIndex = 1;
                ShowCheckInOut();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                //txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
                txtStaffType.Text = ex.xStaffType;
                txtEmployeeAcc.Text = ex.xNoted;
                xFingerScanID = Convert.ToInt16(ex.xFingerScan_ID);

                txtStaffType.Text = "";
                EmployeeSTECPayroll employeeSTECPayroll = new EmployeeSTECPayroll();
                employeeSTECPayroll = _hrisSTECPayrollLayer.GetEmployeeSTECPayroll(ex.xNoted);
                if (employeeSTECPayroll != null)
                {
                    if (employeeSTECPayroll.Staff_type == "1")
                    {
                        this.txtStaffType.Text = "พนักงานประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "2")
                    {
                        this.txtStaffType.Text = "พนักงานรายวันประจำ";
                    }
                    else if (employeeSTECPayroll.Staff_type == "3")
                    {
                        this.txtStaffType.Text = "พนักงานรายวัน";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void getOTDetail(string employee_ID, DateTime otDate, string otType) //แสดงรายละเอียด OT เฉพาะรายการนั้น    
        {
            try
            {
                OT ot = new OT();
                ot = _hrisBusinessLayer.GetOTByEmployeeOneRow(employee_ID, otDate, otType);

                OTDate.Text = ot.OTDate.ToString();
                if (otType == "H")
                {
                    txtOTType.Text = "Holiday";
                    txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    switch (ot.RequestType)
                    {
                        case "A":
                            txtRequest.Text = "OT-ก่อนเริ่มงาน";
                            break;
                        case "B":
                            txtRequest.Text = "OT-หลังเลิกงาน";
                            break;
                        default:
                            txtRequest.Text = "";
                            break;
                    }

                    txtPayrollLot.Text = ot.PayrollLot;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                    cmbManagerApproveStatus.SelectedValue = ot.ManagerApproveStatus;
                    txtRemarkFromManager.Text = ot.RemarkFromManager;

                }
                else if (otType == "S")
                {
                    //List<OTType> otTypeList = new List<OTType>();
                    //OTType h5 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
                    //otTypeList.Add(h5);
                    //cmbOTType.ItemsSource = otTypeList;
                    //cmbOTType.SelectedIndex = 0;
                    txtOTType.Text = "OT-holiday";
                    //txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    //XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    //cmbRequest.SelectedValue = ot.RequestType;
                    //txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                    //cmbManagerApproveStatus.SelectedIndex = 0;
                    //txtRemarkFromManager.Text = ot.RemarkFromManager;
                    txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    switch (ot.RequestType)
                    {
                        case "A":
                            txtRequest.Text = "OT-ก่อนเริ่มงาน";
                            break;
                        case "B":
                            txtRequest.Text = "OT-หลังเลิกงาน";
                            break;
                        default:
                            txtRequest.Text = "";
                            break;
                    }

                    txtPayrollLot.Text = ot.PayrollLot;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                    cmbManagerApproveStatus.SelectedValue = ot.ManagerApproveStatus;
                    txtRemarkFromManager.Text = ot.RemarkFromManager;
                }
                else
                {
                    //cmbOTType.SelectedValue = ot.OTType;
                    txtOTType.Text = "Normal";
                    //XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    //txtNcountOT.Text = Convert.ToString(XCurrentOT);
                    //cmbRequest.SelectedValue = ot.RequestType;
                    //txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                    //cmbManagerApproveStatus.SelectedIndex = 0;
                    //txtRemarkFromManager.Text = ot.RemarkFromManager;
                    txtNcountOT.Text = Convert.ToString(ot.NcountOT);
                    XCurrentOT = Convert.ToDouble(ot.NcountOT);
                    switch (ot.RequestType)
                    {
                        case "A":
                            txtRequest.Text = "OT-ก่อนเริ่มงาน";
                            break;
                        case "B":
                            txtRequest.Text = "OT-หลังเลิกงาน";
                            break;
                        default:
                            txtRequest.Text = "";
                            break;
                    }

                    txtPayrollLot.Text = ot.PayrollLot;
                    txtRemarkFromAdmin.Text = ot.RemarkFromAdmin;
                    cmbManagerApproveStatus.SelectedValue = ot.ManagerApproveStatus;
                    txtRemarkFromManager.Text = ot.RemarkFromManager;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       
        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (LeaveDateFromSearch.Text == "" || LeaveDateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SearchNameButton.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        
        public void SendEmailForApprove(string _toEmployee_ID)
        {

            string fromPerson = "it-info@siamtobacco.com";
            string fromPassword = "Up2uy@dmin$tecE!";

            MailMessage mailmessage = new MailMessage();

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(fromPerson, fromPassword);
            client.Port = 587;
            client.Host = "smtp.emailsrvr.com";

            mailmessage.From = new MailAddress("it-info@siamtobacco.com");
            mailmessage.To.Add("pitchayapa@siamtobacco.com");
            mailmessage.To.Add("pennapa@siamtobacco.com");
            mailmessage.CC.Add(_singleton.emailFrom);
            mailmessage.Subject = "The request OT online for approve from HRIS system.";
            string messages = "";
            messages = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียนฝ่าย HRM </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจากอีเมลล์ " + _singleton.emailFrom + "  กรุณาตรวจสอบข้อมูลในโปรแกรม HRIS </td>" +
                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +

                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear  HRM department</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>You have received a new request OT online for approve from " + _singleton.emailFrom + " and could you please consider in the HRIS system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;
            //mailmessage.
            mailmessage.Body = messages;
            mailmessage.IsBodyHtml = true;
            try
            {
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void SendEmailForUpdate(string _toEmployee_ID)
        {

            //Employee e = new Employee();
            //e = _hrisBusinessLayer.GetEmployeeInformation(_toEmployee_ID);
            //string toName = e.Person.FirstNameTH + " " + e.Person.LastNameTH;
            //string toEmail = e.Email;
            string toEmail = "pitchayapa@siamtobacco.com;pennapa@siamtobacco.com";
            //string toEmail = "saruta@siamtobacco.com";
            string fromPerson = "it-info@siamtobacco.com";
            string fromPassword = "Up2uy@dmin$tecE!";

            MailMessage mailmessage = new MailMessage();

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(fromPerson, fromPassword);
            client.Port = 587;
            client.Host = "smtp.emailsrvr.com";

            mailmessage.From = new MailAddress("it-info@siamtobacco.com");
            //mailmessage.To.Add(toEmail); // to manager
            foreach (var address in toEmail.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                mailmessage.To.Add(address);
            }
            mailmessage.CC.Add(_singleton.emailFrom);
            mailmessage.Subject = "The request modified OT online for approve from HRIS system.";
            string messages = "<table>" +
                                                "<tr>" +
                                                    "<td>เรียนฝ่าย HRM </td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจากอีเมลล์ " + _singleton.emailFrom + " เนื่องจากมีการแก้ไขข้อมูลการขออนุมัติการทำโอที  กรุณาตรวจสอบข้อมูลและอนุมัติการขอทำโอทีในโปรแกรม HRIS </td>" +
                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +

                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Dear  HRM department</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>*** This is an automatically generated email , please do not reply***</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>You have received a new request OT online for approve from " + _singleton.emailFrom + " and could you please consider on the HRIS system. </td>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +

                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>Kind Regards,</td>" +
                                                "</tr>" +
                                                "<tr>" +
                                                    "<td>IT department</td>" +
                                                "</tr>" +
                                            "</table>" +
                                            "<br />"
                                           ;
            //mailmessage.
            mailmessage.Body = messages;
            mailmessage.IsBodyHtml = true;
            try
            {
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SendEmailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกพนักงานตัวแทนฝ่าย/แผนกที่ต้องการส่งอีเมลล์แจ้งผู้จัดการฝ่าย HR กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("คุณต้องการส่งอีเมลล์ถึงผู้จัดการฝ่าย HR เพื่อขออนุมัติการทำโอทีใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                if (Convert.ToString(_singleton.singleton_employee_ID) == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบรายชื่อผู้อนุมัติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                SendEmailForApprove(Convert.ToString(_singleton.singleton_employee_ID));

                //if (XType == "Add")
                //{
                //    
                //}
                //else if (XType == "Edit")
                //{
                //    SendEmailForUpdate(Convert.ToString(_singleton.singleton_employee_ID));
                //}
                
                MessageBox.Show("ส่งอีเมลล์ถึงผู้จัดการฝ่าย HR เพื่อขออนุมัติการทำโอทีแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearData()
        {
            try
            {
                XType = "Add";
                AddButton.IsEnabled = true;
                OTDate.IsEnabled = false;
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";
                txtPayrollLot.Text = "";
                txtEmployeeAcc.Text = "";
                txtOTType.Text = "";
                txtRequest.Text = "";
                txtNcountOT.Text = "";
                txtScanIn.Text = "";
                txtScanOut.Text = "";
                txtTotalWork.Text = "";
                txtStaffType.Text = "";
                txtRemarkFromAdmin.Text = "";
                cmbManagerApproveStatus.SelectedIndex = 0;
                txtRemarkFromManager.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private Boolean ChkDateRange(DateTime _OTdate)
        {
            Boolean xresult = false;
            DateTime Currentdate = DateTime.Now;
            var beginDate = Currentdate;
            var endDate = Currentdate;
            //เช็คว่าเป็นเดือน 1 ใช่หรือไม่ ถ้าใช่ปีจะต้องลบไป1
            if (Currentdate.Month == 1)
            {
                beginDate = new DateTime(Currentdate.Year - 1, 12, 26); //26ของเดือนที่แล้ว คือวันที่สุดท้ายที่คีย์OTย้อนหลังได้
                endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);//25ของเดือนนี้ คือวันสุดท้ายที่คีย์ล่วงหน้าได้
            }
            else
            {
                beginDate = new DateTime(Currentdate.Year, Currentdate.Month - 1, 26);
                endDate = new DateTime(Currentdate.Year, Currentdate.Month, 25);
            }

            //ไม่อนุญาติให้คีย์วันที่ทำโอทีล่วงหน้าได้
            if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
            {
                xresult = false;
            }
            else
            {
                //เช็คว่าถ้าวันที่น้อยกว่า 25 
                if (_OTdate.Day <= 25 && _OTdate.Month != Currentdate.Month)
                {
                    if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate))
                    {
                        xresult = false;
                    }
                    else if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(endDate))
                    {
                        xresult = false;
                    }
                    else
                    {
                        xresult = true;
                    }
                }
                else if (Convert.ToInt16(_OTdate.Day) <= 25 && _OTdate.Month == Currentdate.Month)
                {
                    if (Convert.ToDateTime(_OTdate) <= Convert.ToDateTime(endDate))
                    {
                        xresult = true;
                    }
                    else
                    {
                        xresult = false;
                    }
                }
                else //เช็คว่าถ้าวันที่มากกว่า 25 จะต้องใช้งวดถัดไป
                {

                    if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(beginDate) && _OTdate.Month != Currentdate.Month)
                    {
                        xresult = false;
                    }
                    else if (Convert.ToDateTime(_OTdate) < Convert.ToDateTime(endDate) && _OTdate.Month == Currentdate.Month)
                    {
                        xresult = false;
                    }
                    else
                    {
                        xresult = true;
                    }
                }
            }
            return xresult;

            ////ไม่อนุญาตให้คีย์ล่วงหน้า
            //if(Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
            //{
            //    xresult = false;
            //}
            //else if ((Currentdate.Year == _OTdate.Year) && (Currentdate.Month == _OTdate.Month))//ถ้าเป็นปีและเดือนเดียวกัน  
            //{

            //}else if(_OTdate.Month < endPreviouseMonth.Month) //ถ้าวันที่โอทีน้อยกว่าวันที่ 26 ของเดือนที่แล้ว

            ////ไม่อนุญาตให้คีย์วันทีทำโอทีล่วงหน้า
            //if (Convert.ToDateTime(_OTdate) > Convert.ToDateTime(Currentdate))
            //{
            //    xresult = false;
            //}
            //else if (Convert.ToDateTime(_OTdate) <= Convert.ToDateTime(lastCurrentMonth))
            //{
            //    if (Convert.ToInt16(_OTdate.Month) < Convert.ToInt16(lastCurrentMonth.Month))
            //    {
            //        xresult = false;
            //    }
            //    else
            //    {
            //        xresult = true;
            //    }
            //}
            //else
            //{
            //    xresult = true;
            //}

            ////1.เช็คว่าถ้าเป็นปีและเดือนเดียวกัน
            //if ((Currentdate.Year == _OTdate.Year && Currentdate.Month == _OTdate.Month))
            //{
            //    //1.1 ถ้าวันที่โอทีน้อยกว่าวันที่ปัจจุบัน และ วันที่โอทีมากกว่าวันที่ 25 
            //    if (_OTdate.Date <= Currentdate.Date && _OTdate <= Convert.ToDateTime(lastCurrentMonth))
            //    {
            //        xresult = true;
            //    }
            //    else
            //    {
            //        xresult = false;
            //    }
            //}//2.เช็คว่าถ้าไม่ใช่เดือนเดียวกัน
            //else if (_OTdate < Convert.ToDateTime(endPreviouseMonth))
            //{
            //    xresult = false;
            //}
            //else if (_OTdate > Convert.ToDateTime(lastCurrentMonth) && _OTdate > Currentdate)
            //{
            //    xresult = false;
            //}

            //else
            //{
            //    xresult = true;
            //}



        }

        private void GetDataFromSearch()
        {
            try
            {

                DataGrid.ItemsSource = null;
                if ((LeaveDateFromSearch.Text != "") && (LeaveDateToSearch.Text != ""))
                {
                    _otOnlineInformationList = GetOTOnlineInformation(Convert.ToDateTime(LeaveDateFromSearch.Text), Convert.ToDateTime(LeaveDateToSearch.Text)).ToList();
                    if (ChkAllSearch.IsChecked == true)
                    {
                        if (_fromManager != null)
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.managerEmployee_ID == _singleton.singleton_employee_ID).ToList();
                        }
                    }
                    else if (ChkFilterSearch.IsChecked == true)
                    {
                        if (_fromManager != null)
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.managerEmployee_ID == _singleton.singleton_employee_ID && b.EMPFirstName.Contains(txtSearch.Text)).ToList();
                        }
                        else
                        {
                            _otOnlineInformationList = _otOnlineInformationList.Where(b => b.EMPFirstName.Contains(txtSearch.Text)).ToList();
                        }
                    }
                    if (RdtPending.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Pending").ToList();
                    }
                    else if (RdtApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Approve").ToList();
                    }
                    else if (RdtUnApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.ManagerApproveStatus == "Not Approve").ToList();
                    }

                    else if (RdtHRUnApproved.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Not Approve").ToList();
                    }
                    else if (RdtHRPending.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Pending").ToList();
                    }
                    else if (RdtHRApprove.IsChecked == true)
                    {
                        _otOnlineInformationList = _otOnlineInformationList.Where(b => b.HRApproveStatus == "Approve").ToList();
                    }
                    if (_otOnlineInformationList.Count <= 0)
                    {
                        MessageBox.Show("ไม่มีข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    DataGrid.ItemsSource = _otOnlineInformationList;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowCheckInOut()
        {
            userinfo u;
            u = _timeSTECBuisnessLayer.GetUserInfoByFingerID(xFingerScanID);

            //เช็ค Time In จาก manualworkdate
            ManualWorkDate w = new ManualWorkDate();
            w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A01", Convert.ToDateTime(OTDate.Text));
            if (w != null)
            {
                txtScanIn.Text = Convert.ToString(w.Times);
            }
            else
            {
                //เช็คจาก TimeScan
                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
                if (_checkInout.Count > 0)
                {
                    txtScanIn.Text = Convert.ToString(_checkInout.Min(c => c.checktime.TimeOfDay));

                }
                else
                {
                    txtScanIn.Text = "";
                }
            }

            //เช็ค Time Out จาก manualworkdate
            w = _hrisBusinessLayer.GetManualWorkDateByEmployeeAndType(txtEmployee_ID.Text, "A02", Convert.ToDateTime(OTDate.Text));
            if (w != null)
            {
                txtScanOut.Text = Convert.ToString(w.Times);
            }
            else
            {
                //เช็คจาก TimeScan
                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();
                if (_checkInout.Count > 0)
                {
                    txtScanOut.Text = Convert.ToString(_checkInout.Max(c => c.checktime.TimeOfDay));
                }
                else
                {
                    txtScanOut.Text = "";
                }
            }

            if (txtScanIn.Text != "" && txtScanOut.Text != "")
            {
                TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
                txtTotalWork.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
            }


        }

        private void RdtSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = false;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtPending_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = true;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = false;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtUnApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = true;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRUnApproved_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = false;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRPending_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = false;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtHRApprove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddButton.IsEnabled = false;
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_singleton.singleton_employee_ID == "")
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบว่า manager มี emailที่บันทึกไว้ในหน้า employee หรือยัง 
                Employee e2 = new Employee();
                e2 = _hrisBusinessLayer.GetEmployeeInformation(Convert.ToString(_singleton.singleton_employee_ID));
                if (e2 == null)
                {
                    MessageBox.Show("กรุณาตรวจสอบชื่อผู้จัดการ/หัวหน้าที่มีสิทธิ์อนุมัติของพนักงานท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    if (e2.Email == "" || e2.Email == null)
                    {
                        MessageBox.Show("กรุณาแจ้งให้ฝ่ายบุคคลบันทึก Email ของผู้จัดการ/หัวหน้าท่านนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                if (OTDate.Text == "")
                {
                    MessageBox.Show("ไม่พบข้อมูลวันที่ทำโอที ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (ChkDateRange(Convert.ToDateTime(OTDate.Text)) == false)
                {
                    MessageBox.Show("ไม่สามารถคีย์วันที่ดังกล่าวเนื่องจากระบบไม่สามารถคีย์วันที่ทำโอทีของงวดที่แล้วและคีย์ล่วงหน้าได้ !!! กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtScanIn.Text == "" || txtScanOut.Text == "")
                {
                    MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกคำร้องขออนุมัติโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้วกับฝ่ายบุคคลอีกครั้ง ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtNcountOT.Text == "")
                {
                    MessageBox.Show("ไม่มีจำนวนชั่วโมง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtPayrollLot.Text == "")
                {
                    MessageBox.Show("ไม่มีงวดการจ่ายเงิน กรุณาติดต่อแผนกไอทีให้ตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //else if (Convert.ToDecimal(txtNcountOT.Text) > 12)
                //{
                //    MessageBox.Show("จำนวนชั่วโมง OT ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //else if (cmbOTType.SelectedValue == "H" && Convert.ToDecimal(txtNcountOT.Text) > 1)
                //{
                //    MessageBox.Show("ไม่มีจำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                if (txtRequest.Text == "")
                {
                    MessageBox.Show("ไม่มีOT request", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (otselected.HRApproveStatus.Substring(0,1) == "A")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HRได้อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งฝ่าย HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (otselected.HRApproveStatus.Substring(0, 1) == "N")
                {
                    MessageBox.Show("เนื่องจากฝ่าย HR ไม่อนุมัติการทำโอทีแล้วคุณจึงไม่สามารถแก้ไขข้อมูลได้อีก หากต้องการแก้ไขข้อมูลดังกล่าวให้แจ้งฝ่าย HR ทำการแก้ไขสถานะการอนุมัติให้เป็น Pending", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbManagerApproveStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุผลการอนุมัติ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cmbManagerApproveStatus.Focus();
                    return;
                }
                
                


                //List<OT> s = new List<OT>();
                //s = _hrisBusinessLayer.GetOTByEmployee(txtEmployee_ID.Text).Where(b => b.OTDate == Convert.ToDateTime(OTDate.Text) && b.OTType == (string)cmbOTType.SelectedValue).ToList();
                //if (s.Count() > 0)
                //{
                //    MessageBox.Show("มีข้อมูลวันที่นี้แล้ว กรุณาตรวจสอบอีกครั้ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}



                if (txtRemarkFromManager.Text == ""  && (string)cmbManagerApproveStatus.SelectedValue == "N")
                {
                    MessageBox.Show("กรุณาระบุหมายเหตุที่ไม่อนุมัติการขอทำโอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtRemarkFromManager.Focus();
                    return;
                }
                string xOTType;
                if (txtOTType.Text == "Normal")
                {
                    xOTType = "N";
                }
                else if (txtOTType.Text == "Holiday")
                {
                    xOTType = "H";
                }
                else
                {
                    xOTType = "S";
                }

                string xRequestType;
                if (txtRequest.Text == "OT-ก่อนเริ่มงาน")
                {
                    xRequestType = "A";
                }else{
                    xRequestType = "B";
                }


                OT query1 = new OT();  // created 1 instance or object  
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.OTDate = Convert.ToDateTime(otselected.OTDATE);
                query1.OTType = xOTType;
                query1.NcountOT = Convert.ToDecimal(txtNcountOT.Text);
                query1.Status = true;
                query1.ModifiedDate = DateTime.Now;
                query1.RequestType = xRequestType;
                query1.ManagerApproveStatus = (string)cmbManagerApproveStatus.SelectedValue;
                query1.HRApproveStatus = otselected.HRApproveStatus.Substring(0, 1);
                query1.RemarkFromAdmin = txtRemarkFromAdmin.Text;
                query1.PayrollLot = txtPayrollLot.Text.ToString();
                query1.Admin_ID = otselected.Admin_ID;
                query1.RemarkFromManager = txtRemarkFromManager.Text;
                query1.RemarkFromHR = otselected.RemarkFromHR;
                _hrisBusinessLayer.UpdateOT(query1);
               
                
                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T009";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = Convert.ToString(OTDate.Text);
                transactionLog1.OldData = "";
                transactionLog1.NewData = Convert.ToString(txtNcountOT.Text);
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLog1.Noted = "Approve status by Manager "  + (string)cmbManagerApproveStatus.SelectedValue + " Requesttype " + xRequestType;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);

                //SendEmailForUpdate(Convert.ToString(_singleton.singleton_employee_ID ));
                //MessageBox.Show("ส่งอีเมลล์ถึงผู้จัดการ/หัวหน้าเพื่อขออนุมัติการแก้ไขการทำโอทีแล้ว", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Information);

                //LeaveDateFromSearch.Text = OTDate.Text;
                //LeaveDateToSearch.Text = OTDate.Text;
                ClearData();
                GetDataFromSearch();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

       
        

        
    }
}
