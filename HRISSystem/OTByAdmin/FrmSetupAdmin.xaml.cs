﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Security.Principal;

namespace HRISSystem.OTByAdmin
{
    /// <summary>
    /// Interaction logic for FrmSetupOTByAdmin.xaml
    /// </summary>
    public partial class FrmSetupAdmin : Page
    {
        public List<OTOnlineSetupAdmin> ObjList;
        public List<OTOnlineSetupAdminDetail> objDList;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private OTOnlineSetupAdminDetail objSelectedLst;
        private OTOnlineSetupAdmin objSelectLstAdmin;
        private OTOnlineSetupAdminDetail obj1;

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _timeSTECBusinessLayer = new BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer();

        PrincipalContext myDomain;
        UserPrincipal user;

        public FrmSetupAdmin()
        {
            InitializeComponent();
        }
        public FrmSetupAdmin(EmployeeCurrentInformation x3,string _xFromPage, string _xFromEmployee_ID)
        {
            InitializeComponent();

            xFromEmployeeCurrentInformation = x3;
            if (x3 != null && _xFromPage == "SetupOTAdmin")
            {
                GetEmployeeInformation(x3, "SetupOTAdmin");
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = x3.xFirstNameTH;
                txtSearch.IsEnabled = true;
            }
            else if (x3 != null && _xFromPage == "SetupOTAdminForManager")
            {
                GetEmployeeInformation(x3, "SetupOTAdminForManager");
                GetDataFromEmployee_ID(_xFromEmployee_ID, "SetupOTAdmin");
            }
        }
        

        public void GetEmployeeInformation(EmployeeCurrentInformation x,string _xfrom)
        {
            if (_xfrom == "SetupOTAdmin")
            {
                txtEmployee_ID.Text = x.xEmployee_ID;
                txtTitleNameTH.Text = x.xTitleNameTH;
                txtFirstNameTH.Text = x.xFirstNameTH;
                txtLastNameTH.Text = x.xLastNameTH;
                txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
            }
            else if (_xfrom == "SetupOTAdminForManager")
            {
                txtManagerEmployee_ID.Text = x.xEmployee_ID;
                txtManagerTitleNameTH.Text = x.xTitleNameTH;
                txtManagerFirstNameTH.Text = x.xFirstNameTH;
                txtManagerLastNameTH.Text = x.xLastNameTH;                
            }
            
        }
    
        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                txtAdminID.Text = "";
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";
                txtAdName.Text = "";
                txtManagerEmployee_ID.Text = "";
                txtManagerTitleNameTH.Text = "";
                txtManagerFirstNameTH.Text = "";
                txtManagerLastNameTH.Text = "";
                txtManagerAdName.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void GetDataFromSearch()
        {
            try
            {     
                DataGrid.ItemsSource = null;

                objDList = _hrisBusinessLayer.GetAllOTOnlineSetupAdminDetails().ToList();
                if (objDList.Count() <= 0)
                {
                    MessageBox.Show("ไม่มีข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ChkAllSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = objDList;
                }
                else if (ChkFilterSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = objDList.Where(p => p.OTOnlineSetupAdmin.Employee.Person.FirstNameTH.Contains(txtSearch.Text) );
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();


                cmbDeptCode.ItemsSource = null;
                cmbDeptCode.ItemsSource = _timeSTECBusinessLayer.GetAllDepartments().OrderBy(b => b.code);
                //DataGrid.ItemsSource = null; //clear 
                //DataGrid.ItemsSource = objDList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string GetOU(string username, string findType)
        {

            string result = string.Empty;

            //Finding the user
            user = UserPrincipal.FindByIdentity(myDomain, username);
            if (user != null)
            {
                if (findType == "Email")
                {
                    result = user.EmailAddress;
                }
                else if (findType == "OU")
                {
                    //Getting the DirectoryEntry
                    DirectoryEntry directoryEntry = (user.GetUnderlyingObject() as DirectoryEntry);
                    //if the directoryEntry is not null
                    if (directoryEntry != null)
                    {
                        //Getting the directoryEntry's path and spliting with the "," character
                        string[] directoryEntryPath = directoryEntry.Path.Split(',');
                        //Getting the each items of the array and splitting again with the "=" character
                        foreach (var splitedPath in directoryEntryPath)
                        {
                            string[] elements = splitedPath.Split('=');
                            //If the 1st element of the array is "OU" string then get the 2nd element
                            if (elements[0].Trim() == "OU")
                            {
                                result = elements[1].Trim();
                                break;
                            }
                        }
                    }

                }
            }
            return result;          
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAdName.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อ Ad nameของพนักงานAdminที่่ต้องการให้ใช้สำหรับLockinเข้าระบบขอทำโอทีออนไลน์ โดยจะต้องเป็นชื่อเดียวกันกับLoginที่ใช้งานคอมพิวเตอร์ในบริษัทฯ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtAdName.Focus();
                    return;
                }

                
                string xEmail;
                xEmail = txtAdName.Text + "@siamtobacco.com";

                //Check account from AD
                //Getting the domain;
                myDomain = new PrincipalContext(ContextType.Domain);
                if (GetOU(txtAdName.Text + "@siamtobacco.com", "Email") != xEmail)
                {
                    MessageBox.Show("กรุณาคีย์ชื่อ Ad nameที่่ต้องการให้ใช้สำหรับLockinเข้าระบบขอทำโอทีออนไลน์ โดยจะต้องเป็นชื่อเดียวกันกับLoginที่ใช้งานคอมพิวเตอร์ในบริษัทฯ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtAdName.Focus();
                    return;
                }              
               //----

                


                if (txtManagerTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานระดับ Manager ที่ใช้สำหรับอนุมัติการขอโอที  กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtManagerAdName.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อ Ad nameของManagerที่่ต้องการให้ใช้สำหรับLockinเข้าระบบขออนุมัติโอทีออนไลน์ โดยจะต้องเป็นชื่อเดียวกันกับLoginที่ใช้งานคอมพิวเตอร์ในบริษัทฯ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtManagerAdName.Focus();
                    return;
                }

                xEmail = txtManagerAdName.Text + "@siamtobacco.com";

                //Check account from AD
                //Getting the domain;
                myDomain = new PrincipalContext(ContextType.Domain);
                if (GetOU(txtManagerAdName.Text + "@siamtobacco.com", "Email") != xEmail)
                {
                    MessageBox.Show("กรุณาคีย์ชื่อ Ad nameที่่ต้องการให้ใช้สำหรับLockinเข้าระบบอนุมัติโอทีออนไลน์ โดยจะต้องเป็นชื่อเดียวกันกับLoginที่ใช้งานคอมพิวเตอร์ในบริษัทฯ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtManagerAdName.Focus();
                    return;
                }
                //----


                if (cmbDeptCode.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์แผนก/ฝ่ายที่ต้องการให้พนักงานคีย์การขอทำโอที", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (RdtActive.IsChecked == false && RdtUnactive.IsChecked == false)
                {
                    MessageBox.Show("กรุณาเลือกสถานะในปัจจุบันว่าพนักงานรหัสนี้ยังใช้งานอยู่หรือไม่ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (RdtUnactive.IsChecked == true && EndDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดที่ให้พนักงานรหัสนี้สามารถคีย์ข้อมูลโอทีในแผนก/ฝ่ายนี้ได้  กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


               
                //check employee_id and if it's exit  then used the current admin_id
                OTOnlineSetupAdmin a = new OTOnlineSetupAdmin();
                a = _hrisBusinessLayer.GetOTonlineSetuAdminByEmployee(txtEmployee_ID.Text);
                if (a == null)
                {
                    ObjList = _hrisBusinessLayer.GetAllOTOnlineSetupAdmins().ToList();
                    int xAdmin_Id;
                    if (ObjList.Count > 0)
                    {
                        xAdmin_Id = Convert.ToInt16(ObjList.Max(c => c.Admin_ID)) + 1;
                    }
                    else
                    {
                        xAdmin_Id = 1;
                    }
                    txtAdminID.Text = Convert.ToString(xAdmin_Id);

                    OTOnlineSetupAdmin[] oTOnlineSetupAdminObj = new OTOnlineSetupAdmin[1];
                    OTOnlineSetupAdmin query1 = new OTOnlineSetupAdmin();  // created 1 instance or object

                    query1.Admin_ID = Convert.ToInt16(txtAdminID.Text);
                    query1.Employee_ID = txtEmployee_ID.Text;
                    query1.AdName = txtAdName.Text;
                    query1.CreateDate = DateTime.Now;
                    query1.ModifiedDate = DateTime.Now;
                    query1.ValidFrom = DateTime.Now;
                    query1.EndDate = DateTime.MaxValue;
                    oTOnlineSetupAdminObj[0] = query1;
                    _hrisBusinessLayer.AddOTOnlineSetupAdmin(oTOnlineSetupAdminObj);
                }
                else
                {
                    txtAdminID.Text = a.Admin_ID.ToString();
                }

                OTOnlineSetupAdminDetail[] OTOnlineSetupAdminDetailObj = new OTOnlineSetupAdminDetail[1];
                OTOnlineSetupAdminDetail query2 = new OTOnlineSetupAdminDetail();
                query2.Admin_ID = Convert.ToInt16(txtAdminID.Text);
                query2.DeptID = (string)cmbDeptCode.SelectedValue;
                query2.ReportTo = txtManagerEmployee_ID.Text;
                query2.CreatedDate = DateTime.Now;
                query2.ModifiedDate = DateTime.Now;
                query2.ValidFrom = DateTime.Now;
                query2.EndDate = DateTime.MaxValue;
                OTOnlineSetupAdminDetailObj[0] = query2;
                _hrisBusinessLayer.AddOTOnlineSetupAdminDetail(OTOnlineSetupAdminDetailObj);

                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(txtEmployee_ID.Text);
                //update email for adminEmployee
                Employee queryE = new Employee();  // created 1 instance or object
                queryE.Employee_ID = txtEmployee_ID.Text;
                queryE.Person_ID = ex.xPerson_ID;
                queryE.FingerScanID = ex.xFingerScan_ID;
                queryE.CreatedDate = ex.xCreatedDateEmployee;
                queryE.StartDate = ex.xStartDate;
                queryE.EndDate = ex.xEnddate;
                queryE.BaseSalary = ex.xBaseSalary;
                queryE.ModifiedDate = DateTime.Now;
                queryE.Noted = ex.xNoted;
                queryE.Email = Convert.ToString(txtAdName.Text) + cnxDomain.Text;
                _hrisBusinessLayer.UpdateEmployee(queryE);


                EmployeeCurrentInformation exM = new EmployeeCurrentInformation(txtManagerEmployee_ID.Text);
                //update email for manager employee
                Employee queryM = new Employee();  // created 1 instance or object
                queryM.Employee_ID = txtManagerEmployee_ID.Text;
                queryM.Person_ID = exM.xPerson_ID;
                queryM.FingerScanID = exM.xFingerScan_ID;
                queryM.CreatedDate = exM.xCreatedDateEmployee;
                queryM.StartDate = exM.xStartDate;
                queryM.EndDate = exM.xEnddate;
                queryM.BaseSalary = exM.xBaseSalary;
                queryM.ModifiedDate = DateTime.Now;
                queryM.Noted = exM.xNoted;
                queryM.Email = Convert.ToString(txtManagerAdName.Text) + cnxDomain.Text;
                _hrisBusinessLayer.UpdateEmployee(queryM);


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T027";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = txtAdName.Text;
                transactionLog1.OldData = "";
                transactionLog1.NewData = "Report To " + txtManagerEmployee_ID.Text + " DeptID " + (string)cmbDeptCode.SelectedValue;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                
                GetDataFromSearch();             
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtAdName.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อ Ad nameที่่ต้องการให้ใช้สำหรับLockinเข้าระบบขอทำโอทีออนไลน์ โดยจะต้องเป็นชื่อเดียวกันกับLoginที่ใช้งานในคอมพิวเตอร์ในบริษัทฯ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtManagerTitleNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานระดับ Manager ที่ใช้สำหรับอนุมัติการขอโอที  กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }




                if (RdtActive.IsChecked == false && RdtUnactive.IsChecked == false)
                {
                    MessageBox.Show("กรุณาเลือกสถานะในปัจจุบันว่าพนักงานรหัสนี้ยังใช้งานอยู่หรือไม่ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (RdtUnactive.IsChecked == false && EndDate.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่สิ้นสุดที่ให้พนักงานรหัสนี้สามารถคีย์ข้อมูลโอทีในแผนก/ฝ่ายนี้ได้  กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                OTOnlineSetupAdmin m = new OTOnlineSetupAdmin();
                m = _hrisBusinessLayer.GetOTOnlineSetupAdminByAdminID(obj1.Admin_ID);
                m.Admin_ID = Convert.ToInt16(txtAdminID.Text);
                m.Employee_ID = txtEmployee_ID.Text;
                m.AdName = txtAdName.Text;
                m.CreateDate = objSelectLstAdmin.CreateDate;
                m.ModifiedDate = DateTime.Now;
                m.ValidFrom = objSelectLstAdmin.ValidFrom;
                if (RdtUnactive.IsChecked == true)
                {
                    m.EndDate = Convert.ToDateTime(EndDate.Text);
                }
                else
                {
                    m.EndDate = objSelectLstAdmin.EndDate;
                }               
                _hrisBusinessLayer.UpdateOTOnlineSetupAdmin(m);


                obj1.Admin_ID = Convert.ToInt16(txtAdminID.Text);
                obj1.DeptID = (string)cmbDeptCode.SelectedValue;
                obj1.ReportTo = txtManagerEmployee_ID.Text;
                obj1.CreatedDate = obj1.CreatedDate;
                obj1.ModifiedDate = DateTime.Now;
                obj1.ValidFrom = obj1.ValidFrom;
                if (RdtUnactive.IsChecked == true)
                {
                    obj1.EndDate = Convert.ToDateTime(EndDate.Text);
                }
                else
                {
                    obj1.EndDate = objSelectLstAdmin.EndDate;
                }
                _hrisBusinessLayer.UpdateOTOnlineSetupAdminDetail(obj1);
                

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "E";
                transactionLog1.Table_ID = "T027";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Employee_ID";
                transactionLog1.PKFields = txtEmployee_ID.Text;
                transactionLog1.FKFields = txtAdName.Text;
                transactionLog1.OldData = "Report To " + obj1.ReportTo + " DeptID " + obj1.DeptID + " EndDate " + obj1.EndDate;
                transactionLog1.NewData = "Report To " + txtManagerEmployee_ID.Text + " DeptID " + (string)cmbDeptCode.SelectedValue + " EndDate " + (string)EndDate.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                if (objSelectedLst != null &&  objSelectLstAdmin != null)
                {

                    ////check if .recordcount <= 0 then remove otonlinesetupdmin too.
                    List<OTOnlineSetupAdminDetail> b = new List<OTOnlineSetupAdminDetail>();
                    b = _hrisBusinessLayer.GetOTonLineSetupAdminDetailByAdminID(objSelectedLst.Admin_ID).ToList();
                    if (b.Count() <= 1)
                    {
                        _hrisBusinessLayer.RemoveOTOnlineSetupAdminDetail(obj1);
                        _hrisBusinessLayer.RemoveOTOnlineSetupAdmin(objSelectLstAdmin);
                    }
                    else
                    {
                        _hrisBusinessLayer.RemoveOTOnlineSetupAdminDetail(obj1);
                    }



                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                    transactionLog1.TransactionType_ID = "D";
                    transactionLog1.Table_ID = "T027";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "Employee_ID";
                    transactionLog1.PKFields = txtEmployee_ID.Text;
                    transactionLog1.FKFields = txtAdName.Text;
                    transactionLog1.OldData = "Report To " + txtManagerEmployee_ID.Text + " DeptID " + (string)cmbDeptCode.SelectedValue + " EndDate " + (string)EndDate.Text;
                    transactionLog1.NewData = "";
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                }
                

                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtManagerEmployee_ID.Text == "")
                {
                    this.NavigationService.Navigate(new HumanResource.EmployeeSearch("SetupOTAdmin"));
                }
                else
                {
                    this.NavigationService.Navigate(new HumanResource.EmployeeSearch("SetupOTAdmin",txtManagerEmployee_ID.Text));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text,"SetupOTAdmin");
                    //myDomain = new PrincipalContext(ContextType.Domain);
                    //txtAdName.Text = GetOU(txtAdName.Text, "Email");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void GetDataFromEmployee_ID(string _xEmployee_ID,string _xfrom)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                if (_xfrom == "SetupOTAdmin")
                {
                    txtEmployee_ID.Text = ex.xEmployee_ID;
                    txtTitleNameTH.Text = ex.xTitleNameTH;
                    txtFirstNameTH.Text = ex.xFirstNameTH;
                    txtLastNameTH.Text = ex.xLastNameTH;
                    txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
                    if (ex.xEmail  != null  )
                    {
                        txtAdName.Text = ex.xEmail.Substring(0,(ex.xEmail.Length)-16) ;                        
                    }
                    else
                    {
                        txtAdName.Text = ex.xEmail;
                    }
                    
                }
                else if (_xfrom == "SetupOTAdminForManager")
                {
                    txtManagerEmployee_ID.Text = ex.xEmployee_ID;
                    txtManagerTitleNameTH.Text = ex.xTitleNameTH;
                    txtManagerFirstNameTH.Text = ex.xFirstNameTH;
                    txtManagerLastNameTH.Text = ex.xLastNameTH;

                    if (ex.xEmail != null)
                    {
                       txtManagerAdName.Text = ex.xEmail.Substring(0, (ex.xEmail.Length) - 16);                        
                    }
                    else
                    {
                        txtManagerAdName.Text = ex.xEmail;
                    }

                    
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                objSelectedLst = new OTOnlineSetupAdminDetail();  // created 1 instance or object
                objSelectedLst = (OTOnlineSetupAdminDetail)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                obj1 = new OTOnlineSetupAdminDetail();
                obj1 = _hrisBusinessLayer.GetOTOnlineSetupAdminDetailByAdminAndDeptID(objSelectedLst.Admin_ID, objSelectedLst.DeptID);

                objSelectLstAdmin = _hrisBusinessLayer.GetOTOnlineSetupAdminByAdminID(objSelectedLst.Admin_ID);
                if (objSelectedLst == null)
                {
                    AddButton.IsEnabled = true;
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);

                    txtAdminID.Text = "";
                    txtAdName.Text = "";
                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtEmployee_ID.IsReadOnly = true;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.LightGray);
                    GetDataFromEmployee_ID(objSelectedLst.OTOnlineSetupAdmin.Employee_ID,"SetupOTAdmin");
                    GetDataFromEmployee_ID(objSelectedLst.ReportTo, "SetupOTAdminForManager");

                    txtAdminID.Text = objSelectedLst.Admin_ID.ToString();
                    txtAdName.Text = objSelectedLst.OTOnlineSetupAdmin.AdName;
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToManagerSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtEmployee_ID.Text == "")
                {
                    this.NavigationService.Navigate(new HumanResource.EmployeeSearch("SetupOTAdminForManager"));
                }
                else
                {
                    this.NavigationService.Navigate(new HumanResource.EmployeeSearch("SetupOTAdminForManager", txtEmployee_ID.Text));
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtManagerEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtManagerEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานตำแหน่ง Manager ที่ต้องการให้ approve OT ออนไลน์ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtManagerEmployee_ID.Text, "SetupOTAdminForManager");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
