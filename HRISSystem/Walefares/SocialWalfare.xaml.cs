﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for SocialWalfare.xaml
    /// </summary>
    public partial class SocialWalfare : Page
    {
        public List<WorkUpCountry> ObjList;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public SocialWalfare()
        {
            InitializeComponent();
        }

        public SocialWalfare(SSOHospitalList _x)
        {
            InitializeComponent();
            GetSocialWalfareList(_x);

        }
       
        public SocialWalfare(EmployeeCurrentInformation x3)
        {
            InitializeComponent();
           
            ObjList = new List<WorkUpCountry>();
            xFromEmployeeCurrentInformation = x3;
            if (x3 != null)
            {
                GetEmployeeInformation(x3);              
            }            
        }

        public void GetSocialWalfareList(SSOHospitalList x)
        {
            txtID.Text = x.SSOHospital_ID;
            txtNameTH.Text = x.HospitalNameTH;
        }

        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            //EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
        }
        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";              
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                if (xFromEmployeeCurrentInformation != null)
                    NavigationService.RemoveBackEntry();                
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = ObjList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (JoinDate.Text == "")
                    JoinDate.Text = Convert.ToString(DateTime.MinValue);
                if (EndDate.Text == "")
                    EndDate.Text = Convert.ToString(DateTime.MaxValue);

                if (txtID.Text== "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อโรงพยาบาล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ต้องการบันทึกข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }



                SSOHospital[] ssoHospitalObj = new SSOHospital[1];
                SSOHospital query1 = new SSOHospital();  // created 1 instance or object
                query1.SSOHospital_ID = txtID.Text;
                query1.Employee_ID = txtEmployee_ID.Text;               
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(JoinDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                ssoHospitalObj[0] = query1;
                _hrisBusinessLayer.AddSSOHospital(ssoHospitalObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T025";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "HospitalList_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = txtEmployee_ID.Text;
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

               
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (JoinDate.Text == "")
                    JoinDate.Text = Convert.ToString(DateTime.MinValue);
                if (EndDate.Text == "")
                    EndDate.Text = Convert.ToString(DateTime.MaxValue);

                if (txtID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ชื่อโรงพยาบาล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (txtEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ต้องการบันทึกข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }



                SSOHospital[] ssoHospitalObj = new SSOHospital[1];
                SSOHospital query1 = new SSOHospital();  // created 1 instance or object
                query1.SSOHospital_ID = txtID.Text;
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(JoinDate.Text);
                query1.EndDate = Convert.ToDateTime(EndDate.Text);
                query1.ModifiedDate = DateTime.Now;
                ssoHospitalObj[0] = query1;
                _hrisBusinessLayer.AddSSOHospital(ssoHospitalObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T025";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "HospitalList_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = txtEmployee_ID.Text;
                transactionLog1.OldData = "";
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchHospitalButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new Walefares.SocialWalfareHospitalList("SocialWalfare"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("SocialWalfare"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสโรงพยาบาลให้ถูกต้อง กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    
                    GetSocialWalfareList(txtID.Text.ToUpper());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetSocialWalfareList(string _xID)
        {
            try
            {               
                SSOHospitalList ex = new SSOHospitalList();

                ex = _hrisBusinessLayer.GetSSOHospitalListByID(_xID );
                if (ex != null)
                {
                    txtID.Text = ex.SSOHospital_ID;
                    txtNameTH.Text = ex.HospitalNameTH;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
