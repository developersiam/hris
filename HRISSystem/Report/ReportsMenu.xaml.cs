﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for ReportsMenu.xaml
    /// </summary>
    public partial class ReportsMenu : Page
    {
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();

        public ReportsMenu()
        {
            InitializeComponent();
        }

        private void NavigateToEmployeeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR011());
        }

        private void NavigateToWorkingEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR012());
        }       
        private void NavigateToDetailWorkingEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR015());
        }

        private void NavigateToWorkAllEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR016());
        }

        private void NavigateToSumByDeptButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR017());
        }

        private void NavigateToPayrollButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR018());
        }

        private void NavigateToSSOwalfareInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR020());
        }

        private void NavigateToManpowerInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR022());
        }

        private void NavigateToDetailWorkingDateInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR023());
        }

        private void NavigateToDetailScanInOutInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR024());
        }

        private void NavigateToPayrollStaffButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR025());
        }

        private void NavigateToPrintPaySlipButton_Click(object sender, RoutedEventArgs e)
        {
            if (_singleton.deptCode == "HRM" )
            {
                MessageBox.Show("ไม่มีสิทธิ์ใช้งานรายงานนี้ หากต้องการให้เปิดใช้งานกรุณาแจ้งแผนกไอทีเพื่อทำการแก้ไขต่อไป", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            this.NavigationService.Navigate(new RepRPTHR026());
        }

        private void NavigateToExportPND1AButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RepRPTHR027());
        }
    }
}
