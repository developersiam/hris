﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class TimeAttendanceClass
    {
        public DateTime CalenDarDate { get; set; }
        public string code { get; set; }
        public string Deptname { get; set; }
        public int UserID { get; set; }
        public int FingerScanCode { get; set; }
        public string Employee_ID { get; set; }
        public string ACC_ID { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Person_ID { get; set; }
        public string Payroll_ID { get; set; }
        public string Staff_Type { get; set; }
        public DateTime CheckIN { get; set; }
        public DateTime CheckOut { get; set; }
        public DateTime HolidayDate { get; set; }
        public int HolidayFlag { get; set; }
        public DateTime WorkDate { get; set; }
        public DateTime LeaveDate { get; set; }
        public string LeaveDateType { get; set; }
        public decimal NCountLeave { get; set; }        
        public string ManualWorkDateTypeA01 { get; set; }
        public DateTime ManualWorkDateDate01 { get; set; }
        public DateTime ManualWorkDateTime01 { get; set; }
        public string ManualWorkDateTypeA02 { get; set; }
        public DateTime ManualWorkDateDate02 { get; set; }
        public DateTime ManualWorkDateTime02 { get; set; }
        public DateTime SpecialDates { get; set; }
        public decimal NOTnormal { get; set; }
        public decimal NOTHoliday { get; set; }
        public decimal NOTHolidays { get; set; }
        public decimal NWorkingDate { get; set; }

    }
}
