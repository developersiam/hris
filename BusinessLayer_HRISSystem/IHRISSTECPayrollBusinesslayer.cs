﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem
{
   public interface IHRISSTECPayrollBusinesslayer
    {
       List<BusinessObject.LabourWorkingDateClass> GetLabourWorkingDate(DateTime dateFrom, DateTime dateTo);
       List<BusinessObject.DepartMentFromSTECPayrollClass> GetDepartmentFromSTECPayroll();

        //OT_Detail
        IList<OT_Detail> GetAllOT_Details();
        OT_Detail GetOT_DetailByLot(string lot_Month, string lot_Year, string employee_ID);
        void AddOT_Detail(params OT_Detail[] ot_Details);
        void RemoveOT_Detail(OT_Detail ot_Detail);

        //Labour_work
        IList<Labour_work> GetAllLabour_Works();
        Labour_work GetLabour_workByLot(string lot_Month, string Lot_Year, string employee_ID);
        void AddLabour_work(params Labour_work[] labour_work);
        void RemoveLabour_work(Labour_work labour_work);


        IList<EmployeeSTECPayroll> GetAllEmployeeSTECPayrolls();
        EmployeeSTECPayroll GetEmployeeSTECPayroll(string employee_ID);
        void AddEmployeeSTEcPayroll(params EmployeeSTECPayroll[] employeeSTECPayroll);
        void UpdateEmployeeSTECPayroll(params EmployeeSTECPayroll[] employeeSTECPayroll);
        void RemoveEmployeeSTECPayroll(EmployeeSTECPayroll employeeSTECPayroll);


       //Lot_number
        IList<Lot_Number> GetAllLot_Numbers();
        Lot_Number GetLotNumberByDate(DateTime date);
        Lot_Number GetLot_Number(string lot_month,string lot_year);
        void UpdateLot_Number(params Lot_Number[] lot_Number);
    }
}
