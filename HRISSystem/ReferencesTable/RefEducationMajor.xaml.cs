﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for RefEducationMajor.xaml
    /// </summary>
    public partial class RefEducationMajor : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XType;
        private string XPersonID;
        private string EduID;
        Major XFromMajorRefPage;
        Major majorEdit = new Major();
        public RefEducationMajor(string _xType, string _eduID , string _majorID, string _personId, string _employeeID)
        {
            InitializeComponent();
            if (_xType == "Add")
            {
                XType = "Add";
                XPersonID = _personId;
                EduID = _eduID;
                EducationQualification edu = _hrisBusinessLayer.GetEducationQualificationByID(_eduID);
                TxtEduID.Text = edu.EducationQualificaionTH;
            }
            else if (_xType == "Edit")
            {
                XType = "Edit";
                XPersonID = _personId;                
                ShowDetail(_majorID);
            }
        }
        public RefEducationMajor(Major eX)
        {
            InitializeComponent();
            XFromMajorRefPage = eX;
            ShowDetail(XFromMajorRefPage.Major_ID);

        }

        private void ShowDetail(string majorID)
        {
            majorEdit = _hrisBusinessLayer.GetMajorByID(majorID);

            TxtID.Text = majorEdit.Major_ID;
            TxtEduID.Text = majorEdit.EducationQualification.EducationQualificaionTH;
            EduID = majorEdit.EducationQualification_ID;
            TxtNameTH.Text = majorEdit.MajorTH;
            TxtNameEN.Text = majorEdit.MajorEN;
            TxtIniNameTH.Text = majorEdit.MajorInitialTH;
            TxtIniNameEN.Text = majorEdit.MajorInitialEN;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (XFromMajorRefPage != null)
                    NavigationService.RemoveBackEntry();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //getAll and added 1
                string XId;
                List<Major> major = new List<Major>();
                major = _hrisBusinessLayer.GetAllMajors().OrderBy(b => b.Major_ID).ToList();
                if (major.Count() > 0)
                {
                    XId = Convert.ToString(Convert.ToDouble(major.Max(d => d.Major_ID) + 1));
                }
                else
                {
                    XId = string.Format("{0:0000}", 0001);
                }
                TxtID.Text = XId;


                if (TxtID.Text == "")
                {
                    MessageBox.Show("กรุณาแจ้ง IT ให้ตรวจสอบรหัสนี้");
                    return;
                }
                if (TxtEduID.Text == "")
                {
                    MessageBox.Show("กรุณาแจ้ง IT ให้ตรวจสอบข้อมูลนี้");
                    return;
                }

                if (TxtNameTH.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชื่อเต็มสาขาวิชาภาษาไทย");
                    TxtNameTH.Focus();
                    return;
                }


                if (XType == "Add")
                {
                    Major[] majorObj = new Major[1];
                    Major majorAdd = new Major();
                    majorAdd.Major_ID = TxtID.Text;
                    majorAdd.EducationQualification_ID = EduID;
                    majorAdd.MajorTH = TxtNameTH.Text;
                    majorAdd.MajorEN = TxtNameEN.Text;
                    majorAdd.MajorInitialTH = TxtIniNameTH.Text;
                    majorAdd.MajorInitialEN = TxtIniNameEN.Text;
                    majorAdd.ModifiedDate = DateTime.Now;
                    majorObj[0] = majorAdd;
                    _hrisBusinessLayer.AddMajor(majorObj);
                }
                else if (XType == "Edit")
                {
                    if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }
                    majorEdit.Major_ID = TxtID.Text;
                    majorEdit.EducationQualification_ID = EduID;                   
                    majorEdit.MajorTH = TxtNameTH.Text;
                    majorEdit.MajorEN = TxtNameEN.Text;
                    majorEdit.MajorInitialTH = TxtIniNameTH.Text;
                    majorEdit.MajorInitialEN = TxtIniNameEN.Text;
                    majorEdit.ModifiedDate = DateTime.Now;
                    _hrisBusinessLayer.UpdateMajor(majorEdit);                    
                }
                //NavigationService.GoBack();
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupEduMajor", "", TxtID.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtNameTH.Text = "";
                TxtNameEN.Text = "";
                TxtIniNameTH.Text = "";
                TxtIniNameEN.Text = "";
                TxtNameTH.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupEduMajor", "", TxtID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
