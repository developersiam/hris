﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR013.xaml
    /// </summary>
    public partial class RepRPTHR013 : Page
    {
        string XLot_month;
        string XLot_year;
        public RepRPTHR013(string _lot_month, string _lot_year)
        {
            InitializeComponent();
            XLot_month = _lot_month;
            XLot_year = _lot_year;
            _ReportViewer1.Load += ReportViewer_Load;
        }

        private bool _isReportViewerLoaded;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSour1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                HRISReportDataset dataset = new HRISReportDataset();
                dataset.BeginInit();

                reportDataSour1.Name = "LabourWorkAndOTDataSet";//Name of the report dataset in our .RDLC file
                reportDataSour1.Value = dataset.sp_GetLabourWorkAndOTFromSTEC_payroll ;
                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSour1);
                this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR013.rdlc";

                dataset.EndInit();
                HRISReportDatasetTableAdapters.sp_GetLabourWorkAndOTFromSTEC_payrollTableAdapter sp_getLabourWorkAndOTFromSTEC_payrollAdapter = new HRISReportDatasetTableAdapters.sp_GetLabourWorkAndOTFromSTEC_payrollTableAdapter();
                sp_getLabourWorkAndOTFromSTEC_payrollAdapter.ClearBeforeFill = true;
                sp_getLabourWorkAndOTFromSTEC_payrollAdapter.Fill(dataset.sp_GetLabourWorkAndOTFromSTEC_payroll,XLot_year,XLot_month);

             
                _ReportViewer1.RefreshReport();

                _isReportViewerLoaded = true;
            }
        }
    }
}
