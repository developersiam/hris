﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR017.xaml
    /// </summary>
    public partial class RepRPTHR017 : Page
    {
        public RepRPTHR017()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DateFrom.Text == "" )
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                

                _ReportViewer1.Reset();
                HRISReportDataset.sp_GetEmployeeTimeAttendanceByDate1DataTable sp_GetEmployeeTimeAttendanceByDate1DataTable = new HRISReportDataset.sp_GetEmployeeTimeAttendanceByDate1DataTable();
                HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDate1TableAdapter sp_GetEmployeeTimeAttendanceByDate1TableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDate1TableAdapter();
                sp_GetEmployeeTimeAttendanceByDate1TableAdapter.Fill(sp_GetEmployeeTimeAttendanceByDate1DataTable, Convert.ToDateTime(DateFrom.Text), Convert.ToDateTime(DateFrom.Text));
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource1.Name = "EmployeeTimeAttendanceDataSet";
                reportDataSource1.Value = sp_GetEmployeeTimeAttendanceByDate1DataTable;

                this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR017.rdlc";
                _ReportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
