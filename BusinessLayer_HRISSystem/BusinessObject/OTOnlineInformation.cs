﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem.BusinessObject
{
   public class OTOnlineInformation
    {
       public int Admin_ID { get; set; }
        public string Employee_ID { get; set; }
        public string EMPACCCode { get; set; }
        public string FingerScanID { get; set; }
        public string EMPTitlename { get; set; }
        public string EMPFirstName { get; set; }
        public string EMPlastName { get; set; }
        public Nullable<System.DateTime> OTDATE { get; set; }       
        public string RequestType { get; set; }
        public string ManagerApproveStatus { get; set; }
        public string HRApproveStatus { get; set; }
        public string RemarkFromAdmin { get; set; }
        public string RemarkFromManager { get; set; }
        public string RemarkFromHR { get; set; }
        public decimal OTnormal { get; set; }
        public decimal Holiday { get; set; }
        public decimal OTHoliday { get; set; }
        public string AdminEmployee_ID { get; set; }
        public string AdminACCCode { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public string DeptID { get; set; }
        public string managerEmployee_ID { get; set; }
        public string ManagerACCCode { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerLastName { get; set; }
        public string PayrollLot { get; set; }

        

        
    }
} 
