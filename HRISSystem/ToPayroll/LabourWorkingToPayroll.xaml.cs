﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;

namespace HRISSystem.ToPayroll
{
    /// <summary>
    /// Interaction logic for LabourWorkingToPayroll.xaml
    /// </summary>
    public partial class LabourWorkingToPayroll : Page
    {
        private List<BusinessLayer_HRISSystem.BusinessObject.LabourWorkingDateClass> labourWorkingListClass = new List<BusinessLayer_HRISSystem.BusinessObject.LabourWorkingDateClass>();
        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _hrisTimeSTECBusinessLayer = new BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer();

        //--------for transaction start
        //public List<TransactionLog> transactionLog;
        //Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        //--------for transaction end

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public LabourWorkingToPayroll()
        {
            InitializeComponent();
            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DateFromSearch.Text = FirstDayOfMonth(DateTime.Now).ToString();
                DateToSearch.Text = DateTime.Now.ToString();
                CallDataButton.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SendDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var labourWork_by_Lot = _hrisSTECPayrollBusinessLayer.GetLabour_workByLot_List(lot_numberList.Lot_month, lot_numberList.Lot_year);
                foreach (var item in labourWork_by_Lot)
                {   //ให้ลบข้อมูลใน OT_Detail ในแต่ละรายการ 
                    Labour_work labour_work = new Labour_work();
                    labour_work = _hrisSTECPayrollBusinessLayer.GetLabour_workByLot(lot_numberList.Lot_month, lot_numberList.Lot_year, item.Employee_id);
                    if (labour_work != null)
                    {
                        _hrisSTECPayrollBusinessLayer.RemoveLabour_work(labour_work);
                    }
                }
                foreach (BusinessLayer_HRISSystem.BusinessObject.LabourWorkingDateClass item in labourWorkingListClass)
                {   //add data to  database. ในแต่ละรายการ
                    _hrisSTECPayrollBusinessLayer.AddLabour_work(new Labour_work
                    {
                        Lot_Month = lot_numberList.Lot_month,
                        Lot_Year = lot_numberList.Lot_year,
                        Employee_id = item.AccCode,
                        CWork = Convert.ToDouble(item.NWorking),
                        Username = _singleton.username,
                        Ddate = DateTime.Now
                    });
                }

                //foreach (BusinessLayer_HRISSystem.BusinessObject.LabourWorkingDateClass item in labourWorkingListClass)
                //{
                //    //ให้ลบข้อมูลใน OT_Detail ในแต่ละรายการ
                //    Labour_work labour_work = new Labour_work();
                //    labour_work = _hrisSTECPayrollBusinessLayer.GetLabour_workByLot(lot_numberList.Lot_month ,lot_numberList.Lot_year , item.AccCode);
                //    if (labour_work != null)
                //    {
                //        _hrisSTECPayrollBusinessLayer.RemoveLabour_work(labour_work);
                //    }


                //    //add data to  database. ในแต่ละรายการ
                //    _hrisSTECPayrollBusinessLayer.AddLabour_work(new Labour_work
                //    {
                //        Lot_Month = lot_numberList.Lot_month,
                //        Lot_Year = lot_numberList.Lot_year,
                //        Employee_id = item.AccCode,
                //        CWork = Convert.ToDouble( item.NWorking),
                //        Username = _singleton.username,
                //        Ddate = DateTime.Now
                //    });
                //}

                MessageBox.Show("Import ข้อมูลการทำงานของพนักงานรายวัน-รายวันประจำ จำนวน " + labourWorkingListClass.Count() + " รายการ เข้าสู่ระบบ Payroll เรียบร้อยแล้ว", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CallDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (Convert.ToDateTime(DateToSearch.Text) < Convert.ToDateTime(DateFromSearch.Text))
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetDataFromSearch(Convert.ToDateTime(DateFromSearch.Text), Convert.ToDateTime(DateToSearch.Text));
                SendDataButton.Content = string.Format("Send Data ({0})", labourWorkingListClass.Count());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromSearch(DateTime _fromDate, DateTime _toDate)
        {
            try
            {         
                labourWorkingListClass = _hrisSTECPayrollBusinessLayer.GetLabourWorkingDate(_fromDate, _toDate);
                DataGrid.ItemsSource = labourWorkingListClass;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = null;

                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
               
                DateFromSearch.Text = Convert.ToString(lot_numberList.Start_date);
                DateToSearch.Text = Convert.ToString(lot_numberList.Finish_date);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ViewReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NavigationService.Navigate(new Report.RepRPTHR013(lot_numberList.Lot_month, lot_numberList.Lot_year));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
