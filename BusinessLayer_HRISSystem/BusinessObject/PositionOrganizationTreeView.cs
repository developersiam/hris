﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class PositionOrganizationTreeView : PositionOrganization
    {
        public string Position_ID2 { get; set; }
        public string Position_Name { get; set; }
        public Int64? STECPositionOrganizationID2 { get; set; }
        public int CountNode { get; set; }
    }
}
