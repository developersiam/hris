﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;


namespace BusinessLayer_HRISSystem
{
    public interface IHRISBusinessLayer
    {

        
        

        IList<SSOHospitalList> GetAllSSOHospitalLists();
        SSOHospitalList GetSSOHospitalListByID(string _ssoHospitallist_ID);
        void AddSSOHospitalList(params SSOHospitalList[] _ssoHospitalList);
        void UpdateSSOHospitalList(SSOHospitalList _ssoHospitalList);
        void RemoveSSOHospitalList(SSOHospitalList _ssoHospitalList);

        IList<SSOHospital> GetAllSSOHospitals();
        IList<SSOHospital> GetSSOHospitaltByID(string _ssoHospital_ID);
        SSOHospital GetSSOHospitalBySSOHopsitalIDAndEmployeeID(string _ssoHospital_ID, string _employeeID);
        void AddSSOHospital(params SSOHospital[] _ssoHospital);
        void UpdateSSOHospital(SSOHospital _ssoHospital);
        void RemoveSSOHospital(SSOHospital _ssoHospital);

        IList<TrainingCourse> GetAllTrainingCourses();
        TrainingCourse GetTrainingCourseByID(string _trainingCourse_ID);
        void AddTrainingCourse(params TrainingCourse[] _trainingCourse);
        void UpdateTrainingCourse(TrainingCourse _trainingCourse);
        void RemoveTrainingCourse(TrainingCourse _trainingCourse);

        IList<Training> GetAllTrainings();
        IList<Training> GetTrainingByEmployee(string _employeeID);
        IList<Training> GetTrainingByTrainingCourse(string _trainingCourseID , DateTime beginDate , DateTime Enddate);
        void AddTraining(params Training[] _training);
        void UpdateTraining(Training _training);
        void RemoveTraining(Training _training);

        IList<ContactPersonInfo> GetAllContactpersonInfos();
        IList<ContactPersonInfo> GetContactPersonInfoByPersonID(string personID);
        ContactPersonInfo GetContactPersonInfoByID(string _contactPersonInfoID);
        void AddContactPersonInfo(params ContactPersonInfo[] _contactPersonInfo);
        void UpdateContactPersonInfo(ContactPersonInfo _contactPersonInfo);
        void RemoveContactPersonInfo(ContactPersonInfo _contactPersonInfo);

        IList<OriginalWorkHistory> GetAllOriginalWorkHistorys();
        IList<OriginalWorkHistory> GetOriginalWorkHistoryByPersonID(string personID);
        OriginalWorkHistory GetOriginalWorkHistoryByID(string _orginalWorkHistoryID);
        void AddOriginalWorkHistory(params OriginalWorkHistory[] _originalWorkHistory);
        void UpdateOriginalWorkHistory(OriginalWorkHistory _originalWorkHistory);
        void RemoveOriginalWorkHistory(OriginalWorkHistory _originalWorkHistory);

        IList<PayrollFlag> GetAllPayrollFlag();
        IList<PayrollFlag> GetPayrollFlagByID(string employee_ID);
        PayrollFlag GetPayrollFlagByIDAndvalidFrom(string employee_ID, DateTime validfrom);
        void AddPayrollFlag(params PayrollFlag[] _payrollFlag);
        void UpdatePayrollFlag(PayrollFlag _payrollFlag);
        void RemovePayrollFlag(PayrollFlag _payrollFlag);

        IList<Education> GetAllEducations();
        IList<Education> GetEducationByPersonID(string person_Id);
        Education GetEducationByID(string person_ID,string educationLevel_ID,string educationQualification_ID);
        void AddEducation(params Education[] education);
        void UpdateEducation(Education education);
        void RemoveEducation(Education education);

        IList<EducationLevel> GetAllEducationLevels();
        EducationLevel GetEducationLevelByID(string educationLevel_ID);

        IList<EducationQualification> GetAllEducationQualifications();
        EducationQualification GetEducationQualificationByID(string educationQualification_ID);
        void AddEducationQualification(params EducationQualification[] educationQualification);
        void UpdateEducationQualification(EducationQualification educationQualification);
        void RemoveEducationQualification(EducationQualification educationQualification);

        IList<Institute> GetAllInstitutes();
        Institute GetInstituteByID(string institute_ID);

        IList<Major> GetAllMajors();
        Major GetMajorByID(string major_ID);
        void AddMajor(params Major[] major);
        void UpdateMajor(Major major);
        void RemoveMajor(Major major);
       
        IList<MaritalStatu> GetAllMaritalStatus();
        MaritalStatu GetMaritalStatusByID(string maritalStatus_ID);

        IList<Nationality> GetAllNationality();
        Nationality GetNationalityByID(string nationality_ID);

        IList<Race> GetAllRace();
        Race GetRaceByID(string race_ID);

        IList<Religion> GetAllReligion();
        Religion GetReligionByID(string religion_ID);

        //AddressType
        IList<AddressType> GetAllAddressTypes();
        AddressType GetAddressTypeByName(string AddressTypeName);
        void AddAddressType(params AddressType[] addressTypes);
        void UpdateAddressType(AddressType addressTypes);
        void RemoveAddressType(AddressType addressType);

        //Address
        //IList<Address> GetAllAddresss();
        IList< Address> GetAddressByPerson(string person_ID);
        void AddAddress(params Address[] address);
        void UpdateAddress(Address address);
        void RemoveAddress(string addressType, string person_ID);

        //Blood
        IList<Blood> GetAllBloods();
        Blood GetBloodByID(string blood_ID);
        void AddBlood(params Blood[] blood);
        void UpdateBlood(Blood blood);
        void RemoveBlood(Blood blood);

        //CropSetup
        IList<CropSetup> GetAllCropSetups();
        CropSetup GetCropSetup(int crop);
        void AddCropSetup(params CropSetup[] cropSetup);
        void UpdateCropSetup(CropSetup cropSetup);
        void RemoveCropSetup(CropSetup cropSetup);

        //Holiday     
        IList<Holiday> GetHolidayByCrop(int crop);
        Holiday GetHoliday(DateTime HolidayDate);
        void AddHoliday(params Holiday[] holiday);
        void UpdateHoliday(Holiday holiday);
        void RemoveHoliday(Holiday holiday);

        //LeaveType
        IList<LeaveType> GetAllLeaveTypes();
        LeaveType GetLeaveType(string leaveType);
        void AddLeaveType(params LeaveType[] leaveType);
        void UpdateLeaveType(LeaveType leaveType);
        void RemoveLeaveType(LeaveType leaveType);

        //LeaveTypeBenefits
        //IList<LeaveTypeBenefit> GetLeaveTypeBenefitsByPositionType(string positionType_ID);
        //void AddLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits);
        //void UpdateLeaveTypeBenefits(params LeaveTypeBenefit[] leaveTypeBenefits);
        //void RemoveLeaveTypeBenefits(LeaveTypeBenefit leaveTypeBenefits);


        //Shift
        IList<Shift> GetAllShifts();
        Shift GetShift(string Shift_ID);
        void AddShift(params Shift[] shift);
        void UpdateShift(Shift shift);
        void RemoveShift(Shift shift);


        //ShiftControl
        IList<ShiftControl> GetAllShiftControls();
        IList<ShiftControl> GetShiftControlByEmployee(string EmployeeID);
        void AddShiftControl(params ShiftControl[] shiftControl);
        void UpdateShiftControl(params ShiftControl[] shiftControl);
        void RemoveShiftControl(ShiftControl shiftControl);

        //Branch
        IList<Branch> GetAllBranchs();
        Branch GetBranch(string branch);
        void AddBranch(params Branch[] branch);
        void UpdateBranch(Branch branch);
        void RemoveBranch(Branch branch);

        //JobTitle
        IList<JobTitle> GetAllJobTitles();
        JobTitle GetJobTitle(string JobTitle);
        void AddJobTitle(params JobTitle[] jobTitle);
        void UpdateJobTitle(JobTitle jobTitle);
        void RemoveJobTitle(JobTitle jobTitle);

        //PositionType
        IList<PositionType> GetAllPositionTypes();
        PositionType GetPositionType(string positionType);
        void AddPositionType(params PositionType[] positionType);
        void UpdatePositionType(PositionType positionType);
        void RemovePositionType(PositionType positionType);


        //Position
        IList<Position> GetAllPositions();
        Position GetPosition(string position);
        void AddPosition(params Position[] position);
        void UpdatePosition(Position position);
        void RemovePosition(Position position);

        //PositionOrganization
        IList<PositionOrganization> GetAllPositionOrganizations();
        IList<PositionOrganization> GetPositionOrganizationByOrganization_ID(string organization_ID);
        PositionOrganization GetPositionOrganization(string positionOrganization_ID);
        PositionOrganization GetPositionOrganizationRootNode();
        void AddPositionOrganization(params PositionOrganization[] positionOrganization);
        void UpdatePositionOrganization(params PositionOrganization[] positionOrganization);
        void RemovePositionOrganization(PositionOrganization positionOrganization);

        //OrganizationUnit
        IList<OrganizationUnit> GetAllOrganizationUnits();
        OrganizationUnit GetOrganizationUnit(string organizationUnit);
        void AddOrganizationUnit(params OrganizationUnit[] organizationUnit);
        void updateOrganizationUnit(OrganizationUnit organizationUnit);
        void RemoveOrganizationUnit(OrganizationUnit organizationUnit);

        //OrganizationType
        IList<OrganizationType> GetAllOrganizationTypes();
        OrganizationType GetOrganizationType(string organizationType);
        void AddOrganizationType(params OrganizationType[] organizationType);
        void UpdateOrganizationType(OrganizationType organizationType);
        void RemoveOrganizationType(OrganizationType organizationType);


        //Organization
        IList<Organization> GetAllOrganizations();
        Organization GetOrganization(Organization organization);
        Organization GetOrganizationByOrganizationID(string organizationID);
        //Organization Get
        IList<Organization> GetAllDepartments();
        //IList< Organization> GetSection();       
        void AddOrganization(params Organization[] organization);
        void UpdateOrganization(Organization organization);
        void RemoveOrganization(Organization organization);

        //Person
        IList<Person> GetAllPersons();
        
        Person GetPerson(string person);       
        void AddPerson(params Person[] person);
        void UpdatePerson(Person person);
        void RemovePerson(Person person);

        //Employee
        IList<Employee> GetAllEmployees();
        IList<Employee> GetPersonHistory(string person_ID);
        Employee GetEmployeeInformation(string employee);
        Employee GetEmployeeByEmail(string _email);
        Employee GetEmployeeByShift(string shift);
        //Employee SearchEmployeeByFirstNameTH(string employee);
        void AddEmployee(params Employee[] employee);
        void UpdateEmployee(Employee employee);
        void RemoveEmployee(Employee employee);

        //EmployeePosition
        IList<EmployeePosition> GetAllEmployeePositions();
        IList<EmployeePosition> GetPositionByEmployee(string employee);
        IList<EmployeePosition> GetEmployeePostionByPositionOrganizationID(string positionOrganization_ID); 
        void AddEmployeePosition(params EmployeePosition[] employeePosition);
        void UpdateEmployeePosition(params EmployeePosition[] employeePosition);
        void RemoveEmployeePosition(EmployeePosition employeePosition);

        //ManualWorkDateType
        IList<ManualWorkDateType> GetAllManualWorkDateTypes();
        //void AddGender(params Gender[] gender);
        //void UpdateGender(params Gender[] gender);
        //void RemoveGender(Gender gender);
        
        //ManualWorkDate
        IList<ManualWorkDate> GetAllManualWorkDates();
        ManualWorkDate GetManualWorkDateByEmployeeAndType(string _employee_ID, string _manualWorkDateType, DateTime _manualWorkDate);
        void AddManualWorkDate(params ManualWorkDate[] manualWorkDate);
        void UpdateManualWorkDate(params ManualWorkDate[] manualWorkDate);
        void RemoveManualWorkDate(ManualWorkDate manualWorkDate);

        //Gender
        IList<Gender> GetAllGender();
        void AddGender(params Gender[] gender);
        void UpdateGender(params Gender[] gender);
        void RemoveGender(Gender gender);

        //TitleName
        IList<TitleName> GetAllTitleName();
        void AddTitleName(params TitleName[] titlename);
        void UpdateTitleName(params TitleName[] titlename);
        void RemoveTitleName(TitleName titlename);

        //Leave
        IList<Leave> GetAllLeaves();
        IList<Leave> GetLeaveByEmployee(string employee);
        IList<Leave> GetLeaveByLeaveDate(DateTime LeaveDate);
        void AddLeave(params Leave[] leave);
        void UpdateLeave(params Leave[] leave);
        void RemoveLeave(Leave leave);

        //WorkUpCountry
        IList<WorkUpCountry> GetAllWorkUpCountry();
        IList<WorkUpCountry> GetWorkUpCountryByEmployee(string employee);
        void AddWorkUpcountry(params WorkUpCountry[] workUpCountry);
        void UpdateWorkUpcountry(params WorkUpCountry[] workUpCountry);
        void RemoveWorkUpcountry(WorkUpCountry workUpCountry);

        //OT
        IList<OT> GetAllOTs();
        IList<OT> GetOTByEmployee(string employee);
        OT GetOTByEmployeeOneRow(string employee_ID, DateTime ot_Date, string ot_Type);
        OT GetOTByEmployeeOneRow(string employee_ID, DateTime ot_Date, string ot_Type, string requestType);
        void AddOT(params OT[] ot);
        void UpdateOT(params OT[] ot);
        void RemoveOT(OT ot);

        //OTonlineSetupAdmin
        IList<OTOnlineSetupAdmin> GetAllOTOnlineSetupAdmins();
        OTOnlineSetupAdmin GetOTonlineSetuAdminByEmployee(string _employee_id);
        OTOnlineSetupAdmin GetOTOnlineSetupAdminByAdminID(int _adminID);
        OTOnlineSetupAdmin GetOTOnlineSetupAdminByADName(string _adName);
        void AddOTOnlineSetupAdmin(params OTOnlineSetupAdmin[] _ot);
        void UpdateOTOnlineSetupAdmin( OTOnlineSetupAdmin _ot);
        void RemoveOTOnlineSetupAdmin(OTOnlineSetupAdmin _ot);

        //OTonlineSetupAdminDetail
        IList<OTOnlineSetupAdminDetail> GetAllOTOnlineSetupAdminDetails();
        //IList<OTOnlineSetupAdminDetail> GetOTonlineSetuAdminDetailByEmployee(string _employee_id);
        IList<OTOnlineSetupAdminDetail> GetOTonLineSetupAdminDetailByAdminID(int _adminID);
        OTOnlineSetupAdminDetail GetOTOnlineSetupAdminDetailByAdminAndDeptID(int _admin, string _deptID);
        IList<OTOnlineSetupAdminDetail> GetOTOnlineSetupAdminDetailbyReportTo(string _employee_ID);
        void AddOTOnlineSetupAdminDetail(params OTOnlineSetupAdminDetail[] _ot);
        void UpdateOTOnlineSetupAdminDetail( OTOnlineSetupAdminDetail _ot);
        void RemoveOTOnlineSetupAdminDetail(OTOnlineSetupAdminDetail _ot);


        //IDCardInfo
        IList<IDCardInfo> GetAllIDCard();
        IList<IDCardInfo> GetIDCardInfo(string person_ID);
        IDCardInfo GetIDCardByPerson_id(string person_ID);
        
        //IList<OT> GetOTByEmployee(string employee);
        void AddIDCardInfo(params IDCardInfo[] idCardInfo);
        void UpdateIDCardInfo(params IDCardInfo[] idCardInfo);
        void RemoveIDCardInfo(IDCardInfo idCardInfo);

        //Province
        Province GetProvince(string province_ID);
        IList<Province> GetAllProvinces();
        void AddProvince(params Province[] province);
        void UpdateProvince(params Province[] province);
        void RemoveProvince(Province province);

        //District
        District GetDistrict(string district_ID);
        IList<District> GetDistrictByProvince(string province_ID);

        IList<District> GetAllDistrict();
        void AddDistrict(params District[] district);
        void UpdateDistrict(params District[] district);
        void RemoveDistrict(District district);

        //SubDistrict
        SubDistrict GetSubDistrict(string subDistrict_ID);
        IList<SubDistrict> getAllSubDistrict();
        IList<SubDistrict> GetSubDistrictByDistrict(string District_ID);
        void AddSubDistrict(params SubDistrict[] subDistrict);
        void UpdateSubDistrict(params SubDistrict[] subDistrict);
        void RemoveSubDistrict(SubDistrict subDistrict);

        //Country
        IList<Country> GetAllCountrys();
        Country GetCountry(string country_ID);
        void AddCountry(params Country[] country);
        void UpdateCountry(params Country[] country);
        void RemoveCountry(Country country);

        //TransactionLog
        IList<TransactionLog> GetAllTransactionLogs();
        void AddTransactionLog(params TransactionLog[] transactionLog);
        IList<TransactionLog> GetTransactionLogByTable_Id(string table_ID);

        //Tables
        IList<Table> GetAllTables();
        

        //VitalStatus
        IList<VitalStau> GetAllVitalStatuss();
        VitalStau GetVitalStatusByID(string vitalStatus_ID);

        //FamilyMemberType
        IList<FamilyMemberType> GetAllFamilyMemberTypes();
        FamilyMemberType GetFamilyMemberTypeByID(string familyMemberType_ID);
        void AddFamilyMemberType(params FamilyMemberType[] familyMemeberType);
        void UpdateFamilyMemberType(FamilyMemberType familyMemeberType);
        void RemoveFamilyMemberType(FamilyMemberType familyMemberType);


        //FamilyMember
        IList<FamilyMember> GetAllFamilyMembers();
        FamilyMember GetFamilyMemberByID(string familyMember_ID);
        IList<FamilyMember> GetFamilyMemberByPerson_ID(string person_ID);
        void AddFamilyMember(params FamilyMember[] familyMember);
        void UpdateFamilyMember(FamilyMember familyMember);
        void RemoveFamilyMember(FamilyMember familyMember);
    }
}
