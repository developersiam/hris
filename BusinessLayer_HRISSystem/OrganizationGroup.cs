﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel_HRISSystem;

namespace BusinessLayer_HRISSystem
{
  public  class OrganizationGroup : Organization
    {
        //public int OrgCurrentID { get; set; }
        public int? STECParentID { get; set; }
        //public string OrgCode { get; set; }
        public string OrgName { get; set; }
        //public  string Employee_ID { get; set; }
        public string EmpName { get; set; }
        public List<OrganizationGroup> Children { get; set; }
        public bool ShowOrganization { get; set; }
        public OrganizationGroup()
        {
            ShowOrganization = true;
        }
    }
}
