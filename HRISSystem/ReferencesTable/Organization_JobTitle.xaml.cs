﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_JobTitle.xaml
    /// </summary>
    public partial class Organization_JobTitle : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XFromPage;
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        public Organization_JobTitle()
        {
            InitializeComponent();
        }

        public Organization_JobTitle(string _xfromPage)
        {
            InitializeComponent();
            XFromPage = _xfromPage;
        }

        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                txtJobTitle_ID.IsReadOnly = false;
                txtJobTitle_ID.Background = new SolidColorBrush(Colors.White);
                txtJobTitle_ID.Text = "";
                txtJobTitleName.Text = "";
                txtNoted.Text = "";

                //ValidFromDatePicker.Text = Convert.ToString( DateTime.Now);
                //EndDateDatePicker.Text = Convert.ToString(DateTime.Now);   
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllJobTitles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllJobTitles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private static string SubRight(string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                string X_ID;
                List<JobTitle> Xvalue;
                Xvalue = _hrisBusinessLayer.GetAllJobTitles().ToList();
                if (Xvalue.Count > 0)
                {
                    X_ID = Convert.ToString( Convert.ToInt32( SubRight( Xvalue.Max(c=>c.JobTitle_ID),4))+1);
                    X_ID = "J" + string.Format("{0:0000}", Convert.ToInt32(X_ID));
                    
                }
                else
                {
                    X_ID = "J0001";
                }
                txtJobTitle_ID.Text = X_ID;

                JobTitle[] queryObj = new JobTitle[1];
                JobTitle query1 = new JobTitle();  // created 1 instance or object

                query1.JobTitle_ID = txtJobTitle_ID.Text;
                query1.JobTitleName = txtJobTitleName.Text;
                query1.Noted = txtNoted.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.Enddate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;
                queryObj[0] = query1;
                _hrisBusinessLayer.AddJobTitle(queryObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T015";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Oranization_ID";
                transactionLog1.PKFields = txtJobTitle_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtJobTitleName.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                JobTitle query1 = new JobTitle();  // created 1 instance or object
                JobTitle query2 = new JobTitle();  // created 1 instance or object

                query2 = (JobTitle)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.JobTitle_ID = txtJobTitle_ID.Text;
                query1.JobTitleName = txtJobTitleName.Text;
                query1.Noted = txtNoted.Text;
                query1.CreatedDate = query2.CreatedDate;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.Enddate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateJobTitle(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T015";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Oranization_ID";
                transactionLog1.PKFields = txtJobTitle_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = query2.JobTitleName;
                transactionLog1.NewData = txtJobTitleName.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                JobTitle query1 = new JobTitle();  // created 1 instance or object
                query1 = (JobTitle)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveJobTitle(query1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T015";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Oranization_ID";
                transactionLog1.PKFields = txtJobTitle_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = txtJobTitleName.Text;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                JobTitle query1 = new JobTitle();  // created 1 instance or object
                query1 = (JobTitle)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtJobTitle_ID.IsReadOnly = false;
                    txtJobTitle_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtJobTitle_ID.IsReadOnly = true;
                    txtJobTitle_ID.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
