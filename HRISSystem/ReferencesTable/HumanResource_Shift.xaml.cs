﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for HumanResource_Shift.xaml
    /// </summary>
    public partial class HumanResource_Shift : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        
        public HumanResource_Shift()
        {
            InitializeComponent();
        }

        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                TxtShift_ID.IsReadOnly = false;
                TxtShift_ID.Background = new SolidColorBrush(Colors.White);
                TxtShift_ID.Text = "";
                TxtStartTime.Text = "";
                TxtEndTime.Text = "";

                //ValidFromDatePicker.Text = Convert.ToString( DateTime.Now);
                //EndDateDatePicker.Text = Convert.ToString(DateTime.Now);   
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllShifts();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if(string.IsNullOrEmpty(TxtStartTime.Text) || string.IsNullOrEmpty(TxtEndTime.Text))
                {
                    MessageBox.Show("กรุณาคีย์เวลาที่เข้างาน - ออกงาน");
                    return;
                }
                TimeSpan StartTime;
                TimeSpan EndTime;
                if (!TimeSpan.TryParse(TxtStartTime.Text, out StartTime) || !TimeSpan.TryParse(TxtEndTime.Text, out EndTime))
                {
                    MessageBox.Show("รูปแบบเวลาไม่ถูกต้อง ตัวอย่าง 01:30 หรือ 10:00 เป็นต้น");
                    return;
                }


                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDatePicker.Text == "")
                    EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                Shift[] queryObj = new Shift[1];
                Shift query1 = new Shift();  // created 1 instance or object

                query1.Shift_ID = TxtShift_ID.Text;
                //query1.StartTime = TimeSpan.Parse(TxtStartTime.Text);
                //query1.EndTime = TimeSpan.Parse(TxtEndTime.Text);
                query1.StartTime = StartTime;
                query1.EndTime = EndTime;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;

                queryObj[0] = query1;
                _hrisBusinessLayer.AddShift(queryObj);
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TxtStartTime.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์เวลาที่เข้างาน");
                    return;
                }
                if (TxtEndTime.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์เวลาที่ออกงาน");
                    return;
                }

                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDatePicker.Text == "")
                    EndDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                Shift query1 = new Shift();  // created 1 instance or object
                Shift query2 = new Shift();
                query2 = (Shift)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.Shift_ID = TxtShift_ID.Text;
                query1.StartTime = TimeSpan.Parse(TxtStartTime.Text);
                query1.EndTime = TimeSpan.Parse(TxtEndTime.Text);
                query1.CreatedDate = query2.CreatedDate;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateShift(query1);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Shift query1 = new Shift();  // created 1 instance or object
                query1 = (Shift)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemoveShift(query1);
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllShifts();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Shift query1 = new Shift();  // created 1 instance or object
                query1 = (Shift)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    TxtShift_ID.IsReadOnly = false;
                    TxtShift_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    TxtShift_ID.IsReadOnly = true;
                    TxtShift_ID.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
