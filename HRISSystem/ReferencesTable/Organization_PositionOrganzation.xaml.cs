﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem.BusinessObject;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_PositionOrganzation.xaml
    /// </summary>
    public partial class Organization_PositionOrganzation : Page
    {
        //private string XParentID;  
        public List<Position> _xposition;
        private string XFromOrganization_ID;
        public List<TransactionLog> transactionLog;
        PositionOrganization _xSelectedPositionOrganization;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        TreeViewPositionOrganizationModel TreeViewModel_After;
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        private class CurrentFlag //class
        {
            public bool CurrentFlags { get; set; }  //attribute
            public string CurrentFlagName { get; set; } //attribute
        }

        private class PositionLevel
        {
            public int LevelID { get; set; }
            public string LevelName { get; set; }
        }
       
        public Organization_PositionOrganzation()
        {
            InitializeComponent();        
            ShowData();
        }

        public Organization_PositionOrganzation(string _xFromOrganization)
        {
            InitializeComponent();           
            Organization xquery = new Organization();
            xquery = _hrisBusinessLayer.GetOrganizationByOrganizationID(_xFromOrganization);
            txtDepartment.Text = xquery.OrganizationUnit.OrganizationUnitName;
            XFromOrganization_ID = _xFromOrganization;
            ShowData();

        }
        public void ShowData()
        {
            IList<JobTitle> jobTitle = _hrisBusinessLayer.GetAllJobTitles();
            cmbJobTitle_ID.ItemsSource = jobTitle;
            cmbJobTitle_ID.SelectedIndex = 0;


            //create new instant           
            CurrentFlag h1 = new CurrentFlag { CurrentFlags = true, CurrentFlagName = "Active" };
            CurrentFlag h2 = new CurrentFlag { CurrentFlags = false, CurrentFlagName = "Not Active" };
            List<CurrentFlag> currentFlagList = new List<CurrentFlag>();
            currentFlagList.Add(h1);
            currentFlagList.Add(h2);
            cmbCurrentFlag.ItemsSource = currentFlagList;
            cmbCurrentFlag.SelectedIndex = 0;


            PositionLevel h3 = new PositionLevel { LevelID = 1, LevelName = "Top" };
            PositionLevel h4 = new PositionLevel { LevelID = 2, LevelName = "Middle" };
            PositionLevel h5 = new PositionLevel { LevelID = 3, LevelName = "Lower" };
            PositionLevel h6 = new PositionLevel { LevelID = 4, LevelName = "Lowest" };
            List<PositionLevel> positionLevelList = new List<PositionLevel>();
            positionLevelList.Add(h3);
            positionLevelList.Add(h4);
            positionLevelList.Add(h5);
            positionLevelList.Add(h6);
            cmbPositionLevel.ItemsSource = positionLevelList;
            cmbPositionLevel.SelectedIndex = 0;


            IList<PositionType> positionType = _hrisBusinessLayer.GetAllPositionTypes();
            cmbPositionType_ID.ItemsSource = positionType;

            IList<Position> xquery = _hrisBusinessLayer.GetAllPositions();
            cmbPosition_ID.ItemsSource = null;
            cmbPosition_ID.ItemsSource = xquery;

          
        }
        public void ClearData()
        {
            try
            {   
                AddButton.IsEnabled = true;
                ShowData();
                showPositionOrg();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (cmbPosition_ID.Text == null || cmbPosition_ID.Text == "")
                {
                    MessageBox.Show("การเลือก Position", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtCountOfPosition.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จำนวนพนักงานต้องการของตำแหน่งนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
               
                if (cmbCurrentFlag.Text == "")
                {
                    MessageBox.Show("กรุณาเลือก Current flag", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);


                //check ข้อมูลว่ามี parent ที่เลือกหรือไม่  หากไม่มีให้ get ค่า root node โดย select where  stecPositionNode is null
                if (_xSelectedPositionOrganization == null)
                {
                    _xSelectedPositionOrganization = _hrisBusinessLayer.GetPositionOrganizationRootNode();
                }

                //วนลูปตามจำนวนตำแหน่งที่ระบุ
                for (int i = 1; i <= Convert.ToInt16(txtCountOfPosition.Text); i++)
                {
                    Int64 XMaxPositionOrganization_ID;
                    Int64 XMax2;
                    List<PositionOrganization> XOrg;
                    XOrg = _hrisBusinessLayer.GetPositionOrganizationByOrganization_ID(XFromOrganization_ID).ToList();
                    if (XOrg.Count > 0)
                    {
                        XMax2 = Convert.ToInt64(XOrg.Max(c => c.PositionOrganization_ID)) + 1;
                        XMaxPositionOrganization_ID = XMax2;
                    }
                    else
                    {
                        XMaxPositionOrganization_ID = Convert.ToInt64(XFromOrganization_ID + "0001");
                    }
                    txtPositionOrganization_ID.Text = Convert.ToString(XMaxPositionOrganization_ID);

                    PositionOrganization[] positionObj = new PositionOrganization[1];
                    PositionOrganization position1 = new PositionOrganization(); //created  1 instanct or object
                    position1.PositionOrganization_ID = txtPositionOrganization_ID.Text;
                    position1.Position_ID = (string)cmbPosition_ID.SelectedValue;
                    position1.Organization_ID = XFromOrganization_ID;
                    position1.JobTitle_ID = (string)cmbJobTitle_ID.SelectedValue;
                    position1.CurrentFlag = (Boolean)cmbCurrentFlag.SelectedValue;
                    position1.PositionNode = _xSelectedPositionOrganization.PositionOrganization_ID;
                    position1.ParentPositionBusinessNode_ID = _xSelectedPositionOrganization.PositionOrganization_ID;
                    position1.STECPositionNode_ID = _xSelectedPositionOrganization.PositionOrganization_ID;
                    position1.PositionLevel = (string)cmbPositionLevel.Text;
                    position1.PositionType_ID = (string)cmbPositionType_ID.SelectedValue;
                    position1.CreatedDate = DateTime.Now;
                    position1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                    position1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                    position1.ModifiedDate = DateTime.Now;
                    positionObj[0] = position1;
                    _hrisBusinessLayer.AddPositionOrganization(positionObj);

                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                    transactionLog1.TransactionType_ID = "A";
                    transactionLog1.Table_ID = "T014";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "PositionOranization_ID";
                    transactionLog1.PKFields = txtPositionOrganization_ID.Text;
                    transactionLog1.FKFields = "";
                    transactionLog1.OldData = "";
                    transactionLog1.NewData = "";
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                }
                
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbPosition_ID.Text == null || cmbPosition_ID.Text == "")
                {
                    MessageBox.Show("การเลือก Position", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (txtCountOfPosition.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์จำนวนพนักงานต้องการของตำแหน่งนี้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (cmbCurrentFlag.Text == "")
                {
                    MessageBox.Show("การเลือก Current flag", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                PositionOrganization position1 = new PositionOrganization(); //created  1 instanct or object
                position1.PositionOrganization_ID = _xSelectedPositionOrganization.PositionOrganization_ID;
                position1.Position_ID = (string)cmbPosition_ID.SelectedValue;
                position1.Organization_ID = _xSelectedPositionOrganization.Organization_ID;
                position1.JobTitle_ID = (string)cmbJobTitle_ID.SelectedValue;
                position1.CurrentFlag = (Boolean)cmbCurrentFlag.SelectedValue;
                position1.PositionNode = _xSelectedPositionOrganization.PositionNode;
                position1.ParentPositionBusinessNode_ID = _xSelectedPositionOrganization.ParentPositionBusinessNode_ID;
                position1.PositionLevel = cmbPositionLevel.Text;
                position1.PositionType_ID = (string)cmbPositionType_ID.SelectedValue;
                position1.CreatedDate = _xSelectedPositionOrganization.CreatedDate;
                position1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                position1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                position1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdatePositionOrganization(position1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T014";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "PositionOranization_ID";
                transactionLog1.PKFields = txtPositionOrganization_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = _xSelectedPositionOrganization.PositionLevel + "-" + _xSelectedPositionOrganization.PositionType_ID + "-" + _xSelectedPositionOrganization.CurrentFlag + _xSelectedPositionOrganization.JobTitle_ID;
                transactionLog1.NewData = cmbPositionLevel.Text + "-" + cmbPositionType_ID.SelectedItem + "-" + cmbCurrentFlag.Text + "-" + cmbJobTitle_ID.SelectedItem;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
            
                
                PositionOrganization query1 = _hrisBusinessLayer.GetPositionOrganization(txtPositionOrganization_ID.Text);

                //check ว่าหากมีการนำ positionorganization_id นี้ไปใช้ใน  employeeposition แล้วจะไม่อนุญาตให้ลบข้อมูล
                List< EmployeePosition> query2 = _hrisBusinessLayer.GetEmployeePostionByPositionOrganizationID(txtPositionOrganization_ID.Text).ToList();
                if (query2.Count > 0)
                {
                    MessageBox.Show("รหัสนี้มีการนำไปใช้กับพนักงานแล้ว ไม่สามารถลบข้อมูลได้", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                _hrisBusinessLayer.RemovePositionOrganization(query1);


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T014";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "PositionOranization_ID";
                transactionLog1.PKFields = txtPositionOrganization_ID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = _xSelectedPositionOrganization.PositionLevel + "-" + _xSelectedPositionOrganization.PositionType_ID + "-" + _xSelectedPositionOrganization.CurrentFlag + "-"+_xSelectedPositionOrganization.JobTitle_ID;
                transactionLog1.NewData = cmbPositionLevel.Text + "-" + cmbPositionType_ID.SelectedValue + "-" + cmbCurrentFlag.Text + "-" + cmbJobTitle_ID.SelectedValue;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLog1.Noted = "Position " + "-" + _xSelectedPositionOrganization.Position.PositionNameEN; 
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);


                txtPositionOrganization_ID.Text = "";
                txtCountOfPosition.Text = "";
                txtParent.Text = "";

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }           
        private void showPositionOrg()
        {
            bool xChkNode = false;

            IList<PositionOrganization> OrgList = _hrisBusinessLayer.GetPositionOrganizationByOrganization_ID(XFromOrganization_ID);

            //List<PositionOrganizationTreeView> positionList = (from o in _hrisBusinessLayer.GetPositionOrganizationByOrganization_ID(XFromOrganization_ID)
            //                                                   group o by new { o.Position_ID, o.Position.PositionNameEN, o.STECPositionNode_ID } into g
            //                                                   select new PositionOrganizationTreeView
            //                                                   {
            //                                                       Position_ID = g.Key.Position_ID,
            //                                                       Position_Name = g.Key.PositionNameEN,
            //                                                       STECPositionOrganizationID2 = Convert.ToInt64( g.Key.STECPositionNode_ID),                                                                 
            //                                                       CountNode = g.Count()                                                                  
            //                                                   }).ToList();


            //var flatList = new List<PositionOrganizationGroup>();
            //foreach(PositionOrganizationTreeView i in positionList)
            //{
            //    flatList.Add(new PositionOrganizationGroup()
            //    {
            //        Position_ID = i.Position_ID
            //        ,Position_ID2 = i.Position_ID
            //        ,STECPositionOrganizationID2 = i.STECPositionOrganizationID2 
            //        ,STECPositionNode_ID = Convert.ToString( i.STECPositionOrganizationID2)
            //        ,Position_Name = i.Position_Name
            //        ,PositionOrganization_ID = "1"
            //    });    
            //}

            //foreach (PositionOrganizationGroup i in flatList)
            //    {
            //        if (i.STECPositionOrganizationID2  == 0)
            //        {
            //            xChkNode = true;
            //        }
                   
            //    }

                //ถ้าไม่มี stecpositionnode_id == null ให้ add ไปอีก 
            //if (xChkNode == false)
            //{
            //    //List<PositionOrganization> xquery = _hrisBusinessLayer.GetAllPositionOrganizations().Where(e => e.STECPositionNode_ID == null).ToList();
            //    //ถ้ายังไม่มี node ให้ไปเอา PositionOrganization  where  stecpositionnode_id isnull
            //    List<PositionOrganizationTreeView> xquery = (from o in _hrisBusinessLayer.GetAllPositionOrganizations().Where(e =>e.STECPositionNode_ID==null)
            //                                                       group o by new { o.Position_ID, o.Position.PositionNameEN, o.STECPositionNode_ID } into g
            //                                                       select new PositionOrganizationTreeView
            //                                                       {
            //                                                           Position_ID = g.Key.Position_ID,
            //                                                           Position_Name = g.Key.PositionNameEN,
            //                                                           STECPositionOrganizationID2 = Convert.ToInt64(g.Key.STECPositionNode_ID),                                                               
            //                                                           CountNode = g.Count()                                                                       
            //                                                       }).ToList();
            //    foreach (PositionOrganizationTreeView i in xquery)
            //    {
            //        flatList.Add(new PositionOrganizationGroup()
            //        {
            //            Position_ID = i.Position_ID
            //            ,
            //            Position_ID2 = i.Position_ID
            //            ,
            //            STECPositionOrganizationID2 = i.STECPositionOrganizationID2
            //            ,
            //            STECPositionNode_ID = Convert.ToString(i.STECPositionOrganizationID2)
            //            ,Position_Name = i.Position_Name
            //            ,PositionOrganization_ID = "1"


            //        });
            //    }
            //}

            //var tree = flatList.BuildPositionTree();
            //TreeView1.ItemsSource = tree;
            //TreeViewModel_After = new TreeViewPositionOrganizationModel();
            //TreeViewModel_After.Items = new List<PositionOrganizationGroup>();

            //*******************************************



            if (OrgList.Count <= 0)
            {
                List<PositionOrganization> xquery = _hrisBusinessLayer.GetAllPositionOrganizations().Where(e => e.STECPositionNode_ID == null).ToList();
                //ถ้ายังไม่มี node ให้ไปเอา PositionOrganization  where  stecpositionnode_id isnull
                foreach (PositionOrganization i in xquery)
                {
                    txtParent.Text = i.PositionOrganization_ID;
                }
            }
            else
            {
                var flatList = new List<PositionOrganizationGroup>();
                foreach (PositionOrganization i in OrgList)
                {
                    flatList.Add(new PositionOrganizationGroup()
                    {
                        PositionOrganization_ID = i.PositionOrganization_ID
                        ,
                        STECPositionParentID = i.STECPositionNode_ID == null ? 0 : Convert.ToInt64(i.STECPositionNode_ID)
                        ,
                        STECPositionNode_ID = i.STECPositionNode_ID == null ? "" : i.STECPositionNode_ID
                        ,
                        ParentPositionBusinessNode_ID = i.ParentPositionBusinessNode_ID
                        ,
                        Position_ID = i.Position_ID
                        ,
                        PositionName = i.Position.PositionNameEN == null ? "" : i.Position.PositionNameEN
                        ,
                        Organization_ID = i.Organization_ID == null ? "" : i.Organization_ID
                        ,
                        OrganizationUnit_ID = i.Organization.OrganizationUnit_ID == null ? "" : i.Organization.OrganizationUnit_ID
                        ,
                        OrganizationName = i.Organization.OrganizationUnit.OrganizationUnitName == null ? "" : i.Organization.OrganizationUnit.OrganizationUnitName
                        ,
                        JobTitle_ID = i.JobTitle_ID == null ? "" : i.JobTitle_ID
                        ,
                        CurrentFlag = i.CurrentFlag
                        ,
                        PositionNode = i.PositionNode
                        ,
                        PositionLevel = i.PositionLevel
                        ,
                        PositionType_ID = i.PositionType_ID == null ? "" : i.PositionType_ID
                        ,
                        CreatedDate = i.CreatedDate
                        ,
                        ValidFrom = i.ValidFrom
                        ,
                        EndDate = i.EndDate
                    });
                }

                foreach (PositionOrganizationGroup i in flatList)
                {
                    if (i.STECPositionParentID == 0)
                    {
                        xChkNode = true;
                    }

                }

                //ถ้าไม่มี stecpositionnode_id == null ให้ add ไปอีก 
                if (xChkNode == false)
                {
                    List<PositionOrganization> xquery = _hrisBusinessLayer.GetAllPositionOrganizations().Where(e => e.STECPositionNode_ID == null).ToList();
                    //ถ้ายังไม่มี node ให้ไปเอา PositionOrganization  where  stecpositionnode_id isnull
                    foreach (PositionOrganization i in xquery)
                    {
                        flatList.Add(new PositionOrganizationGroup()
                        {
                            PositionOrganization_ID = i.PositionOrganization_ID
                            ,
                            STECPositionParentID = i.STECPositionNode_ID == null ? 0 : Convert.ToInt64(i.STECPositionNode_ID)
                            ,
                            STECPositionNode_ID = i.STECPositionNode_ID == null ? "" : i.STECPositionNode_ID
                            ,
                            ParentPositionBusinessNode_ID = i.ParentPositionBusinessNode_ID
                            ,
                            Position_ID = i.Position_ID
                            ,
                            PositionName = i.Position.PositionNameEN == null ? "" : i.Position.PositionNameEN
                            ,
                            Organization_ID = i.Organization_ID == null ? "" : i.Organization_ID
                            ,
                            OrganizationUnit_ID = i.Organization.OrganizationUnit_ID == null ? "" : i.Organization.OrganizationUnit_ID
                            ,
                            OrganizationName = i.Organization.OrganizationUnit.OrganizationUnitName == null ? "" : i.Organization.OrganizationUnit.OrganizationUnitName
                            ,
                            JobTitle_ID = i.JobTitle_ID == null ? "" : i.JobTitle_ID
                            ,
                            CurrentFlag = i.CurrentFlag
                            ,
                            PositionNode = i.PositionNode
                            ,
                            PositionLevel = i.PositionLevel
                            ,
                            PositionType_ID = i.PositionType_ID == null ? "" : i.PositionType_ID
                            ,
                            CreatedDate = i.CreatedDate
                            ,
                            ValidFrom = i.ValidFrom
                            ,
                            EndDate = i.EndDate
                        });
                    }
                }
                var tree = flatList.BuildPositionTree();
                TreeView1.ItemsSource = tree;
                TreeViewModel_After = new TreeViewPositionOrganizationModel();
                TreeViewModel_After.Items = new List<PositionOrganizationGroup>();
            }                    
        }
        public TreeViewPositionOrganizationModel TreeModel
        {
            get
            {
                return TreeViewModel_After;
            }
        }

        Point _lastMouseDown;
        PositionOrganizationGroup draggedItem, _target;

        private void TreeView1_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                var selectedNode = (e.Source as TreeView).SelectedItem as PositionOrganizationGroup;

                _xSelectedPositionOrganization = _hrisBusinessLayer.GetPositionOrganization(selectedNode.PositionOrganization_ID);
                txtPositionOrganization_ID.Text = Convert.ToInt64(_xSelectedPositionOrganization.PositionOrganization_ID) == 0 ? "" : _xSelectedPositionOrganization.PositionOrganization_ID;
                cmbPosition_ID.SelectedValue = _xSelectedPositionOrganization.Position_ID;
                

                List<PositionOrganization> xquery1 = _hrisBusinessLayer.GetAllPositionOrganizations()
                    .Where(x => x.Position_ID == _xSelectedPositionOrganization.Position_ID
                        && x.Organization_ID == _xSelectedPositionOrganization.Organization_ID
                        && x.STECPositionNode_ID == _xSelectedPositionOrganization.STECPositionNode_ID).ToList();
                txtCountOfPosition.Text = xquery1.Count.ToString();
                cmbJobTitle_ID.SelectedValue = _xSelectedPositionOrganization.JobTitle_ID;
                cmbCurrentFlag.SelectedValue = _xSelectedPositionOrganization.CurrentFlag;
                txtParent.Text = _xSelectedPositionOrganization.STECPositionNode_ID;
                cmbPositionType_ID.SelectedValue = _xSelectedPositionOrganization.PositionType_ID;
                cmbPositionLevel.Text = _xSelectedPositionOrganization.PositionLevel;
                ValidFromDatePicker.Text = Convert.ToString(_xSelectedPositionOrganization.ValidFrom);
                EndDateDatePicker.Text = Convert.ToString(_xSelectedPositionOrganization.EndDate);

               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }     
        private void TreeView1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                _lastMouseDown = e.GetPosition(TreeView1);
            }
        }
        private bool CheckGridSplitter(UIElement element)
        {
            if (element is GridSplitter)
            {
                return true;
            }

            GridSplitter GridSplitter = FindParent<GridSplitter>(element);

            if (GridSplitter != null)
            {
                return true;
            }
            return false;

        }
        private void TreeView1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    UIElement element = e.OriginalSource as UIElement;
                    bool bGridSplitter = CheckGridSplitter(element);


                    Point currentPosition = e.GetPosition(TreeView1);


                    if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                        (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                    {
                        draggedItem = (PositionOrganizationGroup)TreeView1.SelectedItem;

                        if ((draggedItem != null) && !bGridSplitter)
                        {
                            DragDropEffects finalDropEffect = DragDrop.DoDragDrop(TreeView1, TreeView1.SelectedValue,
                                DragDropEffects.Move);
                            //Checking target is not null and item is dragging(moving)
                            if ((finalDropEffect == DragDropEffects.Move) && (_target != null))
                            {

                                // A Move drop was accepted
                                if (draggedItem.Organization_ID != _target.Organization_ID)
                                {
                                    CopyItem(draggedItem, _target);
                                    _target = null;
                                    draggedItem = null;
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TreeView1_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                Point currentPosition = e.GetPosition(TreeView1);


                if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                    (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                {
                    // Verify that this is a valid drop and then store the drop target
                    PositionOrganizationGroup item = GetNearestContainer(e.OriginalSource as UIElement);
                    if (CheckDropTarget(draggedItem, item))
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;
                    }
                }
                e.Handled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TreeView1_Drop(object sender, DragEventArgs e)
        {
            try
            {

                e.Effects = DragDropEffects.None;
                e.Handled = true;

                // Verify that this is a valid drop and then store the drop target
                PositionOrganizationGroup TargetItem = GetNearestContainer(e.OriginalSource as UIElement);
                if (TargetItem != null && draggedItem != null)
                {
                    _target = TargetItem;
                    e.Effects = DragDropEffects.Move;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool CheckDropTarget(PositionOrganizationGroup _sourceItem, PositionOrganizationGroup _targetItem)
        {
            //Check whether the target item is meeting your condition
            bool _isEqual = false;

            if (_sourceItem.Organization_ID != _targetItem.Organization_ID)
            {
                _isEqual = true;
            }

            return _isEqual;

        }
        private void CopyItem(PositionOrganizationGroup _sourceItem, PositionOrganizationGroup _targetItem)
        {
            //Asking user wether he want to drop the dragged TreeViewItem here or not
            if (MessageBox.Show("Would you like to drop  '" + _sourceItem.PositionName + "'  into '" + _targetItem.PositionName + "'", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    _targetItem.Children.Add(_sourceItem);

                    PositionOrganization query1 = new PositionOrganization();
                    query1.PositionOrganization_ID = _sourceItem.PositionOrganization_ID ;
                    query1.Position_ID = _sourceItem.Position_ID ;
                    query1.Organization_ID =  _sourceItem.Organization_ID ;
                    query1.JobTitle_ID = _sourceItem.JobTitle_ID;
                    query1.CurrentFlag = _sourceItem.CurrentFlag;
                    query1.PositionNode = _targetItem.PositionNode;
                    query1.ParentPositionBusinessNode_ID = _targetItem.ParentPositionBusinessNode_ID;
                    query1.STECPositionNode_ID = _targetItem.STECPositionNode_ID;
                    query1.PositionLevel = _sourceItem.PositionLevel;
                    query1.PositionType_ID = _sourceItem.PositionType_ID;
                    query1.CreatedDate = _sourceItem.CreatedDate;
                    query1.ValidFrom = _sourceItem.ValidFrom;
                    query1.EndDate = _sourceItem.EndDate;
                    query1.ModifiedDate = DateTime.Now;
                    _hrisBusinessLayer.UpdatePositionOrganization(query1);


                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    if (transactionLog.Count > 0)
                    {
                        XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                    }
                    else
                    {
                        XMaxTransactionLog_ID = 1;
                    }
                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    TransactionLog transactionLog1 = new TransactionLog();
                    transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                    transactionLog1.TransactionType_ID = "U";
                    transactionLog1.Table_ID = "T014";
                    transactionLog1.TransactionDate = DateTime.Now;
                    transactionLog1.FieldName = "PositionOrganization_ID";
                    transactionLog1.PKFields = _sourceItem.PositionOrganization_ID;
                    transactionLog1.FKFields = "";
                    transactionLog1.OldData = _sourceItem.STECPositionNode_ID + "-"+ _sourceItem.STECPositionNode_ID + "-" + _sourceItem.ParentPositionBusinessNode_ID ;
                    transactionLog1.NewData = _targetItem.Position_ID;
                    transactionLog1.ModifiedDate = DateTime.Now;
                    transactionLog1.ModifiedUser = _singleton.username;
                    transactionLogObj[0] = transactionLog1;
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                    ClearData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private PositionOrganizationGroup GetNearestContainer(UIElement element)
        {
            // Walk up the element tree to the nearest tree view item.
            TreeViewItem UIContainer = FindParent<TreeViewItem>(element);
            PositionOrganizationGroup NVContainer = null;

            if (UIContainer != null)
            {
                NVContainer = UIContainer.DataContext as PositionOrganizationGroup;
            }
            return NVContainer;
        }
        private static Parent FindParent<Parent>(DependencyObject child)
               where Parent : DependencyObject
        {
            DependencyObject parentObject = child;
            parentObject = VisualTreeHelper.GetParent(parentObject);

            //check if the parent matches the type we're looking for
            if (parentObject is Parent || parentObject == null)
            {
                return parentObject as Parent;
            }
            else
            {
                //use recursion to proceed with next level
                return FindParent<Parent>(parentObject);
            }
        }
        private static string SubRight(string value, int length)
        {
            return value.Substring(value.Length - length);
        }
        
        private void AddPosition_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_Position("PositionOrganization"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddJobTitle_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_JobTitle("PositionOrganization"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddPositionType_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new ReferencesTable.Organization_PositionType("PositionOrganization"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtCountOfPosition_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(txtCountOfPosition.Text , "[^0-9]"))
                {
                    MessageBox.Show("Please enter only numbers.");
                    txtCountOfPosition.Text = txtCountOfPosition.Text.Remove(txtCountOfPosition.Text.Length - 1);
                    txtCountOfPosition.SelectionStart = txtCountOfPosition.Text.Length;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToResetPostionOrganization_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_xSelectedPositionOrganization  == null )
                {
                    MessageBox.Show("กรุณาเลือกรายการที่ต้องการรีเซ็ตตำแหน่ง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                this.NavigationService.Navigate(new ReferencesTable.ResetupPositionOrganization(_xSelectedPositionOrganization.PositionOrganization_ID));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        

        
        
    }

   

    
  

}
