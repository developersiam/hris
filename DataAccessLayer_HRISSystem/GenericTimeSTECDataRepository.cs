﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Data;
using System.Data.Entity.Validation;

namespace DataAccessLayer_HRISSystem
{
   public class GenericTimeSTECDataRepository<T> : IGenericTimeSTECDataRepository<T> where T : class
    {
        public void AddTimeSTEC(params T[] items)
        {
            
            try
            {
                using (var context = new TimeSTECEntities())
                {
                    foreach (T item in items)
                    {
                        context.Entry(item).State = System.Data.Entity.EntityState.Added;

                    }
                    context.SaveChanges();
                }
            }
            catch(DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public IList<T> GetAllTimeSTEC(params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            List<T> list;
            using (var context = new TimeSTECEntities())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList<T>();
            }
            return list;
        }

        public IList<T> GetListTimeSTEC(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            List<T> list;
            using (var context = new TimeSTECEntities())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();
            }
            return list;
        }

        public T GetSingleTimeSTEC(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            //throw new NotImplementedException();
            T item = null;
            using (var context = new TimeSTECEntities())
            {
                IQueryable<T> dbQuery = context.Set<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
            }
            return item;
        }

        public void RemoveTimeSTEC(params T[] items)
        {
            using (var context = new TimeSTECEntities())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                }
                context.SaveChanges();
            }
        }

        public void UpdateTimeSTEC(params T[] items)
        {
            using (var context = new TimeSTECEntities())
            {
                foreach (T item in items)
                {
                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                }
                context.SaveChanges();
            }
        }
    }
}
