﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for ResetupPositionOrganization.xaml
    /// </summary>
    public partial class ResetupPositionOrganization : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        string  XFromPositionOrganization_ID;
        private PositionOrganization XFromPositionOrganization;
        private PositionOrganization XNewPositionOrganization;
        public  List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        TreeViewPositionOrganizationModel TreeViewModel_After;


        public  ResetupPositionOrganization()
        {
            InitializeComponent();
        }
        public ResetupPositionOrganization(string _xPositionOrganization_ID)
        {
            InitializeComponent();
            XFromPositionOrganization_ID = _xPositionOrganization_ID;
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

            ShowCurrentPositionOrganization();
        }

        private void ShowCurrentPositionOrganization()
        {
            try
            {
                XFromPositionOrganization =  _hrisBusinessLayer.GetPositionOrganization(XFromPositionOrganization_ID);
                if (XFromPositionOrganization != null )
                {
                    txtPositionOrganization_ID.Text = XFromPositionOrganization.PositionOrganization_ID;
                    txtPosition.Text = XFromPositionOrganization.Position.Position_ID == null ? "" : XFromPositionOrganization.Position.PositionNameEN;
                     
                     //find current parent
                    PositionOrganization xquery = _hrisBusinessLayer.GetPositionOrganization(XFromPositionOrganization.STECPositionNode_ID);
                     if(xquery !=null)
                         txtCurrentParent.Text = xquery.Position.Position_ID == null ? "" : xquery.Position.PositionNameEN;
                }

                ShowPositionOrg();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowPositionOrg()
        {
            //IList<PositionOrganization> OrgList = _hrisBusinessLayer.GetAllPositionOrganizations();
            //if (OrgList.Count > 0)
            //{
            //    var flatList = new List<PositionOrganizationGroup>();
            //    foreach (PositionOrganization i in OrgList)
            //    {
            //        flatList.Add(new PositionOrganizationGroup()
            //        {
            //            PositionOrganization_ID = i.PositionOrganization_ID
            //            ,
            //            STECPositionParentID = i.STECPositionNode_ID == null ? 0 : Convert.ToInt64(i.STECPositionNode_ID)
            //            ,
            //            STECPositionNode_ID = i.STECPositionNode_ID == null ? "" : i.STECPositionNode_ID
            //            ,
            //            ParentPositionBusinessNode_ID = i.ParentPositionBusinessNode_ID
            //            ,
            //            PositionName = i.Position.PositionNameEN == null ? "" : i.Position.PositionNameEN
            //            ,
            //            Organization_ID = i.Organization_ID == null ? "" : i.Organization_ID
            //            ,
            //            OrganizationUnit_ID = i.Organization.OrganizationUnit_ID == null ? "" : i.Organization.OrganizationUnit_ID
            //            ,
            //            OrganizationName = i.Organization.OrganizationUnit.OrganizationUnitName == null ? "" : i.Organization.OrganizationUnit.OrganizationUnitName
            //            ,
            //            JobTitle_ID = i.JobTitle_ID == null ? "" : i.JobTitle_ID
            //            ,
            //            CurrentFlag = i.CurrentFlag
            //            ,
            //            PositionNode = i.PositionNode
            //            ,
            //            PositionLevel = i.PositionLevel
            //            ,
            //            PositionType_ID = i.PositionType_ID == null ? "" : i.PositionType_ID
            //            ,
            //            CreatedDate = i.CreatedDate
            //            ,
            //            ValidFrom = i.ValidFrom
            //            ,
            //            EndDate = i.EndDate
            //        });
            //    }

            //    var tree = flatList.BuildPositionTree();
            //    TreeView1.ItemsSource = tree;
            //    TreeViewModel_After = new TreeViewPositionOrganizationModel();
            //    TreeViewModel_After.Items = new List<PositionOrganizationGroup>();
            //}            
        }

        public TreeViewPositionOrganizationModel TreeModel
        {
            get
            {
                return TreeViewModel_After;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               
                //find count of this positio and need to move to new positionOrganization
                List<PositionOrganization> xquery1 = _hrisBusinessLayer.GetAllPositionOrganizations()
                    .Where(x => x.Position_ID == XFromPositionOrganization.Position_ID 
                        && x.Organization_ID == XFromPositionOrganization.Organization_ID 
                        && x.STECPositionNode_ID == XFromPositionOrganization.STECPositionNode_ID).ToList();

                if (xquery1.Count > 0)
                {
                    //วนลูป
                    foreach(PositionOrganization xI  in xquery1)
                    {
                        PositionOrganization[] positionObj = new PositionOrganization[1];
                        PositionOrganization position1 = new PositionOrganization(); //created  1 instanct or object
                        position1.PositionOrganization_ID = xI.PositionOrganization_ID;
                        position1.Position_ID = xI.Position_ID;
                        position1.Organization_ID = XNewPositionOrganization.Organization_ID;
                        position1.JobTitle_ID = xI.JobTitle_ID;
                        position1.CurrentFlag = xI.CurrentFlag;
                        position1.PositionNode = XNewPositionOrganization.PositionOrganization_ID;
                        position1.ParentPositionBusinessNode_ID = XNewPositionOrganization.PositionOrganization_ID;
                        position1.STECPositionNode_ID = XNewPositionOrganization.PositionOrganization_ID;
                        position1.PositionLevel = xI.PositionLevel;
                        position1.PositionType_ID = xI.PositionType_ID;
                        position1.CreatedDate = xI.CreatedDate;
                        position1.ValidFrom = xI.ValidFrom;
                        position1.EndDate = xI.EndDate;
                        position1.ModifiedDate = DateTime.Now;
                        positionObj[0] = position1;
                        _hrisBusinessLayer.UpdatePositionOrganization(positionObj);

                        transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                        if (transactionLog.Count > 0)
                        {
                            XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                        }
                        else
                        {
                            XMaxTransactionLog_ID = 1;
                        }
                        TransactionLog[] transactionLogObj = new TransactionLog[1];
                        TransactionLog transactionLog1 = new TransactionLog();
                        transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                        transactionLog1.TransactionType_ID = "U";
                        transactionLog1.Table_ID = "T014";
                        transactionLog1.TransactionDate = DateTime.Now;
                        transactionLog1.FieldName = "PositionOranization_ID";
                        transactionLog1.PKFields = xI.PositionOrganization_ID;
                        transactionLog1.FKFields = "";
                        transactionLog1.OldData = xI.STECPositionNode_ID;
                        transactionLog1.NewData = XNewPositionOrganization.STECPositionNode_ID;
                        transactionLog1.ModifiedDate = DateTime.Now;
                        transactionLog1.ModifiedUser = _singleton.username;
                        transactionLogObj[0] = transactionLog1;
                        _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                    }
                }

                ShowPositionOrg();

                //go back to previouse page
                //if (XFromPositionOrganization_ID != "")
                //    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void TreeView1_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                var selectedNode = (e.Source as TreeView).SelectedItem as PositionOrganizationGroup;

                XNewPositionOrganization = _hrisBusinessLayer.GetPositionOrganization(selectedNode.PositionOrganization_ID);
                if (XNewPositionOrganization != null)
                    txtNewParent.Text = XNewPositionOrganization.Position.Position_ID == null ? "" : XNewPositionOrganization.Position.PositionNameEN;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
