﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR018.xaml
    /// </summary>
    public partial class RepRPTHR018 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass>();

        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();
        public RepRPTHR018()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //get department into combobox
                departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
                CmbDept.ItemsSource = departmentFromSTECPayrollList;

                lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
                cmbLot.ItemsSource = lot_NumberClassList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                if (RdtByDept.IsChecked == true && CmbDept.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกแผนก/ฝ่าย", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (RdtAllDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();


                    HRISReportDataset.sp_GetPAYROLLOFLABOURDataTable sp_GetPayrollofLabourDataTable = new HRISReportDataset.sp_GetPAYROLLOFLABOURDataTable();
                    HRISReportDatasetTableAdapters.sp_GetPAYROLLOFLABOURTableAdapter sp_GetPayrollofLabourTableAdapter = new HRISReportDatasetTableAdapters.sp_GetPAYROLLOFLABOURTableAdapter();
                    sp_GetPayrollofLabourTableAdapter.Fill(sp_GetPayrollofLabourDataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource1.Name = "PayrollOfLabourDataSet";

                    HRISReportDataset.sp_GetWorkingDateLabourFromPayrollDataTable sp_GetWorkingDateLabourFromPayrollDataTable = new HRISReportDataset.sp_GetWorkingDateLabourFromPayrollDataTable();
                    HRISReportDatasetTableAdapters.sp_GetWorkingDateLabourFromPayrollTableAdapter sp_GetWorkingDateLabourFromPayrollTableAdapter = new HRISReportDatasetTableAdapters.sp_GetWorkingDateLabourFromPayrollTableAdapter();
                    sp_GetWorkingDateLabourFromPayrollTableAdapter.Fill(sp_GetWorkingDateLabourFromPayrollDataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource2.Name = "WorkingDateLabourFromPayrollDataSet";
                 
                    if (RdtStaffTypeAll.IsChecked == true)
                    {
                        reportDataSource1.Value = sp_GetPayrollofLabourDataTable;
                        reportDataSource2.Value = sp_GetWorkingDateLabourFromPayrollDataTable;
                    }                    
                    else if (RdtStaffType2.IsChecked == true)
                    {
                        DataTable D2 = sp_GetPayrollofLabourDataTable.Clone();
                        DataRow[] result = sp_GetPayrollofLabourDataTable.Select("Staff_type = 2");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;

                        DataTable D3 = sp_GetWorkingDateLabourFromPayrollDataTable.Clone();
                        DataRow[] result3 = sp_GetWorkingDateLabourFromPayrollDataTable.Select("Staff_type = 2");
                        foreach (DataRow d in result)
                        {
                            D3.ImportRow(d);
                        }
                        reportDataSource2.Value = D3;

                    }
                    else if (RdtStaffType3.IsChecked == true)
                    {
                        DataTable D2 = sp_GetPayrollofLabourDataTable.Clone();
                        DataRow[] result = sp_GetPayrollofLabourDataTable.Select("Staff_type = 3");
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;

                        DataTable D3 = sp_GetWorkingDateLabourFromPayrollDataTable.Clone();
                        DataRow[] result3 = sp_GetWorkingDateLabourFromPayrollDataTable.Select("Staff_type = 3");
                        foreach (DataRow d in result)
                        {
                            D3.ImportRow(d);
                        }
                        reportDataSource2.Value = D3;
                    }


                    

                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR018.rdlc";
                    _ReportViewer1.RefreshReport();
                }
                else if (RdtByDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();


                    HRISReportDataset.sp_GetPAYROLLOFLABOURDataTable sp_GetPayrollofLabourDataTable = new HRISReportDataset.sp_GetPAYROLLOFLABOURDataTable();
                    HRISReportDatasetTableAdapters.sp_GetPAYROLLOFLABOURTableAdapter sp_GetPayrollofLabourTableAdapter = new HRISReportDatasetTableAdapters.sp_GetPAYROLLOFLABOURTableAdapter();
                    sp_GetPayrollofLabourTableAdapter.Fill(sp_GetPayrollofLabourDataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource1.Name = "PayrollOfLabourDataSet";

                    HRISReportDataset.sp_GetWorkingDateLabourFromPayrollDataTable sp_GetWorkingDateLabourFromPayrollDataTable = new HRISReportDataset.sp_GetWorkingDateLabourFromPayrollDataTable();
                    HRISReportDatasetTableAdapters.sp_GetWorkingDateLabourFromPayrollTableAdapter sp_GetWorkingDateLabourFromPayrollTableAdapter = new HRISReportDatasetTableAdapters.sp_GetWorkingDateLabourFromPayrollTableAdapter();
                    sp_GetWorkingDateLabourFromPayrollTableAdapter.Fill(sp_GetWorkingDateLabourFromPayrollDataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource2.Name = "WorkingDateLabourFromPayrollDataSet";

                    if (RdtStaffTypeAll.IsChecked == true)
                    {

                        DataTable D2 = sp_GetPayrollofLabourDataTable.Clone();
                        string expression;
                        expression = "dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result = sp_GetPayrollofLabourDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;

                        DataTable D3 = sp_GetWorkingDateLabourFromPayrollDataTable.Clone();
                        //string expression3;
                        expression = "dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result3 = sp_GetWorkingDateLabourFromPayrollDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D3.ImportRow(d);
                        }
                        reportDataSource2.Value = D3;
                    }                    
                    else if (RdtStaffType2.IsChecked == true)
                    {
                        DataTable D2 = sp_GetPayrollofLabourDataTable.Clone();
                        string expression;
                        expression = "Staff_type = 2 and dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result = sp_GetPayrollofLabourDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;

                        DataTable D3 = sp_GetWorkingDateLabourFromPayrollDataTable.Clone();
                        //string expression3;
                        expression = "Staff_type = 2 dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result3 = sp_GetWorkingDateLabourFromPayrollDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D3.ImportRow(d);
                        }
                        reportDataSource2.Value = D3;
                    }
                    else if (RdtStaffType3.IsChecked == true)
                    {
                        DataTable D2 = sp_GetPayrollofLabourDataTable.Clone();
                        string expression;
                        expression = "Staff_type = 3 and dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result = sp_GetPayrollofLabourDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D2.ImportRow(d);
                        }
                        reportDataSource1.Value = D2;

                        DataTable D3 = sp_GetWorkingDateLabourFromPayrollDataTable.Clone();
                        //string expression3;
                        expression = "Staff_type = 3 dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                        DataRow[] result3 = sp_GetWorkingDateLabourFromPayrollDataTable.Select(expression);
                        foreach (DataRow d in result)
                        {
                            D3.ImportRow(d);
                        }
                        reportDataSource2.Value = D3;
                    }

                    

                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR018.rdlc";
                    _ReportViewer1.RefreshReport();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtByDept_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RdtByDept.IsChecked == true)
                {
                    CmbDept.IsEnabled = true;
                    CmbDept.Focus();
                }
                else if (RdtByDept.IsChecked == false)
                {
                    CmbDept.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
