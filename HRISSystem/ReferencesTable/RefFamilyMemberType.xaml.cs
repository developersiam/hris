﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for RefFamilyMemberType.xaml
    /// </summary>
    public partial class RefFamilyMemberType : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XType;
        private string XPersonID;
        FamilyMemberType XFromRefPage;
        FamilyMemberType famType = new FamilyMemberType();
        
        public RefFamilyMemberType(string _xType, string _personId)
        {
            InitializeComponent();
            if (_xType == "Add")
            {
                XType = "Add";
                XPersonID = _personId;
               
            }
            else if (_xType == "Edit")
            {
                XType = "Edit";
                XPersonID = _personId;
                
            }
        }

        public RefFamilyMemberType(FamilyMemberType eX)
        {
            InitializeComponent();
            XFromRefPage = eX;
            ShowDetail(eX.FamilyMemberType_ID);

        }
        private void ShowDetail(string familyMemberTypeId)
        {
            famType = _hrisBusinessLayer.GetFamilyMemberTypeByID(familyMemberTypeId);

            TxtID.Text = famType.FamilyMemberType1;            
        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (XFromRefPage != null)
                    NavigationService.RemoveBackEntry();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //getAll and added 1
                string XId;
                List<FamilyMemberType> famType2 = new List<FamilyMemberType>();
                famType2 = _hrisBusinessLayer.GetAllFamilyMemberTypes().OrderBy(b => b.FamilyMemberType_ID).ToList();
                
                if (famType2.Count() > 0)
                {                    
                    XId = Convert.ToString(famType2.Max(d => Convert.ToDouble(d.FamilyMemberType_ID)) + 1);
                }
                else
                {
                    XId = "1";
                }
                TxtID.Text = XId;


                if (TxtID.Text == "")
                {
                    MessageBox.Show("กรุณาแจ้ง IT ให้ตรวจสอบรหัสนี้");
                    return;
                }
                if (TxtFamilyMemberType.Text == "")
                {
                    MessageBox.Show("กรุณาระบุชื่อความสัมพันธุ์  เช่น พ่อ แม่ น้องสาว น้องชาย ฯลฯ");
                    return;
                }


                if (XType == "Add")
                {


                    FamilyMemberType[] famTypeObj = new FamilyMemberType[1];
                    FamilyMemberType famTypeAdd = new FamilyMemberType();
                    famTypeAdd.FamilyMemberType_ID = TxtID.Text;
                    famTypeAdd.FamilyMemberType1 = TxtFamilyMemberType.Text;        
                    famTypeAdd.ModifiedDate = DateTime.Now;
                    famTypeObj[0] = famTypeAdd;
                    _hrisBusinessLayer.AddFamilyMemberType(famTypeObj);
                }
                else if (XType == "Edit")
                {
                    if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return;
                    }
                    famType.FamilyMemberType_ID = TxtID.Text;
                    famType.FamilyMemberType1 = TxtFamilyMemberType.Text;
                    famType.ModifiedDate = DateTime.Now;
                    _hrisBusinessLayer.UpdateFamilyMemberType(famType);

                }
                //NavigationService.GoBack();
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupfamilyMemberType", "", TxtID.Text));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtFamilyMemberType.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new Employees.Person_Person(XPersonID, "SetupfamilyMemberType", "", TxtID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
