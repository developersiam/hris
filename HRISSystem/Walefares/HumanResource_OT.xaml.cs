﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
using System.Text.RegularExpressions;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for HumanResource_OT.xaml
    /// 
    public class TestOT
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
       
        public string Employee_ID { get; set; }
        public string AccCode { get; set; }
        public string FingerScanID { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public Nullable<System.DateTime> OTDATE { get; set; }
        public Nullable<decimal> OTnormal { get; set; }
        public Nullable<decimal> Holiday { get; set; }
        public Nullable<decimal> OTHoliday { get; set; }
        public Nullable<decimal> OTSpecial { get; set; }
        public string Remark { get; set; }
        public string PayrollLot { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeName { get; set; }
        public string AdminApproveStatus { get; set; }
        public string ManagerApproveStatus { get; set; }
        public string FinalApproverStatus { get; set; }
        public string HRApproveStatus { get; set; }

        public List<TestOT> getTestOT(string payrollLot)
        {
            List<Employee> oE = new List<Employee>();
            oE = _hrisBusinessLayer.GetAllEmployees().ToList();

            List<OT> OjAll = new List<OT>();
            //OjAll = _hrisBusinessLayer.GetAllOTs().Where(p => p.OTDate >= _xstartdate && p.OTDate <= _xendDate).ToList();
            OjAll = _hrisBusinessLayer.GetAllOTs().Where(p => p.PayrollLot == payrollLot).ToList();
            var query = (
                            from a in oE
                            join b in OjAll on a.Employee_ID equals b.Employee_ID
                            group new { a, b } by new { a.Employee_ID, b.OTDate, a.Noted
                                , a.FingerScanID, a.Person.TitleName.TitleNameInitialTH
                                , a.Person.FirstNameTH, a.Person.LastNameTH, b.RequestType
                                , b.Remark, b.PayrollLot
                                , b.AdminApproveStatus, b.ManagerApproveStatus, b.FinalApproverStatus, b.HRApproveStatus}
                            into g
                            select new TestOT
                            {
                                Employee_ID = g.Key.Employee_ID,
                                AccCode = g.Key.Noted,
                                FingerScanID = g.Key.FingerScanID,
                                TitleNameTH = g.Key.TitleNameInitialTH,
                                FirstNameTH = g.Key.FirstNameTH,
                                LastNameTH = g.Key.LastNameTH,
                                OTDATE = g.Key.OTDate,
                                OTnormal = g.Where(e => e.b.OTType == "N").Sum(e => e.b.NcountOT),
                                Holiday = g.Where(e => e.b.OTType == "H").Sum(e => e.b.NcountOT),
                                OTHoliday = g.Where(e => e.b.OTType == "S").Sum(e => e.b.NcountOT),
                                OTSpecial = g.Where(e => e.b.OTType == "SP").Sum(e => e.b.NcountOT),
                                Remark = g.Key.Remark == null ? "" : g.Key.Remark,
                                PayrollLot = g.Key.PayrollLot,
                                RequestTypeName = g.Key.RequestType == "A" ? "OT-ก่อนเริ่มงาน" : "OT-หลังเลิกงาน",
                                RequestType = g.Key.RequestType,
                                AdminApproveStatus = g.Key.AdminApproveStatus,
                                ManagerApproveStatus = g.Key.ManagerApproveStatus,
                                FinalApproverStatus = g.Key.FinalApproverStatus,
                                HRApproveStatus = g.Key.HRApproveStatus
                            });
            return query.ToList();        
        }  
    }

    public partial class HumanResource_OT : Page
    {
       
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private Double XCurrentOT;
        Boolean XchkDate = false;
        Boolean XFromOTInformation = false;
        public List<TransactionLog> transactionLog;
        Decimal XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private int xFingerScanID, cioFingerScanID;
        private string xPayrollLot = "";
        private string cioOTDate;

        public DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        private class StatusFlags
        {
            public bool StatusFlag { get; set; }
            public string StatusFlagName { get; set; }
        }

        private class RequestFlags
        {
            public string RequestFlag { get; set; }
            public string RequestFlagName { get; set; }
        }

        private class OTMinutes
        {
            public string Minutes { get; set; }
            public decimal Multiplier { get; set; }
        }

        private class OTType
        {
            public string OTTypeCode { get; set; }
            public string OTTypeName { get; set; }
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _timeSTECBuisnessLayer = new HRISTimeSTECBusinessLayer();
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        public HumanResource_OT()
        {
            InitializeComponent();
        }

        public HumanResource_OT(EmployeeCurrentInformation x3)
        {
            //From EmployeeSearch page
            InitializeComponent();
            xFromEmployeeCurrentInformation = x3;

            if (x3 != null)
            {
                txtEmployee_ID.Text = x3.xEmployee_ID;
                GetDataFromEmployee_ID(x3.xEmployee_ID);
            }
        }

        public HumanResource_OT(DateTime otDate, string otType,string employee_ID,Boolean xchkFrom,string xfrom)
        {
            //From OT Online
            MessageBox.Show("Pending");
            //InitializeComponent();
            //XFromOTInformation = xchkFrom;

            //BindLot();
            //cmbLot.SelectedIndex = 0;
            //BindStatusFlag();
            //cmbStatus.SelectedIndex = 0;

            //GetDataFromEmployee_ID(employee_ID);
            //getOTDetail(employee_ID,otDate,otType);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (xFromEmployeeCurrentInformation != null) NavigationService.RemoveBackEntry();

                BindLot();
                BindOTType();
                BindRequestFlag();
                BindStatusFlag();
                BindOTHour();
                BindOTMinute();

                cmbLot.SelectedValue = lot_numberList.CurrentLot(); // Set default lot to Current month
                cmbStatus.SelectedIndex = 0;
                cmbLotSearch.SelectedIndex = 0;
                DataGrid.ItemsSource = null; //clear 

                if (XFromOTInformation == true)
                {
                    //txtNcountOT.Text = XCurrentOT.ToString();
                    string otAmount = XCurrentOT.ToString();
                    decimal otHour = Convert.ToDecimal(otAmount.Substring(0, 1));
                    decimal otMimute = Convert.ToDecimal(otAmount) - otHour;
                    cmbOTHour.SelectedValue = otHour;
                    cmbOTMinute.SelectedValue = otMimute;
                    GetDataFromSearch();
                    AddButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindOTType()
        {
            List<OTType> otTypeList = new List<OTType>();
            otTypeList.Add(new OTType { OTTypeCode = "N", OTTypeName = "OT-normal" });
            otTypeList.Add(new OTType { OTTypeCode = "H", OTTypeName = "Holiday" });
            otTypeList.Add(new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" });
            cmbOTType.ItemsSource = null;
            cmbOTType.ItemsSource = otTypeList;
        }

        private void BindStatusFlag()
        {
            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();
            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbStatus.ItemsSource = null;
            cmbStatus.ItemsSource = statusFlagList;
        }

        private void BindRequestFlag()
        {
            List<RequestFlags> requestFlagList = new List<RequestFlags>();
            RequestFlags h1 = new RequestFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            RequestFlags h2 = new RequestFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };
            requestFlagList.Add(h1);
            requestFlagList.Add(h2);
            cmbRequest.ItemsSource = null;
            cmbRequest.ItemsSource = requestFlagList;
        }

        private void BindOTHour()
        {
            //23 Apr 2019 Multiplier by HR requirement
            List<decimal> otHourList = new List<decimal>();
            for (decimal i = 0; i < 13; i++)
            {
                otHourList.Add(i);
            }
            cmbOTHour.ItemsSource = null;
            cmbOTHour.ItemsSource = otHourList;
            cmbOTHour.SelectedIndex = 0;
        }

        private void BindOTMinute()
        {
            //23 Apr 2019 Multiplier by HR requirement
            List<OTMinutes> otMinuteList = new List<OTMinutes>();
            otMinuteList.Add(new OTMinutes { Minutes = "0", Multiplier = 0m });
            otMinuteList.Add(new OTMinutes { Minutes = "10", Multiplier = 0.167m });
            otMinuteList.Add(new OTMinutes { Minutes = "20", Multiplier = 0.33m });
            otMinuteList.Add(new OTMinutes { Minutes = "30", Multiplier = 0.50m });
            otMinuteList.Add(new OTMinutes { Minutes = "40", Multiplier = 0.67m });
            otMinuteList.Add(new OTMinutes { Minutes = "50", Multiplier = 0.83m });
            cmbOTMinute.ItemsSource = null;
            cmbOTMinute.ItemsSource = otMinuteList;
            cmbOTMinute.SelectedIndex = 0;
        }

        private void BindManualWorkdateType()
        {
            if (cmbManualWorkDateStatus.Items.Count > 0) return;
            IList<ManualWorkDateType> manualWorkDateType = _hrisBusinessLayer.GetAllManualWorkDateTypes();
            cmbManualWorkDateType_ID.ItemsSource = manualWorkDateType.OrderBy(b => b.ManualWorkDateType_ID);
        }

        private void BindManualWorkdateTypeStatus()
        {
            if (cmbManualWorkDateStatus.Items.Count > 0) return;
            List<ManualWorkDateStatus> manualWorkDateStatus = new List<ManualWorkDateStatus>();
            ManualWorkDateStatus S1 = new ManualWorkDateStatus { ManualWorkDateStatusID = true, ManualWorkDateStatusName = "Approve" };
            ManualWorkDateStatus S2 = new ManualWorkDateStatus { ManualWorkDateStatusID = false, ManualWorkDateStatusName = "Not Approve" };
            manualWorkDateStatus.Add(S1);
            manualWorkDateStatus.Add(S2);
            cmbManualWorkDateStatus.ItemsSource = manualWorkDateStatus;
        }

        private void BindLot()
        {
            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);

            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
            cmbLotSearch.ItemsSource = null;
            cmbLotSearch.ItemsSource = lot_NumberClassList;
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (DataGrid.SelectedItems.Count != 1) return;

            XFromOTInformation = false;

            TestOT model = new TestOT();  // created 1 instance or object
            model = (TestOT)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
            // If Admin, Approver, Final or HR status is not Approve data will not allow to update
            if (model.OTSpecial != 0)
            {
                SpecialOTWindows specialOT = new SpecialOTWindows(model.Employee_ID, model);
                specialOT.ShowDialog();

                cmbLotSearch.SelectedIndex = cmbLot.SelectedIndex;
                GetDataFromSearch();
                ClearInformationData("A");
            }
            else
            {
                if (model.Remark == "Create from OT Online" && (model.AdminApproveStatus != "A" || model.ManagerApproveStatus != "A" || model.FinalApproverStatus != "A" || model.HRApproveStatus != "A"))
                {
                    string unApprover = "";
                    unApprover = model.AdminApproveStatus != "A" ? "Admin " : "";
                    unApprover += model.ManagerApproveStatus != "A" ? "Approver " : "";
                    unApprover += model.FinalApproverStatus != "A" ? "Final Approver " : "";
                    unApprover += model.HRApproveStatus != "A" ? "HR Approver " : "";
                    MessageBox.Show("ไม่สามารถแก้ไขข้อมูลได้ สถานะข้อมูลยังไม่ได้ถูกอนุมัติจาก " + unApprover, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (model.Holiday != 0 && model.OTHoliday != 0)
                {
                    this.NavigationService.Navigate(new GetOTInformation(Convert.ToDateTime(model.OTDATE), Convert.ToDateTime(model.OTDATE), model.Employee_ID.ToString(), "OT", ""));
                }
                else
                {
                    string strOTType = "";
                    if (model.OTnormal != 0) strOTType = "N";
                    else if (model.Holiday != 0 && model.OTHoliday == 0) strOTType = "H";
                    else if (model.Holiday == 0 && model.OTHoliday != 0) strOTType = "S";

                    GetDataFromEmployee_ID(model.Employee_ID);
                    getOTDetail(model.Employee_ID.ToString(), Convert.ToDateTime(model.OTDATE), strOTType, model.RequestType);

                    AddButton.IsEnabled = false;
                    cmbLot.IsEnabled = false;
                    OTDate.IsEnabled = false;
                    cmbOTType.IsEnabled = false;
                    cmbRequest.IsEnabled = false;
                    cmbStatus.IsEnabled = false;
                }
            }            
        }

        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                if (!string.IsNullOrEmpty(ex.xEmployee_ID))
                {
                    txtEmployee_ID.Text = ex.xEmployee_ID;
                    txtEmployee_ID.IsEnabled = false;
                    txtTitleNameTH.Text = ex.xTitleNameTH;
                    txtFirstNameTH.Text = ex.xFirstNameTH;
                    txtLastNameTH.Text = ex.xLastNameTH;
                    txtStaffType.Text = ex.xStaffType;
                    xFingerScanID = Convert.ToInt16(ex.xFingerScan_ID);
                    OTDate.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void getOTDetail(string employee_ID, DateTime otDate, string otType, string requestType) //แสดงรายละเอียด OT เฉพาะรายการนั้น    
        {
            try
            {
                OT ot = new OT();
                ot = _hrisBusinessLayer.GetOTByEmployeeOneRow(employee_ID, otDate, otType, requestType);
                xPayrollLot = ot.PayrollLot;
                cmbLot.SelectedValue = ot.PayrollLot;
                OTDate.Text = ot.OTDate.ToString();
                cmbOTType.SelectedValue = ot.OTType;
                cmbRequest.SelectedValue = requestType;
                XCurrentOT = Convert.ToDouble(ot.NcountOT);
                //txtNcountOT.Text = Convert.ToString(XCurrentOT);
                string otAmount = ot.NcountOT.ToString();
                decimal otHour = Convert.ToDecimal(XCurrentOT.ToString().Substring(0, 1));
                decimal otMimute = Convert.ToDecimal(otAmount) - otHour;
                cmbOTHour.SelectedValue = otHour;
                cmbOTMinute.SelectedValue = otMimute;
                
                cmbStatus.SelectedValue = ot.Status;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationActionButton_Click(object sender, RoutedEventArgs e)
        {
            // A is Add. U is Update. D is Delete. C is Clear
            var obj = (Button)sender;
            string actionType = obj.Name == "AddButton" ? "A" : (
                                obj.Name == "EditButton" ? "U" : (
                                obj.Name == "DeleteButton" ? "D" : (
                                obj.Name == "ClearButton" ? "C" : "")));
            if (actionType != "C" && actionType != "" && !UpdateOTData(actionType)) return;
            ClearInformationData(actionType);
        }

        private bool UpdateOTData(string actionType)
        {
            try
            {
                if (isRequiredDataCorrect(actionType))
                {
                    var otHour = Convert.ToDecimal(cmbOTHour.Text.ToString());
                    var otMinute = Convert.ToDecimal(cmbOTMinute.SelectedValue.ToString()); // Multiplier value
                    decimal requestedTime = otHour + otMinute;

                    string oldData = actionType == "A" ? "" : XCurrentOT.ToString();
                    string newData = actionType == "D" ? "" : requestedTime + (actionType == "A" ? " Lot" + xPayrollLot : "");
                    //string newData = actionType == "D" ? "" : txtNcountOT.Text + (actionType == "A" ? " Lot" + xPayrollLot : "");
                    OT[] otObject = new OT[1];
                    otObject[0] = new OT()
                    {
                        Employee_ID = txtEmployee_ID.Text,
                        OTDate = Convert.ToDateTime(OTDate.Text),
                        OTType = (string)cmbOTType.SelectedValue,
                        RequestType = (string)cmbRequest.SelectedValue,
                        //NcountOT = Convert.ToDecimal(txtNcountOT.Text),
                        NcountOT = requestedTime,
                        Status = (Boolean)cmbStatus.SelectedValue,
                        ModifiedDate = DateTime.Now,
                        AdminApproveStatus = "A",
                        ManagerApproveStatus = "A",
                        FinalApproverStatus = "A",
                        HRApproveStatus = "A",
                        Remark = "Create by HRM",
                        PayrollLot = xPayrollLot
                    };
                    if (actionType == "A") _hrisBusinessLayer.AddOT(otObject);
                    else if (actionType == "U") _hrisBusinessLayer.UpdateOT(otObject);
                    else if (actionType == "D") _hrisBusinessLayer.RemoveOT(otObject[0]);

                    //Get new TransactionLog_ID
                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    XMaxTransactionLog_ID = transactionLog.Count > 0 ? Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1 : 1;

                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    transactionLogObj[0] = new TransactionLog()
                    {
                        TransactionLog_ID = XMaxTransactionLog_ID,
                        TransactionType_ID = actionType,
                        Table_ID = "T009",
                        TransactionDate = DateTime.Now,
                        FieldName = "Employee_ID",
                        PKFields = txtEmployee_ID.Text,
                        FKFields = OTDate.Text,
                        OldData = oldData,
                        NewData = newData,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = _singleton.username
                    };
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                    cmbLotSearch.SelectedIndex = cmbLot.SelectedIndex;
                    if (actionType == "A")
                    {
                        var newEventArgs = new RoutedEventArgs(CheckBox.ClickEvent);
                        ChkFilterSearch.RaiseEvent(newEventArgs);
                        txtSearch.Text = txtFirstNameTH.Text;
                    }
                    GetDataFromSearch();
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                if (ex.Message == "ไม่สามารถบันทึกข้อมูลซ้ำได้ ข้อมูลนี้มีอยู่ในระบบแล้ว") ShowDuplicateOT();
                return false;
            }
        }

        private bool isRequiredDataCorrect(string actionType)
        {
            string confirmationText = actionType == "D" ? "ต้องการลบข้อมูลใช่หรือไม่?" : "ต้องการบันทึกข้อมูลใช่หรือไม่?";
            if (MessageBox.Show(confirmationText, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return false;

            if (txtTitleNameTH.Text == "")
            {
                MessageBox.Show("กรุณาคีย์รหัสพนักงาน กรุณาตรวจสอบข้อมูล", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (cmbLot.Text == "" || DateFromSearch.Text == "" || DateToSearch.Text == "" || xPayrollLot == null)
            {
                MessageBox.Show("กรุณาระบุ Lot", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (OTDate.Text == "")
            {
                MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else if (Convert.ToDateTime(OTDate.Text) > Convert.ToDateTime(DateTime.Now)) //21 June 2018 ไม่อนุญาตให้คีย์ล่วงหน้าได้
            {
                MessageBox.Show("ไม่สามารถคีย์วันที่ทำโอทีล่วงหน้าได้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else if (Convert.ToDateTime(OTDate.Text) < Convert.ToDateTime(DateFromSearch.Text)) // 29Oct2018 ไม่อนุญาติให้คีย์ข้อมูลย้อนหลัง (OT ไม่ตรง Lot)
            {
                MessageBox.Show("เนื่องจากคุณระบุวันที่ทำโอทีย้อนหลังจาก Lot ปัจจุบัน หากต้องการทำรายการกรุณาติดต่อแผนก IT", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
            lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
            if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
            {
                MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (lot_numberList.Lock_Hr_Labor == "2")
            {
                MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (txtScanIn.Text == "" || txtScanOut.Text == "")
            {
                MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้ว ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
            TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
            if (XchkDate == false)
            {
                if (span.Hours < 8)
                {
                    MessageBox.Show("ไม่สามารถบันทึกการขอทำโอที เนื่องจากมีการมีการทำงานน้อยกว่า 8 ชัวโมง กรุณาตรวจสอบการข้อมูลการสแกนนิ้ว", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }


            if (!isOTAmountCorrect())
            {
                MessageBox.Show("จำนวนชั่วโมง OT ไม่ถูกต้อง กรุณาตรวจสอบ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else
            {
                if (XchkDate == false)
                {
                    //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                    var otHour = Convert.ToDecimal(cmbOTHour.Text.ToString());
                    var otMinute = Convert.ToDecimal(cmbOTMinute.Text.ToString()); // Minute unit
                    decimal scanedTime = (((span.Hours - 8) * 60) + (span.Minutes)) - 30; // Subtract 30m for Break
                    //decimal scanedTime = ((span.Hours - 8) * 60) + (span.Minutes);
                    //decimal requestedTime = (Convert.ToDecimal(txtNcountOT.Text) * 60);
                    decimal requestedTime = ((otHour * 60) + otMinute);
                    if (requestedTime > scanedTime)
                    {
                        if (MessageBox.Show("จำนวนโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ คุณต้องการบันทึกข้อมูลต่อใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.No) return false;
                    }
                }
            }

            if (cmbOTType.Text == "")
            {
                MessageBox.Show("กรุณาเลือกประเภท OT", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //else if (cmbOTType.SelectedValue.ToString() == "H" && Convert.ToDecimal(cmbOTHour.Text.ToString()) > 2)
            else if (cmbOTType.SelectedValue.ToString() == "H" && (Convert.ToDecimal(cmbOTHour.Text.ToString()) + Convert.ToDecimal(cmbOTMinute.Text.ToString())) > 2)
            {
                MessageBox.Show("จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (cmbStatus.Text == "")
            {
                MessageBox.Show("กรุณาเลือก Status", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
       
        private void ShowDuplicateOT()
        {
            //OTnormal N, Holiday H, OTHoliday S
            string otType = cmbOTType.SelectedValue.ToString();
            DataGrid.ItemsSource = null;
            TestOT ot2 = new TestOT();
            var result = ot2.getTestOT(cmbLotSearch.Text).Where(w => w.Employee_ID == txtEmployee_ID.Text &&
                                                                w.RequestType == cmbRequest.SelectedValue.ToString() &&
                                                                w.OTDATE == Convert.ToDateTime(OTDate.Text)).ToList();
            List<TestOT> temp = new List<TestOT>();
            if (otType == "N") temp = result.Where(w => w.OTnormal > 0).ToList();
            else if (otType == "H") temp = result.Where(w => w.Holiday > 0).ToList();
            else if (otType == "S") temp = result.Where(w => w.OTHoliday > 0).ToList();
            result = temp;
            if (result.Any()) DataGrid.ItemsSource = result;
        }

        private void txtNcountOT_KeyUp(object sender, KeyEventArgs e)
        {
            isOTAmountCorrect();
        }

        private bool isOTAmountCorrect()
        {
            //23 Apr 2019 Cancel fix 0.0/0.5 to dropdown by HR requirement
            //if (string.IsNullOrEmpty(txtNcountOT.Text.Trim())) return false; //return if null
            //Match match = Regex.Match(txtNcountOT.Text, @"^[0-9]\d{0,1}(\.\d{1,1})?%?$"); // Regax 
            //bool allowCase = match.Success; //true if only number 0-9, one digit, is decimal, on digit decimal
            //bool allowNumber = false;
            //if (allowCase) allowNumber = decimal.Parse(txtNcountOT.Text) > 0 && (decimal.Parse(txtNcountOT.Text) * 10) % 5 == 0; //true if more than 0 or decimal .0 or .5
            //bool result = allowCase && allowNumber;
            //txtRemark.Text = result ? "" : "*กรุณาตรวจสอบข้อมูล! ตย. 0.5, 2, 3.0";
            //return result;
            if (cmbOTHour.SelectedIndex < 0 || cmbOTMinute.SelectedIndex < 0) return false;
            var otHour = Convert.ToDecimal(cmbOTHour.SelectedValue.ToString());
            var otMinute = Convert.ToDecimal(cmbOTMinute.SelectedValue.ToString());
            return otHour + otMinute > 0 ? true : false;

        }

        private void GetDataFromSearch()
        {
            try
            {
                if (!string.IsNullOrEmpty(cmbLotSearch.Text) && !string.IsNullOrEmpty(LeaveDateFromSearch.Text) && !string.IsNullOrEmpty(LeaveDateToSearch.Text))
                {
                    DataGrid.ItemsSource = null;
                    TestOT ot2 = new TestOT();
                    var result = ot2.getTestOT(cmbLotSearch.Text);

                    if (ChkAllSearch.IsChecked.Value)
                        result = result.Where(w => w.OTDATE >= LeaveDateFromSearch.SelectedDate && 
                                                   w.OTDATE <= LeaveDateToSearch.SelectedDate)
                                       .OrderBy(x => x.FirstNameTH).ToList();
                    else if (ChkFilterSearch.IsChecked.Value)
                    {
                        if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                            result = result.Where(x => x.FirstNameTH.Contains(txtSearch.Text) &&
                                                       x.OTDATE >= LeaveDateFromSearch.SelectedDate &&
                                                       x.OTDATE <= LeaveDateToSearch.SelectedDate)
                                           .OrderBy(x => x.FirstNameTH).ToList();
                        else
                        { MessageBox.Show("กรุณาใส่ชื่อพนักงาน", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning); return; }
                    }
                    result = result.Where(p => p.Remark == "Create by HRM" ||
                                         (p.Remark == "Create from OT Online" &&
                                          p.AdminApproveStatus == "A" &&
                                          p.ManagerApproveStatus == "A" &&
                                          p.FinalApproverStatus == "A" &&
                                          p.HRApproveStatus == "A")).ToList();
                    if (result.Any()) DataGrid.ItemsSource = result;
                    else MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            ChkFilterSearch.IsChecked = false;
            txtSearch.Text = string.Empty;
            txtSearch.IsReadOnly = true;
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            ChkAllSearch.IsChecked = false;
            ChkFilterSearch.IsChecked = true;
            txtSearch.Text = string.Empty;
            txtSearch.IsReadOnly = false;
            txtSearch.Focus();
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            GetDataFromSearch();
        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HumanResource.EmployeeSearch("OT"));
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            if (txtEmployee_ID.Text == "")
            {
                MessageBox.Show("กรุณาคีย์รหัสพนักงาน กรุณาตรวจสอบข้อมูล", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            GetDataFromEmployee_ID(txtEmployee_ID.Text);
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetDataFromSearch();
        }

        private void cmbOTType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbOTType.SelectedValue != null) txtOTUnit.Text = cmbOTType.SelectedValue.ToString() == "H" ? "แรง" : "ชั่วโมง";
        }

        private void ShowCheckInOut()
        {
            if (xFingerScanID != cioFingerScanID || OTDate.Text != cioOTDate || AddManualWork.IsChecked == true)
            {
                cioFingerScanID = xFingerScanID;
                cioOTDate = OTDate.Text;

                TimeSpan? scanIn, scanOut;

                userinfo u;
                u = _timeSTECBuisnessLayer.GetUserInfoByFingerID(xFingerScanID);

                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();

                List<ManualWorkDate> workDate = new List<ManualWorkDate>();
                workDate = _hrisBusinessLayer.GetManualWorkDateByEmployee(txtEmployee_ID.Text, Convert.ToDateTime(OTDate.Text));

                if (workDate.Count() <= 0 && _checkInout.Count() <= 0)
                {
                    txtScanIn.Text = "";
                    txtScanOut.Text = "";
                    txtTotalWork.Text = "";
                    MessageBox.Show("ไม่พบเวลาการทำงานในวันที่ " + OTDate.Text, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                //เช็คเวลาเข้าทำงาน
                if (workDate.Where(e => e.ManualWorkDateType_ID == "A01").Count() > 0) scanIn = workDate.Where(e => e.ManualWorkDateType_ID == "A01").FirstOrDefault().Times;
                else if (_checkInout.Count > 0) scanIn = _checkInout.Min(c => c.checktime.TimeOfDay);
                else scanIn = null;
                txtScanIn.Text = scanIn.ToString();

                //เช็คเวลาออกงาน
                if (workDate.Where(e => e.ManualWorkDateType_ID == "A02").Count() > 0) scanOut = workDate.Where(e => e.ManualWorkDateType_ID == "A02").FirstOrDefault().Times;
                else if (_checkInout.Count > 0) scanOut = _checkInout.Max(c => c.checktime.TimeOfDay);
                else scanOut = null;
                txtScanOut.Text = scanOut.ToString();

                //คำนวนเวลาทำงาน
                if (scanIn != null && scanOut != null)
                {
                    TimeSpan span = scanOut.GetValueOrDefault() - scanIn.GetValueOrDefault();
                    span = (scanOut.GetValueOrDefault() != scanIn.GetValueOrDefault()) ? span.Subtract(new TimeSpan(0, 30, 0)) : span; // If timein <> timeout the Subtract 30 minutes for Break.
                    txtTotalWork.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
                }
                else txtTotalWork.Text = "";
            }
        }

        private void OTDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (XFromOTInformation == false && !string.IsNullOrEmpty(OTDate.Text.Trim()) && !string.IsNullOrEmpty(txtEmployee_ID.Text.Trim()))
                {
                    xPayrollLot = cmbLot.Text;
                    ShowCheckInOut();

                    if (IsWeekend(Convert.ToDateTime(OTDate.Text)) == true)
                    {
                        txtChkWeekend.Text = "วันอาทิตย์";
                        txtChkWeekend.Foreground = Brushes.Red;
                        XchkDate = true;
                        cmbOTType.SelectedValue = "H";
                    }
                    else
                    {
                        //เช็คว่าเป็น holiday ที่กำหนดไว้หรือไม่  
                        Holiday holiday = _hrisBusinessLayer.GetHoliday(Convert.ToDateTime(OTDate.Text));
                        if (holiday == null)
                        {
                            txtChkWeekend.Text = "วันธรรมดา";
                            txtChkWeekend.Foreground = Brushes.Black;
                            XchkDate = false;
                            cmbOTType.SelectedValue = "N";
                        }                      
                        else
                        {
                            if (holiday.HolidayFlag == true)//ตามประกาศบริษัทฯ
                            {
                                txtChkWeekend.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดตามประกาศบริษัท)";
                                txtChkWeekend.Foreground = Brushes.Blue;
                                XchkDate = true;
                                cmbOTType.SelectedValue = "H";
                            }
                            else //ถ้าเป็นวันหยุดพิเศษ เช่น งานเลี้ยง คริตส์มาส 
                            {
                                txtChkWeekend.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดพิเศษ)";
                                txtChkWeekend.Foreground = Brushes.OrangeRed;
                                XchkDate = true;
                                cmbOTType.SelectedValue = "H";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbLot.SelectedItem == null) return;
            lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
            
            DateFromSearch.Text = lot_numberList.Start_date.ToString();
            DateToSearch.Text = lot_numberList.Finish_date.ToString();
        }

        private void cmbOTType_Loaded(object sender, RoutedEventArgs e)
        {
            BindOTType();
        }

        private void cmbLotSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLotSearch.SelectedItem;
            DateTime startDate = lot_numberList.Start_date;
            DateTime endDate = lot_numberList.Finish_date;
            LeaveDateFromSearch.DisplayDateStart = startDate;
            LeaveDateFromSearch.SelectedDate = startDate;
            LeaveDateToSearch.DisplayDateEnd = endDate;
            LeaveDateToSearch.SelectedDate = endDate;
        }
        
        public void ClearInformationData(string ActionType)
        {
            try
            {
                xPayrollLot = null;
                cioFingerScanID = 0;
                cioOTDate = null;
                XCurrentOT = 0;
                XFromOTInformation = false;

                if (ActionType != "A")
                {
                    txtEmployee_ID.IsReadOnly = false;
                    txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                    txtEmployee_ID.IsEnabled = true;
                    txtEmployee_ID.Text = "";
                    txtTitleNameTH.Text = "";
                    txtFirstNameTH.Text = "";
                    txtLastNameTH.Text = "";
                    txtStaffType.Text = "";
                    OTDate.IsEnabled = false;
                }

                if (cmbLot.Items.Count > 0) cmbLot.SelectedValue = lot_numberList.CurrentLot(); // Set default lot to Current month
                OTDate.Text = "";
                txtChkWeekend.Text = "";

                cmbOTType.Text = "";
                cmbOTType.IsEnabled = true;
                cmbRequest.Text = "";
                cmbRequest.IsEnabled = true;
                cmbStatus.SelectedIndex = 0;
                cmbStatus.IsEnabled = true;

                txtScanIn.Text = "";
                txtScanOut.Text = "";
                txtTotalWork.Text = "";
                //txtNcountOT.Text = "";
                cmbOTHour.SelectedIndex = 0;
                cmbOTMinute.SelectedIndex = 0;

                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private class ManualWorkDateStatus
        {
            public bool ManualWorkDateStatusID { get; set; }
            public string ManualWorkDateStatusName { get; set; }
        }

        private void AddManualWork_Click(object sender, RoutedEventArgs e)
        {
            bool isCheck = AddManualWork.IsChecked.GetValueOrDefault();
            if (isCheck)
            {
                BindManualWorkdateType();
                BindManualWorkdateTypeStatus();
            }
            if (string.IsNullOrEmpty(txtEmployee_ID.Text.Trim()) && isCheck)
            {
                MessageBox.Show("กรุณาคีย์รหัสพนักงาน", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                AddManualWork.IsChecked = false;
                txtEmployee_ID.Focus();
                return;
            }
            ClearManualWork(isCheck);
        }

        private void AddManualWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateMWD();
        }

        private bool isMWDRequiredDataCorrect()
        {
            try
            {
                if (cmbLot.Text == "" || DateFromSearch.Text == "" || DateToSearch.Text == "")
                {
                    MessageBox.Show("กรุณาระบุ Lot", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
                lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
                if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
                {
                    MessageBox.Show("ฝ่ายบัญชี ได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                if (lot_numberList.Lock_Hr_Labor == "2")
                {
                    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                if (cmbManualWorkDateType_ID.Text == "")
                {
                    MessageBox.Show("กรุณาระบุประเภท (Type)", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                if (ManualWorkDateDate.Text == "" || ManualWorkDateDateTo.Text == "")
                {
                    MessageBox.Show("กรุณาระบุวันที่ ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                else if (Convert.ToDateTime(ManualWorkDateDate.Text) > Convert.ToDateTime(ManualWorkDateDateTo.Text) ||
                         Convert.ToDateTime(ManualWorkDateDate.Text) < Convert.ToDateTime(DateFromSearch.Text) ||
                         Convert.ToDateTime(ManualWorkDateDateTo.Text) > Convert.ToDateTime(DateToSearch.Text))
                {
                    MessageBox.Show("กรุณาระบุช่วงวันที่ให้ถูกต้อง ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                TimeSpan addHour;
                bool isHourCorrect = TimeSpan.TryParse(txtManualWorkDateTime.Text, out addHour);
                if (txtManualWorkDateTime.Text == "")
                {
                    MessageBox.Show("กรุณาระบุเวลา", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                else if (!isHourCorrect)
                {
                    MessageBox.Show("กรุณาระบุเวลาให้ถูกต้อง เช่น 8:00 หรือ 20:30 เป็นต้น", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                if (cmbManualWorkDateStatus.Text == "")
                {
                    MessageBox.Show("กรุณาระบุ Status ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
        }

        private void UpdateMWD()
        {
            try
            {
                if (isMWDRequiredDataCorrect())
                {
                    TimeSpan addHour = TimeSpan.Parse(txtManualWorkDateTime.Text);
                    DateTime addDate = Convert.ToDateTime(ManualWorkDateDate.Text);
                    TimeSpan getTotalDays = (Convert.ToDateTime(ManualWorkDateDateTo.Text) - Convert.ToDateTime(ManualWorkDateDate.Text));
                    Decimal totalDays = Convert.ToDecimal(getTotalDays.TotalDays + 1);
                    ManualWorkDate[] manualWorkDateObj = new ManualWorkDate[1];
                    TransactionLog[] transactionLogObj = new TransactionLog[1];

                    for (int i = 0; i < totalDays; i++)
                    {
                        manualWorkDateObj[0] = new ManualWorkDate()
                        {
                            ManualWorkDateDate = addDate.AddDays(i),
                            ManualWorkDateType_ID = (string)cmbManualWorkDateType_ID.SelectedValue,
                            Employee_ID = txtEmployee_ID.Text,
                            Times = addHour,
                            Status = (Boolean)cmbManualWorkDateStatus.SelectedValue,
                            ModifiedDate = DateTime.Now
                        };
                        _hrisBusinessLayer.AddManualWorkDate(manualWorkDateObj);

                        transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                        XMaxTransactionLog_ID = transactionLog.Count > 0 ? Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1 : 1;
                        transactionLogObj[0] = new TransactionLog()
                        {
                            TransactionLog_ID = XMaxTransactionLog_ID,
                            TransactionType_ID = "A",
                            Table_ID = "T007",
                            TransactionDate = DateTime.Now,
                            FieldName = "Employee_ID",
                            PKFields = txtEmployee_ID.Text,
                            FKFields = (String)cmbManualWorkDateType_ID.SelectedValue,
                            OldData = "",
                            NewData = Convert.ToString(addDate.AddDays(i)),
                            ModifiedDate = DateTime.Now,
                            ModifiedUser = _singleton.username,
                            Noted = "From OT screen by HRM"
                        };
                        _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                    }
                    ShowCheckInOut();
                    AddManualWork.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SpecialOTButton_Click(object sender, RoutedEventArgs e)
        {
            if (txtEmployee_ID.Text != string.Empty)
            {
                SpecialOTWindows specialOT = new SpecialOTWindows(txtEmployee_ID.Text);
                specialOT.ShowDialog();

                cmbLotSearch.SelectedIndex = cmbLot.SelectedIndex;
                var newEventArgs = new RoutedEventArgs(CheckBox.ClickEvent);
                ChkFilterSearch.RaiseEvent(newEventArgs);
                txtSearch.Text = txtFirstNameTH.Text;
                GetDataFromSearch();
                ClearInformationData("A");
            }
            else
            {
                MessageBox.Show("กรุณาเลือกพนักงาน", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearManualWork(bool isCheck)
        {
            cmbManualWorkDateType_ID.Text = "";
            cmbManualWorkDateStatus.Text = "";
            ManualWorkDateDate.Text = isCheck ? OTDate.Text : "";
            ManualWorkDateDateTo.Text = isCheck ? OTDate.Text : "";
            txtManualWorkDateTime.Text = "";
            GbManualWork.IsEnabled = isCheck;
        }
    }
}
