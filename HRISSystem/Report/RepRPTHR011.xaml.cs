﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;
using Microsoft.Reporting.WinForms;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR011.xaml
    /// </summary>
    public partial class RepRPTHR011 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass>();

        public RepRPTHR011()
        {
            InitializeComponent();
            //_ReportViewer1.Load += ReportViewer_Load;

            
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //get department into combobox
                //departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
                //CmbDept.ItemsSource = departmentFromSTECPayrollList;

            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DateFrom.Text == "" || DateTo.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (Convert.ToDateTime(DateFrom.Text) > Convert.ToDateTime(DateTo.Text))
                {
                    MessageBox.Show("กรุณาเลือกช่วงวันที่ให้ถูกต้อง", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                 _ReportViewer1.Reset();

                    HRISReportDataset.sp_GetEmployeeInformationsDataTable sp_GetEmployeeInformationsDataTable = new HRISReportDataset.sp_GetEmployeeInformationsDataTable();
                    HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter sp_GetEmployeeInformationsTableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter();

                    sp_GetEmployeeInformationsTableAdapter.Fill(sp_GetEmployeeInformationsDataTable);

                    ReportDataSource sp_GetEmployeeInformationsDataSource = new ReportDataSource();

                    sp_GetEmployeeInformationsDataSource.Name = "DataSet1";
                    DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                    DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                    foreach (DataRow d in result)
                    {
                        D2.ImportRow(d);
                    }
                    sp_GetEmployeeInformationsDataSource.Value = D2;
                    _ReportViewer1.LocalReport.DataSources.Add(sp_GetEmployeeInformationsDataSource);
                    _ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";
                    _ReportViewer1.RefreshReport();



                //if (RdtAllDept.IsChecked == true)
                //{
                //    _ReportViewer1.Reset();

                //    HRISReportDataset.sp_GetEmployeeInformationsDataTable sp_GetEmployeeInformationsDataTable = new HRISReportDataset.sp_GetEmployeeInformationsDataTable();
                //    HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter sp_GetEmployeeInformationsTableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter();

                //    sp_GetEmployeeInformationsTableAdapter.Fill(sp_GetEmployeeInformationsDataTable);

                //    ReportDataSource sp_GetEmployeeInformationsDataSource = new ReportDataSource();

                //    sp_GetEmployeeInformationsDataSource.Name = "DataSet1";
                //    DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //    DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //    foreach (DataRow d in result)
                //    {
                //        D2.ImportRow(d);
                //    }
                //    sp_GetEmployeeInformationsDataSource.Value = D2;
                //    _ReportViewer1.LocalReport.DataSources.Add(sp_GetEmployeeInformationsDataSource);
                //    _ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";
                //    _ReportViewer1.RefreshReport();


                //    if (RdtStaffTypeAll.IsChecked == true)
                //    {

                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;

                //    }
                //    else if (RdtStaffType1.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select("Staff_type = 1" +" and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;

                        

                //    }
                //    else if (RdtStaffType2.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select("Staff_type = 2 and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType3.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" Staff_type = 3  and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType4.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select("Staff_type is null and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }

                //    _ReportViewer1.LocalReport.DataSources.Add(sp_GetEmployeeInformationsDataSource);

                //    _ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";

                //    _ReportViewer1.RefreshReport();
                //}
                //else if (RdtByDept.IsChecked == true)
                //{
                //    _ReportViewer1.Reset();

                //    HRISReportDataset.sp_GetEmployeeInformationsDataTable sp_GetEmployeeInformationsDataTable = new HRISReportDataset.sp_GetEmployeeInformationsDataTable();
                //    HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter sp_GetEmployeeInformationsTableAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter();

                //    sp_GetEmployeeInformationsTableAdapter.Fill(sp_GetEmployeeInformationsDataTable);

                //    ReportDataSource sp_GetEmployeeInformationsDataSource = new ReportDataSource();

                //    sp_GetEmployeeInformationsDataSource.Name = "DataSet1";

                //    if (RdtStaffTypeAll.IsChecked == true)
                //    {
                       
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType1.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" Staff_type = 1  and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType2.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select("Staff_type = 2 and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType3.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select(" Staff_type = 3  and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }
                //    else if (RdtStaffType4.IsChecked == true)
                //    {
                //        DataTable D2 = sp_GetEmployeeInformationsDataTable.Clone();
                //        DataRow[] result = sp_GetEmployeeInformationsDataTable.Select("Staff_type is null and StartDateFromPayroll >= #" + Convert.ToDateTime(DateFrom.Text) + "# and StartDateFromPayroll <= #" + Convert.ToDateTime(DateTo.Text) + "#");
                //        foreach (DataRow d in result)
                //        {
                //            D2.ImportRow(d);
                //        }
                //        sp_GetEmployeeInformationsDataSource.Value = D2;
                //    }


                //    _ReportViewer1.LocalReport.DataSources.Add(sp_GetEmployeeInformationsDataSource);

                //    _ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";

                //    _ReportViewer1.RefreshReport();
                //}



                //Microsoft.Reporting.WinForms.ReportDataSource reportDataSour1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                //HRISReportDataset dataset = new HRISReportDataset();
                //dataset.BeginInit();

                //reportDataSour1.Name = "DataSet1";//Name of the report dataset in our .RDLC file
                //reportDataSour1.Value = dataset.sp_GetEmployeeInformations;
                //this._ReportViewer1.LocalReport.DataSources.Add(reportDataSour1);
                //this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";

                //dataset.EndInit();
                //HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter sp_getEmployeeInformationTabletAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter();
                //sp_getEmployeeInformationTabletAdapter.ClearBeforeFill = true;
                //sp_getEmployeeInformationTabletAdapter.Fill(dataset.sp_GetEmployeeInformations);


                //_ReportViewer1.RefreshReport();

                //_isReportViewerLoaded = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }             
        }

       
        //private bool _isReportViewerLoaded;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            //if (!_isReportViewerLoaded)
            //{
            //    Microsoft.Reporting.WinForms.ReportDataSource reportDataSour1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            //    HRISReportDataset dataset = new HRISReportDataset();
            //    dataset.BeginInit();

            //    reportDataSour1.Name = "DataSet1";//Name of the report dataset in our .RDLC file
            //    reportDataSour1.Value = dataset.sp_GetEmployeeInformations;
            //    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSour1);
            //    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR011.rdlc";

            //    dataset.EndInit();
            //    HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter sp_getEmployeeInformationTabletAdapter = new HRISReportDatasetTableAdapters.sp_GetEmployeeInformationsTableAdapter();
            //    sp_getEmployeeInformationTabletAdapter.ClearBeforeFill = true;
            //    sp_getEmployeeInformationTabletAdapter.Fill(dataset.sp_GetEmployeeInformations);


            //    _ReportViewer1.RefreshReport();

            //    _isReportViewerLoaded = true;
            //}
        }

        private void RdtByDept_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (RdtByDept.IsChecked == true)
            //    {
            //        CmbDept.IsEnabled = true;
            //        CmbDept.Focus();
            //    }
            //    else if (RdtByDept.IsChecked == false)
            //    {
            //        CmbDept.IsEnabled = false;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
        }

        

      
    }
}
