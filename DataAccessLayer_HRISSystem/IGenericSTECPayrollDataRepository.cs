﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace DataAccessLayer_HRISSystem
{
   public interface IGenericSTECPayrollDataRepository<T> where T : class
    {
        IList<T> GetAllSTECPayroll(params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetListSTECPayroll(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingleSTECPayroll(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void AddSTECPayroll(params T[] items);
        void UpdateSTECPayroll(params T[] items);
        void RemoveSTECPayroll(params T[] items);
    }
}
