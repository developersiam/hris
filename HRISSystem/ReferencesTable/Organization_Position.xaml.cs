﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_Position.xaml
    /// </summary>
    public partial class Organization_Position : Page
    {
        
        private class CurrentFlag //class
        {
            public bool CurrentFlags { get; set; }  //attribute
            public string CurrentFlagName { get; set; } //attribute
        }

        private  BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private string XFromPage;
        public Organization_Position()
        {
            InitializeComponent();         
        }

        public Organization_Position(string _xfromPage)
        {
            InitializeComponent();
            XFromPage = _xfromPage;
        }
        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
               

                txtSearch.Text = "";
                txtPosition_ID.Text = "";
                txtPositionNameTH.Text = "";
                txtPositionNameEN.Text = "";
                txtJobspecification.Text = "";
           
                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllPositions();  
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {               
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                double XMaxPosition_ID;
                List<Position> XOrg;
                XOrg = _hrisBusinessLayer.GetAllPositions().ToList();
                if (XOrg.Count > 0)
                {
                    XMaxPosition_ID = Convert.ToDouble(XOrg.Max(c => c.Position_ID)) + 1;
                }
                else
                {
                    XMaxPosition_ID = 1;
                }
                txtPosition_ID.Text = XMaxPosition_ID.ToString();

                Position[] positionObj = new Position[1];
                Position position1 = new Position(); //created  1 instanct or object
                position1.Position_ID = XMaxPosition_ID.ToString();
                position1.PositionNameTH = txtPositionNameTH.Text;             
                position1.PositionNameEN = txtPositionNameEN.Text;            
                position1.JobSpecification = txtJobspecification.Text;
                position1.CreatedDate = DateTime.Now;
                position1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                position1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                position1.ModifiedDate = DateTime.Now;

                positionObj[0] = position1;
                _hrisBusinessLayer.AddPosition(positionObj);


                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T012";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Position_ID";
                transactionLog1.PKFields = XMaxPosition_ID.ToString();
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtPositionNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {             
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Position position1 = new Position(); //created  1 instanct or object
                Position query2 = new Position();  // created 1 instance or object
                query2 = (Position)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                position1.Position_ID = txtPosition_ID.Text;
                position1.PositionNameTH = txtPositionNameTH.Text;
                position1.PositionNameEN = txtPositionNameEN.Text;
                position1.JobSpecification = txtJobspecification.Text;
                position1.CreatedDate = query2.CreatedDate;
                position1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                position1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                position1.ModifiedDate = DateTime.Now;
                _hrisBusinessLayer.UpdatePosition(position1);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "U";
                transactionLog1.Table_ID = "T012";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Position_ID";
                transactionLog1.PKFields = query2.Position_ID;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = query2.PositionNameTH ;
                transactionLog1.NewData = txtPositionNameTH.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                Position query2 = new Position();  // created 1 instance or object
                query2 = (Position)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                _hrisBusinessLayer.RemovePosition(query2);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T012";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "Position_ID";
                transactionLog1.PKFields = query2.Position_ID;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = query2.PositionNameTH;
                transactionLog1.NewData = "";
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Position position1 = new Position();
                position1 = (Position)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                if (position1.Position_ID == null )
                {
                    AddButton.IsEnabled = true;
                    //txtPosition_ID.IsReadOnly = false;
                    //txtPosition_ID.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    AddButton.IsEnabled = false;
                    //txtPosition_ID.IsReadOnly = true;
                    //txtPosition_ID.Background = new SolidColorBrush(Colors.LightGray);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void getDataFromSearch()
        {
            try
            {
                if (ChkAllSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = null;
                    DataGrid.ItemsSource = _hrisBusinessLayer.GetAllPositions().OrderByDescending(b=>b.PositionNameTH);
                }
                else if (ChkFilterSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = null;
                    DataGrid.ItemsSource = _hrisBusinessLayer.GetAllPositions().Where(p => p.PositionNameTH.Contains(txtSearch.Text));
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = true;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
