﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for GetOTInformation.xaml
    public class OT2
    {
        private class OTType
        {
            public string OTTypeCode { get; set; }
            public string OTTypeName { get; set; }
        }

        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        public string Employee_ID { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public Nullable<System.DateTime> OTDate { get; set; }
        public string OTTypeCode { get; set; }
        public string OTTypeName { get; set; }
        public Nullable<decimal> NCountOT { get; set; }

        public List<OT2> getOT(DateTime _xstartdate, DateTime _xendDate, string _employee_ID)
        {
            List<OT> OjAll = new List<OT>();
            OjAll = _hrisBusinessLayer.GetAllOTs().Where(p => p.OTDate >= _xstartdate && p.OTDate <= _xendDate && p.Employee_ID == _employee_ID).ToList();

            List<OTType> otTypeList = new List<OTType>();
            OTType h4 = new OTType { OTTypeCode = "H", OTTypeName = "Holiday" };
            OTType h5 = new OTType { OTTypeCode = "S", OTTypeName = "OT-holiday" };
            otTypeList.Add(h4);
            otTypeList.Add(h5);

            var query = (
                            from a in OjAll
                            join b in otTypeList on a.OTType equals b.OTTypeCode
                            select new OT2
                            {
                                Employee_ID = a.Employee_ID,
                                TitleNameTH = a.Employee.Person.TitleName.TitleNameInitialTH,
                                FirstNameTH = a.Employee.Person.FirstNameTH,
                                LastNameTH = a.Employee.Person.LastNameTH,
                                OTDate = a.OTDate,
                                OTTypeCode = a.OTType,
                                OTTypeName = b.OTTypeName,
                                NCountOT = a.NcountOT,
                            });
            return query.ToList();
        }
    }

    public partial class GetOTInformation : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private DateTime _startDate;
        private DateTime _endDate;
        private string _employee_ID;
        private string _xfromPage;
        private string XType;

        public GetOTInformation( DateTime startDate, DateTime endDate, string employee_ID,string xFromPage,string _xtype)
        {
            InitializeComponent();
            _startDate = startDate;
            _endDate = endDate;
            _employee_ID = employee_ID;
            _xfromPage = xFromPage;
            XType = _xtype;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                OT2 ot2 = new OT2();

                OTDetailsDataGrid.ItemsSource = ot2.getOT(_startDate, _endDate, _employee_ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OTDetailsDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (OTDetailsDataGrid.SelectedIndex <= -1)
                    return;

                OT2 ot2 = new OT2();
                ot2 = (OT2)OTDetailsDataGrid.SelectedItem;

                if (_xfromPage == "OT")
                {
                    this.NavigationService.Navigate(new HumanResource_OT(Convert.ToDateTime(ot2.OTDate), ot2.OTTypeCode.ToString(), ot2.Employee_ID.ToString(), true, _xfromPage));
                }
                else if (_xfromPage == "RequstOTOnline")
                {
                    this.NavigationService.Navigate(new OTByAdmin.FrmRequestOT(Convert.ToDateTime(ot2.OTDate), ot2.OTTypeCode.ToString(), ot2.Employee_ID.ToString(), true, _xfromPage));
                }
                else if (_xfromPage == "ManagerApproveOTOnline")
                {
                    this.NavigationService.Navigate(new OTByAdmin.FrmApproveOTByManager(Convert.ToDateTime(ot2.OTDate), ot2.OTTypeCode.ToString(), ot2.Employee_ID.ToString(), true, _xfromPage,XType));
                }
                else if (_xfromPage == "HRMApproveOTOnline")
                {
                    this.NavigationService.Navigate(new OTByAdmin.FrmApproveOTByHRM(Convert.ToDateTime(ot2.OTDate), ot2.OTTypeCode.ToString(), ot2.Employee_ID.ToString(), true, _xfromPage, XType));
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
