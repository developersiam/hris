﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using System.Data;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR025.xaml
    /// </summary>
    public partial class RepRPTHR025 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass> departmentFromSTECPayrollList = new List<BusinessLayer_HRISSystem.BusinessObject.DepartMentFromSTECPayrollClass>();

        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();

        public RepRPTHR025()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //get department into combobox
                departmentFromSTECPayrollList = _hrisSTECPayrollBusinessLayer.GetDepartmentFromSTECPayroll();
                CmbDept.ItemsSource = departmentFromSTECPayrollList;

                lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
                cmbLot.ItemsSource = lot_NumberClassList;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RdtByDept_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RdtByDept.IsChecked == true)
                {
                    CmbDept.IsEnabled = true;
                    CmbDept.Focus();
                }
                else if (RdtByDept.IsChecked == false)
                {
                    CmbDept.IsEnabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbLot.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (RdtByDept.IsChecked == true && CmbDept.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกแผนก/ฝ่าย", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (RdtAllDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();

                    HRISReportDataset.sp_GetPAYROLLOFSTAFF_RPTHR025DataTable sp_GetPAYROLLOFSTAFF_RPTHR025DataTable = new HRISReportDataset.sp_GetPAYROLLOFSTAFF_RPTHR025DataTable();
                    HRISReportDatasetTableAdapters.sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter = new HRISReportDatasetTableAdapters.sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter();
                    sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter.Fill(sp_GetPAYROLLOFSTAFF_RPTHR025DataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource1.Name = "sp_GETPAYROLLOFSTAFF_RPTHR025DataSet";

                    reportDataSource1.Value = sp_GetPAYROLLOFSTAFF_RPTHR025DataTable;
                  
                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
                    
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR025.rdlc";
                    _ReportViewer1.RefreshReport();
                }
                else if (RdtByDept.IsChecked == true)
                {
                    _ReportViewer1.Reset();


                    HRISReportDataset.sp_GetPAYROLLOFSTAFF_RPTHR025DataTable sp_GetPAYROLLOFSTAFF_RPTHR025DataTable = new HRISReportDataset.sp_GetPAYROLLOFSTAFF_RPTHR025DataTable();
                    HRISReportDatasetTableAdapters.sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter = new HRISReportDatasetTableAdapters.sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter();
                    sp_GetPAYROLLOFSTAFF_RPTHR025TableAdapter.Fill(sp_GetPAYROLLOFSTAFF_RPTHR025DataTable, cmbLot.Text);
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource1.Name = "sp_GETPAYROLLOFSTAFF_RPTHR025DataSet";


                    DataTable D2 = sp_GetPAYROLLOFSTAFF_RPTHR025DataTable.Clone();
                    string expression;
                    expression = "dept_code = '" + CmbDept.SelectedValue.ToString() + "'";
                    DataRow[] result = sp_GetPAYROLLOFSTAFF_RPTHR025DataTable.Select(expression);
                    foreach (DataRow d in result)
                    {
                        D2.ImportRow(d);
                    }
                    reportDataSource1.Value = D2;

                    this._ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);                   
                    this._ReportViewer1.LocalReport.ReportEmbeddedResource = "HRISSystem.RDLC.RPTHR025.rdlc";
                    _ReportViewer1.RefreshReport();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
