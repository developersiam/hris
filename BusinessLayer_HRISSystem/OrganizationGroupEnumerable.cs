﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem
{
  public static class OrganizationGroupEnumerable
    {
        public static IList<OrganizationGroup> BuildOrganizationTree(this IEnumerable<OrganizationGroup> source)
        {
            var groups = source.GroupBy(i => i.STECParentID);

            var roots = groups.FirstOrDefault(g => g.Key.Value == 0).ToList();

            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddOrganizationChildren(roots[i], dict);
            }

            return roots;
        }

        private static void AddOrganizationChildren(OrganizationGroup node, IDictionary<int, List<OrganizationGroup>> source)
        {
            if (source.ContainsKey(Convert.ToInt32(node.Organization_ID)))
            {
                node.Children = source[Convert.ToInt32(node.Organization_ID)];
                for (int i = 0; i < node.Children.Count; i++)
                    AddOrganizationChildren(node.Children[i], source);
            }
            else
            {
                node.Children = new List<OrganizationGroup>();
            }
        }
    }
}
