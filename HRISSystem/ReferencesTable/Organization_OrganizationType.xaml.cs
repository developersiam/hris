﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;

namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for Organization_OrganizationType.xaml
    /// </summary>
    public partial class Organization_OrganizationType : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        private string XFromPage;
        public Organization_OrganizationType()
        {
            InitializeComponent();
        }

        public Organization_OrganizationType(string _xfromPage)
        {
            InitializeComponent();
            XFromPage = _xfromPage;
        }

        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;
                txtOrganizationType_ID.IsReadOnly = false;
                txtOrganizationType_ID.Background = new SolidColorBrush(Colors.White);
                txtOrganizationType_ID.Text = "";
                txtOrganizationTypeName.Text = "";
                txtNoted.Text = "";
                //ValidFromDatePicker.Text = Convert.ToString( DateTime.Now);
                //EndDateDatePicker.Text = Convert.ToString(DateTime.Now);   
                DataGrid.ItemsSource = null; //clear 
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllOrganizationTypes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid.ItemsSource = _hrisBusinessLayer.GetAllOrganizationTypes();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                OrganizationType[] queryObj = new OrganizationType[1];
                OrganizationType query1 = new OrganizationType();  // created 1 instance or object

                query1.OrganizationType_ID = txtOrganizationType_ID.Text;
                query1.OrganizationTypeName = txtOrganizationTypeName.Text;
                query1.Noted = txtNoted.Text;
                query1.CreatedDate = DateTime.Now;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;

                queryObj[0] = query1;
                _hrisBusinessLayer.AddOrganizationType(queryObj);
                ClearData();

                if(XFromPage != "")
                    NavigationService.GoBack();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidFromDatePicker.Text == "")
                    ValidFromDatePicker.Text = Convert.ToString(DateTime.MinValue);
                if (EndDateDatePicker.Text == "")
                    EndDateDatePicker.Text = Convert.ToString(DateTime.MaxValue);

                if (MessageBox.Show("Do you want to edit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }
                OrganizationType query1 = new OrganizationType();  // created 1 instance or object
                OrganizationType query2 = new OrganizationType();  // created 1 instance or object

                query2 = (OrganizationType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                query1.OrganizationType_ID = txtOrganizationType_ID.Text;
                query1.OrganizationTypeName = txtOrganizationTypeName.Text;
                query1.Noted = txtNoted.Text;
                query1.CreatedDate = query2.CreatedDate;
                query1.ValidFrom = Convert.ToDateTime(ValidFromDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.ModifiedDate = DateTime.Now;

                _hrisBusinessLayer.UpdateOrganizationType(query1);
                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    return;
                }

                OrganizationType query1 = new OrganizationType();  // created 1 instance or object
                query1 = (OrganizationType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
              
                _hrisBusinessLayer.RemoveOrganizationType(query1);
                ClearData();

                if (XFromPage != "")
                    NavigationService.GoBack();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                OrganizationType query1 = new OrganizationType();  // created 1 instance or object
                query1 = (OrganizationType)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
               
                if (query1 == null)
                {
                    AddButton.IsEnabled = true;
                    txtOrganizationType_ID.IsReadOnly = false;
                    txtOrganizationType_ID.Background = new SolidColorBrush(Colors.White);


                }
                else
                {
                    AddButton.IsEnabled = false;
                    txtOrganizationType_ID.IsReadOnly = true;
                    txtOrganizationType_ID.Background = new SolidColorBrush(Colors.LightGray);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
