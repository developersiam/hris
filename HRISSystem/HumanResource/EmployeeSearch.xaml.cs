﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;

namespace HRISSystem.HumanResource
{
    /// <summary>
    /// Interaction logic for EmployeeSearch.xaml
    /// </summary>
    public partial class EmployeeSearch : Page
    {
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        public string XFromPagse;
        TrainingCourse XTrainingCourse;
        DateTime BeginDate;
        DateTime Enddate;
        private string XFromEmployee_ID;
        public EmployeeSearch()
        {
            InitializeComponent();
        }

        public EmployeeSearch(string xFromPage)
        {
            InitializeComponent();
            XFromPagse = xFromPage;
        }
        public EmployeeSearch(string xFromPage, string _employee_id)
        {
            InitializeComponent();
            XFromPagse = xFromPage;
            XFromEmployee_ID = _employee_id;

        }

        public EmployeeSearch(string XFromPage,TrainingCourse _xTrainingID,DateTime _beginDate,DateTime _endDate)
        {
            InitializeComponent();
            XFromPagse = XFromPage;
            XTrainingCourse = _xTrainingID;
            BeginDate = _beginDate;
            Enddate = _endDate;

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkAllSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;
                ChkResignSearch.IsChecked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChkFilterSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                ChkAllSearch.IsChecked = false;
                ChkResignSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = true;
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (XFromPagse == "SocialWalfare")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        NavigationService.Navigate(new Walefares.SocialWalfare(query1));
                    }
                }

                if(XFromPagse == "WorkUpCountry")
                {
                    if(ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        NavigationService.Navigate(new Walefares.HumanResource_WorkUpCountry(query1));
                    }
                         
                }
                else if(XFromPagse == "Leave")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new Walefares.HumanResource_Leave(query1));
                        //NavigationService.GoBack();
                    }


                }
                else if (XFromPagse == "OT")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new Walefares.HumanResource_OT(query1));
                        //NavigationService.GoBack();
                    }

                }
                else if (XFromPagse == "OTRequest")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new OTByAdmin.FrmRequestOT(query1));
                        //NavigationService.GoBack();
                    }
                }

                else if(XFromPagse == "CurrentEmployee")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                        this.NavigationService.Navigate(new Employees.Employees(query1));
                    }
                    else
                    {
                        EmployeeResignInformation query1 = new EmployeeResignInformation();  // created 1 instance or object                   
                        query1 = (EmployeeResignInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                        this.NavigationService.Navigate(new Employees.Employees(query1));
                    }                                     
                }
                else if(XFromPagse =="Report1"){
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new Report.RepRPTHR015(query1));
                    }
                }
                else if (XFromPagse == "Training")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                        this.NavigationService.Navigate(new ReferencesTable.RefTraining(query1, XTrainingCourse, BeginDate, Enddate));
                    }
                      
                }
                else if (XFromPagse == "Report20")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new Report.RepRPTHR020(query1));
                    }
                }
                else if (XFromPagse == "Report26")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new Report.RepRPTHR026(query1));
                    }
                }
                else if (XFromPagse == "SetupOTAdminForManager")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new OTByAdmin.FrmSetupAdmin(query1, XFromPagse, XFromEmployee_ID));
                    }
                }
                else if (XFromPagse == "SetupOTAdmin")
                {
                    if (ChkResignSearch.IsChecked == false)
                    {
                        EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                        query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้

                        this.NavigationService.Navigate(new OTByAdmin.FrmSetupAdmin(query1,XFromPagse,XFromEmployee_ID));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GetDataFromSearch()
        {
            try
            {
                EmployeeCurrentInformation employeeCurrentInformation = new EmployeeCurrentInformation();
                EmployeeResignInformation employeeResignInformation = new EmployeeResignInformation();
                DataGrid.ItemsSource = null;

                if (ChkAllSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = employeeCurrentInformation.GetAllEmployeeCurrentInformation();
                }
                else if (ChkFilterSearch.IsChecked == true)
                {
                    
                    DataGrid.ItemsSource = employeeCurrentInformation.GetAllEmployeeCurrentInformation().Where(p=> p.xFirstNameTH.Contains(txtSearch.Text));
                }
                else if (ChkResignSearch.IsChecked == true)
                {
                    DataGrid.ItemsSource = employeeResignInformation.GetAllEmployeeResignInformation();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChkFilterSearch.IsChecked == true && txtSearch.Text =="") 
                {
                    MessageBox.Show("กรุณาคีย์ชื่อที่ต้องการค้นหาข้อมูล");
                    return;
                }
                GetDataFromSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToPersonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChkResignSearch.IsChecked == true)
                {
                    EmployeeResignInformation query1 = new EmployeeResignInformation();
                    query1 = (EmployeeResignInformation)DataGrid.SelectedItem;
                    if (query1 == null)
                    {
                        MessageBox.Show("กรุณาเลือกแถวที่มีข้อมูลจากตารางด้านบน");
                        return;
                    }
                    string xPersonID = query1.xPerson_ID;


                    this.NavigationService.Navigate(new Employees.Person_Person(xPersonID,"EmpSearch",query1.xEmployee_ID,""));
                }
                else
                {
                    EmployeeCurrentInformation query1 = new EmployeeCurrentInformation();  // created 1 instance or object                   
                    query1 = (EmployeeCurrentInformation)DataGrid.SelectedItem; //การดึงค่าในแถวที่ select ที่ละแถวไว้
                    if (query1 == null)
                    {
                        MessageBox.Show("กรุณาเลือกแถวที่มีข้อมูลจากตารางด้านบน");
                        return;
                    }
                    string xPersonID = query1.xPerson_ID;


                    this.NavigationService.Navigate(new Employees.Person_Person(xPersonID,"EmpSearch",query1.xNoted,""));
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

     
        private void ChkResignSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChkAllSearch.IsChecked = false;
                ChkFilterSearch.IsChecked = false;
                txtSearch.Text = "";
                txtSearch.IsEnabled = false;               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SearchNameButton.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
