﻿using System;
using System.ComponentModel;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class PNDViewModel
    {
        [Description("#")]
        public int pndIndex { get; set; }
        [Description("Staff Type")]
        public string staffType { get; set; }
        [Description("Tax Type")]
        public string taxType { get; set; }
        [Description("Citizen ID")]
        public string citizenID { get; set; }
        [Description("Title")]
        public string titleName { get; set; }
        [Description("Name")]
        public string employeeName { get; set; }
        [Description("Paid Date")]
        public DateTime? paidDate { get; set; }
        [Description("Actual Salary")]
        public decimal? actualSalary { get; set; }
        [Description("Incoming Tax")]
        public decimal? incomeTax { get; set; }
        [Description("Actual Salary (Imported)")]
        public decimal? importedActualSalary { get; set; }
        [Description("Incoming Tax (Imported)")]
        public decimal? importedIncomeTax { get; set; }
        [Description("Actual Salary (Total)")]
        public decimal? totalActualSalary { get; set; }
        [Description("Incoming Tax (Total)")]
        public decimal? totalIncomeTax { get; set; }
        [Description("เงื่อนไขการเสียภาษี")]
        public int revenueCondition { get; set; }
        [Description("Import")]
        public bool? importStatus { get; set; }
    }
}
