﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public   class NotScanInAndOutDetails
    {     
        public string Employee_ID { get; set; }
        public string EMPACCCode { get; set; }
        public string FingerScanID { get; set; }
        public string EMPTitlename { get; set; }
        public string EMPFirstName { get; set; }
        public string EMPlastName { get; set; }  
        public string StaffType { get; set; }
        public int NCount { get; set; }
        
    }
}
