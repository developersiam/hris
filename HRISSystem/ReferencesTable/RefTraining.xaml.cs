﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModel_HRISSystem;
using BusinessLayer_HRISSystem;
namespace HRISSystem.ReferencesTable
{
    /// <summary>
    /// Interaction logic for RefTraining.xaml
    /// </summary>
    public partial class RefTraining : Page
    {
        //public List<Training> ObjList;
        private EmployeeCurrentInformation xFromEmployeeCurrentInformation;
        private TrainingCourse xFromTrainingCourse;
        private TrainingCourse trainingCourse;
        private Training training;

        public List<TransactionLog> transactionLog;
        Double XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();

        public RefTraining()
        {
            InitializeComponent();
        }
        public RefTraining(string employee_ID)
        {
            InitializeComponent();          
            GetDataFromEmployee_ID(employee_ID);
        }
        public RefTraining(TrainingCourse x4)
        {
            InitializeComponent();
            if (x4 != null)
            {
                GetTrainingCourse(x4.TrainingCourse_ID);
                xFromTrainingCourse = x4;
            }
        }
        public RefTraining(TrainingCourse x4,string employee_ID)
        {
            InitializeComponent();
            if (x4 != null)
            {
                GetTrainingCourse(x4.TrainingCourse_ID);
                xFromTrainingCourse = x4;
                GetDataFromEmployee_ID(employee_ID);
            }
        }
        public RefTraining(EmployeeCurrentInformation x3,TrainingCourse xT,DateTime begindate, DateTime enddate)
        {
            InitializeComponent();
            if (x3 != null)
            {
                GetEmployeeInformation(x3);
                xFromEmployeeCurrentInformation = x3;
                GetTrainingCourse(xT.TrainingCourse_ID);
                BeginDatePicker.Text = Convert.ToString(begindate);
                EndDateDatePicker.Text = Convert.ToString(enddate);
                xFromTrainingCourse = xT;
            }  
        }
       
        public void GetEmployeeInformation(EmployeeCurrentInformation x)
        {
            txtEmployee_ID.Text = x.xEmployee_ID;
            txtTitleNameTH.Text = x.xTitleNameTH;
            txtFirstNameTH.Text = x.xFirstNameTH;
            txtLastNameTH.Text = x.xLastNameTH;
            txtPositionTypeNameTH.Text = x.xPositionTypeNameTH;
        } 
       
        public void GetTrainingCourse(string x)
        {
            trainingCourse = _hrisBusinessLayer.GetTrainingCourseByID(x);
            txtID.Text = trainingCourse.TrainingCourse_ID;
            txtNameTH.Text = trainingCourse.TrainingCourseNameTH;
        }

         

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                if (xFromEmployeeCurrentInformation != null || xFromTrainingCourse != null)
                    NavigationService.RemoveBackEntry();

                DataGrid.ItemsSource = null; //clear 

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtID.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกหัวข้อการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BeginDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่เริ่มการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BeginDatePicker.Focus();
                    return;
                }
                if (EndDateDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่สิ้นสุดการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    EndDateDatePicker.Focus();
                    return;
                }

                if (txtEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ต้องการ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }

                List<Training> t = new List<Training>();
                t = _hrisBusinessLayer.GetTrainingByEmployee(txtEmployee_ID.Text).Where(b => b.BeginDate >= Convert.ToDateTime(BeginDatePicker.Text) && b.EndDate <= Convert.ToDateTime(EndDateDatePicker.Text)).ToList();
                if (t.Count() > 0 )
                {
                    MessageBox.Show("รหัสพนักงานนี้ได้เพิ่มใน Training course และช่วงวันที่ดังกล่าวแล้ว กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }


                Training[] queryObj = new Training[1];
                Training query1 = new Training();  // created 1 instance or object

                query1.TrainingCourse_ID = txtID.Text;
                query1.BeginDate = Convert.ToDateTime(BeginDatePicker.Text);
                query1.EndDate = Convert.ToDateTime(EndDateDatePicker.Text);
                query1.Employee_ID = txtEmployee_ID.Text;
                query1.Status = true;
                query1.CreatedDate = DateTime.Now;
                query1.ModifiedDate = DateTime.Now;
                queryObj[0] = query1;
                _hrisBusinessLayer.AddTraining(queryObj);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "A";
                transactionLog1.Table_ID = "T023";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "TrainingCourse_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtEmployee_ID.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BeginDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่เริ่มการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BeginDatePicker.Focus();
                    return;
                }
                if (EndDateDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์วันที่สิ้นสุดการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    EndDateDatePicker.Focus();
                    return;
                }

                if (txtEmployee_ID.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์รหัสพนักงานที่ต้องการ กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }

                List<Training> t = new List<Training>();
                t = _hrisBusinessLayer.GetTrainingByEmployee(txtEmployee_ID.Text).Where(b => b.BeginDate >= Convert.ToDateTime(BeginDatePicker.Text) && b.EndDate <= Convert.ToDateTime(EndDateDatePicker.Text)).ToList();
                if (t.Count() == 0)
                {
                    MessageBox.Show("ไม่มีรหัสพนักงานนี้ในช่วงการ Training course ดังกล่าว กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }

                if (training.Employee_ID == "")
                {
                    MessageBox.Show("ไม่มีรหัสพนักงานนี้ในช่วงการ Training course ดังกล่าว กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtEmployee_ID.Focus();
                    return;
                }
            
                _hrisBusinessLayer.RemoveTraining(training);

                transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                if (transactionLog.Count > 0)
                {
                    XMaxTransactionLog_ID = Convert.ToDouble(transactionLog.Max(c => c.TransactionLog_ID)) + 1;
                }
                else
                {
                    XMaxTransactionLog_ID = 1;
                }
                TransactionLog[] transactionLogObj = new TransactionLog[1];
                TransactionLog transactionLog1 = new TransactionLog();
                transactionLog1.TransactionLog_ID = Convert.ToDecimal(XMaxTransactionLog_ID);
                transactionLog1.TransactionType_ID = "D";
                transactionLog1.Table_ID = "T023";
                transactionLog1.TransactionDate = DateTime.Now;
                transactionLog1.FieldName = "TrainingCourse_ID";
                transactionLog1.PKFields = txtID.Text;
                transactionLog1.FKFields = "";
                transactionLog1.OldData = "";
                transactionLog1.NewData = txtEmployee_ID.Text;
                transactionLog1.ModifiedDate = DateTime.Now;
                transactionLog1.ModifiedUser = _singleton.username;
                transactionLogObj[0] = transactionLog1;
                _hrisBusinessLayer.AddTransactionLog(transactionLogObj);

                ClearData();

               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearData()
        {
            try
            {
                AddButton.IsEnabled = true;               
                txtEmployee_ID.IsReadOnly = false;
                txtEmployee_ID.Background = new SolidColorBrush(Colors.White);
                txtEmployee_ID.Text = "";
                txtTitleNameTH.Text = "";
                txtFirstNameTH.Text = "";
                txtLastNameTH.Text = "";

                DataGrid.ItemsSource = null;
                DataGrid.ItemsSource = _hrisBusinessLayer.GetTrainingByTrainingCourse(txtID.Text, Convert.ToDateTime(BeginDatePicker.Text), Convert.ToDateTime(EndDateDatePicker.Text)).ToList();
                txtEmployee_ID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void NavigateToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BeginDatePicker.Text == "" || EndDateDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ช่วงวันที่อบรมให้เรียบร้อยก่อนระบุรหัสพนักงานที่ต้องการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BeginDatePicker.Focus();
                    return;;
                }
                this.NavigationService.Navigate(new HumanResource.EmployeeSearch("Training", trainingCourse, Convert.ToDateTime(BeginDatePicker.Text), Convert.ToDateTime(EndDateDatePicker.Text)));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            if (txtID.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกหัวข้อการอบรม", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
             if (BeginDatePicker.Text == "" || EndDateDatePicker.Text == "")
                {
                    MessageBox.Show("กรุณาคีย์ช่วงวันที่อบรมให้เรียบร้อยก่อนระบุรหัสพนักงานที่ต้องการอบรม กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BeginDatePicker.Focus();
                    return;;
                }
            this.NavigationService.Navigate(new Report.RepRPTHR019(txtID.Text,Convert.ToDateTime(BeginDatePicker.Text),Convert.ToDateTime(EndDateDatePicker.Text)));
        }

        private void NavigateToSearchTrainingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NavigationService.Navigate(new RefTrainingCourse("Training",txtEmployee_ID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void txtEmployee_ID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (txtEmployee_ID.Text == "")
                    {
                        MessageBox.Show("กรุณาคีย์รหัสพนักงานหรือรหัสพนักงานนี้ไม่มีข้อมูล กรุณาตรวจสอบข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    GetDataFromEmployee_ID(txtEmployee_ID.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void GetDataFromEmployee_ID(string _xEmployee_ID)
        {
            try
            {
                EmployeeCurrentInformation ex = new EmployeeCurrentInformation(_xEmployee_ID);
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                txtPositionTypeNameTH.Text = ex.xPositionTypeNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {

                training = (Training)DataGrid.SelectedValue;
                GetTrainingCourse(training.TrainingCourse_ID);
                GetDataFromEmployee_ID(training.Employee_ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EndDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (txtID.Text != "" && BeginDatePicker.Text != "" && EndDateDatePicker.Text != "")
                {
                    DataGrid.ItemsSource = null;
                    DataGrid.ItemsSource = _hrisBusinessLayer.GetTrainingByTrainingCourse(txtID.Text, Convert.ToDateTime(BeginDatePicker.Text), Convert.ToDateTime(EndDateDatePicker.Text)).ToList();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void BeginDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (txtID.Text != "" && BeginDatePicker.Text != "" && EndDateDatePicker.Text != "")
                {
                    DataGrid.ItemsSource = null;
                    DataGrid.ItemsSource = _hrisBusinessLayer.GetTrainingByTrainingCourse(txtID.Text, Convert.ToDateTime(BeginDatePicker.Text), Convert.ToDateTime(EndDateDatePicker.Text)).ToList();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
