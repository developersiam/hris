﻿using System;
using System.ComponentModel;

namespace BusinessLayer_HRISSystem.BusinessObject
{
    public class ShiftControlViewModel
    {
        //[Description("#")]
        public string Employee_ID { get; set; }
        public string Name { get; set; }
        public string Staff_type { get; set; }
        public string Staff_status { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
