﻿using BusinessLayer_HRISSystem;
using DomainModel_HRISSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Walefares
{
    /// <summary>
    /// Interaction logic for SpecialOTWindows.xaml
    /// </summary>
    public partial class SpecialOTWindows : Window
    {
        private class OTType
        {
            public string OTTypeCode { get; set; }
            public string OTTypeName { get; set; }
        }
        private class StatusFlags
        {
            public bool StatusFlag { get; set; }
            public string StatusFlagName { get; set; }
        }
        private class RequestFlags
        {
            public string RequestFlag { get; set; }
            public string RequestFlagName { get; set; }
        }
        private class OTMinutes
        {
            public string Minutes { get; set; }
            public decimal Multiplier { get; set; }
        }

        private int xFingerScanID, cioFingerScanID;
        private List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass> lot_NumberClassList = new List<BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass>();
        private BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass lot_numberList = new BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass();
        private string xPayrollLot = "";
        private string cioOTDate;
        private BusinessLayer_HRISSystem.HRISTimeSTECBusinessLayer _timeSTECBuisnessLayer = new HRISTimeSTECBusinessLayer();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();
        Boolean XchkDate = false;
        public List<TransactionLog> transactionLog;
        Decimal XMaxTransactionLog_ID;
        SingletonConfiguration _singleton = SingletonConfiguration.getInstance();
        string _formState = "";

        public SpecialOTWindows(string EmployeeID)
        {
            try
            {
                InitializeComponent();
                _formState = "Add";

                BindLot();
                BindOTType();
                BindRequestFlag();
                BindOTHour();
                BindOTMinute();
                BindStatusFlag();

                cmbLot.SelectedValue = lot_numberList.CurrentLot();
                cmbOTType.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;

                GetEmployeeInformation(EmployeeID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public SpecialOTWindows(string EmployeeID, TestOT model)
        {
            try
            {
                InitializeComponent();
                _formState = "Update";

                BindLot();
                BindOTType();
                BindRequestFlag();
                BindOTHour();
                BindOTMinute();
                BindStatusFlag();

                cmbLot.SelectedValue = lot_numberList.CurrentLot();
                cmbOTType.SelectedIndex = 0;

                GetEmployeeInformation(EmployeeID);
                getOTDetail(model.Employee_ID.ToString(), Convert.ToDateTime(model.OTDATE), "SP", model.RequestType);

                cmbLot.IsEnabled = false;
                OTDate.IsEnabled = false;
                cmbOTType.IsEnabled = false;
                cmbRequest.IsEnabled = false;
                cmbStatus.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void getOTDetail(string employee_ID, DateTime otDate, string otType, string requestType) //แสดงรายละเอียด OT เฉพาะรายการนั้น    
        {
            try
            {
                OT ot = new OT();
                ot = _hrisBusinessLayer.GetOTByEmployeeOneRow(employee_ID, otDate, otType, requestType);
                xPayrollLot = ot.PayrollLot;
                cmbLot.SelectedValue = ot.PayrollLot;
                OTDate.Text = ot.OTDate.ToString();
                cmbOTType.SelectedValue = ot.OTType;
                cmbRequest.SelectedValue = requestType;
                double XCurrentOT = Convert.ToDouble(ot.NcountOT);
                //txtNcountOT.Text = Convert.ToString(XCurrentOT);
                string otAmount = ot.NcountOT.ToString();
                decimal otHour = Convert.ToDecimal(XCurrentOT.ToString().Substring(0, 1));
                decimal otMimute = Convert.ToDecimal(otAmount) - otHour;
                cmbOTHour.SelectedValue = otHour;
                cmbOTMinute.SelectedValue = otMimute;

                cmbStatus.SelectedValue = ot.Status;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GetEmployeeInformation(string EmployeeID)
        {
            EmployeeCurrentInformation ex = new EmployeeCurrentInformation(EmployeeID);
            if (!string.IsNullOrEmpty(ex.xEmployee_ID))
            {
                txtEmployee_ID.Text = ex.xEmployee_ID;
                txtEmployee_ID.IsEnabled = false;
                txtTitleNameTH.Text = ex.xTitleNameTH;
                txtFirstNameTH.Text = ex.xFirstNameTH;
                txtLastNameTH.Text = ex.xLastNameTH;
                txtStaffType.Text = ex.xStaffType;
                xFingerScanID = Convert.ToInt16(ex.xFingerScan_ID);
                OTDate.IsEnabled = true;
            }
        }

        private void BindLot()
        {
            lot_NumberClassList = lot_numberList.getLot_Number(DateTime.Now.Year);
            cmbLot.ItemsSource = null;
            cmbLot.ItemsSource = lot_NumberClassList;
        }

        private void BindOTType()
        {
            List<OTType> otTypeList = new List<OTType>();
            otTypeList.Add(new OTType { OTTypeCode = "SP", OTTypeName = "OT-Special" }); //OT แบบเหมา
            cmbOTType.ItemsSource = null;
            cmbOTType.ItemsSource = otTypeList;
        }

        private void BindRequestFlag()
        {
            List<RequestFlags> requestFlagList = new List<RequestFlags>();
            RequestFlags h1 = new RequestFlags { RequestFlag = "A", RequestFlagName = "OT-ก่อนเริ่มงาน" };
            RequestFlags h2 = new RequestFlags { RequestFlag = "B", RequestFlagName = "OT-หลังเลิกงาน" };
            requestFlagList.Add(h1);
            requestFlagList.Add(h2);
            cmbRequest.ItemsSource = null;
            cmbRequest.ItemsSource = requestFlagList;
        }

        private void BindOTHour()
        {
            List<decimal> otHourList = new List<decimal>();
            for (decimal i = 0; i < 13; i++)
            {
                otHourList.Add(i);
            }
            cmbOTHour.ItemsSource = null;
            cmbOTHour.ItemsSource = otHourList;
            cmbOTHour.SelectedIndex = 0;
        }

        private void BindOTMinute()
        {
            List<OTMinutes> otMinuteList = new List<OTMinutes>();
            otMinuteList.Add(new OTMinutes { Minutes = "0", Multiplier = 0m });
            otMinuteList.Add(new OTMinutes { Minutes = "10", Multiplier = 0.167m });
            otMinuteList.Add(new OTMinutes { Minutes = "20", Multiplier = 0.33m });
            otMinuteList.Add(new OTMinutes { Minutes = "30", Multiplier = 0.50m });
            otMinuteList.Add(new OTMinutes { Minutes = "40", Multiplier = 0.67m });
            otMinuteList.Add(new OTMinutes { Minutes = "50", Multiplier = 0.83m });
            cmbOTMinute.ItemsSource = null;
            cmbOTMinute.ItemsSource = otMinuteList;
            cmbOTMinute.SelectedIndex = 0;
        }

        private void BindStatusFlag()
        {
            //Create view model class for holiday flag combo box.
            List<StatusFlags> statusFlagList = new List<StatusFlags>();
            StatusFlags h1 = new StatusFlags { StatusFlag = true, StatusFlagName = "Active" };
            StatusFlags h2 = new StatusFlags { StatusFlag = true, StatusFlagName = "Not Active" };
            statusFlagList.Add(h1);
            statusFlagList.Add(h2);
            cmbStatus.ItemsSource = null;
            cmbStatus.ItemsSource = statusFlagList;
        }

        private void cmbLot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbLot.SelectedItem == null) return;
            lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;

            DateFromSearch.Text = lot_numberList.Start_date.ToString();
            DateToSearch.Text = lot_numberList.Finish_date.ToString();
        }

        private void OTDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(OTDate.Text.Trim()) && !string.IsNullOrEmpty(txtEmployee_ID.Text.Trim()))
                {
                    xPayrollLot = cmbLot.Text;
                    ShowCheckInOut();

                    if (IsWeekend(Convert.ToDateTime(OTDate.Text)) == true)
                    {
                        txtChkWeekend.Text = "วันอาทิตย์";
                        txtChkWeekend.Foreground = Brushes.Red;
                        XchkDate = true;
                    }
                    else
                    {
                        //เช็คว่าเป็น holiday ที่กำหนดไว้หรือไม่  
                        Holiday holiday = _hrisBusinessLayer.GetHoliday(Convert.ToDateTime(OTDate.Text));
                        if (holiday == null)
                        {
                            txtChkWeekend.Text = "วันธรรมดา";
                            txtChkWeekend.Foreground = Brushes.Black;
                            XchkDate = false;
                        }
                        else
                        {
                            if (holiday.HolidayFlag == true)//ตามประกาศบริษัทฯ
                            {
                                txtChkWeekend.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดตามประกาศบริษัท)";
                                txtChkWeekend.Foreground = Brushes.Blue;
                                XchkDate = true;
                            }
                            else //ถ้าเป็นวันหยุดพิเศษ เช่น งานเลี้ยง คริตส์มาส 
                            {
                                txtChkWeekend.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดพิเศษ)";
                                txtChkWeekend.Foreground = Brushes.OrangeRed;
                                XchkDate = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowCheckInOut()
        {
            if (xFingerScanID != cioFingerScanID || OTDate.Text != cioOTDate)
            {
                cioFingerScanID = xFingerScanID;
                cioOTDate = OTDate.Text;

                TimeSpan? scanIn, scanOut;

                userinfo u;
                u = _timeSTECBuisnessLayer.GetUserInfoByFingerID(xFingerScanID);

                List<checkinout> _checkInout;
                _checkInout = _timeSTECBuisnessLayer.getCheckInOut(u.userid, Convert.ToDateTime(OTDate.Text)).ToList();

                List<ManualWorkDate> workDate = new List<ManualWorkDate>();
                workDate = _hrisBusinessLayer.GetManualWorkDateByEmployee(txtEmployee_ID.Text, Convert.ToDateTime(OTDate.Text));

                if (workDate.Count() <= 0 && _checkInout.Count() <= 0)
                {
                    txtScanIn.Text = "";
                    txtScanOut.Text = "";
                    txtTotalWork.Text = "";
                    MessageBox.Show("ไม่พบเวลาการทำงานในวันที่ " + OTDate.Text, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                //เช็คเวลาเข้าทำงาน
                if (workDate.Where(e => e.ManualWorkDateType_ID == "A01").Count() > 0) scanIn = workDate.Where(e => e.ManualWorkDateType_ID == "A01").FirstOrDefault().Times;
                else if (_checkInout.Count > 0) scanIn = _checkInout.Min(c => c.checktime.TimeOfDay);
                else scanIn = null;
                txtScanIn.Text = scanIn.ToString();

                //เช็คเวลาออกงาน
                if (workDate.Where(e => e.ManualWorkDateType_ID == "A02").Count() > 0) scanOut = workDate.Where(e => e.ManualWorkDateType_ID == "A02").FirstOrDefault().Times;
                else if (_checkInout.Count > 0) scanOut = _checkInout.Max(c => c.checktime.TimeOfDay);
                else scanOut = null;
                txtScanOut.Text = scanOut.ToString();

                //คำนวนเวลาทำงาน
                if (scanIn != null && scanOut != null)
                {
                    TimeSpan span = scanOut.GetValueOrDefault() - scanIn.GetValueOrDefault();
                    span = (scanOut.GetValueOrDefault() != scanIn.GetValueOrDefault()) ? span.Subtract(new TimeSpan(0, 30, 0)) : span; // If timein <> timeout the Subtract 30 minutes for Break.
                    txtTotalWork.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
                }
                else txtTotalWork.Text = "";
            }
        }

        public bool IsWeekend(DateTime dateToCheck)
        {
            DayOfWeek day = (DayOfWeek)dateToCheck.DayOfWeek;
            if (day == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string actionType = _formState == "Add" ? "A" : "D";
            if (UpdateOTData(actionType))
            {
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            } 
        }

        private bool UpdateOTData(string actionType)
        {
            try
            {
                if (isRequiredDataCorrect(actionType))
                {
                    var otHour = Convert.ToDecimal(cmbOTHour.Text.ToString());
                    var otMinute = Convert.ToDecimal(cmbOTMinute.SelectedValue.ToString()); // Multiplier value
                    decimal requestedTime = otHour + otMinute;

                    string oldData = actionType == "A" ? "" : requestedTime.ToString();
                    string newData = actionType == "D" ? "" : requestedTime + (actionType == "A" ? " Lot" + xPayrollLot : "");
                    OT[] otObject = new OT[1];
                    otObject[0] = new OT()
                    {
                        Employee_ID = txtEmployee_ID.Text,
                        OTDate = Convert.ToDateTime(OTDate.Text),
                        OTType = (string)cmbOTType.SelectedValue,
                        RequestType = (string)cmbRequest.SelectedValue,
                        NcountOT = requestedTime,
                        Status = (Boolean)cmbStatus.SelectedValue,
                        ModifiedDate = DateTime.Now,
                        AdminApproveStatus = "A",
                        ManagerApproveStatus = "A",
                        FinalApproverStatus = "A",
                        HRApproveStatus = "A",
                        Remark = "Create by HRM",
                        PayrollLot = xPayrollLot
                    };
                    if (actionType == "A") _hrisBusinessLayer.AddOT(otObject);
                    else if (actionType == "U") _hrisBusinessLayer.UpdateOT(otObject);
                    else if (actionType == "D") _hrisBusinessLayer.RemoveOT(otObject[0]);

                    //Get new TransactionLog_ID
                    transactionLog = _hrisBusinessLayer.GetAllTransactionLogs().ToList();
                    XMaxTransactionLog_ID = transactionLog.Count > 0 ? Convert.ToDecimal(transactionLog.Max(c => c.TransactionLog_ID)) + 1 : 1;

                    TransactionLog[] transactionLogObj = new TransactionLog[1];
                    transactionLogObj[0] = new TransactionLog()
                    {
                        TransactionLog_ID = XMaxTransactionLog_ID,
                        TransactionType_ID = actionType,
                        Table_ID = "T009",
                        TransactionDate = DateTime.Now,
                        FieldName = "Employee_ID",
                        PKFields = txtEmployee_ID.Text,
                        FKFields = OTDate.Text,
                        OldData = oldData,
                        NewData = newData,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = _singleton.username
                    };
                    _hrisBusinessLayer.AddTransactionLog(transactionLogObj);
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
        }

        private bool isRequiredDataCorrect(string actionType)
        {
            string confirmationText = actionType == "D" ? "ต้องการลบข้อมูลใช่หรือไม่?" : "ต้องการบันทึกข้อมูลใช่หรือไม่?";
            if (MessageBox.Show(confirmationText, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return false;

            if (txtTitleNameTH.Text == "")
            {
                MessageBox.Show("กรุณาคีย์รหัสพนักงาน กรุณาตรวจสอบข้อมูล", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (cmbLot.Text == "" || DateFromSearch.Text == "" || DateToSearch.Text == "" || xPayrollLot == null)
            {
                MessageBox.Show("กรุณาระบุ Lot", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (OTDate.Text == "")
            {
                MessageBox.Show("กรุณาคีย์วันที่ทำ OT ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else if (Convert.ToDateTime(OTDate.Text) > Convert.ToDateTime(DateTime.Now)) //21 June 2018 ไม่อนุญาตให้คีย์ล่วงหน้าได้
            {
                MessageBox.Show("ไม่สามารถคีย์วันที่ทำโอทีล่วงหน้าได้ กรุณาตรวจสอบข้อมูลอีกครั้ง ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else if (Convert.ToDateTime(OTDate.Text) < Convert.ToDateTime(DateFromSearch.Text)) // 29Oct2018 ไม่อนุญาติให้คีย์ข้อมูลย้อนหลัง (OT ไม่ตรง Lot)
            {
                MessageBox.Show("เนื่องจากคุณระบุวันที่ทำโอทีย้อนหลังจาก Lot ปัจจุบัน หากต้องการทำรายการกรุณาติดต่อแผนก IT", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
            lot_numberList = (BusinessLayer_HRISSystem.BusinessObject.Lot_NumberClass)cmbLot.SelectedItem;
            if (lot_numberList.Lock_Acc == "2" || lot_numberList.Lock_Acc_Labor == "2")
            {
                MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (lot_numberList.Lock_Hr_Labor == "2")
            {
                MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (txtScanIn.Text == "" || txtScanOut.Text == "")
            {
                MessageBox.Show("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบข้อมูลการสแกนนิ้ว ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //ตรวจสอบว่า หากเป็นวันธรรมดาที่ไม่ใช่วันอาทิตย์ และไม่ใช่วันหยุดประจำปีของบริษัท และทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
            TimeSpan span = (Convert.ToDateTime(txtScanOut.Text) - Convert.ToDateTime(txtScanIn.Text));
            if (XchkDate == false)
            {
                if (span.Hours < 8)
                {
                    MessageBox.Show("ไม่สามารถบันทึกการขอทำโอที เนื่องจากมีการมีการทำงานน้อยกว่า 8 ชัวโมง กรุณาตรวจสอบการข้อมูลการสแกนนิ้ว", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }


            if (!isOTAmountCorrect())
            {
                MessageBox.Show("จำนวนชั่วโมง OT ไม่ถูกต้อง กรุณาตรวจสอบ", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            else
            {
                if (XchkDate == false)
                {
                    //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                    var otHour = Convert.ToDecimal(cmbOTHour.Text.ToString());
                    var otMinute = Convert.ToDecimal(cmbOTMinute.Text.ToString()); // Minute unit
                    decimal scanedTime = (((span.Hours - 8) * 60) + (span.Minutes)) - 30; // Subtract 30m for Break
                    decimal requestedTime = ((otHour * 60) + otMinute);
                    if (requestedTime > scanedTime)
                    {
                        if (MessageBox.Show("จำนวนโอทีที่คีย์มากกว่าที่ระบบคำนวนได้ คุณต้องการบันทึกข้อมูลต่อใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.No) return false;
                    }
                }
            }

            if (cmbOTType.Text == "")
            {
                MessageBox.Show("กรุณาเลือกประเภท OT", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //else if (cmbOTType.SelectedValue.ToString() == "H" && Convert.ToDecimal(cmbOTHour.Text.ToString()) > 2)
            else if (cmbOTType.SelectedValue.ToString() == "H" && (Convert.ToDecimal(cmbOTHour.Text.ToString()) + Convert.ToDecimal(cmbOTMinute.Text.ToString())) > 2)
            {
                MessageBox.Show("จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (cmbStatus.Text == "")
            {
                MessageBox.Show("กรุณาเลือก Status", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private bool isOTAmountCorrect()
        {
            if (cmbOTHour.SelectedIndex < 0 || cmbOTMinute.SelectedIndex < 0) return false;
            var otHour = Convert.ToDecimal(cmbOTHour.SelectedValue.ToString());
            var otMinute = Convert.ToDecimal(cmbOTMinute.SelectedValue.ToString());
            return otHour + otMinute > 0 ? true : false;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (UpdateOTData("D"))
            {
                MessageBox.Show("ลบข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            } 
        }
    }
}
