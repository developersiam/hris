﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using System.Globalization;
using System.Threading;

namespace HRISSystem.Report
{
    /// <summary>
    /// Interaction logic for RepRPTHR027.xaml
    /// </summary>
    public partial class RepRPTHR027 : Page
    {
        private BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer _hrisSTECPayrollBusinessLayer = new BusinessLayer_HRISSystem.HRISSTECPayrollBusinessLayer();
        private BusinessLayer_HRISSystem.HRISBusinessLayer _hrisBusinessLayer = new BusinessLayer_HRISSystem.HRISBusinessLayer();


        public string PaymentDay { get; set; }
        public string PaymentMonth { get; set; }
        public string PaymentYear { get; set; }
        public string xTax { get; set; }

        public RepRPTHR027()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TxtYear.Text == "")
                {
                    MessageBox.Show("กรุณาเลือกงวดที่ต้องการ", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Paydate.Text == "")
                {
                    MessageBox.Show("กรุณาวันที่จ่ายเงินเนื่องจากเป็นข้อบังคับของรูปแบบสรรพากรที่ต้องระบุข้อมูล", "Critical Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                int xstafffType = 0;
                if (Rdtstafftype1.IsChecked == true){
                    xstafffType = 1;
                }
                else if (Rdtstafftype2.IsChecked == true)
                {
                    xstafffType = 0;
                }

                string strFilePath;
                strFilePath = "";
                StreamWriter strWriter;
                
                if (xstafffType == 0)
                {
                    strFilePath = "C:/HRIS/" + Convert.ToInt16( TxtYear.Text) + "Labor_" + Convert.ToDateTime(Paydate.Text).Year + DateTime.Now.ToString("HHmmss") + ".txt";
                }
                else if (xstafffType == 1)
                {
                    strFilePath = "C:/HRIS/" + Convert.ToInt16(TxtYear.Text) + "Staff_" + Convert.ToDateTime(Paydate.Text).Year + DateTime.Now.ToString("HHmmss") + ".txt";
                }

                HRISReportDataset.sp_GetPND1ADataTable sp_GetPND1ADataTable = new HRISReportDataset.sp_GetPND1ADataTable();
                HRISReportDatasetTableAdapters.sp_GetPND1ATableAdapter  sp_GetPND1ATableAdapter = new HRISReportDatasetTableAdapters.sp_GetPND1ATableAdapter();
                sp_GetPND1ATableAdapter.Fill(sp_GetPND1ADataTable, Convert.ToInt16( TxtYear.Text),xstafffType.ToString(),Convert.ToDateTime(Paydate.Text));
                                                    
                strWriter = new StreamWriter(strFilePath, false);

                StringBuilder strBuilder = new StringBuilder();

                foreach (DataRow item in sp_GetPND1ATableAdapter.GetData( Convert.ToInt16( TxtYear.Text),xstafffType.ToString(),Convert.ToDateTime(Paydate.Text)).Rows)
                {
                   
                    PaymentDay = Convert.ToDateTime(Paydate.Text).Day.ToString().Length == 1 ? "0" + Convert.ToDateTime(Paydate.Text).Day.ToString() : Convert.ToDateTime(Paydate.Text).Day.ToString();
                    PaymentMonth = Convert.ToDateTime(Paydate.Text).Month.ToString().Length == 1 ? "0" + Convert.ToDateTime(Paydate.Text).Month.ToString() : Convert.ToDateTime(Paydate.Text).Month.ToString();
                    PaymentDay = (Convert.ToDateTime(Paydate.Text).Year + 543).ToString();

                    xTax = item["TAX"].ToString() == "" ? "0.00" : Convert.ToDecimal(item["TAX"]).ToString("F2");


                    strBuilder.Append(
                        item["FROMTYPE"].ToString()+ "|" +
                        item["COMPIN"].ToString() + "|" +
                        item["COMTIN"].ToString() + "|" +
                        item["BRANO"].ToString()+ "|" +
                        item["PIN"].ToString()+ "|" +
                        item["TIN"].ToString()+ "|" +
                        item["PER_N1"].ToString()+ "|" +
                        item["NAME1"].ToString() + "|" +
                        item["SUR_N1"].ToString() + "|" +
                        item["ADDRESS1"].ToString() + "|" +
                        item["ADDRESS2"].ToString() + "|" +
                        item["POSCOD"].ToString() + "|" +
                        item["TAXMONTH"].ToString() + "|" +
                        item["TAXYEAR"].ToString() + "|" +
                        item["INCOMECODE"].ToString() + "|" +
                        PaymentDay+ PaymentMonth + PaymentYear + "|" +
                        item["TAXRATE"].ToString() + "|" +                       
                        Convert.ToDecimal( item["PAYMENT"]).ToString("F2") + "|" +
                        xTax + "|" +
                        item["TAXCONDITION"].ToString()                        
                        );

                    strWriter.WriteLine(strBuilder.ToString());
                    strBuilder.Clear();
                }

                strWriter.Close();

                MessageBox.Show("ทำการExportข้อมูลเรียบร้อยแล้ว ไฟล์เก็บอยู่ที่ " + strFilePath, "Message", MessageBoxButton.OK, MessageBoxImage.Information);               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
